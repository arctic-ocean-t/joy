local Theorem = require 'packages/joy/record/baseme'

--将技能添加进池子
Theorem.insertSkill=function(skill, skills,filterfuc)
  local skill_name = skill.name
  if filterfuc(skill) then --过滤
    table.insert(skills, skill_name)
  end
end

--从技能池中选择技能
---@param filterfuc @过滤函数，自定义什么函数不能过滤
---@param n @传参上限,即找多少个技能，太多有点浪费时间。下限为10
---@param m @选择技能数
---@param x @获得技能数
Theorem.getskillfrompool=function(player,room,filterfuc,n,m,x)
      local skill_pool, general_pool = {}, {}--技能池子
      local total=math.max(10,n)--不要遍历太多次
      local i = 0
      --此处添加技能
      local all_generals = Fk:getAllGenerals({"tshu__xiaosha","tshu__xiaojiu","tshu__xiaotao","tshu__xiaole"}) 
      --嵌入技能-- replace Engine:getGeneralsRandomly
      for _ = 1, 999 do
        local general = table.random(all_generals)
        local skills = {}
        table.forEach(general.skills, function(s) Theorem.insertSkill(s, skills,filterfuc) end)
        local skill = table.random(skills)
        if skill and not table.contains(skill_pool, skill) then
          i = i + 1
          table.insert(skill_pool, skill)
          table.insert(general_pool, general.name)

          if i == total then break end
        end
      end
      --获取table前n项
      local function getElements(tbl,n)
        local result = {}
        for i = 1, n do
            table.insert(result, tbl[i])
        end
        return result
      end
      if #skill_pool==0 then return false end
      local pskills=getElements(skill_pool,math.min(m,#skill_pool))
      local generals=getElements(general_pool,math.min(m,#general_pool))
      player.default_reply = table.random(pskills, math.min(x,#skill_pool))
      room:askForCustomDialog(player, nil,
      "packages/utility/qml/ChooseSkillBox.qml", {
        pskills, x, x, "#skillpo-choose:::" .. tostring(x),generals
      })
    local choice = player.reply_ready and json.decode(player.client_reply) or player.default_reply
    room:handleAddLoseSkills(player, table.concat(choice, "|"), nil, true,true)
    return choice

end

--选择自己的技能
Theorem.chooseplayerskill=function(player,room)
  local skills={}
  for _, s in ipairs(player.player_skills) do
    if not (s.attached_equip or s.name[#s.name] == "&") and not string.find(s.name, "#") then
      table.insertIfNeed(skills, s.name)
    end
  end
  
  room:askForCustomDialog(player, nil,
  "packages/utility/qml/ChooseSkillBox.qml", {
    skills, 0, #skills, "#playerskillpo-choose:::" .. tostring(x)
  })
  if  player.reply_ready then
    local choice = json.decode(player.client_reply) 
    return choice
  end


end
Theorem.choosemarkskill=function(player,skills,room,x)
  room:askForCustomDialog(player, nil,
  "packages/utility/qml/ChooseSkillBox.qml", {
    skills, x,  x, "#markskillpo-choose:::" .. tostring(x)
  })
  if  player.reply_ready then
    local choice = json.decode(player.client_reply) 
   
    return choice
  end


end
Fk:loadTranslationTable{
  ["#skillpo-choose"]="天书：选择获得%arg个技能",

  ["#playerskillpo-choose"]="选择自己的若干技能",
  
  ["#markskillpo-choose"] = "天书：选择%arg个技能移除之",

}
  return Theorem