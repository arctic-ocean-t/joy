local extension = Package("joy")
local joy_other = require "packages/joy/joy_other"
local joy_ex = require "packages/joy/joy_ex"
local joy_shzl = require "packages/joy/joy_shzl"
local joy_yjcm = require "packages/joy/joy_yjcm"
local joy_xhly = require "packages/joy/joy_xhly"
local joy_sp = require "packages/joy/joy_sp"
local joy_mou = require "packages/joy/joy_mou"
local joy_nya = require "packages/joy/joy_nya"
local joy_nian = require "packages/joy/joy_nian"
local joy_qyhc = require "packages/joy/joy_qyhc"
local joy_xd2 = require "packages/joy/joy_xd2"
--local joy_ccxh = require "packages/joy/joy_ccxh"
--local joy_nzbz = require "packages/joy/joy_nzbz"
local joy_god = require "packages/joy/joy_god"
local joy_skin = require "packages/joy/joy_skin"
local joy_token = require "packages/joy/joy_token"
Fk:addGameMode(require "packages/joy/joy8")
Fk:addGameMode(require "packages/joy/joy2v2")
--Fk:addGameMode(require "packages/joy/joy1v2")
Fk:addGameMode(require "packages/joy/joy1v1")
Fk:loadTranslationTable{ ["joy"] = "欢乐" }

--- 快捷输出玩家的 主将名/副将名 字符串
---@param p Player @ 玩家
function Pg(p)
    local g1,g2 = p.general,p.deputyGeneral
    if g2 ~= nil and g2 ~= "" then
      return " "..Fk:translate(g1).."/"..Fk:translate(g2).." "
    else
      return " "..Fk:translate(g1).." "
    end
  end

return {
    joy_other,
    joy_ex,
    joy_shzl,
    joy_yjcm,
    joy_xhly,
    joy_sp,
    joy_mou,
    joy_nya,
    joy_nian,
    joy_qyhc,
    joy_xd2,
    joy_god,
    joy_token,
    joy_skin,
}
