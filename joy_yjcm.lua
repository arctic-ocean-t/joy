local extension = Package("joy_yjcm")
extension.extensionName = "joy"
local U = require "packages/utility/utility"
Fk:loadTranslationTable{
  ["joy_yjcm"] = "欢乐-一将成名",
}

--曹植 于禁 荀攸 曹彰 步练师 刘封 伏皇后 周仓 曹休 孙登 徐氏 曹节
local yujin = General(extension, "joy__yujin", "wei", 4)
local yizhong = fk.CreateTriggerSkill{
  name = "joy__yizhong",
  anim_type = "defensive",
  frequency = Skill.Compulsory,
  events = {fk.PreCardEffect},
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(self) and data.card.trueName == "slash" and player.id == data.to and
      data.card.suit == Card.Club and #player.player_cards[Player.Equip] == 0
  end,
  on_use = Util.TrueFunc,
}
yujin:addSkill(yizhong)
yujin:addSkill("ty_ex__zhenjun")
Fk:loadTranslationTable{
  ["joy__yujin"] = "于禁",
  ["joy__yizhong"] = "毅重",
  [":joy__yizhong"] = "锁定技，若你的装备区没有牌，梅花【杀】对你无效。",
}

local xunyou = General(extension, "joy__xunyou", "wei", 3)
local joy__qice = fk.CreateViewAsSkill{
  name = "joy__qice",
  prompt = "#joy__qice",
  interaction = function()
    local names = {}
    for _, id in ipairs(Fk:getAllCardIds()) do
      local card = Fk:getCardById(id)
      if card:isCommonTrick() and not card.is_derived and card.skill:canUse(Self, card) then
        table.insertIfNeed(names, card.name)
      end
    end
    return UI.ComboBox {choices = names}
  end,
  card_filter = function(self, to_select, selected)
    return Fk:currentRoom():getCardArea(to_select) == Player.Hand
  end,
  view_as = function(self, cards)
    if #cards == 0 or not self.interaction.data then return end
    local card = Fk:cloneCard(self.interaction.data)
    card:addSubcards(cards)
    card.skillName = self.name
    return card
  end,
  enabled_at_play = function(self, player)
    return player:usedSkillTimes(self.name, Player.HistoryPhase) == 0 and not player:isKongcheng()
  end,
}
local joy__zhiyu = fk.CreateTriggerSkill{
  name = "joy__zhiyu",
  anim_type = "masochism",
  events = {fk.Damaged},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self.name)
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    player:drawCards(2, self.name)
    if not player.dead and not player:isKongcheng() then
      room:askForDiscard(player, 1, 1, false, self.name, false)
    end
    local cards = table.simpleClone(player:getCardIds("h"))
    player:showCards(cards)
    if player.dead or not data.from or data.from.dead or data.from:getHandcardNum() <= player:getHandcardNum() then return end
    if table.every(cards, function(id) return #cards == 0 or Fk:getCardById(id).color == Fk:getCardById(cards[1]).color end) and
      room:askForSkillInvoke(player, self.name, nil, "#joy__zhiyu-discard::"..data.from.id) then
      room:doIndicate(player.id, {data.from.id})
      local n = data.from:getHandcardNum() - player:getHandcardNum()
      room:askForDiscard(data.from, n, n, false, self.name, false)
    end
  end,
}
xunyou:addSkill(joy__qice)
xunyou:addSkill(joy__zhiyu)
Fk:loadTranslationTable{
  ["joy__xunyou"] = "荀攸",
  ["joy__qice"] = "奇策",
  [":joy__qice"] = "出牌阶段限一次，你可以将任意张手牌当任意一张普通锦囊牌使用。",
  ["joy__zhiyu"] = "智愚",
  [":joy__zhiyu"] = "当你受到伤害后，你可以摸两张牌并弃置一张牌，然后展示所有手牌，若颜色均相同，你可以令伤害来源将手牌弃至与你相同。",
  ["#joy__qice"] = "奇策：你可以将任意张手牌当一张普通锦囊牌使用",
  ["#joy__zhiyu-discard"] = "智愚：你可以令 %dest 将手牌弃至与你相同",
}

local fuhuanghou = General(extension, "joy__fuhuanghou", "qun", 3, 3, General.Female)
local joy__zhuikong = fk.CreateTriggerSkill{
  name = "joy__zhuikong",
  anim_type = "control",
  events = {fk.TurnStart},
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(self.name) and target ~= player and not player:isKongcheng() and not target:isKongcheng()
  end,
  on_cost = function(self, event, target, player, data)
    return player.room:askForSkillInvoke(player, self.name, data, "#joy__zhuikong-invoke::"..target.id)
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    room:doIndicate(player.id, {target.id})
    local pindian = player:pindian({target}, self.name)
    if pindian.results[target.id].winner == player then
      room:setPlayerMark(target, "joy__zhuikong_prohibit-turn", 1)
    else
      room:setPlayerMark(player, "joy__zhuikong-turn", 1)
    end
  end
}
local joy__zhuikong_prohibit = fk.CreateProhibitSkill{
  name = "#joy__zhuikong_prohibit",
  is_prohibited = function(self, from, to, card)
    return from:getMark("joy__zhuikong_prohibit-turn") > 0 and from ~= to
  end,
}
local joy__zhuikong_distance = fk.CreateDistanceSkill{
  name = "#joy__zhuikong_distance",
  fixed_func = function(self, from, to)
    if to:usedSkillTimes("joy__zhuikong", Player.HistoryTurn)> 0 and to:getMark("joy__zhuikong-turn") > 0 then
      return 1
    end
  end,
}
local joy__qiuyuan = fk.CreateTriggerSkill{
  name = "joy__qiuyuan",
  anim_type = "control",
  events = {fk.TargetConfirming},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self.name) and data.card.trueName == "slash"
  end,
  on_cost = function(self, event, target, player, data)
    local room = player.room
    local targets = table.map(table.filter(room:getOtherPlayers(player), function(p)
      return p.id ~= data.from end), function (p) return p.id end)
    local tos = room:askForChoosePlayers(player, targets, 1, 3, "#joy__qiuyuan-choose", self.name, true)
    if #tos > 0 then
      self.cost_data = tos
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    room:sortPlayersByAction(self.cost_data)
    room:doIndicate(player.id, self.cost_data)
    for _, id in ipairs(self.cost_data) do
      local p = room:getPlayerById(id)
      if not p.dead then
        local yes = true
        if not player.dead then
          local card = room:askForCard(p, 1, 1, false, self.name, true, "jink", "#joy__qiuyuan-give:"..player.id.."::"..data.card:toLogString())
          if #card > 0 then
            room:obtainCard(player.id, card[1], true, fk.ReasonGive)
            yes = false
          end
        end
        if yes then
          TargetGroup:pushTargets(data.targetGroup, id)
          if not p:isNude() then
            room:askForDiscard(p, 1, 1, true, self.name, false)
          end
        end
      end
    end
  end,
}
joy__zhuikong:addRelatedSkill(joy__zhuikong_prohibit)
joy__zhuikong:addRelatedSkill(joy__zhuikong_distance)
fuhuanghou:addSkill(joy__zhuikong)
fuhuanghou:addSkill(joy__qiuyuan)
Fk:loadTranslationTable{
  ["joy__fuhuanghou"] = "伏皇后",
  ["joy__zhuikong"] = "惴恐",
  [":joy__zhuikong"] = "其他角色回合开始时，你可以与其拼点：若你赢，其本回合不能对除其以外的角色使用牌；若你没赢，本回合其与你的距离视为1。",
  ["joy__qiuyuan"] = "求援",
  [":joy__qiuyuan"] = "当你成为【杀】的目标时，你可以令除使用者以外至多三名角色依次选择一项：1.交给你一张【闪】；2.成为此【杀】的目标并弃置一张牌。",
  ["#joy__zhuikong-invoke"] = "惴恐：你可以与 %dest 拼点，若赢则其本回合不能对除其以外的角色使用牌",
  ["#joy__qiuyuan-choose"] = "求援：你可以令至多三名角色选择：交给你一张【闪】，或成为此【杀】的目标并弃置一张牌",
  ["#joy__qiuyuan-give"] = "求援：你需交给 %src 一张【闪】，否则你也成为此%arg目标并弃置一张牌",
}

local sundeng = General(extension, "joy__sundeng", "wu", 4)
local joy__kuangbi = fk.CreateActiveSkill{
  name = "joy__kuangbi",
  anim_type = "control",
  card_num = 0,
  target_num = 1,
  prompt = "#joy__kuangbi",
  can_use = function(self, player)
    return player:usedSkillTimes(self.name, Player.HistoryPhase) == 0
  end,
  card_filter = function(self, to_select, selected)
    return false
  end,
  target_filter = function(self, to_select, selected, selected_cards)
    return #selected == 0 and to_select ~= Self.id and not Fk:currentRoom():getPlayerById(to_select):isNude()
  end,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    local target = room:getPlayerById(effect.tos[1])
    local cards = room:askForCard(target, 1, 3, true, self.name, false, ".", "#joy__kuangbi-card:"..player.id)
    local dummy = Fk:cloneCard("dilu")
    dummy:addSubcards(cards)
    player:addToPile(self.name, dummy, false, self.name)
    if player.dead or target.dead then return end
    if room:askForSkillInvoke(player, self.name, nil, "#joy__kuangbi-draw::"..target.id..":"..#dummy.subcards) then
      target:drawCards(#dummy.subcards, self.name)
    end
  end,
}
local joy__kuangbi_trigger = fk.CreateTriggerSkill {
  name = "#joy__kuangbi_trigger",
  mute = true,
  events = {fk.TurnStart, fk.AfterCardsMove},
  can_trigger = function(self, event, target, player, data)
    if event == fk.TurnStart then
      return target == player and #player:getPile("joy__kuangbi") > 0
    elseif player:hasSkill("joy__kuangbi") and (table.every(player:getCardIds("h"), function(id)
      return Fk:getCardById(id):getMark("@@joy__kuangbi") == 0 end)) then
      for _, move in ipairs(data) do
        if move.from == player.id and move.extra_data and move.extra_data.joy__kuangbi then
          return true
        end
      end
    end
  end,
  on_cost = function(self, event, target, player, data)
    return true
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    player:broadcastSkillInvoke("joy__kuangbi")
    room:notifySkillInvoked(player, "joy__kuangbi", "support")
    if event == fk.TurnStart then
      local dummy = Fk:cloneCard("dilu")
      dummy:addSubcards(player:getPile("joy__kuangbi"))
      for _, id in ipairs(player:getPile("joy__kuangbi")) do
        room:setCardMark(Fk:getCardById(id), "@@joy__kuangbi", 1)
      end
      room:obtainCard(player, dummy, false, fk.ReasonJustMove)
    else
      player:drawCards(1, "joy__kuangbi")
      if player:isWounded() and not player.dead then
        room:recover{
          who = player,
          num = 1,
          recoverBy = player,
          skillName = "joy__kuangbi",
        }
      end
    end
  end,

  refresh_events = {fk.BeforeCardsMove},
  can_refresh = function(self, event, target, player, data)
    if player:hasSkill("joy__kuangbi") then
      for _, move in ipairs(data) do
        if move.from == player.id then
          for _, info in ipairs(move.moveInfo) do
            if info.fromArea == Card.PlayerHand and Fk:getCardById(info.cardId):getMark("@@joy__kuangbi") > 0 then
              return true
            end
          end
        end
      end
    end
  end,
  on_refresh = function(self, event, target, player, data)
    for _, move in ipairs(data) do
      if move.from == player.id then
        local yes = false
        for _, info in ipairs(move.moveInfo) do
          if info.fromArea == Card.PlayerHand and Fk:getCardById(info.cardId):getMark("@@joy__kuangbi") > 0 then
            player.room:setCardMark(Fk:getCardById(info.cardId), "@@joy__kuangbi", 0)
            yes = true
          end
        end
        if yes then
          move.extra_data = move.extra_data or {}
          move.extra_data.joy__kuangbi = true
        end
      end
    end
  end,
}
joy__kuangbi:addRelatedSkill(joy__kuangbi_trigger)
sundeng:addSkill(joy__kuangbi)
Fk:loadTranslationTable{
  ["joy__sundeng"] = "孙登",
  ["joy__kuangbi"] = "匡弼",
  [":joy__kuangbi"] = "出牌阶段限一次，你可以令一名其他角色将其一至三张牌置于你的武将牌上，然后你可令其摸等量的牌。你的回合开始时，"..
  "你获得武将牌上的所有牌。当你失去手牌中最后一张“匡弼”牌时，你摸一张牌并回复1点体力。",
  ["#joy__kuangbi"] = "匡弼：令一名角色将至多三张牌置为“匡弼”牌，你可以令其摸等量牌，你回合开始时获得“匡弼”牌",
  ["#joy__kuangbi-card"] = "匡弼：将至多三张牌置为 %src 的“匡弼”牌",
  ["#joy__kuangbi-draw"] = "匡弼：是否令 %dest 摸%arg张牌？",
  ["@@joy__kuangbi"] = "匡弼",
}

local joy__guanping = General(extension, "joy__guanping", "shu", 4)
local joy__longyin = fk.CreateTriggerSkill{
  name = "joy__longyin",
  anim_type = "support",
  events = {fk.CardUsing},
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(self) and target.phase == Player.Play and data.card.trueName == "slash" and not player:isNude()
  end,
  on_cost = function(self, event, target, player, data)
    local cards = player.room:askForDiscard(player, 1, 1, true, self.name, true, ".", "#joy__longyin-invoke::"..target.id, true)
    if #cards > 0 then
      self.cost_data = cards
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    player.room:throwCard(self.cost_data, self.name, player, player)
    if not data.extraUse then
      data.extraUse = true
      target:addCardUseHistory(data.card.trueName, -1)
    end
    if data.card.color == Card.Red and not player.dead then
      player:drawCards(1, self.name)
    end
    if data.card.suit == Fk:getCardById(self.cost_data[1]).suit and player:usedSkillTimes("joy__jiezhong", Player.HistoryGame) > 0 then
      player:setSkillUseHistory("joy__jiezhong", 0, Player.HistoryGame)
    end
  end,
}
local joy__jiezhong = fk.CreateTriggerSkill{
  name = "joy__jiezhong",
  anim_type = "drawcard",
  frequency = Skill.Limited,
  events = {fk.EventPhaseStart},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and player.phase == Player.Play and
      player.maxHp > player:getHandcardNum() and player:usedSkillTimes(self.name, Player.HistoryGame) == 0
  end,
  on_cost = function(self, event, target, player, data)
    local draw = player.maxHp - player:getHandcardNum()
    return player.room:askForSkillInvoke(player, self.name, nil, "#joy__jiezhong-invoke:::"..draw)
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local n = player.maxHp - player:getHandcardNum()
    player:drawCards(n, self.name)
  end,
}
joy__guanping:addSkill(joy__longyin)
joy__guanping:addSkill(joy__jiezhong)
Fk:loadTranslationTable{
  ["joy__guanping"] = "关平",
  ["#joy__guanping"] = "忠臣孝子",

  ["joy__longyin"] = "龙吟",
  [":joy__longyin"] = "每当一名角色在其出牌阶段使用【杀】时，你可以弃置一张牌令此【杀】不计入出牌阶段使用次数，若此【杀】为红色，你摸一张牌。"..
  "若你以此法弃置的牌花色与此【杀】相同，你重置〖竭忠〗。",
  ["#joy__longyin-invoke"] = "龙吟：你可以弃置一张牌令 %dest 的【杀】不计入次数限制",
  ["joy__jiezhong"] = "竭忠",
  [":joy__jiezhong"] = "限定技，出牌阶段开始时，若你的手牌数小于体力上限，你可以将手牌补至体力上限。",
  ["#joy__jiezhong-invoke"] = "竭忠：是否发动“竭忠”摸%arg张牌？ ",

}

local joy__xushu = General(extension, "joy__xushu", "shu", 3)
local joy__jujian = fk.CreateTriggerSkill{
  name = "joy__jujian",
  anim_type = "support",
  events = {fk.EventPhaseStart},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and (player.phase == Player.Finish or player.phase == Player.Start)and not player:isNude()
  end,
  on_cost = function(self, event, target, player, data)
    local room = player.room
    local tos, id = player.room:askForChooseCardAndPlayers(player, table.map(room.alive_players, Util.IdMapper), 1, 1, ".|.|.|.|.|^basic", "#joy__jujian-choose", self.name, true)
    if #tos > 0 then
      self.cost_data = {tos[1], id}
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local to = room:getPlayerById(self.cost_data[1])
    room:throwCard({self.cost_data[2]}, self.name, player, player)
    local choices = {"draw2"}
    if to:isWounded() then
      table.insert(choices, "recover")
    end
    if not to.faceup or to.chained then
      table.insert(choices, "joy__jujian_reset")
    end
    local choice = room:askForChoice(to, choices, self.name, nil, false, {"draw2", "recover", "joy__jujian_reset"})
    if choice == "draw2" then
      to:drawCards(2, self.name)
    elseif choice == "recover" then
      room:recover({
        who = to,
        num = 1,
        recoverBy = player,
        skillName = self.name
      })
    else
      to:reset()
    end
  end,
}
joy__xushu:addSkill("wuyan")
joy__xushu:addSkill(joy__jujian)
Fk:loadTranslationTable{
  ["joy__xushu"] = "徐庶",
  ["#joy__xushu"] = "忠孝的侠士",

  ["joy__jujian"] = "举荐",
  [":joy__jujian"] = "准备或结束阶段，你可以弃置一张非基本牌，令一名角色选择一项：摸两张牌；回复1点体力；复原武将牌。",
  ["#joy__jujian-choose"] = "举荐：你可以弃置一张非基本牌，令一名角色选择摸俩张牌/回复体力/复原武将牌",
  ["joy__jujian_reset"] = "复原武将牌",

}

local joyex__liaohua = General(extension, "joyex__liaohua", "shu", 4)
local joyex__dangxian = fk.CreateTriggerSkill{
  name = "joyex__dangxian",
  anim_type = "offensive",
  events = {fk.TurnStart},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self)
  end,
  on_use = function(self, event, target, player, data)
    local cards = player.room:getCardsFromPileByRule("slash", 1)
    if #cards > 0 then
      player.room:obtainCard(player, cards[1], true, fk.ReasonJustMove)
    end
    player:gainAnExtraPhase(Player.Play)
  end,
}
local joyex__fuli = fk.CreateTriggerSkill{
  name = "joyex__fuli",
  anim_type = "defensive",
  frequency = Skill.Limited,
  events = {fk.AskForPeaches},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and player.dying and player:usedSkillTimes(self.name, Player.HistoryGame) == 0
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    room:setPlayerMark(player, self.name, 1)
    local kingdoms = {}
    for _, p in ipairs(room:getAlivePlayers()) do
      table.insertIfNeed(kingdoms, p.kingdom)
    end
    room:recover({
      who = player,
      num = math.min(#kingdoms, player.maxHp) - player.hp,
      recoverBy = player,
      skillName = self.name
    })
    if player:getHandcardNum() < #kingdoms and not player.dead then
      player:drawCards(#kingdoms - player:getHandcardNum())
    end
    if #kingdoms > 3 and not player.dead then
      player:turnOver()
    end
  end,
}
joyex__liaohua:addSkill(joyex__dangxian)
joyex__liaohua:addSkill(joyex__fuli)
Fk:loadTranslationTable{
  ["joyex__liaohua"] = "界廖化",
  ["#joyex__liaohua"] = "历尽沧桑",

  ["joyex__dangxian"] = "当先",
  [":joyex__dangxian"] = "回合开始时你进行一个额外的出牌阶段并摸一张【杀】。",
  ["joyex__fuli"] = "伏枥",
  [":joyex__fuli"] = "限定技，当你处于濒死状态时，你可以将体力回复至X点且手牌摸至X张（X为全场势力数）"..
  "若X大于3，你翻面。",

}

local caoxiu = General(extension, "joy__caoxiu", "wei", 4)
local joy__qingxi = fk.CreateTriggerSkill{
  name = "joy__qingxi",
  events = {fk.TargetSpecified},
  anim_type = "offensive",
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and (data.card.trueName == "slash" or data.card.trueName == "duel")
  end,
  on_cost = function(self, event, target, player, data)
    local room = player.room
    local n = 0
    for _, p in ipairs(room.alive_players) do
      if player:inMyAttackRange(p) then
        n = n + 1
      end
    end
    local max_num = #player:getEquipments(Card.SubtypeWeapon) > 0 and 4 or 2
    n = math.min(n, max_num)
    if player.room:askForSkillInvoke(player, self.name, data, "#joy__qingxi::" .. data.to..":"..n) then
      self.cost_data = n
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local to = room:getPlayerById(data.to)
    local num = self.cost_data
    if #room:askForDiscard(to, num, num, false, self.name, true, ".", "#joy__qingxi-discard:::"..num) == num then
      local weapon = player:getEquipments(Card.SubtypeWeapon)
      if #weapon > 0 then
        room:throwCard(weapon, self.name, player, to)
      end
    else
      data.extra_data = data.extra_data or {}
      data.extra_data.ty_ex__qingxi = data.to
      local judge = {
        who = player,
        reason = self.name,
        pattern = ".|.|club,spade,heart,diamond",
      }
      room:judge(judge)
      if judge.card.color == Card.Red then
        data.disresponsive = true
      elseif judge.card.color == Card.Black and not player.dead then
        player:drawCards(2,self.name)
      end
    end
  end,
}
local joy__qingxi_delay = fk.CreateTriggerSkill{
  name = "#joy__qingxi_delay",
  events = {fk.DamageCaused},
  mute = true,
  can_trigger = function(self, event, target, player, data)
    if target == player then
      local e = player.room.logic:getCurrentEvent():findParent(GameEvent.CardEffect)
      if e then
        local use = e.data[1]
        if use.extra_data and use.extra_data.ty_ex__qingxi == data.to.id then
          return true
        end
      end
    end
  end,
  on_cost = Util.TrueFunc,
  on_use = function(self, event, target, player, data)
    data.damage = data.damage + 1
  end,
}
joy__qingxi:addRelatedSkill(joy__qingxi_delay)
caoxiu:addSkill("qianju")
caoxiu:addSkill(joy__qingxi)
Fk:loadTranslationTable{
  ["joy__caoxiu"] = "曹休",
  ["#joy__caoxiu"] = "千里骐骥",

  ["joy__qingxi"] = "倾袭",
  [":joy__qingxi"] = "当你使用【杀】或【决斗】指定一名角色为目标后，你可以令其选择一项："..
  "1.弃置等同于你攻击范围内的角色数张手牌（至多为2，若你武器区里有武器牌则改为至多为4），然后弃置你装备区里的武器牌；"..
  "2.令此牌对其造成的基础伤害值+1且你进行一次判定，若结果为红色，该角色不能响应此牌;若结果为黑色，你摸两张牌",
  ["#joy__qingxi"] = "倾袭：可令 %dest 选一项：1.弃 %arg 张手牌并弃置你的武器；2.伤害+1且你判定，为红不能响应，为黑摸两张牌",
  ["#joy__qingxi-discard"] = "倾袭：你需弃置 %arg 张手牌，否则伤害+1且其判定，结果为红你不能响应,结果为黑其摸两张牌",

}

local caorui = General(extension, "joy__caorui", "wei", 3)
local mingjian = fk.CreateActiveSkill{
  name = "joy__mingjian",
  anim_type = "support",
  min_card_num = 1,
  target_num = 1,
  can_use = function(self, player)
    return not player:isKongcheng() and player:usedSkillTimes(self.name, Player.HistoryPhase) == 0
  end,
  card_filter = function(self, to_select, selected)
    return Fk:currentRoom():getCardArea(to_select) ~= Card.PlayerEquip
  end,
  target_filter = function(self, to_select, selected)
    return #selected == 0 and to_select ~= Self.id
  end,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    local target = room:getPlayerById(effect.tos[1])
    local cards = effect.cards
    local dummy = Fk:cloneCard("dilu")
    room:moveCardTo(cards, Player.Hand, target, fk.ReasonGive, self.name, nil, false)
    room:addPlayerMark(target, "@@" .. self.name, 1)
  end,
}
local mingjian_record = fk.CreateTriggerSkill{
  name = "#joy__mingjian_record",

  refresh_events = {fk.TurnStart},
  can_refresh = function(self, event, target, player, data)
    return player:getMark("@@joy__mingjian") > 0 and target == player
  end,
  on_refresh = function(self, event, target, player, data)
    local room = player.room
    room:addPlayerMark(player, "@@joy__mingjian-turn", player:getMark("@@joy__mingjian"))
    room:addPlayerMark(player, MarkEnum.AddMaxCardsInTurn, player:getMark("@@joy__mingjian"))
    room:setPlayerMark(player, "@@joy__mingjian", 0)
  end,
}
local mingjian_targetmod = fk.CreateTargetModSkill{
  name = "#joy__mingjian_targetmod",
  residue_func = function(self, player, skill, scope)
    if skill.trueName == "slash_skill" and player:getMark("@@joy__mingjian-turn") > 0 and scope == Player.HistoryPhase then
      return player:getMark("@@joy__mingjian-turn")
    end
  end,
}
local xingshuai = fk.CreateTriggerSkill{
  name = "joy__xingshuai$",
  anim_type = "defensive",
  frequency = Skill.Limited,
  events = {fk.EnterDying},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and player:usedSkillTimes(self.name, Player.HistoryGame) == 0 and
      not table.every(player.room:getOtherPlayers(player), function(p) return p.kingdom ~= "wei" end)
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local targets = {}
    for _, p in ipairs(room:getOtherPlayers(player)) do
      if p.kingdom == "wei" and room:askForSkillInvoke(p, self.name, data, "#joy__xingshuai-invoke::"..player.id) then
        table.insert(targets, p)
      end
    end
    if #targets > 0 then
      for _, p in ipairs(targets) do
        room:recover{
          who = player,
          num = 1,
          recoverBy = p,
          skillName = self.name
        }
      end
    end
    if not player.dying then
      for _, p in ipairs(targets) do
        room:damage{
          to = p,
          damage = 1,
          skillName = self.name,
        }
        if not p.dead then
            room:drawCards(p,1,self.name)
        end
      end
    end
  end,
}
mingjian:addRelatedSkill(mingjian_record)
mingjian:addRelatedSkill(mingjian_targetmod)
caorui:addSkill("huituo")
caorui:addSkill(mingjian)
caorui:addSkill(xingshuai)
Fk:loadTranslationTable{
  ["joy__caorui"] = "曹叡",

  ["joy__mingjian"] = "明鉴",
  [":joy__mingjian"] = "出牌阶段限一次，你可以将任意张手牌交给一名其他角色，然后该角色下回合的手牌上限+1，且出牌阶段内可以多使用一张【杀】。",
  ["joy__xingshuai"] = "兴衰",
  [":joy__xingshuai"] = "主公技，限定技，当你进入濒死状态时，你可令其他魏势力角色依次选择是否令你回复1点体力。选择是的角色在此次濒死结算结束后"..
  "受到1点无来源的伤害并摸一张牌。",

  ["@@joy__mingjian"] = "明鉴",
  ["@@joy__mingjian-turn"] = "明鉴",
  ["#joy__xingshuai-invoke"] = "兴衰：你可以令%dest回复1点体力，结算后你受到1点伤害并摸一张牌",

}

local zhangchunhua = General(extension, "joyex__zhangchunhua", "wei", 3, 3, General.Female)
local joyex__jueqing_trigger = fk.CreateTriggerSkill{
  name = "#joyex__jueqing_trigger",
  anim_type = "offensive",
  events = {fk.PreDamage},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self)  and not data.chain
  end,
  on_cost = function(self, event, target, player, data)
    return player.room:askForSkillInvoke(player, self.name, nil, "#joyex__jueqing-invoke1::"..data.to.id)
  end,
  on_use = function(self, event, target, player, data)
     player.room:loseHp(player, 1, self.name)
    data.damage = data.damage +1
  end,
}
local joyex__jueqing = fk.CreateTriggerSkill{
  name = "joyex__jueqing",
  anim_type = "offensive",
  events = {fk.DamageCaused},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self)
  end,
  on_cost = function(self, event, target, player, data)
    return player.room:askForSkillInvoke(player, self.name, nil, "#joyex__jueqing-invoke2::"..data.to.id)
  end,
  on_use = function(self, event, target, player, data)
    player.room:loseHp(data.to, data.damage, self.name)
    return true
  end,
}

local joyex__shangshi_drawcard = fk.CreateTriggerSkill{
  name = "#joyex__shangshi_drawcard",
  anim_type = "negative",
  events = {fk.DrawNCards,fk.EventPhaseChanging},
  frequency = Skill.Compulsory,
  can_trigger = function(self, event, target, player, data)
    if event == fk.EventPhaseChanging and target.skipped_phases[Player.Draw] then
      return player:hasSkill(self.name) and target == player and player:getMark("@joyex__shangshi") > 0
    elseif event == fk.DrawNCards then
      return  player:hasSkill(self.name) and target == player and player:getMark("@joyex__shangshi") > 0
    end
  end,
  on_use = function(self, event, target, player, data)
    local m = player:getMark("@joyex__shangshi")
    if event == fk.DrawNCards then
      data.n = data.n + m
    end
    player.room:removePlayerMark(player, "@joyex__shangshi", m)
   
  end,
}
local joyex__shangshi = fk.CreateTriggerSkill{
  name = "joyex__shangshi",
  anim_type = "drawcard",
  events = {fk.HpChanged, fk.MaxHpChanged, fk.AfterCardsMove},
  can_trigger = function(self, event, target, player, data)
    if player:hasSkill(self) and (player:getHandcardNum() < player:getLostHp() or player:getHandcardNum() < 1) then
      if event == fk.AfterCardsMove then
        for _, move in ipairs(data) do
          return move.from == player.id
        end
      else
        return target == player
      end
    end
  end,
  on_cost = function(self, event, target, player, data)
    local n = math.max(player:getLostHp(),1)
    return player.room:askForSkillInvoke(player, self.name, nil, "#joyex__shangshi-invoke1:::"..n)
  end,
  on_use = function(self, event, target, player, data)
    local n = math.max(player:getLostHp(),1)
    local m = n - player:getHandcardNum()
    player:drawCards(m, self.name)
    if player:getMark("joyex__jueqing_discard-turn") == 0 and not player:isKongcheng() then
      local hand = player:getCardIds(Player.Hand)
      local card = player.room:askForCard(player, 1, 999, false, self.name, true, ".", "#joyex__shangshi-invoke2")
      if #card > 0 then
        player:setMark("joyex__jueqing_discard-turn",1)
        if #card == #hand then
          player.room:addPlayerMark(player,"@joyex__shangshi",1)
        end
        player.room:throwCard(card, self.name, player, player)
      end
    end
  end,
}
joyex__jueqing:addRelatedSkill(joyex__jueqing_trigger)
joyex__shangshi:addRelatedSkill(joyex__shangshi_drawcard)
zhangchunhua:addSkill(joyex__jueqing)
zhangchunhua:addSkill(joyex__shangshi)
Fk:loadTranslationTable{
  ["joyex__zhangchunhua"] = "界张春华",
  ["#joyex__zhangchunhua"] = "冷血皇后",

  ["joyex__jueqing"] = "绝情",
  ["#joyex__jueqing_trigger"] = "绝情",
  [":joyex__jueqing"] = "<br/>①当你造成伤害时，你可以失去1点体力令此伤害+1。<br/>②你造成的伤害可以视为体力流失。",
  ["joyex__shangshi"] = "伤逝",
  ["#joyex__shangshi_drawcard"] = "伤逝",
  [":joyex__shangshi"] = "<br/>①每当你的手牌数小于X时，可立即将手牌数摸至X。(X为你已损失的体力值且至少为1)"..
  "<br/>②每回合限一次，当你因伤逝而摸牌时，你可以弃置任意张手牌；若你以此法弃置了所有手牌，你下回合摸牌数+1",
  ["#joyex__shangshi-invoke1"] = "伤逝:是否将手牌摸至 %arg 张？",
  ["#joyex__shangshi-invoke2"] = "伤逝:可以选择弃置任意张手牌（每回合限一次），若弃置所有手牌则你下回合摸牌数+1",
  ["#joyex__jueqing-invoke1"] = "绝情:是否失去1点体力令即将对 %dest 造成的伤害+1？",
  ["#joyex__jueqing-invoke2"] = "绝情:是否令即将对 %dest 造成的伤害视为失去体力？",
  ["@joyex__shangshi"] = "伤逝",

  ["$joyex__jueqing1"] = "弃我情意，与君绝情。",
  ["$joyex__jueqing2"] = "天若老，情亦绝。",
  ["$joyex__shangshi1"] = "花开瓣落无人怜，叶去枝孤冷年月。",
  ["$joyex__shangshi2"] = "星辰陨落，相思伤逝。",
  ["~joyex__zhangchunhua"] = "天涯海角与谁同？",
}

local lingtong = General(extension, "joy__lingtong", "wu", 4)
lingtong:addSkill("ty_ex__xuanfeng")
lingtong:addSkill("ex__yongjin")
Fk:loadTranslationTable{
  ["joy__lingtong"] = "凌统",
  ["#joy__lingtong"] = "豪情烈胆",
}

local xushi = General(extension, "joy__xushi", "wu", 3, 3, General.Female)
local wengua = fk.CreateActiveSkill{
  name = "joy__wengua",
  anim_type = "support",
  card_num = 1,
  target_num = 0,
  prompt = "#wengua",
  can_use = function(self, player)
    return player:usedSkillTimes(self.name, Player.HistoryPhase) == 0 and not player:isNude()
  end,
  card_filter = function(self, to_select, selected)
    return #selected == 0 
  end,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    local choices = {"Cancel", "Top", "Bottom"}
    local id = effect.cards[1]
    local card = Fk:getCardById(id)
    player:showCards(id)
    if card.type == Card.TypeTrick then
      if player.maxHp < 5 then
        room:changeMaxHp(player, 1)
      end
      if player:isWounded() then
        player.room:recover({
          who = player,
          num = 1,
          recoverBy = player,
          skillName = self.name
          })
      end
    end
    local choice = room:askForChoice(player, choices, self.name,
      "#wengua-choice::"..player.id..":"..Fk:getCardById(effect.cards[1]):toLogString())
    if choice == "Cancel" then return end
    local index = 1
    if choice == "Bottom" then
      index = -1
    end
    room:moveCards({
      ids = effect.cards,
      from = player.id,
      toArea = Card.DrawPile,
      moveReason = fk.ReasonJustMove,
      skillName = self.name,
      drawPilePosition = index,
    })
    if player.dead then return end
    if choice == "Top" then
      if not player.dead then
        player:drawCards(1, self.name, "bottom")
      end
    else
      if not player.dead then
        player:drawCards(1, self.name)
      end
    end
  end,
}
local wengua_trigger = fk.CreateTriggerSkill{
  name = "#joy__wengua_trigger",

  refresh_events = {fk.GameStart, fk.EventAcquireSkill, fk.EventLoseSkill, fk.Deathed},
  can_refresh = function(self, event, target, player, data)
    if event == fk.GameStart then
      return player:hasSkill(self.name, true)
    elseif event == fk.EventAcquireSkill or event == fk.EventLoseSkill then
      return data == self and not table.find(player.room:getOtherPlayers(player), function(p) return p:hasSkill("joy__wengua", true) end)
    else
      return target == player and player:hasSkill(self.name, true, true) and
        not table.find(player.room:getOtherPlayers(player), function(p) return p:hasSkill("joy__wengua", true) end)
    end
  end,
  on_refresh = function(self, event, target, player, data)
    local room = player.room
    if event == fk.GameStart or event == fk.EventAcquireSkill then
      if player:hasSkill(self.name, true) then
        for _, p in ipairs(room:getOtherPlayers(player)) do
          room:handleAddLoseSkills(p, "joy__wengua&", nil, false, true)
        end
      end
    elseif event == fk.EventLoseSkill or event == fk.Deathed then
      for _, p in ipairs(room:getOtherPlayers(player)) do
        room:handleAddLoseSkills(p, "-joy__wengua&", nil, false, true)
      end
    end
  end,
}
local wengua_active = fk.CreateActiveSkill{
  name = "joy__wengua&",
  anim_type = "support",
  card_num = 1,
  target_num = 1,
  prompt = "#wengua&",
  can_use = function(self, player)
    return player:usedSkillTimes(self.name, Player.HistoryPhase) == 0 and not player:isKongcheng()
  end,
  card_filter = function(self, to_select, selected)
    return #selected == 0
  end,
  target_filter = function(self, to_select, selected)
    return #selected == 0 and to_select ~= Self.id and Fk:currentRoom():getPlayerById(to_select):hasSkill("joy__wengua")
  end,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    local target = room:getPlayerById(effect.tos[1])
    local id = effect.cards[1]
    local card = Fk:getCardById(id)
    player:showCards(id)
    room:obtainCard(target.id, id, false, fk.ReasonGive)
    if card.type == Card.TypeTrick then
      if target.maxHp < 5 then
        room:changeMaxHp(target, 1)
      end
      if target:isWounded() then
        target.room:recover({
          who = target,
          num = 1,
          recoverBy = target,
          skillName = self.name
          })
      end
    end
    if room:getCardOwner(id) ~= target or room:getCardArea(id) ~= Card.PlayerHand then return end
    local choices = {"Cancel", "Top", "Bottom"}
    local choice = room:askForChoice(target, choices, "wengua",
      "#wengua-choice::"..player.id..":"..Fk:getCardById(id):toLogString())
    if choice == "Cancel" then return end
    local index = 1
    if choice == "Bottom" then
      index = -1
    end
    room:moveCards({
      ids = effect.cards,
      from = target.id,
      toArea = Card.DrawPile,
      moveReason = fk.ReasonJustMove,
      skillName = "wengua",
      drawPilePosition = index,
    })
    if player.dead then return end
    if choice == "Top" then
      player:drawCards(1, "wengua", "bottom")
      if not target.dead then
        target:drawCards(1, "wengua", "bottom")
      end
    else
      player:drawCards(1, "wengua")
      if not target.dead then
        target:drawCards(1, "wengua")
      end
    end
  end,
}
local fuzhu = fk.CreateTriggerSkill{
  name = "joy__fuzhu",
  anim_type = "offensive",
  events = {fk.EventPhaseStart},
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(self) and target ~= player and target.phase == Player.Finish and
        #player.room.draw_pile <= 10 * player.maxHp
  end,
  on_cost = function(self, event, target, player, data)
    return player.room:askForSkillInvoke(player, self.name, nil, "#fuzhu-invoke::"..target.id)
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    room:doIndicate(player.id, {target.id})
    local n = 0
    local cards = table.simpleClone(room.draw_pile)
    for _, id in ipairs(cards) do
      local card = Fk:getCardById(id, true)
      if card.trueName == "slash" then
        room:useCard({
          from = player.id,
          tos = {{target.id}},
          card = card,
        })
        n = n + 1
      end
      if n >= #room.players or player.dead or target.dead then
        break
      end
    end
    room:shuffleDrawPile()
  end,
}
Fk:addSkill(wengua_active)
wengua:addRelatedSkill(wengua_trigger)
xushi:addSkill(wengua)
xushi:addSkill(fuzhu)
Fk:loadTranslationTable{
  ["joy__xushi"] = "徐氏",
  ["joy__wengua"] = "问卦",
  [":joy__wengua"] = "每名角色出牌阶段限一次，其可以交给你一张牌并展示，若该牌为锦囊牌，则你加一点体力上限（不会超过5）并回复一点体力,"..
  "然后你可以将此牌置于牌堆顶或牌堆底，你与其从另一端摸一张牌。",
  ["joy__fuzhu"] = "伏诛",
  [":joy__fuzhu"] = "一名角色结束阶段，若牌堆剩余牌数不大于你体力上限的十倍，你可以依次对其使用牌堆中所有的【杀】（不能超过游戏人数），然后洗牌。",
  ["#joy__wengua"] = "问卦：你可以将一张牌置于牌堆顶或牌堆底，从另一端摸两张牌",
  ["joy__wengua&"] = "问卦",
  [":joy__wengua&"] = "出牌阶段限一次，你可以交给徐氏一张牌并展示之，若该牌为锦囊牌，则其加一点体力上限（不会超过5）并回复一点体力,"..
  "然后其可以将此牌置于牌堆顶或牌堆底，其与你从另一端摸一张牌。",
}

local zhangsong = General(extension, "joy__zhangsong", "shu", 3)
zhangsong:addSkill("qiangzhi")
zhangsong:addSkill("ty_ex__xiantu")
Fk:loadTranslationTable{
  ["joy__zhangsong"] = "张松",
  ["#joy__zhangsong"] = "怀璧待凤仪",
}

local zhuran = General(extension, "joy__zhuran", "wu", 4)
zhuran:addSkill("ty_ex__danshou")
Fk:loadTranslationTable{
  ["joy__zhuran"] = "朱然",
  ["#joy__zhuran"] = "不动之督",
}

local wangyi = General(extension, "joy__wangyi", "wei", 4, 4, General.Female)
wangyi:addSkill("zhenlie")
wangyi:addSkill("miji")
Fk:loadTranslationTable{
  ["joy__wangyi"] = "王异",
  ["#joy__wangyi"] = "决意的巾帼",
}

local zhuzhi = General(extension, "joy__zhuzhi", "wu", 4)
local function doAnguo(player, anguo_type, source)
  local room = player.room
  if anguo_type == "draw" then
    if table.every(room.alive_players, function (p) return p:getHandcardNum() >= player:getHandcardNum() end) then
      player:drawCards(1, "anguo")
      return true
    end
  elseif anguo_type == "recover" then
    if player:isWounded() and table.every(room.alive_players, function (p) return p.hp >= player.hp end) then
      room:recover({
        who = player,
        num = 1,
        recoverBy = source,
        skillName = "anguo",
      })
      return true
    end
  elseif anguo_type == "equip" then
    if table.every(room.alive_players, function (p)
      return #p.player_cards[Player.Equip] >= #player.player_cards[Player.Equip] end) then
      local cards = {}
      for _, id in ipairs(room.draw_pile) do
        local card = Fk:getCardById(id)
        if card.type == Card.TypeEquip and player:canUse(card) and not player:prohibitUse(card) then
          table.insert(cards, card)
        end
      end
      if #cards > 0 then
        room:useCard({
          from = player.id,
          tos = {{player.id}},
          card = table.random(cards),
        })
        return true
      end
    end
  end
  return false
end
local anguo = fk.CreateActiveSkill{
  name = "joy__anguo",
  anim_type = "support",
  prompt = "#anguo-active",
  card_num = 0,
  target_num = 1,
  can_use = function(self, player)
    return player:usedSkillTimes(self.name, Player.HistoryPhase) == 0
  end,
  card_filter = Util.FalseFunc,
  target_filter = function(self, to_select, selected)
    return #selected == 0
  end,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    local target = room:getPlayerById(effect.tos[1])
    local types = {"equip", "recover", "draw"}
    for i = 3, 1, -1 do
      if doAnguo(target, types[i], player) then
        table.removeOne(types, types[i])
        if target.dead then
          break
        end
      end
    end
    for i = #types, 1, -1 do
      if player.dead then break end
      doAnguo(player, types[i], player)
    end
  end,
}
zhuzhi:addSkill(anguo)
Fk:loadTranslationTable{
  ["joy__zhuzhi"] = "朱治",
  ["joy__anguo"] = "安国",
  [":joy__anguo"] = "出牌阶段限一次，你可以选择一名角色，令其依次执行：若其手牌数为全场最少，其摸一张牌；"..
  "体力值为全场最低，回复1点体力；装备区内牌数为全场最少，随机使用牌堆中一张装备牌。"..
  "然后若该角色有未执行的效果且你满足条件，你执行之。",

  ["#joy__anguo-active"] = "发动 安国，选择一其他角色",

}

local zhonghui = General(extension, "joy__zhonghui", "wei", 3)
local quanji_trigger = fk.CreateTriggerSkill{
  name = "#joy__quanji_trigger",
  anim_type = "drawcard",
  events = {fk.Damaged},
  on_trigger = function(self, event, target, player, data)
    self.cancel_cost = false
    for i = 1, data.damage do
      if self.cancel_cost or player.dead then break end
      self:doCost(event, target, player, data)
    end
  end,
  on_cost = function(self, event, target, player, data)
    local room = player.room
    if room:askForSkillInvoke(player, self.name, data,"#joy__quanji") then
      return true
    end
    self.cancel_cost = true
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    player:drawCards(2, self.name)
  end,
}
local quanji = fk.CreateActiveSkill{
  name = "joy__quanji",
  anim_type = "control",
  min_card_num = 1,
  can_use = function(self, player)
    return not player:isKongcheng() and #player:getPile("joy__zhonghui_quan") < 51
  end,
  card_filter = function(self, to_select, selected)
    return Fk:currentRoom():getCardArea(to_select) ~= Card.PlayerEquip
  end,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    player:addToPile("joy__zhonghui_quan", effect.cards, true, self.name)
  end,
}
local quanji_maxcards = fk.CreateMaxCardsSkill{
  name = "#joy__quanji_maxcards",
  correct_func = function(self, player)
    if player:hasSkill(self) then
      return math.min(#player:getPile("joy__zhonghui_quan"),5)
    else
      return 0
    end
  end,
}
local paiyi = fk.CreateActiveSkill{
  name = "joy__paiyi",
  anim_type = "control",
  card_num = 1,
  target_num = 1,
  expand_pile = "joy__zhonghui_quan",
  can_use = function(self, player)
    return #player:getPile("joy__zhonghui_quan") > 0 and player:usedSkillTimes(self.name, Player.HistoryPhase) == 0
  end,
  target_filter = function(self, to_select, selected)
    return #selected == 0
  end,
  card_filter = function(self, to_select, selected)
    return #selected == 0 and Self:getPileNameOfId(to_select) == "joy__zhonghui_quan"
  end,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    local target = room:getPlayerById(effect.tos[1])
    room:moveCards({
      from = player.id,
      ids = effect.cards,
      toArea = Card.DiscardPile,
      moveReason = fk.ReasonPutIntoDiscardPile,
      skillName = self.name,
    })
    target:drawCards(2, self.name)
    if target:getHandcardNum() > player:getHandcardNum() then
      room:damage{
        from = player,
        to = target,
        damage = 1,
        skillName = self.name,
      }
    end
  end,
}
quanji:addRelatedSkill(quanji_trigger)
quanji:addRelatedSkill(quanji_maxcards)
zhonghui:addSkill(quanji)
zhonghui:addSkill(paiyi)
Fk:loadTranslationTable{
  ["joy__zhonghui"] = "钟会",
  ["#joy__zhonghui"] = "桀骜的野心家",

  ["joy__quanji"] = "权计",
  [":joy__quanji"] = "每当你受到1点伤害后，你可以摸两张牌;你的出牌阶段，可以将任意张手牌置于武将牌上，称为“权”。"..
  "你的手牌上限+X（X为“权的数量且最大为5）。"..
  '<br /><font color="grey">（为防止某些情况平局，“权”大于50张时将无法继续塞“权”）</font>',
  ["joy__paiyi"] = "排异",
  [":joy__paiyi"] = "出牌阶段限一次，你可以将一张“权”置入弃牌堆，令一名角色摸两张牌，然后若该角色的手牌数大于你的手牌数,则你对其造成一点伤害。",
  ["joy__zhonghui_quan"] = "权",
  ["#joy__quanji"] = "是否发动“权计”摸俩张牌？",
  ["#joy__quanji_trigger"] = "权计",

}

local caochong = General(extension, "joy__caochong", "wei", 3)
local chengxiang = fk.CreateTriggerSkill{
  name = "joy__chengxiang",
  anim_type = "masochism",
  events = {fk.Damaged},
  frequency = Skill.Compulsory,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local cards = room:getNCards(4)
    room:moveCards({
      ids = cards,
      toArea = Card.Processing,
      moveReason = fk.ReasonPut,
    })
    local get = room:askForArrangeCards(player, self.name, {cards},
    "#chengxiang-choose", false, 0, {4, 4}, {0, 1}, ".", "chengxiang_count", {{}, {cards[1]}})[2]
    room:moveCardTo(get, Player.Hand, player, fk.ReasonJustMove, self.name, "", true, player.id)
    cards = table.filter(cards, function(id) return room:getCardArea(id) == Card.Processing end)
    if #cards > 0 then
      room:moveCards({
        ids = cards,
        toArea = Card.DiscardPile,
        moveReason = fk.ReasonPutIntoDiscardPile,
      })
    end
  end
}
caochong:addSkill(chengxiang)
caochong:addSkill("renxin")
Fk:loadTranslationTable{
  ["joy__caochong"] = "曹冲",
  ["#joy__caochong"] = "仁爱的神童",

  ["joy__chengxiang"] = "称象",
  [":joy__chengxiang"] = "<font color='red'>锁定技</font>，每当你受到一次伤害后，你亮出牌堆顶的四张牌，然后获得其中任意数量点数之和小于或等于13的牌，将其余的牌置入弃牌堆。",
  ["#chengxiang-choose"] = "获得任意点数之和小于或等于13的牌",
  ["chengxiang_count"] = "称象",
}

local xinxianying = General(extension, "joy__xinxianying", "wei", 3, 3, General.Female)
xinxianying:addSkill("ty__zhongjian")
local caishi = fk.CreateTriggerSkill{
  name = "joy__caishi",
  anim_type = "control",
  events = {fk.EventPhaseEnd},
  can_trigger = function(self, event, target, player, data)
    if player:hasSkill(self) and player == target and player.phase == Player.Draw then
      return #player.room.logic:getEventsOfScope(GameEvent.MoveCards, 1, function(e)
        local move = e.data[1]
        if move and move.to and player.id == move.to and move.toArea == Card.PlayerHand then
          for _, info in ipairs(move.moveInfo) do
            if info.fromArea == Card.DrawPile then
              return true
            end
          end
        end
      end, Player.HistoryPhase) > 0
    end
  end,
  on_cost = function(self, event, target, player, data)
    local ids = {}
    player.room.logic:getEventsOfScope(GameEvent.MoveCards, 1, function(e)
      local move = e.data[1]
      if move and move.to and player.id == move.to and move.toArea == Card.PlayerHand then
        for _, info in ipairs(move.moveInfo) do
          if info.fromArea == Card.DrawPile then
            table.insertIfNeed(ids, info.cardId)
          end
        end
      end
    end, Player.HistoryPhase)
    if #ids == 0 then return false end
    local different = table.find(ids, function(id) return Fk:getCardById(id).suit ~= Fk:getCardById(ids[1]).suit end)
    self.cost_data = different
    if different and player:isWounded() then
      local card = player.room:askForDiscard(player, 1,1,true,self.name,true, ".", "#joy__caishi-invoke")
      if #card > 0 then
        player.room:throwCard(card,self.name,player)
        return true
      end
    else
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local different = self.cost_data
    if different then
      room:recover({ who = player,  num = 1, skillName = self.name })
    else
      room:setPlayerMark(player, "ty__caishi_twice-turn", 1)
    end
  end,
}
xinxianying:addSkill(caishi)
Fk:loadTranslationTable{
  ["joy__xinxianying"] = "辛宪英",
  ["#joy__xinxianying"] = "忠鉴清识",
  
  ["joy__caishi"] = "才识",
  [":joy__caishi"] = "摸牌阶段结束时，若你本阶段摸的牌：花色相同，本回合〖忠鉴〗改为“出牌阶段限两次”；花色不同，你可以弃置一张牌，然后回复1点体力。",
  ["#joy__caishi-invoke"] = "你可以弃置一张牌并回复1点体力",

}

local wuyi = General(extension, "joy__wuyi", "shu", 4)
wuyi:addSkill("ty_ex__benxi")

Fk:loadTranslationTable{
  ["joy__wuyi"] = "吴懿",
  ["#joy__wuyi"] = "建兴鞍辔",

  ["$ty_ex__benxi_joy__wuyi1"] = " ",
  ["$ty_ex__benxi_joy__wuyi2"] = " ",
  ["~joy__wuyi"] = " ",

}

local sunluban = General(extension, "joy__sunluban", "wu", 3, 3, General.Female)
local zenhui = fk.CreateTriggerSkill{
  name = "joy__zenhui",
  anim_type = "control",
  events = {fk.TargetSpecifying},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and player.phase == Player.Play and 
      (data.card.trueName == "slash" or data.card:isCommonTrick()) and data.firstTarget and player:usedSkillTimes(self.name, Player.HistoryPhase) == 0 and
      U.isOnlyTarget(player.room:getPlayerById(data.to), data, event)
  end,
  on_cost = function(self, event, target, player, data)
    local room = player.room
    return room:askForSkillInvoke(player,self.name,data,"#joy__zenhui-invoke::"..data.to)
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local to = room:getPlayerById(data.to)
      local cards = room:askForCard(to, 1, 1, true, self.name, true, ".",
      "#joy__zenhui-give::"..player.id..":"..data.card:toLogString())
      if #cards > 0 then
        room:obtainCard(player, cards[1], false, fk.ReasonPrey)
      else 
        room:loseHp(to,1,self.name)
      end
  end,
}
sunluban:addSkill(zenhui)
local jiaojin = fk.CreateTriggerSkill{
  name = "joy__jiaojin",
  anim_type = "defensive",
  events = {fk.TargetConfirmed},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and data.from ~= player.id and (data.card.trueName == "slash" or data.card:isCommonTrick()) and not player:isKongcheng()
  end,
  on_cost = function(self, event, target, player, data)
    local cards = player.room:askForDiscard(player, 1, 1, false, self.name, true, ".", "#joy__jiaojin-discard::"..data.from..":"..data.card:toLogString(), true)
    if #cards > 0 then
      self.cost_data = cards
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    room:throwCard(self.cost_data, self.name, player)
    table.insertIfNeed(data.nullifiedTargets, player.id)
  end,
}
sunluban:addSkill(jiaojin)
Fk:loadTranslationTable{
  ["joy__sunluban"] = "孙鲁班",
  ["#joy__sunluban"] = "为虎作伥",

  ["joy__zenhui"] = "谮毁",
  [":joy__zenhui"] = "出牌阶段限一次，当你使用【杀】或普通锦囊牌指定一名角色为唯一目标时，你可其选择一项：1.交给你一张牌；2.失去1点体力。",
  ["#joy__zenhui-invoke"] = "谮毁：出牌阶段限一次，是否令 %dest 选择交给你一张牌或失去1点体力",
  ["#joy__zenhui-give"] = "谮毁：交给 %dest 一张牌；或失去1点体力",
  ["joy__jiaojin"] = "骄矜",
  [":joy__jiaojin"] = "当你成为其他角色使用【杀】或普通锦囊牌的目标后，你可以弃置一张手牌令此牌对你无效。",
  ["#joy__jiaojin-discard"] = "骄矜：你可弃置一张手牌，令 %dest 使用的%arg对你无效。",

}

local bulianshi = General(extension, "joy__bulianshi", "wu", 3, 3, General.Female)
local anxu = fk.CreateTriggerSkill{
  name = "joy__anxu",
  anim_type = "control",
  events = {fk.EventPhaseStart,fk.EventPhaseEnd},
  can_trigger = function(self, event, target, player, data)
    local room = player.room
    local targets = {}
    if target == player and player:hasSkill(self) then
      local max = 0
      for _, p in ipairs(room:getOtherPlayers(player)) do
        if p:getHandcardNum() > max then
          max = p:getHandcardNum()
          targets = {}
        end
        if p:getHandcardNum() == max then
          table.insert(targets, p.id)
        end
      end
      return player.phase == Player.Play  and #targets > 0 and max > 0
    end
  end,
  on_cost = function (self, event, target, player, data)
    local room = player.room
    local targets = {}
    local max = 0
      for _, p in ipairs(room:getOtherPlayers(player)) do
        if p:getHandcardNum() > max then
          max = p:getHandcardNum()
          targets = {}
        end
        if p:getHandcardNum() == max then
          table.insert(targets, p.id)
        end
      end
    if #targets > 0 then
      local target = room:askForChoosePlayers(player,targets,1,1,"#joy__anxu-invoke",self.name,true)
      if #target > 0 then
        self.cost_data = target[1]
        return true
      end
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local to = room:getPlayerById(self.cost_data)
    local card = room:askForCardChosen(player,to,"h",self.name,"#joy__anxu-choose::"..to.id)
    if card > 0 then
      room:obtainCard(player,card,false,fk.ReasonPrey)
    end
    if Fk:getCardById(card).suit == Card.Spade and not to.dead then
      to:drawCards(1,self.name)
    end
  end,
}
local zhuiyi = fk.CreateTriggerSkill{
  name = "joy__zhuiyi",
  anim_type = "support",
  events = {fk.Death},
  frequency = Skill.Compulsory,
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self.name, false, true)
  end,
  on_cost = function(self, event, target, player, data)
    local room = player.room
    local targets = table.map(room.alive_players, Util.IdMapper)
    if data.damage and data.damage.from then
      table.removeOne(targets, data.damage.from.id)
    end
    local p = room:askForChoosePlayers(player, targets, 1, 1, "#joy__zhuiyi-choose", self.name, true)
    if #p > 0 then
      self.cost_data = p[1]
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local to = room:getPlayerById(self.cost_data)
    to:drawCards(3, self.name)
    if to:isWounded() and not to.dead then
      room:recover{
        who = to,
        num = 1,
        recoverBy = player,
        skillName = self.name
      }
    end
  end,
}
bulianshi:addSkill(anxu)
bulianshi:addSkill(zhuiyi)
Fk:loadTranslationTable{
  ["joy__bulianshi"] = "步练师",
  ["joy__anxu"] = "安恤",
  [":joy__anxu"] = "出牌阶段开始时和结束时，你可以获得手牌数最多的其他角色一张手牌。"..
  "（若你获得的牌花色是♠，该角色摸一张牌。）",
  ["#joy__anxu-invoke"] = "安恤：你可以获得手牌数最多的其他角色一张手牌。",
  ["#joy__anxu-choose"] = "安恤：选择要获得 %dest 的一张手牌，若为♠其摸一张牌",
  ["joy__zhuiyi"] = "追忆",
  [":joy__zhuiyi"] = "锁定技，你死亡时，可以令一名其他角色（杀死你的角色除外）摸三张牌并回复1点体力。",
  ["#joy__zhuiyi-choose"] = "追忆：你可以令一名角色摸三张牌并回复1点体力",
}


local wuguotai = General(extension, "joy__wuguotai", "wu", 3, 3, General.Female)
local ganlu = fk.CreateTriggerSkill{
  name = "joy__ganlu",
  events = {fk.EventPhaseStart},
  anim_type = "control",
  frequency = Skill.Compulsory,
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and player.phase == Player.Play
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local _,dat = room:askForUseActiveSkill(player, "joy__ganlu_active", "#joy__ganlu-invoke", true)
    if dat then
      local targets = table.map(dat.targets, Util.Id2PlayerMapper)
      if dat.interaction == "joy__ganlu_move" then
        room:askForMoveCardInBoard(player, targets[1], targets[2], self.name, "e", nil)
      else
        local slots = {}
        for _, id in ipairs(targets[1]:getCardIds("e")) do
          local s = Fk:getCardById(id).sub_type
          if targets[2]:getEquipment(s) ~= nil then
            table.insertIfNeed(slots, Util.convertSubtypeAndEquipSlot(s))
          end
        end
        if #slots == 0 then return end
        local choice = room:askForChoice(player, slots, self.name)
        local id1 = targets[1]:getEquipment(Util.convertSubtypeAndEquipSlot(choice))
        local id2 = targets[2]:getEquipment(Util.convertSubtypeAndEquipSlot(choice))
        local moveInfos = {}
        table.insert(moveInfos, {
          from = targets[1].id,
          ids = {id1},
          toArea = Card.Processing,
          moveReason = fk.ReasonExchange,
          proposer = player.id,
          skillName = self.name,
        })
        table.insert(moveInfos, {
          from = targets[2].id,
          ids = {id2},
          toArea = Card.Processing,
          moveReason = fk.ReasonExchange,
          proposer = player.id,
          skillName = self.name,
        })
        room:moveCards(table.unpack(moveInfos))
        moveInfos = {}
        if not targets[2].dead and room:getCardArea(id1) == Card.Processing
        and targets[2]:hasEmptyEquipSlot(Fk:getCardById(id1).sub_type) then
          table.insert(moveInfos, {
            ids = {id1},
            to = targets[2].id,
            toArea = Card.PlayerEquip,
            moveReason = fk.ReasonExchange,
            proposer = player.id,
            skillName = self.name,
          })
        end
        if not targets[1].dead and room:getCardArea(id2) == Card.Processing
        and targets[1]:hasEmptyEquipSlot(Fk:getCardById(id2).sub_type) then
          table.insert(moveInfos, {
            ids = {id2},
            to = targets[1].id,
            toArea = Card.PlayerEquip,
            moveReason = fk.ReasonExchange,
            proposer = player.id,
            skillName = self.name,
          })
        end
        if #moveInfos > 0 then
          room:moveCards(table.unpack(moveInfos))
        end
        local to_throw = {}
        if room:getCardArea(id1) == Card.Processing then table.insert(to_throw, id1) end
        if room:getCardArea(id2) == Card.Processing then table.insert(to_throw, id2) end
        if #to_throw > 0 then
          room:moveCards({
            ids = to_throw,
            toArea = Card.DiscardPile,
            moveReason = fk.ReasonPutIntoDiscardPile,
          })
        end
      end
    else
      player:drawCards(1, self.name)
    end
  end,
}
local joy__ganlu_active = fk.CreateActiveSkill{
  name = "joy__ganlu_active",
  card_num = 0,
  target_num = 2,
  interaction = function(self)
    return UI.ComboBox {choices = {"joy__ganlu_move", "joy__ganlu_exchange"} }
  end,
  card_filter = Util.FalseFunc,
  target_filter = function(self, to_select, selected)
    if not self.interaction.data or #selected == 2 then return end
    local to = Fk:currentRoom():getPlayerById(to_select)
    if #selected == 0 then
      return #to:getCardIds("e") > 0
    else
      local first = Fk:currentRoom():getPlayerById(selected[1])
      if #first:getCardIds("e") == 0 or firse == to then return end
      if self.interaction.data == "joy__ganlu_move" then
        return first:canMoveCardsInBoardTo(to, "e")
      else
        return table.find(first:getCardIds("e"), function(id) return to:getEquipment(Fk:getCardById(id).sub_type) ~= nil end)
      end
    end
  end,
}
Fk:addSkill(joy__ganlu_active)
wuguotai:addSkill(ganlu)
local buyi = fk.CreateTriggerSkill{
  name = "joy__buyi",
  anim_type = "support",
  events = {fk.EnterDying},
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(self) and not target:isKongcheng() and player:usedSkillTimes(self.name, Player.HistoryTurn) <3
  end,
  on_cost = function(self, event, target, player, data)
    return player.room:askForSkillInvoke(player, self.name, data, "#joy__buyi-invoke::"..target.id)
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local id = room:askForCardChosen(player, target, "h", self.name)
    target:showCards({id})
    if Fk:getCardById(id).type == Card.TypeBasic and table.contains(target.player_cards[Player.Hand], id) then
      room:throwCard({id}, self.name, target, target)
      if target.dead or not target:isWounded() then return end
      room:recover{
        who = target,
        num = 1,
        recoverBy = player,
        skillName = self.name
      }
    end
  end,
}
wuguotai:addSkill(buyi)
Fk:loadTranslationTable{
  ["joy__wuguotai"] = "吴国太",
  ["#joy__wuguotai"] = "武烈皇后",

  ["joy__ganlu"] = "甘露",
  [":joy__ganlu"] = "锁定技，出牌阶段开始时，你须选择一项：1.移动场上的一张装备牌；2.交换两名角色装备区副类别相同的装备牌；3.你摸一张牌。",
  ["joy__buyi"] = "补益",
  [":joy__buyi"] = "每回合限三次，当一名角色进入濒死状态时，你可以展示该角色一张手牌，若为基本牌，则其弃置此牌并回复1点体力。",

  ["#joy__ganlu-invoke"] = "甘露：请移动或交换场上装备牌，点“取消”则摸一张牌",
  ["joy__ganlu_active"] = "甘露",
  ["joy__ganlu_move"] = "移动场上的一张装备牌",
  ["joy__ganlu_exchange"] = "交换两名角色副类别相同的装备牌",
  [""] = "",
  [""] = "",
  [""] = "",
  ["#joy__buyi-invoke"] = "补益：你可以展示 %dest 的一张手牌，若为基本牌，其弃置并回复1点体力",
}

local zhangyi = General(extension, "joy__zhangyi", "shu", 5)
local shizhi = fk.CreateFilterSkill{
  name = "joy__shizhi",
  card_filter = function(self, to_select, player, isJudgeEvent)
    return player:hasSkill(self) and player.phase ~= Player.NotActive and to_select.name == "jink" and
    (table.contains(player.player_cards[Player.Hand], to_select.id) or isJudgeEvent)
  end,
  view_as = function(self, to_select)
    return Fk:cloneCard("slash", to_select.suit, to_select.number)
  end,
}
local shizhi_trigger = fk.CreateTriggerSkill{
  name = "#joy__shizhi_trigger",
  events = {fk.Damage},
  mute = true,
  frequency = Skill.Compulsory,
  can_trigger = function (self, event, target, player, data)
    return player == target and player:hasSkill("joy__shizhi") and data.card
    and table.contains(data.card.skillNames, "joy__shizhi")
  end,
  on_use = function (self, event, target, player, data)
    player:broadcastSkillInvoke("joy__shizhi")
    player.room:notifySkillInvoked(player, "joy__shizhi", "defensive")
    if player:isWounded() then
      player.room:recover { num = 1, skillName = "joy__shizhi", who = player, recoverBy = player}
    end
    if not player.dead then
      player:drawCards(1,self.name)
    end
  end,

}
zhangyi:addSkill("ty_ex__wurong")
shizhi:addRelatedSkill(shizhi_trigger)
zhangyi:addSkill(shizhi)

Fk:loadTranslationTable{
  ["joy__zhangyi"] = "界张嶷",
  ["#joy__zhangyi"] = "通壮逾古",

  ["joy__shizhi"] = "矢志",
  [":joy__shizhi"] = "锁定技，你的回合内，你的【闪】视为【杀】；当你使用这些【杀】造成伤害后，你回复1点体力并摸一张牌。",

}

local caozhi = General(extension, "joy__caozhi", "wei", 3)
local luoying = fk.CreateTriggerSkill{
  name = "joy__luoying",
  anim_type = "drawcard",
  events = {fk.AfterCardsMove},
  can_trigger = function(self, event, target, player, data)
    if player:hasSkill(self) then
      local ids = {}
      local room = player.room
      for _, move in ipairs(data) do
        if move.toArea == Card.DiscardPile then
          if move.moveReason == fk.ReasonDiscard and move.from and move.from ~= player.id then
            for _, info in ipairs(move.moveInfo) do
              if (info.fromArea == Card.PlayerHand or info.fromArea == Card.PlayerEquip) and
              Fk:getCardById(info.cardId).suit == Card.Club and
              room:getCardArea(info.cardId) == Card.DiscardPile then
                table.insertIfNeed(ids, info.cardId)
              end
            end
          elseif move.moveReason == fk.ReasonJudge then
            local judge_event = room.logic:getCurrentEvent():findParent(GameEvent.Judge)
            if judge_event and judge_event.data[1].who ~= player then
              for _, info in ipairs(move.moveInfo) do
                if info.fromArea == Card.Processing and Fk:getCardById(info.cardId).suit == Card.Club and
                room:getCardArea(info.cardId) == Card.DiscardPile then
                  table.insertIfNeed(ids, info.cardId)
                end
              end
            end
          end
        end
      end
      ids = U.moveCardsHoldingAreaCheck(room, ids)
      if #ids > 0 then
        self.cost_data = ids
        return true
      end
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local ids = table.simpleClone(self.cost_data)
    if #ids > 1 then
      local cards, _ = U.askforChooseCardsAndChoice(player, ids, {"OK"}, self.name,
      "#joy__luoying-choose", {"joy__get_all"}, 1, #ids)
      if #cards > 0 then
        ids = cards
      end
    end
    room:moveCardTo(ids, Card.PlayerHand, player, fk.ReasonPrey, self.name)
  end,
}
local luoying_max = fk.CreateMaxCardsSkill{
  name = "#joy__luoying_max",
  exclude_from = function(self, player, card)
    return player:hasSkill("joy__luoying") and card.suit == Card.Club and player.phase == Player.Discard
  end,
}
local jiushi = fk.CreateViewAsSkill{
  name = "joy__jiushi",
  anim_type = "support",
  prompt = "#joy__jiushi-active",
  pattern = "analeptic",
  card_filter = function() return false end,
  before_use = function(self, player)
    player:turnOver()
  end,
  view_as = function(self, cards)
    local c = Fk:cloneCard("analeptic")
    c.skillName = self.name
    return c
  end,
  enabled_at_play = function (self, player)
    return player.faceup
  end,
  enabled_at_response = function (self, player)
    return player.faceup
  end,
}
local jiushi_trigger = fk.CreateTriggerSkill{
  name = "#joy__jiushi_trigger",
  anim_type = "support",
  mute = true,
  events = {fk.Damaged, fk.TurnedOver},
  can_trigger = function(self, event, target, player, data)
    if target == player and player:hasSkill(jiushi.name) then
      if event == fk.Damaged then
        return not player.faceup and data.jiushi
      elseif event == fk.TurnedOver then
        return true
      end
    end
  end,
  on_cost = function(self, event, target, player, data)
    return event == fk.TurnedOver or player.room:askForSkillInvoke(player, jiushi.name)
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    room:notifySkillInvoked(player, jiushi.name)
    player:broadcastSkillInvoke(jiushi.name)
    if event == fk.Damaged then
      player:turnOver()
    elseif event == fk.TurnedOver and not player.dead then
      local cards = player.room:getCardsFromPileByRule(".|.|.|.|.|trick")
      if #cards > 0 then
        room:obtainCard(player, cards[1], true, fk.ReasonJustMove)
      end
    end
  end,
  refresh_events = {fk.DamageInflicted},
  can_refresh = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and not player.faceup
  end,
  on_refresh = function(self, event, target, player, data)
    data.jiushi = true
  end,
}
luoying:addRelatedSkill(luoying_max)
jiushi:addRelatedSkill(jiushi_trigger)
caozhi:addSkill(luoying)
caozhi:addSkill(jiushi)
Fk:loadTranslationTable{
  ["joy__caozhi"] = "曹植",
  ["#joy__caozhi"] = "八斗之才",

  ["joy__luoying"] = "落英",
  [":joy__luoying"] = "①当其他角色的♣牌因弃置或判定进入弃牌堆后，<br>②你可以获得之;弃牌阶段，你的梅花牌不计入手牌上限。",
  ["joy__jiushi"] = "酒诗",
  [":joy__jiushi"] = "①若你的武将牌正面朝上，你可以翻面视为使用一张【酒】；<br>②当你受到伤害时，若你的武将牌背面朝上，你可以在受到伤害后翻至正面;<br>③当你翻面时，你获得牌堆中的一张随机锦囊牌。",
  ["#joy__jiushi_trigger"] = "酒诗",
  ["#joy__jiushi-active"] = "酒诗：你可以翻到背面视为使用一张【酒】",

  ["#joy__luoying-choose"] = "落英：选择要获得的牌",
  ["joy__get_all"] = "全部获得",

  ["$joy__luoying1"] = " ",
  ["$joy__luoying2"] = " ",
  ["$joy__jiushi1"] = " ",
  ["$joy__jiushi2"] = " ",
  ["~joy__caozhi"] = " ",
}

local qinmi = General(extension, "joy__qinmi", "shu", 3)
local jianzhengq = fk.CreateTriggerSkill{
  name = "joy__jianzhengq",
  anim_type = "control",
  events = {fk.TargetSpecifying},
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(self) and target ~= player and data.card.trueName == "slash" and data.firstTarget and
      not table.contains(AimGroup:getAllTargets(data.tos), player.id) and target:inMyAttackRange(player) and not player:isKongcheng()
  end,
  on_cost = function(self, event, target, player, data)
    local card = player.room:askForCard(player, 1, 1, false, self.name, true, ".",
      "#joy__jianzhengq-invoke::"..target.id..":"..data.card:toLogString())
    if #card > 0 then
      self.cost_data = card
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    room:doIndicate(player.id, {target.id})
    room:moveCards({
      ids = self.cost_data,
      from = player.id,
      fromArea = Card.PlayerHand,
      toArea = Card.DrawPile,
      moveReason = fk.ReasonJustMove,
      skillName = self.name,
    })
    for _, id in ipairs(TargetGroup:getRealTargets(data.tos)) do
      AimGroup:cancelTarget(data, id)
    end
    if not player.dead and data.card.color ~= Card.Black then
      room:doIndicate(target.id, {player.id})
      AimGroup:addTargets(room, data, player.id)
    end
  end,
}
local zhuandui = fk.CreateTriggerSkill{
  name = "joy__zhuandui",
  mute = true,
  events ={fk.TargetSpecified, fk.TargetConfirmed},
  can_trigger = function(self, event, target, player, data)
    if target == player and player:hasSkill(self) and data.card.trueName == "slash" and not player:isKongcheng() then
      if event == fk.TargetSpecified then
        return data.to ~= player.id and not player.room:getPlayerById(data.to):isKongcheng()
      else
        return data.from ~= player.id and not player.room:getPlayerById(data.from):isKongcheng()
      end
    end
  end,
  on_cost = function(self, event, target, player, data)
    local prompt
    if event == fk.TargetSpecified then
      prompt = "joy__zhuandui1-invoke::"..data.to..":"..data.card:toLogString()
    else
      prompt = "joy__zhuandui2-invoke::"..data.from..":"..data.card:toLogString()
    end
    return player.room:askForSkillInvoke(player, self.name, nil, prompt)
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    player:broadcastSkillInvoke(self.name)
    local win = false
    if event == fk.TargetSpecified then
      room:notifySkillInvoked(player, self.name, "offensive")
      room:doIndicate(player.id, {data.to})
      local pindian = player:pindian({room:getPlayerById(data.to)}, self.name)
      if pindian.results[data.to].winner == player then
        win = true
        data.disresponsiveList = data.disresponsiveList or {}
        table.insert(data.disresponsiveList, data.to)
      end
    else
      room:notifySkillInvoked(player, self.name, "defensive")
      room:doIndicate(player.id, {data.from})
      local pindian = player:pindian({room:getPlayerById(data.from)}, self.name)
      if pindian.results[data.from].winner == player then
        win = true
        table.insertIfNeed(data.nullifiedTargets, player.id)
      end
    end
    if win and not player.dead then
      player:drawCards(1,self.name)
    end
  end,
}
local tianbian = fk.CreateTriggerSkill{
  name = "joy__tianbian",
  anim_type = "special",
  events ={fk.StartPindian, fk.PindianCardsDisplayed},
  can_trigger = function(self, event, target, player, data)
    if player:hasSkill(self) then
      if event == fk.StartPindian then
        return player == data.from or table.contains(data.tos, player)
      else
        if player == data.from then
          return data.fromCard.suit == Card.Heart
        elseif data.results[player.id] then
          return data.results[player.id].toCard.suit == Card.Heart
        end
      end
    end
  end,
  on_cost = function(self, event, target, player, data)
    if event == fk.StartPindian then
      return player.room:askForSkillInvoke(player, self.name, nil, "#joy__tianbian-invoke")
    else
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    if event == fk.StartPindian then
      if player == data.from then
        data.fromCard = Fk:getCardById(player.room.draw_pile[1])
      else
        data.results[player.id] = data.results[player.id] or {}
        data.results[player.id].toCard = Fk:getCardById(player.room.draw_pile[1])
      end
    else
      if player == data.from then
        data.fromCard.number = 13
      elseif data.results[player.id] then
        data.results[player.id].toCard.number = 13
      end
    end
  end,
}
qinmi:addSkill(jianzhengq)
qinmi:addSkill(zhuandui)
qinmi:addSkill(tianbian)
Fk:loadTranslationTable{
  ["joy__qinmi"] = "秦宓",
  ["#joy__qinmi"] = "彻天之舌",

  ["joy__jianzhengq"] = "谏征",
  [":joy__jianzhengq"] = "其他角色使用【杀】指定其他角色为目标时，若你在其攻击范围内，你可以将一张手牌置于牌堆顶，取消所有目标，然后若此【杀】"..
  "不为黑色，你成为目标。",
  ["joy__zhuandui"] = "专对",
  [":joy__zhuandui"] = "当你使用【杀】指定目标后，你可以与目标拼点：若你赢，其不能响应此【杀】。当你成为【杀】的目标后，你可以与使用者拼点：若你赢，"..
  "此【杀】对你无效。<br>当你因此技能拼点赢时，你摸一张牌",
  ["joy__tianbian"] = "天辩",
  [":joy__tianbian"] = "当你拼点时，你可以改为用牌堆顶的一张牌进行拼点；当你的拼点牌亮出后，若此牌花色为<font color='red'>♥</font>，则点数视为K。",
  ["#joy__jianzhengq-invoke"] = "谏征：%dest 使用%arg，你可以将一张手牌置于牌堆顶取消所有目标",
  ["joy__zhuandui1-invoke"] = "专对：你可以与 %dest 拼点，若赢，其不能响应此%arg",
  ["joy__zhuandui2-invoke"] = "专对：你可以与 %dest 拼点，若赢，此%arg对你无效",
  ["#joy__tianbian-invoke"] = "天辩：是否用牌堆顶牌拼点？",

  ["$joy__jianzhengq1"] = "且慢，此仗打不得！",
  ["$joy__jianzhengq2"] = "天时不当，必难取胜！",
  ["$joy__zhuandui1"] = "黄口小儿，也敢来班门弄斧？",
  ["$joy__zhuandui2"] = "你已无话可说了吧！",
  ["$joy__tianbian1"] = "当今天子为刘，天亦姓刘！",
  ["$joy__tianbian2"] = "阁下知其然，而未知其所以然。",
  ["~joy__qinmi"] = "我竟然，也百口莫辩了……",
}

local xusheng = General(extension, "joyex__xusheng", "wu", 4)
local pojun = fk.CreateTriggerSkill{
  name = "joyex__pojun",
  anim_type = "offensive",
  events = {fk.TargetSpecified,fk.DamageCaused},
  can_trigger = function(self, event, target, player, data)
    if target == player and player:hasSkill(self) and data.card and data.card.trueName == "slash" then
      if event == fk.TargetSpecified and player.phase == Player.Play then
        local to = player.room:getPlayerById(data.to)
        return to.hp > 0 and not to:isNude()
      elseif event == fk.DamageCaused then
        return not data.chain and #player:getCardIds(Player.Hand) >= #data.to:getCardIds(Player.Hand) and
        #player:getCardIds(Player.Equip) >= #data.to:getCardIds(Player.Equip)
      end
    end
  end,
  on_cost = function (self, event, target, player, data)
    if event == fk.TargetSpecified then
      local x = player.room:getPlayerById(data.to).hp + 1
      return player.room:askForSkillInvoke(player, self.name, nil, "#joyex__pojun-invoke::"..data.to..":"..x)
    elseif event == fk.DamageCaused then
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if event == fk.TargetSpecified then
      local to = room:getPlayerById(data.to)
      local cards = room:askForCardsChosen(player, to, 1, to.hp + 1, "he", self.name)
      to:addToPile(self.name, cards, false, self.name)
      local equipC = table.filter(cards, function (id)
        return Fk:getCardById(id).type == Card.TypeEquip
      end)
      if #equipC > 0 then
        local card = room:askForCardChosen(player, to, { card_data = { { "$Throw", equipC }  } }, self.name, "#ty_ex__pojun-throw")
        room:throwCard({card}, self.name, to, player)
      end
      if not player.dead and table.find(cards, function (id)
        return Fk:getCardById(id).name == "jink"
      end) then
        player:drawCards(1, self.name)
      end
    elseif event == fk.DamageCaused then
      data.damage = data.damage + 1
    end
  end,
}
local pojun_delay = fk.CreateTriggerSkill{
  name = "#joyex__pojun_delay",
  mute = true,
  events = {fk.TurnEnd},
  can_trigger = function(self, event, target, player, data)
    return not player.dead and #player:getPile("joyex__pojun") > 0
  end,
  on_cost = Util.TrueFunc,
  on_use = function(self, event, target, player, data)
    local dummy = Fk:cloneCard("zixing")
    dummy:addSubcards(player:getPile("joyex__pojun"))
    local room = player.room
    room:obtainCard(player.id, dummy, false)
  end,
}

pojun:addRelatedSkill(pojun_delay)
xusheng:addSkill(pojun)
Fk:loadTranslationTable{
  ["joyex__xusheng"] = "界徐盛",
  ["#joyex__xusheng"] = "江东的铁壁",

  ["joyex__pojun"] = "破军",
  [":joyex__pojun"] = "<br>①当你于出牌阶段内使用【杀】指定一个目标后,你可以将其至多X张牌扣置于该角色的武将牌旁(X为其体力值+1):"..
  "<br>若其中有装备牌，你弃置其中一张牌；<br>若其中有【闪】，你摸一张牌。<br>若如此做，当前回合结束后，该角色获得其武将牌旁的所有牌。<br>②你使用【杀】对手牌数与装备数均不大于你的角色造成伤害时，此伤害+1。",
  ["#joyex__pojun-invoke"] = "破军：你可以扣置 %dest 至多 %arg 张牌",
  ["#joyex__pojun_delay"] = "破军",
  ["$Throw"] = "弃置",
  ["#joyex__pojun-throw"] = "破军：弃置其中一张牌",

  ["$joyex__pojun1"] = "犯大吴疆土者，盛必击而破之！",
  ["$joyex__pojun2"] = "若敢来犯，必叫你大败而归！",
  ["~joyex__xusheng"] = "盛只恨，不能再为主公，破敌致胜了。",
}

local chengong = General(extension, "joy__chengong", "qun", 3)
local mingce = fk.CreateTriggerSkill{
  name = "joy__mingce",
  anim_type = "offensive",
  events = {fk.EventPhaseStart},
  can_trigger = function(self, event, target, player, data)
    if target == player and player:hasSkill(self) and player.phase == Player.Play then
      local players = player.room.alive_players
      return #players > 1
    end
  end,
  on_cost = function (self, event, target, player, data)
    local room = player.room
    local success, dat = room:askForUseActiveSkill(player, "joy__mingce_choose", "#joy__mingce-promot", true, nil, true)
    if success and dat then
      self.cost_data = dat.targets
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local victim = room:getPlayerById(self.cost_data[2])
    local bro = room:getPlayerById(self.cost_data[1])
    room:doIndicate(player.id, {bro.id})
    if  not bro:prohibitUse(Fk:cloneCard("slash")) and not bro:isProhibited(victim, Fk:cloneCard("slash"))then
      room:useVirtualCard("slash", nil, bro, victim, self.name, true)
    end
  end,
}
local mingce_choose = fk.CreateActiveSkill{
  name = "joy__mingce_choose",
  card_num = 0,
  target_num = 2,
  card_filter = Util.FalseFunc,
  target_filter = function(self, to_select, selected)
    if #selected > 1   then return false end
    if #selected == 0 then
      return to_select ~= Self.id
    else
      local bro = Fk:currentRoom():getPlayerById(selected[1])
      local victim = Fk:currentRoom():getPlayerById(to_select)
      return  bro:inMyAttackRange(victim)
    end
  end,
}
Fk:addSkill(mingce_choose)
chengong:addSkill(mingce)
chengong:addSkill("zhichi")
Fk:loadTranslationTable{
  ["joy__chengong"] = "界陈宫",
  ["#joy__chengong"] = "刚直壮烈",

  ["joy__mingce"] = "明策",
  [":joy__mingce"] = "出牌阶段开始时，你可以选择一名其他角色，视为该角色对其攻击范围内的另一名由你指定的角色使用一张【杀】。",
  ["#joy__mingce-promot"] = "明策：先选择一名其他角色，再选定一名其攻击范围内被其视为使用【杀】的角色",
  ["joy__mingce_choose"] = "明策",

  ["$joy__mingce1"] = " ",
  ["$joy__mingce2"] = " ",
  ["$zhichi_joy__chengong1"] = " ",
  ["$zhichi_joy__chengong2"] = " ",
  ["~joy__chengong"] = " ",
}



local zhugeshang = General(extension, "joy__zhugeshang", "shu", 3)
zhugeshang:addSkill("sangu")
zhugeshang:addSkill("yizu")
Fk:loadTranslationTable{
  ["joy__zhugeshang"] = "诸葛尚",
  ["#joy__zhugeshang"] = "尚节殉义",
}

local lukai = General(extension, "joy__lukai", "wu", 4)
lukai:addSkill("bushil")
lukai:addSkill("zhongzhuang")
Fk:loadTranslationTable{
  ["joy__lukai"] = "陆凯",
  ["#joy__lukai"] = "青辞宰辅",
}


local manchong = General(extension, "joy__manchong", "wei", 3)
local junxing = fk.CreateActiveSkill{
  name = "joy__junxing",
  anim_type = "control",
  min_card_num = 1,
  target_num = 1,
  prompt = "#joy__junxing",
  can_use = function(self, player)
    return player:usedSkillTimes(self.name, Player.HistoryPhase) == 0 and not player:isKongcheng()
  end,
  card_filter = function(self, to_select, selected)
    return Fk:currentRoom():getCardArea(to_select) ~= Player.Equip and not Self:prohibitDiscard(Fk:getCardById(to_select))
  end,
  target_filter = function(self, to_select, selected)
    return #selected == 0 and to_select ~= Self.id
  end,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    local target = room:getPlayerById(effect.tos[1])
    room:throwCard(effect.cards, self.name, player)
    if target.dead then return end
    local cards = room:askForDiscard(target,#effect.cards,#effect.cards,true, self.name, true, ".", "弃置"..#effect.cards.."张牌并失去一点体力，或翻面并摸"..#effect.cards.."张牌") 
    if target.dead then return end
    if #cards > 0 then
      room:loseHp(target,1,self.name)
    else
      target:turnOver()
      if not target.dead then
        target:drawCards(#effect.cards, self.name)
      end
    end
  end
}
manchong:addSkill(junxing)
manchong:addSkill("yuce")
Fk:loadTranslationTable{
  ["joy__manchong"] = "满宠",
  ["#joy__manchong"] = "政法兵谋",
  ["joy__junxing"] = "峻刑",
  [":joy__junxing"] = "出牌阶段限一次，你可以弃置至少一张手牌，令一名其他角色选择一项：1.弃置等量张牌并失去1点体力；2.翻面并摸等同于你弃牌数的牌。",
  ["#joy__junxing"] = "峻刑：弃置任意张手牌，令一名角色选择弃置等量张牌并失去1点体力，或翻面并摸等量牌",
}


local liwan = General(extension, "joy__liwan", "wei", 3, 3, General.Female)
liwan:addSkill("liandui")
liwan:addSkill("biejun")
Fk:loadTranslationTable{
  ["joy__liwan"] = "李婉",
  ["#joy__liwan"] = "才媛淑美",
}
return extension
