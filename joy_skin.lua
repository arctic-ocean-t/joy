local extension = Package("joy_skin")
extension.extensionName = "joy"

Fk:loadTranslationTable{
  ["joy_skin"] = "欢乐-皮肤",
}

local U = require "packages/utility/utility"

local jiangwei2 = General(extension, "joy_skin__jiangwei", "shu", 4)
jiangwei2.hidden = true
jiangwei2:addSkill("joy__tiaoxin")
jiangwei2:addSkill("joy__zhiji")
jiangwei2:addRelatedSkill("joy__guanxing")
Fk:loadTranslationTable{
  ["joy_skin__jiangwei"] = "姜维",
  ["#joy_skin__jiangwei"] = "龙的衣钵",

  ["$joy__tiaoxin_joy_skin__jiangwei2"] = " ",
  ["$joy__guanxing_joy_skin__jiangwei2"] = " ",
  ["$joy__zhiji_joy_skin__jiangwei2"] = " ",
  ["$joy__tiaoxin_joy_skin__jiangwei1"] = " ",
  ["$joy__guanxing_joy_skin__jiangwei1"] = " ",
  ["$joy__zhiji_joy_skin__jiangwei1"] = " ",
  ["~joy_skin__jiangwei"] = " ",
}

local sunce2 = General(extension, "joy_skin__sunce", "wu", 4)
sunce2.hidden = true
sunce2:addSkill("joyex__jiang")
sunce2:addSkill("joyex__hunzi")
sunce2:addRelatedSkill("ex__yingzi")
sunce2:addRelatedSkill("joy__yinghun")
sunce2:addSkill("joyex__zhiba")
Fk:loadTranslationTable{
  ["joy_skin__sunce"] = "界孙策",
  ["#joy_skin__sunce"] = "江东的小霸王",
}

local baosanniang2 = General(extension, "joy_skin__baosanniang", "shu", 3, 3, General.Female)
baosanniang2.hidden = true
baosanniang2:addSkill("joy__wuniang")
baosanniang2:addSkill("joy__xushen")
baosanniang2:addRelatedSkill("ty__zhennan")
Fk:loadTranslationTable{
  ["joy_skin__baosanniang"] = "鲍三娘",
  ["#joy_skin__baosanniang"] = "南中武娘",
  
}

local guansuo2 = General(extension, "joy_skin__guansuo", "shu", 4)
guansuo2.hidden = true
guansuo2:addSkill("joy__zhengnan")
guansuo2:addSkill("joy__xiefang")
guansuo2:addRelatedSkill("joy__wusheng")
guansuo2:addRelatedSkill("joyex__dangxian")
guansuo2:addRelatedSkill("ty_ex__zhiman")
Fk:loadTranslationTable{
  ["joy_skin__guansuo"] = "关索",
  ["#joy_skin__guansuo"] = "倜傥孑侠",

}


local pangtong = General:new(extension, "joy_skin__pangtong", "shu", 3)
pangtong.hidden = true
pangtong:addSkill("joyex__lianhuan")
pangtong:addSkill("joyex__niepan")
pangtong:addRelatedSkill("joy__huoji")
pangtong:addRelatedSkill("joy__bazhen")
pangtong:addRelatedSkill("joy__kanpo")

Fk:loadTranslationTable{
  ["joy_skin__pangtong"] = "界庞统",
}

local zhangxuan = General:new(extension, "joy_skin__zhangxuan", "wu",4,4,General.Female)
zhangxuan.hidden = true
zhangxuan:addSkill("joy__tongli")
Fk:loadTranslationTable{
  ["joy_skin__zhangxuan"] = "张璇",
}
return extension