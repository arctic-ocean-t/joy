local extension = Package("joy_shzl")
extension.extensionName = "joy"

Fk:loadTranslationTable{
  ["joy_shzl"] = "欢乐-神话再临",
}

local U = require "packages/utility/utility"

-- 风

local huangzhong = General(extension, "joyex__huangzhong", "shu", 4)
local liegong = fk.CreateTriggerSkill{
  name = "joyex__liegong",
  anim_type = "offensive",
  events = {fk.TargetSpecified},
  can_trigger = function(self, event, target, player, data)
    if not (target == player and player:hasSkill(self)) then return end
    local to = player.room:getPlayerById(data.to)
    return data.card.trueName == "slash" and (#to:getCardIds(Player.Hand) <= #player:getCardIds(Player.Hand) or to.hp >= player.hp)
  end,
  on_use = function(self, event, target, player, data)
    local to = player.room:getPlayerById(data.to)
    if #to:getCardIds(Player.Hand) <= #player:getCardIds(Player.Hand) then
      data.disresponsive = true 
    end
    if to.hp >= player.hp then
      data.additionalDamage = (data.additionalDamage or 0) + 1
    end
  end,
}
local liegong_targetmod = fk.CreateTargetModSkill{
  name = "#joyex__liegong_target",
  mute = true,
  bypass_distances = function(self, player, skill, scope)
    return player:hasSkill(self) and skill.trueName == "slash_skill" 
  end,
}
liegong:addRelatedSkill(liegong_targetmod)
huangzhong:addSkill(liegong)
Fk:loadTranslationTable{
  ["joyex__huangzhong"] = "界黄忠",
  ["#joyex__huangzhong"] = "老当益壮",

  ["joyex__liegong"] = "烈弓",
  [":joyex__liegong"] = "①你使用【杀】无距离限制。②当你使用【杀】指定一个目标后，你可执行：1.若其手牌数不大于你，此【杀】不能被此目标抵消；2.若其体力值不小于你，此【杀】对此目标的伤害值基数+1。",
  ["#joyex__liegong_target"] = "烈弓",

  ["$joyex__liegong1"] = " ",
  ["$joyex__liegong2"] = " ",
  ["~joyex__huangzhong"] = " ",
}


local joyex__weiyan = General(extension, "joyex__weiyan", "shu", 4)
local joyex__kuanggu = fk.CreateTriggerSkill{
  name = "joyex__kuanggu",
  anim_type = "drawcard",
  events = {fk.Damage},
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(self) and target == player
  end,
  on_trigger = function(self, event, target, player, data)
    self.cancel_cost = false
    for i = 1, data.damage do
      self:doCost(event, target, player, data)
      if self.cost_data == "Cancel" or player.dead then break end
    end
  end,
  on_cost = function(self, event, target, player, data)
    local room = player.room
    local choices = {"draw1", "Cancel"}
    if player:isWounded() then
      table.insert(choices, 2, "recover")
    end
    self.cost_data = room:askForChoice(player, choices, self.name)
    return self.cost_data ~= "Cancel"
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if self.cost_data == "recover" then
      room:recover({
        who = player,
        num = 1,
        recoverBy = player,
        skillName = self.name
      })
    elseif self.cost_data == "draw1" then
      player:drawCards(1, self.name)
    end
  end,
}
local joyex__qimou_targetmod = fk.CreateTargetModSkill{
  name = "#joyex__qimou_targetmod",
  residue_func = function(self, player, skill, scope)
    if skill.trueName == "slash_skill" and scope == Player.HistoryPhase then
      return player:getMark("@joyex__qimou-turn") or 0
    end
  end,
}
local joyex__qimou_distance = fk.CreateDistanceSkill{
  name = "#joyex__qimou_distance",
  correct_func = function(self, from, to)
    return -from:getMark("@joyex__qimou-turn")
  end,
}
local joyex__qimou = fk.CreateActiveSkill{
  name = "joyex__qimou",
  anim_type = "offensive",
  card_num = 0,
  target_num = 0,
  frequency = Skill.Limited,
  interaction = function()
    return UI.Spin {
      from = 1,
      to = Self.hp,
    }
  end,
  can_use = function(self, player)
    return player:usedSkillTimes(self.name, Player.HistoryGame) == 0 and player.hp > 0
  end,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    local tolose = self.interaction.data
    room:loseHp(player, tolose, self.name)
    if player.dead then return end
    room:setPlayerMark(player, "@joyex__qimou-turn", tolose)
    player:drawCards(1, self.name)
  end,
}
joyex__qimou:addRelatedSkill(joyex__qimou_targetmod)
joyex__qimou:addRelatedSkill(joyex__qimou_distance)
joyex__weiyan:addSkill(joyex__kuanggu)
joyex__weiyan:addSkill(joyex__qimou)
Fk:loadTranslationTable{
  ["joyex__weiyan"] = "界魏延",
  ["#joyex__weiyan"] = "嗜血的独狼",

  ["joyex__kuanggu"] = "狂骨",
  [":joyex__kuanggu"] = "你对一名角色造成1点伤害后，你可以选择摸一张牌或回复1点体力。",
  ["joyex__qimou"] = "奇谋",
  [":joyex__qimou"] = "限定技，出牌阶段，你可以失去X点体力，摸1张牌，本回合内与其他角色计算距离-X且可以多使用X张杀。",
  ["@joyex__qimou-turn"] = "奇谋",
}

local xiahouyuan = General(extension, "joyex__xiahouyuan", "wei", 4)
local shensu = fk.CreateTriggerSkill{
  name = "joyex__shensu",
  anim_type = "offensive",
  events = {fk.EventPhaseChanging},
  can_trigger = function(self, event, target, player, data)
    if target == player and player:hasSkill(self) and not player:prohibitUse(Fk:cloneCard("thunder__slash")) then
      if (data.to == Player.Judge and not player.skipped_phases[Player.Draw]) or data.to == Player.Discard then
        return true
      elseif data.to == Player.Play then
        return true
      end
    end
  end,
  on_cost = function(self, event, target, player, data)
    local room = player.room
    local slash = Fk:cloneCard("thunder__slash")
    local max_num = slash.skill:getMaxTargetNum(player, slash)
    local targets = {}
    for _, p in ipairs(room:getOtherPlayers(player)) do
      if not player:isProhibited(p, slash) then
        table.insert(targets, p.id)
      end
    end
    if #targets == 0 or max_num == 0 then return end
    if data.to == Player.Judge then
      local tos = room:askForChoosePlayers(player, targets, 1, max_num, "#joyex__shensu1-choose", self.name, true)
      if #tos > 0 then
        self.cost_data = {tos}
        return true
      end
    elseif data.to == Player.Play then
      local tos = room:askForChoosePlayers(player, targets, 1, max_num, "#joyex__shensu2-choose", self.name, true)
      if #tos > 0  then
        self.cost_data = {tos}
        return true
      end
    elseif data.to == Player.Discard then
      local tos = room:askForChoosePlayers(player, targets, 1, max_num, "#joyex__shensu3-choose", self.name, true)
      if #tos > 0 then
        self.cost_data = {tos}
        return true
      end
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if data.to == Player.Judge then
      player:skip(Player.Judge)
      player:skip(Player.Draw)
    elseif data.to == Player.Play then
      player:skip(Player.Play)
    elseif data.to == Player.Discard then
      player:skip(Player.Discard)
      player:turnOver()
    end

    local slash = Fk:cloneCard("thunder__slash")
    slash.skillName = self.name
    room:useCard({
      from = target.id,
      tos = table.map(self.cost_data[1], function(pid) return { pid } end),
      card = slash,
      extraUse = true,
    })
    return true
  end,
}
local shebian = fk.CreateTriggerSkill{
  name = "joyex__shebian",
  events = { fk.TurnedOver },
  anim_type = "control",
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self)
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local to = room:askForChooseToMoveCardInBoard(player, "#joyex__shebian-choose", self.name, true, "e")
    if #to == 2 then
      room:askForMoveCardInBoard(player, room:getPlayerById(to[1]), room:getPlayerById(to[2]), self.name, "e")
      if not player.dead and player:isWounded() then
        room:recover({
          who = player,
          num = 1,
          recoverBy = player,
          skillName = self.name
          })
      end
    end
  end,
}
xiahouyuan:addSkill(shensu)
xiahouyuan:addSkill(shebian)
Fk:loadTranslationTable{
  ["joyex__xiahouyuan"] = "界夏侯渊",
  ["#joyex__xiahouyuan"] = "疾行的猎豹",

  ["joyex__shensu"] = "神速",
  [":joyex__shensu"] = "①判定阶段开始前，你可跳过此阶段和摸牌阶段来视为使用雷【杀】。②出牌阶段开始前，你可跳过此阶段来视为使用雷【杀】。③弃牌阶段开始前，你可跳过此阶段并翻面来视为使用雷【杀】。",
  ["joyex__shebian"] = "设变",
  [":joyex__shebian"] = "当你翻面后，你可将一名角色装备区里的一张牌置入另一名角色的装备区然后回复1点体力。",
  ["#joyex__shensu1-choose"] = "神速：你可以跳过判定阶段和摸牌阶段，视为使用一张无距离限制的雷【杀】",
  ["#joyex__shensu2-choose"] = "神速：你可以跳过出牌阶段，视为使用一张无距离限制的雷【杀】",
  ["#joyex__shensu3-choose"] = "神速：你可以跳过弃牌阶段并翻面，视为使用一张无距离限制的雷【杀】",
  ["#joyex__shebian-choose"] = "设变：你可以移动场上的一张装备牌然后回复1点体力",

  ["$joyex__shensu1"] = " ",
  ["$joyex__shensu2"] = " ",
  ["$joyex__shebian1"] = " ",
  ["$joyex__shebian2"] = " ",
  ["~joyex__xiahouyuan"] = " ",
}

local caoren = General(extension, "joyex__caoren", "wei", 4)
local jushou_select = fk.CreateActiveSkill{
  name = "#joyex__jushou_select",
  can_use = Util.FalseFunc,
  target_num = 0,
  card_num = 1,
  card_filter = function(self, to_select, selected)
    if #selected == 0 and Fk:currentRoom():getCardArea(to_select) ~= Card.PlayerEquip then
      local card = Fk:getCardById(to_select)
        return card.type == Card.TypeEquip and not Self:prohibitUse(card)
    end
  end,
}
local jushou = fk.CreateTriggerSkill{
  name = "joyex__jushou",
  anim_type = "offensive",
  events = {fk.EventPhaseStart},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and player.phase == Player.Finish
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    player:turnOver()
    if player.dead then return false end
    room:drawCards(player, 4, self.name)
    if player.dead then return false end
    local jushou_card
    for _, id in pairs(player:getCardIds(Player.Hand)) do
      local card = Fk:getCardById(id)
      if card.type == Card.TypeEquip and not player:prohibitUse(card) then
        jushou_card = card
        break
      end
    end
    if not jushou_card then return end
    local _, ret = room:askForUseActiveSkill(player, "#joyex__jushou_select", "#joyex__jushou-select", true)
    if ret then
      jushou_card = Fk:getCardById(ret.cards[1])
      if jushou_card then
        if jushou_card.type == Card.TypeEquip then
          room:useCard({
            from = player.id,
            tos = {{player.id}},
            card = jushou_card,
          })
        end
      end
    end
  end,
}
local jiewei = fk.CreateViewAsSkill{
  name = "joyex__jiewei",
  anim_type = "defensive",
  pattern = "nullification",
  card_filter = function(self, to_select, selected)
    return #selected == 0 and Fk:currentRoom():getCardArea(to_select) == Player.Equip
  end,
  view_as = function(self, cards)
    if #cards ~= 1 then return end
    local c = Fk:cloneCard("nullification")
    c.skillName = self.name
    c:addSubcard(cards[1])
    return c
  end,
  enabled_at_play = Util.FalseFunc,
  enabled_at_response = function (self, player)
    return #player.player_cards[Player.Equip] > 0
  end,
}
local jiewei_trigger = fk.CreateTriggerSkill{
  name = "#joyex__jiewei_trigger",
  events = { fk.TurnedOver },
  anim_type = "control",
  mute = true,
  main_skill = jiewei,
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill("joyex__jiewei") and not player:isNude() and player.faceup
  end,
  on_cost = function(self, event, target, player, data)
    local cards = player.room:askForDiscard(player, 1, 1, true, "joyex__jiewei", true, ".", "#joyex__jiewei-discard", true)
    if #cards > 0 then
      self.cost_data = cards
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    player:broadcastSkillInvoke("joyex__jiewei")
    room:notifySkillInvoked(player, "joyex__jiewei", "control")
    room:throwCard(self.cost_data, self.name, player, player)
    if player.dead then return false end
    local to = room:askForChooseToMoveCardInBoard(player, "#joyex__jiewei-choose", "joyex__jiewei", true)
    if #to == 2 then
      room:askForMoveCardInBoard(player, room:getPlayerById(to[1]), room:getPlayerById(to[2]), "joyex__jiewei")
    end
  end,
}
jushou:addRelatedSkill(jushou_select)
jiewei:addRelatedSkill(jiewei_trigger)
caoren:addSkill(jushou)
caoren:addSkill(jiewei)
Fk:loadTranslationTable{
  ["joyex__caoren"] = "界曹仁",
  ["joyex__jushou"] = "据守",
  ["#joyex__jushou_select"] = "据守",
  [":joyex__jushou"] = "结束阶段，你可翻面，你摸四张牌，然后你可以使用一张为装备牌的手牌。",
  ["joyex__jiewei"] = "解围",
  ["#joyex__jiewei_trigger"] = "解围",
  [":joyex__jiewei"] = "①你可将一张装备区里的牌转化为普【无懈可击】使用。②当你翻面后，若你的武将牌正面朝上，你可弃置一张牌，你可将一名角色装备区或判定区里的一张牌置入另一名角色的相同区域。",

  ["#joyex__jushou-select"] = "据守：你可以使用手牌中的一张装备牌",
  ["#joyex__jiewei-discard"] = "解围：弃置一张牌发动，之后可以移动场上的一张牌",
  ["#joyex__jiewei-choose"] = "解围：你可以移动场上的一张装备牌",

  ["$joyex__jushou1"] = " ",
  ["$joyex__jushou2"] = " ",
  ["$joyex__jiewei1"] = " ",
  ["$joyex__jiewei2"] = " ",
  ["~joyex__caoren"] = " ",
}


local xiaoqiao = General(extension, "joyex__xiaoqiao", "wu", 3, 3, General.Female)
local joyex__tianxiang = fk.CreateTriggerSkill{
  name = "joyex__tianxiang",
  anim_type = "defensive",
  events = {fk.DamageInflicted},
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(self) and target == player
  end,
  on_cost = function(self, event, target, player, data)
    local tar, card =  player.room:askForChooseCardAndPlayers(player, table.map(player.room:getOtherPlayers(player), function (p)
      return p.id end), 1, 1, ".|.|heart|hand", "#joyex__tianxiang-choose", self.name, true)
    if #tar > 0 and card then
      self.cost_data = {tar[1], card}
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local to = room:getPlayerById(self.cost_data[1])
    room:throwCard(self.cost_data[2], self.name, player, player)
    local damage = table.simpleClone(data)
    damage.to = to
    room:damage(damage)
    if not to.dead then
      local n = 1
      if not player.dead then
        local choice = room:askForChoice(player, {"1", tostring(math.min(to:getLostHp(), 5))}, self.name, "#joyex__tianxiang-choice::"..to.id)
        n = tonumber(choice)
      end
      to:drawCards(n, self.name)
    end
    return true
  end,
}
local joyex__hongyan = fk.CreateFilterSkill{
  name = "joyex__hongyan",
  frequency = Skill.Compulsory,
  card_filter = function(self, to_select, player)
    return to_select.suit == Card.Spade and player:hasSkill(self)
  end,
  view_as = function(self, to_select)
    return Fk:cloneCard(to_select.name, Card.Heart, to_select.number)
  end,
}
local joyex__hongyan_maxcards = fk.CreateMaxCardsSkill{
  name = "#joyex__hongyan_maxcards",
  frequency = Skill.Compulsory,
  fixed_func = function(self, player)
    if player:hasSkill("joyex__hongyan") and table.find(player:getCardIds("e"), function(id)
      return Fk:getCardById(id).suit == Card.Heart or Fk:getCardById(id).suit == Card.Spade end) then  --FIXME: 耦！
      return player.maxHp
    end
  end
}
joyex__hongyan:addRelatedSkill(joyex__hongyan_maxcards)
xiaoqiao:addSkill(joyex__tianxiang)
xiaoqiao:addSkill(joyex__hongyan)
Fk:loadTranslationTable{
  ["joyex__xiaoqiao"] = "界小乔",
  ["joyex__tianxiang"] = "天香",
  [":joyex__tianxiang"] = "当你受到伤害时，你可以弃置一张<font color='red'>♥</font>手牌，将此伤害转移给一名其他角色，然后你选择一项："..
  "1.其摸一张牌；2.其摸X张牌（X为其已损失体力值且至多为5）。",
  ["joyex__hongyan"] = "红颜",
  [":joyex__hongyan"] = "锁定技，你的♠牌视为<font color='red'>♥</font>牌。若你的装备区有<font color='red'>♥</font>牌，你的手牌上限等于体力上限。",
  ["#joyex__tianxiang-choose" ] = "天香：弃置一张<font color='red'>♥</font>手牌将此伤害转移给一名其他角色，然后令其摸一张牌或X张牌（X为其已损失体力值）",
  ["#joyex__tianxiang-choice"] = "天香：选择令 %dest 摸牌数",
}


local zhoutai = General(extension, "joyex__zhoutai", "wu", 4)
local buqu = fk.CreateTriggerSkill{
  name = "joyex__buqu",
  anim_type = "defensive",
  events = {fk.DamageCaused,fk.PreHpLost},
  frequency = Skill.Compulsory,
  can_trigger = function(self, event, target, player, data)
    return ((target == player and event == fk.PreHpLost) or (data.to == player and event == fk.DamageCaused)) and player:hasSkill(self) and player:getMark("@joyex__chuang") < 4
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    room:addPlayerMark(player,"@joyex__chuang",1)
    return true
  end,
}
local buqu_maxcards = fk.CreateMaxCardsSkill{
  name = "#joyex__buqu_maxcards",
  frequency = Skill.Compulsory,
  correct_func = function(self, player)
    if player:hasSkill("joyex__buqu") then
      return player:getMark("@joyex__chuang")
    end
  end,
}
local fenji = fk.CreateTriggerSkill{
  name = "joyex__fenji",
  anim_type = "support",
  events = {fk.AfterCardsMove},
  can_trigger = function(self, event, target, player, data)
    if player:hasSkill(self) and player.hp >= 1 then
      for _, move in ipairs(data) do
        if (move.moveReason == fk.ReasonDiscard or move.moveReason == fk.ReasonPrey) then
          if move.from and move.proposer and move.from ~= move.proposer then
            for _, info in ipairs(move.moveInfo) do
              if info.fromArea == Card.PlayerHand then
                return true
              end
            end
          end
        end
      end
    end
  end,
  on_trigger = function(self, event, target, player, data)
    local room = player.room
    local targets = {}
    for _, move in ipairs(data) do
      if not table.contains(targets, move.from) and (move.moveReason == fk.ReasonDiscard or move.moveReason == fk.ReasonPrey) then
        if move.from and move.proposer and move.from ~= move.proposer then
          for _, info in ipairs(move.moveInfo) do
            if info.fromArea == Card.PlayerHand then
              table.insert(targets, move.from)
              break
            end
          end
        end
      end
    end
    room:sortPlayersByAction(targets)
    for _, target_id in ipairs(targets) do
      if not player:hasSkill(self) or player.hp < 1 then break end
      local skill_target = room:getPlayerById(target_id)
      if skill_target and not skill_target.dead then
        self:doCost(event, skill_target, player, data)
      end
    end
  end,
  on_cost = function(self, event, target, player, data)
    return player.room:askForSkillInvoke(player, self.name, nil, "#joyex__fenji-invoke::"..target.id)
  end,
  on_use = function(self, event, target, player, data)
    player.room:doIndicate(player.id, {target.id})
    player.room:loseHp(player, 1, self.name)
    if not target.dead then
      target:drawCards(2, self.name)
    end
  end,
}
local qingchuang = fk.CreateActiveSkill{
  name = "joyex__qingchuang",
  prompt = "#joyex__qingchuang",
  can_use = function (self,player)
    return player:usedSkillTimes(self.name, Player.HistoryPhase) < 1 and player:getMark("@joyex__chuang") > 0
  end,
  target_num = 0,
  card_num = 1,
  card_filter = function(self, to_select, selected)
    if #selected == 0  then
      local card = Fk:getCardById(to_select)
        return card.name == "peach" or card.name == "analeptic"
    end
  end,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    local cards = effect.cards
    room:throwCard(cards,self.name,player,player)
    if not player.dead then
      room:removePlayerMark(player,"@joyex__chuang",1)
      player:drawCards(1,self.name)
    end
  end,
}
buqu:addRelatedSkill(buqu_maxcards)
zhoutai:addSkill(buqu)
zhoutai:addSkill(fenji)
zhoutai:addSkill(qingchuang)
Fk:loadTranslationTable{
  ["joyex__zhoutai"] = "界周泰",
  ["joyex__buqu"] = "不屈",
  [":joyex__buqu"] = "锁定技，①当你受到伤害或失去体力时，若你的“创”小于4，你获得1枚“创”并防止此伤害或体力流失。②你的手牌上限增加“创”数。",
  ["joyex__fenji"] = "奋激",
  [":joyex__fenji"] = "当一名角色因另一名角色的弃置或获得而失去手牌后，你可失去1点体力，令失去手牌的角色摸两张牌。",
  ["joyex__qingchuang"] = "清创",
  [":joyex__qingchuang"] = "出牌阶段限一次，你可以弃置一张【桃】或者【酒】并移去身上的1枚“创”，然后摸一张牌。",

  ["@joyex__chuang"] = "创",
  ["#joyex__fenji-invoke"] = "奋激：你可以失去1点体力，令 %dest 摸两张牌",
  ["#joyex__qingchuang"] = "清创：出牌阶段限一次，你可以弃置一张【桃】或【酒】来移除一枚“创”并摸一张牌。",

  ["$joyex__buqu1"] = " ",
  ["$joyex__buqu2"] = " ",
  ["$joyex__fenji1"] = " ",
  ["$joyex__fenji2"] = " ",
  ["~joyex__zhoutai"] = " ",
}


local zhangjiao = General(extension, "joyex__zhangjiao", "qun", 3)
local leiji = fk.CreateTriggerSkill{
  name = "joyex__leiji",
  anim_type = "offensive",
  events = {fk.CardUsing, fk.CardResponding},
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(self) and target == player and (data.card.name == "jink" or data.card.trueName == "lightning")
  end,
  on_cost = function(self, event, target, player, data)
    local to = player.room:askForChoosePlayers(player, table.map(player.room:getOtherPlayers(player), function (p)
      return p.id end), 1, 1, "#joyex__leiji-choose", self.name, true)
    if #to > 0 then
      self.cost_data = to[1]
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local tar = room:getPlayerById(self.cost_data)
    local judge = {
      who = tar,
      reason = self.name,
      pattern = ".|.|spade,club",
    }
    room:judge(judge)
    if judge.card.suit == Card.Spade then
      room:damage{
        from = player,
        to = tar,
        damage = 2,
        damageType = fk.ThunderDamage,
        skillName = self.name,
      }
    elseif judge.card.suit == Card.Club then
        if player:isWounded() then
          room:recover({
            who = player,
            num = 1,
            recoverBy = player,
            skillName = self.name,
          })
        end
        if player.dead then return false end
        room:damage{
          from = player,
          to = tar,
          damage = 1,
          damageType = fk.ThunderDamage,
          skillName = self.name,
        }
    end
end,
}
local leiji_maxcards = fk.CreateMaxCardsSkill{
  name = "#joyex__leiji_maxcards",
  frequency = Skill.Compulsory,
  correct_func = function(self, player)
     if player:hasSkill("joyex__leiji") then
      return 2
    end
  end,
}
local guidao = fk.CreateTriggerSkill{
  name = "joyex__guidao",
  anim_type = "control",
  events = {fk.AskForRetrial},
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(self) and not player:isNude()
  end,
  on_cost = function(self, event, target, player, data)
    local card = player.room:askForResponse(player, self.name, ".|.|spade,club|hand,equip", "#joyex__guidao-ask::" .. target.id..":"..data.reason, true)
    if card ~= nil then
      self.cost_data = card
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    player.room:retrial(self.cost_data, player, data, self.name, true)
    if not player.dead and self.cost_data.suit == Card.Spade and self.cost_data.number > 1 and self.cost_data.number < 10 then
      player:drawCards(1, self.name)
    end
  end,
}

local huangtian = fk.CreateTriggerSkill{
  name = "joyex__huangtian$",
  mute = true,

  refresh_events = {fk.EventAcquireSkill, fk.EventLoseSkill, fk.BuryVictim, fk.AfterPropertyChange},
  can_refresh = function(self, event, target, player, data)
    if event == fk.EventAcquireSkill or event == fk.EventLoseSkill then
      return data == self
    elseif event == fk.BuryVictim then
      return target:hasSkill(self, true, true)
    elseif event == fk.AfterPropertyChange then
      return target == player
    end
  end,
  on_refresh = function(self, event, target, player, data)
    local room = player.room
    local attached_huangtian = player.kingdom == "qun" and table.find(room.alive_players, function (p)
      return p ~= player and p:hasSkill(self, true)
    end)
    if attached_huangtian and not player:hasSkill("joyex__huangtian_other&", true, true) then
      room:handleAddLoseSkills(player, "joyex__huangtian_other&", nil, false, true)
    elseif not attached_huangtian and player:hasSkill("joyex__huangtian_other&", true, true) then
      room:handleAddLoseSkills(player, "-joyex__huangtian_other&", nil, false, true)
    end
  end,
}
local huangtian_other = fk.CreateActiveSkill{
  name = "joyex__huangtian_other&",
  anim_type = "support",
  prompt = "#joyex__huangtian-active",
  mute = true,
  can_use = function(self, player)
    if player.kingdom ~= "qun" then return false end
    local targetRecorded = player:getTableMark( "joyex__huangtian_sources-phase")
    return table.find(Fk:currentRoom().alive_players, function(p)
      return p ~= player and p:hasSkill("joyex__huangtian") and not table.contains(targetRecorded, p.id)
    end)
  end,
  card_num = 1,
  card_filter = function(self, to_select, selected)
    if #selected < 1 and Fk:currentRoom():getCardArea(to_select) ~= Card.PlayerEquip then
      local card = Fk:getCardById(to_select)
      return card.trueName == "jink" or card.suit == Card.Spade or card.trueName == "lightning"
    end
  end,
  target_num = 1,
  target_filter = function(self, to_select, selected)
    if #selected == 0 and to_select ~= Self.id and Fk:currentRoom():getPlayerById(to_select):hasSkill("joyex__huangtian") then
      local targetRecorded = Self:getMark("joyex__huangtian_sources-phase")
      return type(targetRecorded) ~= "table" or not table.contains(targetRecorded, to_select)
    end
  end,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    local target = room:getPlayerById(effect.tos[1])
    room:notifySkillInvoked(player, "joyex__huangtian")
    target:broadcastSkillInvoke("joyex__huangtian")
    local targetRecorded = player:getTableMark( "joyex__huangtian_sources-phase")
    table.insertIfNeed(targetRecorded, target.id)
    room:setPlayerMark(player, "joyex__huangtian_sources-phase", targetRecorded)
    room:moveCardTo(effect.cards, Player.Hand, target, fk.ReasonGive, self.name, nil, true)
  end,
}

local huangtian_trigger = fk.CreateTriggerSkill{
  name = "#joyex__huangtian_trigger&",
  anim_type = "drawcard",
  events = {fk.CardUsing, fk.CardResponding},
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(self) and target.kingdom == "qun" and target ~= player and not data.card:isVirtual() and data.card.name == "jink" 
    and player:usedSkillTimes(self.name, Player.HistoryTurn) < 1 and player.room:getCardArea(data.card) == Card.Processing
  end,
  on_cost = function(self, event, target, player, data)
    if player.room:askForSkillInvoke(player,self.name,nil,"#joyex__huangtian-get::"..target.id..":"..data.card:toLogString()) then
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    if not player.dead then
      player.room:obtainCard(player.id, data.card, true, fk.ReasonPrey)
    end
  end,
}
leiji:addRelatedSkill(leiji_maxcards)
huangtian:addRelatedSkill(huangtian_trigger)
Fk:addSkill(huangtian_other)
zhangjiao:addSkill(leiji)
zhangjiao:addSkill(guidao)
zhangjiao:addSkill(huangtian)

Fk:loadTranslationTable{
  ["joyex__zhangjiao"] = "界张角",
  ["joyex__leiji"] = "雷击",
  [":joyex__leiji"] = "<br>①当你使用或打出【闪】或【闪电】时，你可令一名其他角色判定，若结果为：黑桃，你对其造成2点雷电伤害；梅花，你回复1点体力，然后对其造成1点雷电伤害。<br>②你的手牌上限+2。",
  ["#joyex__leiji_maxcards"] = "雷击",
  ["joyex__guidao"] = "鬼道",
  [":joyex__guidao"] = "当一名角色的判定结果确定前，你可打出一张黑色牌代替之，你获得原判定牌，若你打出的牌是黑桃2~9，你摸一张牌。",
  ["joyex__huangtian"] = "黄天",
  [":joyex__huangtian"] = "主公技，<br>①其他群势力角色的出牌阶段限一次，该角色可以将一张【闪】,【闪电】或黑桃手牌交给你。<br>②每回合限一次，当其他群雄势力角色使用或打出【闪】时，你可以获得之。",
  ["#joyex__huangtian_trigger&"] = "黄天",
  ["joyex__huangtian_other&"] = "黄天",
  [":joyex__huangtian_other&"] = "出牌阶段限一次，你可将一张【闪】,【闪电】或黑桃手牌（正面朝上移动）交给张角。",

  ["#joyex__leiji-choose"] = "雷击：你可以令一名其他角色进行判定。",
  ["#joyex__guidao-ask"] = "鬼道：可以打出一张黑色牌替换 %dest 的“%arg”判定，若打出♠2~9，你摸一张牌",
  ["#joyex__huangtian-active"] = "发动黄天，选择一张【闪】,【闪电】或黑桃手牌交给一名拥有“黄天”的角色",
  ["#joyex__huangtian-get"] = "黄天：你可以获得 %dest 打出的 %arg 。",

  ["$joyex__leiji1"] = " ",
  ["$joyex__leiji2"] = " ",
  ["$joyex__guidao1"] = " ",
  ["$joyex__guidao2"] = " ",
  ["$joyex__huangtian1"] = " ",
  ["$joyex__huangtian2"] = " ",
  ["~joyex__zhangjiao"] = " ",
}


local yuji = General(extension, "joy__yuji", "qun", 4)
local guhuo = fk.CreateTriggerSkill{
  name = "joy__guhuo",
  anim_type = "drawcard",
  derived_piles = "joy__guhuo",
  events = {fk.CardUseFinished,fk.TurnEnd},
  can_trigger = function(self, event, target, player, data)
    if event == fk.CardUseFinished then
      return target == player and player:hasSkill(self) and ((data.damageDealt and player:getMark("joy__guhuo-turn") == 0) or not data.damageDealt) and data.card and (data.card.trueName == "slash" or data.card.is_damage_card)
    elseif event == fk.TurnEnd then
      return target == player and #player:getPile("joy__guhuo") > 0
    end
  end,
  on_cost = Util.TrueFunc,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if event == fk.TurnEnd and not player.dead then
      room:moveCardTo(player:getPile("joy__guhuo"), Card.PlayerHand, player, fk.ReasonPrey, self.name)
    elseif event == fk.CardUseFinished then
      if data.damageDealt and not player.dead then
        room:setPlayerMark(player,"joy__guhuo-turn",1)
        player:drawCards(1,self.name)
      elseif not data.damageDealt and not player.dead then
        if player.room:getCardArea(data.card) == Card.Processing then
          player:addToPile("joy__guhuo", data.card, true, self.name)
          if not player.dead then
            player:drawCards(1,self.name)
          end
        end
      end
    end
  end,
}
yuji:addSkill(guhuo)
Fk:loadTranslationTable{
  ["joy__yuji"] = "于吉",
  ["joy__guhuo"] = "蛊惑",
  [":joy__guhuo"] = "你使用的【杀】或伤害锦囊牌结算后:<br>①若造成过伤害，则你摸一张牌，每回合仅能以此法摸一张。<br>②若没有造成伤害，则将此牌移出游戏,然后你摸一张牌，回合结束时，你获得所有以此法移出游戏的牌。",
  
  ["$joy__guhuo1"] = " ",
  ["$joy__guhuo2"] = " ",
  ["~joy__yuji"] = " ",
}


-- 火

local wolong = General(extension, "joy__wolong", "shu", 3)
local bazhen = fk.CreateTriggerSkill{
  name = "joy__bazhen",
  events = {fk.AskForCardUse, fk.AskForCardResponse, fk.FinishJudge},
  frequency = Skill.Compulsory,
  mute = true,
  can_trigger = function(self, event, target, player, data)
    if target == player and player:hasSkill(self) then
      if event ~= fk.FinishJudge then
        return not player:isFakeSkill(self) and not player:getEquipment(Card.SubtypeArmor) and player:getMark(fk.MarkArmorNullified) == 0
        and (data.cardName == "jink" or (data.pattern and Exppattern:Parse(data.pattern):matchExp("jink|0|nosuit|none")))
      else
        return data.reason == "eight_diagram" and data.card.color ~= Card.Red
      end
    end
  end,
  on_cost = function(self, event, target, player, data)
    return event == fk.FinishJudge or player.room:askForSkillInvoke(player, self.name)
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    player:broadcastSkillInvoke(self.name)
    if event == fk.FinishJudge then
      room:notifySkillInvoked(player, self.name, "drawcard")
      player:drawCards(1, self.name)
    else
      room:notifySkillInvoked(player, self.name, "defensive")
      local judgeData = {
        who = player,
        reason = "eight_diagram",
        pattern = ".|.|heart,diamond",
      }
      room:judge(judgeData)
      if judgeData.card.color == Card.Red then
        local card = Fk:cloneCard("jink")
        card.skillName = "eight_diagram"
        card.skillName = "bazhen"
        if event == fk.AskForCardUse then
          data.result = { from = player.id, card = card }
          if data.eventData then
            data.result.toCard = data.eventData.toCard
            data.result.responseToEvent = data.eventData.responseToEvent
          end
        else
          data.result = card
        end
        return true
      end
    end
  end
}
wolong:addSkill(bazhen)
local joy__huoji__fireAttackSkill = fk.CreateActiveSkill{
  name = "joy__huoji__fire_attack_skill",
  prompt = "#fire_attack_skill",
  target_num = 1,
  mod_target_filter = function(_, to_select, _, _, _, _)
    return not Fk:currentRoom():getPlayerById(to_select):isKongcheng()
  end,
  target_filter = function(self, to_select, selected, _, card)
    if #selected < self:getMaxTargetNum(Self, card) then
      return self:modTargetFilter(to_select, selected, Self.id, card)
    end
  end,
  on_effect = function(self, room, cardEffectEvent)
    local from = room:getPlayerById(cardEffectEvent.from)
    local to = room:getPlayerById(cardEffectEvent.to)
    if to:isKongcheng() then return end
    local showCard = room:askForCard(to, 1, 1, false, self.name, false, ".|.|.|hand", "#fire_attack-show:" .. from.id)[1]
    to:showCards(showCard)
    local suit = Fk:getCardById(showCard):getSuitString()
    local top = room:getNCards(4)
    for i = 4, 1, -1 do
      table.insert(room.draw_pile, 1, top[i])
    end
    local can_throw = table.filter(top, function(id) return Fk:getCardById(id):getSuitString() == suit end)
    local ids , _ = U.askforChooseCardsAndChoice(from, can_throw, {"OK"}, self.name, "#joy__huoji-card", {"Cancel"}, 1, 1, top)
    if #ids == 0 and not from:isKongcheng() then
      ids = room:askForDiscard(from, 1, 1, false, self.name, true,
      ".|.|" .. suit, "#fire_attack-discard:" .. to.id .. "::" .. suit, true)
    end
    if #ids > 0 then
      room:throwCard(ids, self.name, from, from)
      room:damage({
        from = from,
        to = to,
        card = cardEffectEvent.card,
        damage = 1,
        damageType = fk.FireDamage,
        skillName = self.name
      })
    end
  end,
}
joy__huoji__fireAttackSkill.cardSkill = true
Fk:addSkill(joy__huoji__fireAttackSkill)
local huoji = fk.CreateViewAsSkill{
  name = "joy__huoji",
  anim_type = "offensive",
  pattern = "fire_attack",
  prompt = "#huoji",
  card_filter = function(self, to_select, selected)
    return #selected == 0 and Fk:getCardById(to_select).color == Card.Red and Fk:currentRoom():getCardArea(to_select) ~= Player.Equip
  end,
  view_as = function(self, cards)
    if #cards ~= 1 then return end
    local card = Fk:cloneCard("fire_attack")
    card.skillName = self.name
    card:addSubcard(cards[1])
    return card
  end,
}
local huoji_trigger = fk.CreateTriggerSkill{
  name = "#joy__huoji_trigger",
  events = {fk.PreCardEffect},
  mute = true,
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(huoji) and data.from == player.id and data.card.trueName == "fire_attack"
  end,
  on_cost = Util.TrueFunc,
  on_use = function(self, event, target, player, data)
    local card = data.card:clone()
    local c = table.simpleClone(data.card)
    for k, v in pairs(c) do
      card[k] = v
    end
    card.skill = joy__huoji__fireAttackSkill
    data.card = card
  end,
}
huoji:addRelatedSkill(huoji_trigger)
wolong:addSkill(huoji)
local kanpo = fk.CreateViewAsSkill{
  name = "joy__kanpo",
  anim_type = "control",
  pattern = "nullification",
  prompt = "#kanpo",
  card_filter = function(self, to_select, selected)
    return #selected == 0 and Fk:getCardById(to_select).color == Card.Black and table.contains(Self.player_cards[Player.Hand], to_select)
  end,
  view_as = function(self, cards)
    if #cards ~= 1 then return end
    local card = Fk:cloneCard("nullification")
    card.skillName = self.name
    card:addSubcard(cards[1])
    return card
  end,
  enabled_at_response = function (self, player, response)
    return not response and not player:isKongcheng()
  end,
}
local joy__kanpo_trigger = fk.CreateTriggerSkill{
  name = "#joy__kanpo_trigger",
  refresh_events = {fk.PreCardUse},
  can_refresh = function(self, event, target, player, data)
    return target == player and player:hasSkill(kanpo) and data.card.trueName == "nullification"
  end,
  on_refresh = function(self, event, target, player, data)
    data.disresponsiveList = table.map(player.room.alive_players, Util.IdMapper)
  end,
}
kanpo:addRelatedSkill(joy__kanpo_trigger)
wolong:addSkill(kanpo)
Fk:loadTranslationTable{
  ["joy__wolong"] = "卧龙诸葛亮",
  ["#joy__wolong"] = "卧龙",
  ["joy__bazhen"] = "八阵",
  [":joy__bazhen"] = "锁定技，若你没有装备防具，视为你装备着【八卦阵】。当你因【八卦阵】而判定的非红色判定牌生效后，你摸一张牌。",
  ["joy__huoji"] = "火计",
  [":joy__huoji"] = "你可以将一张红色手牌当【火攻】使用。你执行【火攻】的效果时可观看牌堆顶四张牌，选择一张牌代替你需要弃置的牌。",
  ["joy__kanpo"] = "看破",
  [":joy__kanpo"] = "你可以将一张黑色手牌当【无懈可击】使用。你使用的【无懈可击】不可响应。",
  ["#joy__huoji-card"] = "火计：选择一张弃置",
  ["joy__huoji__fire_attack_skill"] = "火攻",
  ["#joy__huoji_trigger"] = "火计",
  ["#joy__kanpo_trigger"] = "看破",
}

local pangtong = General:new(extension, "joyex__pangtong", "shu", 3)
local lianhuan = fk.CreateTriggerSkill{
  name = "joyex__lianhuan",
  anim_type = "control",
  events = {fk.EventPhaseStart,fk.TargetSpecified},
  can_trigger = function(self, event, target, player, data)
    if event == fk.EventPhaseStart then
      return player:hasSkill(self) and target == player and player.phase == Player.Play and not player:prohibitUse(Fk:cloneCard("iron_chain"))
    else
      local room = player.room
      local to = room:getPlayerById(data.to)
      if player:hasSkill(self) and target == player and data.card.name == "iron_chain" and (to == player or (to ~= player and not (to.dead or to.chained or to:isKongcheng()))) then
        return true
      end
    end
  end,
  on_cost = function(self, event, target, player, data)
    local room = player.room
    if event == fk.EventPhaseStart then
      return room:askForSkillInvoke(player,self.name,nil,"#joyex__lianhuan")
    else
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if event == fk.EventPhaseStart then
      U.askForUseVirtualCard(room, player, "iron_chain", nil, self.name)
    else
      local to = room:getPlayerById(data.to)
      if to == player and not player.dead then
        player:drawCards(1,self.name)
      else
        if to:isKongcheng() then return false end
        local throw = table.random(to:getCardIds("h"), 1)
        room:throwCard(throw, self.name, to, player)
      end
    end
  end,
}
local lianhuan_targetmod = fk.CreateTargetModSkill{
  name = "#joyex__lianhuan_targetmod",
  extra_target_func = function(self, player, skill, card)
    if card and card.name == "iron_chain"   then
      return 999
    end
  end,
}

local niepan = fk.CreateTriggerSkill{
  name = "joyex__niepan",
  anim_type = "defensive",
  frequency = Skill.Limited,
  events = {fk.AskForPeaches},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and player.dying and player:usedSkillTimes(self.name, Player.HistoryGame) == 0
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if player.general == "joyex__pangtong" then 
      player.general = "joy_skin__pangtong"
      room:broadcastProperty(player, "general")
      room:notifySkillInvoked(player, "joyex__niepan","drawcard")
    elseif player.deputyGeneral == "joyex__pangtong" then
      player.deputyGeneral = "joy_skin__pangtong"
      room:broadcastProperty(player, "deputyGeneral")
      room:notifySkillInvoked(player, "joyex__niepan","drawcard")
    end
    player:throwAllCards("hej")
    if player.dead then return false end
    player:reset()
    if player.dead then return false end
    player:drawCards(3, self.name)
    if player.dead then return false end
    if player:isWounded() then
      room:recover({
        who = player,
        num = math.min(3, player.maxHp) - player.hp,
        recoverBy = player,
        skillName = self.name,
      })
      if player.dead then return false end
    end
    local choices = {}
    if not player:hasSkill("joy__huoji",true) then
      table.insert(choices,"joy__huoji")
    end
    if not player:hasSkill("joy__bazhen") and not player:hasSkill("joy__kanpo") then
      table.insert(choices,"joy__bazhen|joy__kanpo")
    end
    if #choices == 0 then return false end
    local choice = player.room:askForChoice(player, choices, self.name, "#joyex__niepan-choice", true,{"joy__huoji","joy__bazhen|joy__kanpo"})
    room:handleAddLoseSkills(player, choice, nil)
  end,
}

lianhuan:addRelatedSkill(lianhuan_targetmod)
pangtong:addSkill(lianhuan)
pangtong:addSkill(niepan)
pangtong:addRelatedSkill("joy__huoji")
pangtong:addRelatedSkill("joy__bazhen")
pangtong:addRelatedSkill("joy__kanpo")

Fk:loadTranslationTable{
  ["joyex__pangtong"] = "界庞统",

  ["joyex__lianhuan"] = "连环",
  [":joyex__lianhuan"] = "<br>①出牌阶段开始时，你可以视为使用一张【铁索连环】；<br>②当你使用【铁索连环】时，你可以多指定任意名目标;<br>③你使用【铁索连环】指定一名角色为目标后，若其是你，你摸一张牌；若其不是你且未横置，你随机弃置其一张手牌。",
  ["#joyex__lianhuan"] = "连环：你可以视为使用一张【铁索连环】。",
  ["#joyex__lianhuan_targetmod"] = "连环",
  ["#joyex__niepan-choice"] = "涅槃：选择要获得的技能。",

  ["joyex__niepan"] = "涅槃",
  [":joyex__niepan"] = "限定技，当你处于濒死状态时，你可以弃置区域里的所有牌，复原你的武将牌，然后摸三张牌并将体力回复至3点，然后选择获得技能:1.〖火计〗；2.〖八阵〗和〖看破〗",
  ["joy__bazhen|joy__kanpo"] = "八阵&看破",
  [":joy__bazhen|joy__kanpo"] = "〖八阵〗:锁定技，若你没有装备防具，视为你装备着【八卦阵】。当你因【八卦阵】而判定的非红色判定牌生效后，你摸一张牌。"..
  "<br>〖看破〗:你可以将一张黑色手牌当【无懈可击】使用。你使用的【无懈可击】不可响应。",
}

local dianwei = General(extension, "joyex__dianwei", "wei", 4)
local joyex__qiangxi = fk.CreateActiveSkill{
  name = "joyex__qiangxi",
  anim_type = "offensive",
  card_num = 0,
  target_num = 1,
  can_use = function(self, player)
    return player.hp > 0
  end,
  card_filter = Util.FalseFunc,
  target_filter = function(self, to_select, selected)
    if #selected == 0 and Self:inMyAttackRange(Fk:currentRoom():getPlayerById(to_select)) then
      return not table.contains(Self:getTableMark(  "joyex__qiangxi_targets-phase"), to_select)
    end
  end,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    local target = room:getPlayerById(effect.tos[1])
    local mark = player:getTableMark(  "joyex__qiangxi_targets-phase")
    table.insertIfNeed(mark, target.id)
    room:setPlayerMark(player, "joyex__qiangxi_targets-phase", mark)
    room:loseHp(player, 1, self.name)
    if not player.dead then
      player:drawCards(1, self.name)
    end
    if not target.dead then
      room:damage{
        from = player,
        to = target,
        damage = 1,
        skillName = self.name,
      }
    end
  end,
}
local joyex__qiangxi_trigger = fk.CreateTriggerSkill{
  name = "#joyex__qiangxi_trigger",
  mute = true,
  main_skill = joyex__qiangxi,
  events = {fk.DamageInflicted},
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(joyex__qiangxi) and target ~= player and not player:isNude()
  end,
  on_cost = function (self, event, target, player, data)
    local cards = player.room:askForDiscard(player, 1, 1, true, "joyex__qiangxi", true, ".|.|.|.|.|equip", "#joyex__qiangxi-cost:"..target.id, true)
    if #cards > 0 then
      self.cost_data = cards
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    room:doIndicate(player.id, { target.id })
    room:throwCard(self.cost_data, "joyex__qiangxi", player, player)
    data.damage = data.damage + 1
  end,
}
joyex__qiangxi:addRelatedSkill(joyex__qiangxi_trigger)
dianwei:addSkill(joyex__qiangxi)

Fk:loadTranslationTable{
  ["joyex__dianwei"] = "界典韦",
  ["#joyex__dianwei"] = "古之恶来",
  ["joyex__qiangxi"] = "强袭",
  [":joyex__qiangxi"] = "出牌阶段每名角色限一次，你可以失去一点体力，并摸一张牌，然后对攻击范围内一名角色造成1点伤害；每当其他角色受到伤害时，你可以弃置一张装备牌，令此伤害+1。",
  ["#joyex__qiangxi_trigger"] = "强袭",
  ["#joyex__qiangxi-cost"] = "强袭：你可以弃置一张装备牌，令 %src 受到的伤害+1",
}

-- 林

local laohuoshou = fk.CreateTriggerSkill{
  name = "joy__huoshou",
  anim_type = "defensive",
  frequency = Skill.Compulsory,
  events = {fk.PreCardEffect, fk.TargetSpecified},
  can_trigger = function(self, event, target, player, data)
    if player:hasSkill(self) and data.card.trueName == "savage_assault" then
      if event == fk.PreCardEffect then
        return player.id == data.to
      else
        return target ~= player and data.firstTarget
      end
    end
  end,
  on_use = function(self, event, target, player, data)
    if event == fk.PreCardEffect then
      return true
    else
      data.extra_data = data.extra_data or {}
      data.extra_data.huoshou = player.id
      if not player.dead then
        player:drawCards(1,self.name)
      end
    end
  end,

  refresh_events = {fk.PreDamage},
  can_refresh = function(self, event, target, player, data)
    if data.card and data.card.trueName == "savage_assault" then
      local e = player.room.logic:getCurrentEvent():findParent(GameEvent.UseCard)
      if e then
        local use = e.data[1]
        return use.extra_data and use.extra_data.huoshou
      end
    end
  end,
  on_refresh = function(self, event, target, player, data)
    local room = player.room
    local e = room.logic:getCurrentEvent():findParent(GameEvent.UseCard)
    if e then
      local use = e.data[1]
      data.from = room:getPlayerById(use.extra_data.huoshou)
    end
  end,
}
Fk:addSkill(laohuoshou)
local menghuo = General(extension, "joyex__menghuo", "shu", 4)
local huoshou = fk.CreateTriggerSkill{
  name = "joyex__huoshou",
  anim_type = "defensive",
  frequency = Skill.Compulsory,
  events = {fk.PreCardEffect, fk.TargetSpecified,fk.EventPhaseStart,fk.CardUsing},
  can_trigger = function(self, event, target, player, data)
    if event == fk.EventPhaseStart then
      if target == player and player.phase == Player.Play then
        return true
      else
        return false
      end
    end
    if player:hasSkill(self) and  data.card and data.card.trueName == "savage_assault" then
      if event == fk.PreCardEffect then
        return player.id == data.to
      elseif event ==  fk.TargetSpecified then
        return target ~= player and data.firstTarget
      elseif event == fk.CardUsing then
        return target == player and player.phase == Player.Play
      end
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if event == fk.PreCardEffect then
      return true
    elseif event == fk.EventPhaseStart then
      local cards = room:getCardsFromPileByRule("savage_assault",1,"DiscardPile")
    if #cards > 0 then
      room:obtainCard(player,cards,true,fk.ReasonPrey,player.id,self.name)
    end
    elseif event == fk.CardUsing then 
      room:setPlayerMark(player,"@@joyex__huoshou-phase",1)
    else
      data.extra_data = data.extra_data or {}
      data.extra_data.huoshou = player.id
      if not player.dead then
        player:drawCards(1,self.name)
      end
    end
  end,

  refresh_events = {fk.PreDamage,fk.EventAcquireSkill},
  can_refresh = function(self, event, target, player, data)
    if event == fk.PreDamage then
      if data.card and data.card.trueName == "savage_assault" then
        local e = player.room.logic:getCurrentEvent():findParent(GameEvent.UseCard)
        if e then
          local use = e.data[1]
          return use.extra_data and use.extra_data.huoshou
        end
      end
    elseif event == fk.EventAcquireSkill and target == player and player:hasSkill(self,true) and player.phase == Player.Play then
      return #player.room.logic:getEventsOfScope(GameEvent.UseCard, 1, function (e)
        local use = e.data[1]
        return use.from == player.id and use.card.trueName == "savage_assault"
      end, Player.HistoryPhase) > 0
    end
  end,
  on_refresh = function(self, event, target, player, data)
    if event == fk.PreDamage then
      local room = player.room
      local e = room.logic:getCurrentEvent():findParent(GameEvent.UseCard)
      if e then
        local use = e.data[1]
        data.from = room:getPlayerById(use.extra_data.huoshou)
      end
    elseif event == fk.EventAcquireSkill then
      player.room:setPlayerMark(player,"@@joyex__huoshou-phase",1)
    end
  end,
}
local huoshou_prohibit = fk.CreateProhibitSkill {
  name = "#joyex__huoshou_prohibit",
  frequency = Skill.Compulsory,
  prohibit_use = function(self, player, card)
    return player:hasSkill(huoshou.name) and player:getMark("@@joyex__huoshou-phase") > 0 and  card.name == "savage_assault"
  end,
}
local zaiqi = fk.CreateTriggerSkill{
  name = "joyex__zaiqi",
  anim_type = "support",
  events = {fk.EventPhaseStart},
  can_trigger = function(self, event, target, player, data)
    if target == player and player:hasSkill(self) then
      if player.phase == Player.Draw and player:isWounded() then
        return true
      elseif player.phase == Player.Finish  then
        local room = player.room
        local turn_event = player.room.logic:getCurrentEvent():findParent(GameEvent.Turn, true)
        if turn_event == nil then return false end
        local n = 0
        player.room.logic:getActualDamageEvents( 1, function(e)
            local damage = e.data[1]
            if damage.from == player then
                n = n + damage.damage
            end
        end, nil, turn_event.id)
        if n > 0 then
            self.cost_data = n
            return true
        end
      end
    end 
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local n = player.phase == Player.Draw and player:getLostHp() or self.cost_data
    local cards = room:getNCards(n)
    room:moveCards{
      ids = cards,
      toArea = Card.Processing,
      moveReason = fk.ReasonJustMove,
      skillName = self.name,
      proposer = player.id,
    }
    room:delay(2000)
    local hearts, to_get = {}, {}
    for _, id in ipairs(cards) do
      if Fk:getCardById(id).suit == Card.Heart then
        table.insert(hearts, id)
      else
        table.insert(to_get, id)
      end
    end
    if #hearts > 0 then
      if player:isWounded() then
        room:recover({
          who = player,
          num = math.min(#hearts, player:getLostHp()),
          recoverBy = player,
          skillName = self.name,
        })
      end
      room:moveCards{
        ids = hearts,
        toArea = Card.DiscardPile,
        moveReason = fk.ReasonPutIntoDiscardPile,
        skillName = self.name,
      }
    end
    if #to_get > 0 and not player.dead then
      room:obtainCard(player.id, to_get, true, fk.ReasonJustMove)
    end
    cards = table.filter(cards, function (id)
      return room:getCardArea(id) == Card.Processing
    end)
    if #cards > 0 then
      room:moveCardTo(cards, Card.DiscardPile, nil, fk.ReasonJustMove, self.name)
    end
    if player.phase == Player.Draw then
      return true
    end
  end,
}
huoshou:addRelatedSkill(huoshou_prohibit)
menghuo:addSkill(huoshou)
menghuo:addSkill(zaiqi)
Fk:loadTranslationTable{
  ["joyex__menghuo"] = "孟获",
  ["#joyex__menghuo"] = "南蛮王",

  ["joy__huoshou"] = "祸首",
  [":joy__huoshou"] = "锁定技，【南蛮入侵】对你无效；当其他角色使用【南蛮入侵】指定目标后，你代替其成为此牌造成的伤害的来源，并摸一张牌。",
  ["joyex__huoshou"] = "祸首",
  ["#joyex__huoshou_prohibit"] = "祸首",
  ["@@joyex__huoshou-phase"] = "禁用 南蛮",
  [":joyex__huoshou"] = "锁定技，【南蛮入侵】对你无效；当其他角色使用【南蛮入侵】指定目标后，你代替其成为此牌造成的伤害的来源，并摸一张牌；出牌阶段开始时，你从弃牌堆中随机获得一张【南蛮入侵】；若你出牌阶段使用过南蛮入侵，你此阶段无法再次使用【南蛮入侵】。",
  ["joyex__zaiqi"] = "再起",
  [":joyex__zaiqi"] = "摸牌阶段/结束阶段 ，若你 已受伤/本回合造成过伤害 ，你可亮出牌堆顶的X张牌（X为你 已损失体力值/本回合造成的伤害量 ）,回复等同其中<font color='red'>♥</font>牌张数的体力，并获得其余非<font color='red'>♥</font>的牌；然后若此时是你的摸牌阶段，跳过之。"

}

local zhurong = General(extension, "joyex__zhurong", "shu", 4, 4, General.Female)
local juxiang = fk.CreateTriggerSkill{
  name = "joyex__juxiang",
  mute = true,
  frequency = Skill.Compulsory,
  events = {fk.PreCardEffect, fk.CardUseFinished, fk.AfterCardsMove},
  can_trigger = function(self, event, target, player, data)
    if player:hasSkill(self) then
      if event == fk.AfterCardsMove then
        for _, move in ipairs(data) do
          if move.from ~= player.id and move.moveReason == fk.ReasonDiscard then
            for _, info in ipairs(move.moveInfo) do
              if Fk:getCardById(info.cardId).trueName == "savage_assault" and player.room:getCardArea(info.cardId) == Card.DiscardPile then
                return true
              end
            end
          end
        end
      elseif data.card and data.card.trueName == "savage_assault" then
        if event == fk.PreCardEffect then
          return data.to == player.id
        elseif event == fk.CardUseFinished then
          return target ~= player and player.room:getCardArea(data.card) == Card.Processing
        end
      end
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    player:broadcastSkillInvoke(self.name)
    if event == fk.PreCardEffect then
      room:notifySkillInvoked(player, self.name, "defensive")
      return true
    elseif event == fk.AfterCardsMove then
      local ids = {}
      for _, move in ipairs(data) do
        if move.from ~= player.id and move.moveReason == fk.ReasonDiscard then
          for _, info in ipairs(move.moveInfo) do
            if Fk:getCardById(info.cardId).trueName == "savage_assault" and room:getCardArea(info.cardId) == Card.DiscardPile then
              table.insertIfNeed(ids, info.cardId)
            end
          end
        end
      end
      if #ids > 0 then
        room:notifySkillInvoked(player, self.name, "drawcard")
        local dummy = Fk:cloneCard("dilu")
        dummy:addSubcards(ids)
        room:obtainCard(player.id, dummy, true, fk.ReasonJustMove)
      end
    elseif event == fk.CardUseFinished then
      room:notifySkillInvoked(player, self.name, "drawcard")
      room:obtainCard(player.id, data.card, true, fk.ReasonJustMove)
      if data.damageDealt and not player.dead then
        player:drawCards(1,self.name)
      end
    end
  end,
}
local lieren = fk.CreateTriggerSkill{
  name = "joyex__lieren",
  anim_type = "control",
  events = {fk.TargetSpecified},
  can_trigger = function(self, event, target, player, data)
    if target == player and player:hasSkill(self) then
        if data.card and data.card.trueName == "slash" and not player:isKongcheng() then
          local to = player.room:getPlayerById(data.to)
          return not to.dead and not to:isKongcheng()
        end
    end
  end,
  on_trigger = function(self, event, target, player, data)
      self:doCost(event, target, player, data.to)
  end,
  on_cost = function(self, event, target, player, data)
    local room = player.room
      if room:askForSkillInvoke(player, self.name, nil, "#joyex__lieren-invoke::"..data) then
        room:doIndicate(player.id, {data})
        return true
      end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
      local to = room:getPlayerById(data)
      local pindian = player:pindian({to}, self.name)
      if room:getCardArea(pindian.results[to.id].toCard) == Card.DiscardPile then
        room:delay(1000)
        room:obtainCard(player, pindian.results[to.id].toCard, true, fk.ReasonJustMove)
      end
      if pindian.results[to.id].winner == player and not player.dead and not to.dead and not to:isNude() then
        local id = room:askForCardChosen(player, to, "he", self.name)
        room:obtainCard(player, id, false, fk.ReasonPrey)
      end
  end,
}
local changbiao = fk.CreateViewAsSkill{
  name = "joyex__changbiao",
  anim_type = "offensive",
  pattern = "slash",
  prompt = "#joyex__changbiao-active",
  card_filter = function(self, to_select, selected)
    return Fk:currentRoom():getCardArea(to_select) ~= Player.Equip
  end,
  view_as = function(self, cards)
    if #cards < 1 then
      return nil
    end
    local c = Fk:cloneCard("slash")
    c.skillName = self.name
    c:addSubcards(cards)
    return c
  end,
  enabled_at_play = function(self, player)
    return player:usedSkillTimes(self.name, Player.HistoryPhase) < 1
  end,
  enabled_at_response = Util.FalseFunc,
  before_use = function(self, player, useData)
    useData.extra_data = useData.extra_data or {}
    useData.extra_data.ol_ex__changbiaoUser = player.id
  end,
}
local changbiao_trigger = fk.CreateTriggerSkill{
  name = "#joyex__changbiao_trigger",
  events = {fk.EventPhaseEnd},
  mute = true,
  can_trigger = function(self, event, target, player, data)
    return player:getMark("@joyex__changbiao_draw-phase") > 0
  end,
  on_cost = Util.TrueFunc,
  on_use = function(self, event, target, player, data)
    local room = player.room
    room:notifySkillInvoked(player, "joyex__changbiao", "drawcard")
    player:drawCards(player:getMark("@joyex__changbiao_draw-phase"), "joyex__changbiao")
  end,

  refresh_events = {fk.CardUseFinished},
  can_refresh = function(self, event, target, player, data)
    return (data.extra_data or {}).ol_ex__changbiaoUser == player.id and data.damageDealt
  end,
  on_refresh = function(self, event, target, player, data)
    player.room:addPlayerMark(player, "@joyex__changbiao_draw-phase", #data.card.subcards)
  end,
}
local changbiao_targetmod = fk.CreateTargetModSkill{
  name = "#joyex__changbiao_targetmod",
  bypass_distances =  function(self, player, skill, card, to)
    return table.contains(card.skillNames, changbiao.name)
  end,
}
changbiao:addRelatedSkill(changbiao_trigger)
changbiao:addRelatedSkill(changbiao_targetmod)
zhurong:addSkill(juxiang)
zhurong:addSkill(lieren)
zhurong:addSkill(changbiao)
Fk:loadTranslationTable{
  ["joyex__zhurong"] = "界祝融",
  ["joyex__juxiang"] = "巨象",
  [":joyex__juxiang"] = "锁定技，【南蛮入侵】对你无效；其他角色使用或弃置的【南蛮入侵】进入弃牌堆时，你获得之,若该牌造成过伤害，则你额外摸一张牌",
  ["joyex__lieren"] = "烈刃",
  [":joyex__lieren"] = "当你使用【杀】指定目标后，你可以与其拼点并获得其拼点牌，若你赢，你获得其一张牌。",
  ["#joyex__lieren-invoke"] = "烈刃：你可以与 %dest 拼点并获得其拼点牌，若你赢，你获得其一张牌",
  ["joyex__changbiao"] = "长标",
  ["#joyex__changbiao_trigger"] = "长标",
  [":joyex__changbiao"] = "出牌阶段限一次，你可将至少一张手牌转化为普【杀】使用（无距离关系的限制），此阶段结束时，若此【杀】造成过伤害，你摸x张牌（X为以此法转化的牌数）。",

  ["#joyex__changbiao-active"] = "发动长标，将任意数量的手牌转化为【杀】使用（无距离限制）",
  ["@joyex__changbiao_draw-phase"] = "长标",
}

local xuhuang = General(extension, "joyex__xuhuang", "wei", 4)
local joyex__duanliang = fk.CreateViewAsSkill{
  name = "joyex__duanliang",
  anim_type = "control",
  pattern = "supply_shortage",
  prompt = "#joyex__duanliang",
  card_filter = function(self, to_select, selected)
    return #selected == 0 and Fk:getCardById(to_select).color == Card.Black and Fk:getCardById(to_select).type ~= Card.TypeTrick
  end,
  view_as = function(self, cards)
    if #cards ~= 1 then return end
    local card = Fk:cloneCard("supply_shortage")
    card.skillName = self.name
    card:addSubcard(cards[1])
    return card
  end,
}
local joyex__duanliang_targetmod = fk.CreateTargetModSkill{
  name = "#joyex__duanliang_targetmod",
  main_skill = joyex__duanliang,
  bypass_distances =  function(self, player, skill, card, to)
    return player:hasSkill(self) and skill.name == "supply_shortage_skill" and to:getHandcardNum() >= player:getHandcardNum()
  end,
}
local joyex__jiezi = fk.CreateTriggerSkill{
  name = "joyex__jiezi",
  anim_type = "drawcard",
  frequency = Skill.Compulsory,
  events = {fk.EventPhaseSkipping},
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(self) and target ~= player and target.skipped_phases[Player.Draw] and  --FIXME: 此时机无data，需补充
      player:usedSkillTimes(self.name, Player.HistoryRound) < 2
  end,
  on_use = function(self, event, target, player, data)
    player:drawCards(2, self.name)
  end,
}
joyex__duanliang:addRelatedSkill(joyex__duanliang_targetmod)
xuhuang:addSkill(joyex__duanliang)
xuhuang:addSkill(joyex__jiezi)
Fk:loadTranslationTable{
  ["joyex__xuhuang"] = "界徐晃",
  ["joyex__duanliang"] = "断粮",
  [":joyex__duanliang"] = "你可以将一张黑色基本牌或装备牌当【兵粮寸断】使用；你对手牌数不小于你的角色使用【兵粮寸断】无距离限制。",
  ["joyex__jiezi"] = "截辎",
  [":joyex__jiezi"] = "锁定技，每轮限两次，一名其他角色跳过摸牌阶段后，你摸两张牌。",
  ["#joyex__duanliang"] = "断粮：你可以将一张黑色基本牌或装备牌当【兵粮寸断】使用",
}



local caopi = General(extension, "joy__caopi", "wei", 3)
local xingshang = fk.CreateTriggerSkill{
  name = "joy__xingshang",
  anim_type = "drawcard",
  events = {fk.Death},
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(self) and not target:isNude()
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    player:broadcastSkillInvoke("fangzhu")
    local cards = target:getCardIds{Player.Hand, Player.Equip}
    if #cards > 0 then
      local dummy = Fk:cloneCard'slash'
      dummy:addSubcards(cards)
      room:obtainCard(player.id, dummy, false, fk.ReasonPrey)
    end
    if not player.dead then
      player:drawCards(1, self.name)
    end
  end,
}
local fangzhu = fk.CreateTriggerSkill{
  name = "joy__fangzhu",
  anim_type = "masochism",
  events = {fk.Damaged},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self)
  end,
  on_cost = function(self, event, target, player, data)
    local to = player.room:askForChoosePlayers(player, table.map(player.room:getOtherPlayers(player), Util.IdMapper), 1, 1, "#joy__fangzhu-choose", self.name, true)
    if #to > 0 then
      self.cost_data = to[1]
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    player:broadcastSkillInvoke("fangzhu")
    local to = player.room:getPlayerById(self.cost_data)
    to:turnOver()
    if not to.dead then
      to:drawCards(1, self.name)
    end
  end,
}
caopi:addSkill(xingshang)
caopi:addSkill(fangzhu)
caopi:addSkill("songwei")
Fk:loadTranslationTable{
  ["joy__caopi"] = "曹丕",
  ["joy__xingshang"] = "行殇",
  [":joy__xingshang"] = "当其他角色死亡时，你可以获得其所有牌并摸一张牌。",
  ["joy__fangzhu"] = "放逐",
  [":joy__fangzhu"] = "当你受到伤害后，你可以令一名其他角色翻面，然后该角色摸一张牌。",
  ["#joy__fangzhu-choose"] = "放逐：你可以令一名其他角色翻面，然后其摸一张牌",
}


local sunjian = General(extension, "joyex__sunjian", "wu", 4)
local yinghun = fk.CreateTriggerSkill{
  name = "joyex__yinghun",
  anim_type = "control",
  events = {fk.EventPhaseStart},
  can_trigger = function(self, event, target, player, data)
    return  target == player and player:hasSkill(self) and player.phase == Player.Start
  end,
  on_cost = function(self, event, target, player, data)
    local room = player.room
    local n = player.maxHp - player.hp
    local choices =  {"joyex__yinghundraw:::"..n,"joyex__yinghundis:::"..n}
    local target = room:askForChoosePlayers(player,table.map(room:getOtherPlayers(player),function(p) return p.id end),1,1,"#joyex__yinghun-choose",self.name,true)
    if #target > 0 then
      local choice = room:askForChoice(player,choices,self.name)
      if choice == "joyex__yinghundraw:::"..n and n > 0 then
        self.cost_data = {target[1],"draw",n}
        return true
      elseif choice == "joyex__yinghundis:::"..n then
        self.cost_data = {target[1],"dis",n}
        return true
      end
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local target = room:getPlayerById(self.cost_data[1])
    local c = self.cost_data[2]
    local n = self.cost_data[3]
    if c == "draw" then
      target:drawCards(n,self.name)
    elseif c == "dis" then
      target:drawCards(1,self.name)
      if n > 0 and not target.dead then
        room:askForDiscard(target,n,n,true,self.name,false)
      end
    end
  end,
}
local wulie_active = fk.CreateActiveSkill{
  name = "#joyex__wulie_active",
  anim_type = "offensive",
  card_num = 0,
  target_num = 0,
  frequency = Skill.Limited,
  interaction = function()
    return UI.Spin {
      from = 1,
      to = Self.hp,
    }
  end,
}
local wulie = fk.CreateTriggerSkill{
  name = "joyex__wulie",
  anim_type = "offensive",
  frequency = Skill.Limited,
  events = {fk.TurnStart},
  can_trigger = function(self, event, target, player, data)
    return  target == player and player:hasSkill(self) and player:usedSkillTimes(self.name, Player.HistoryGame) == 0
  end,
  on_cost = function(self, event, target, player, data)
    local room = player.room
    local _, ret = room:askForUseActiveSkill(player, "#joyex__wulie_active", "#joyex__wulie-hp", true)
    local n
    if ret then
      n = ret.interaction
    if n > 0 then
      self.cost_data = n
      return true
    end
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local n = self.cost_data
    room:loseHp(player,n,self.name)
    if not player.dead then
      room:addPlayerMark(player,"@joyex__lie",n)
      local targets = room:askForChoosePlayers(player,table.map(room:getOtherPlayers(player),function(p) return p.id end),1,n,"#joyex__wulie:::"..n,self.name,true)
      if #targets > 0 then
        for _, pid in ipairs(targets) do
          local p = room:getPlayerById(pid)
          room:addPlayerMark(p,"@joyex__lie",1)
        end
      end
    end
  end,
}
local wulie_buff = fk.CreateTriggerSkill{
  name = "#joyex__wulie_buff",
  anim_type = "offensive",
  events = {fk.DamageCaused},
  can_trigger = function(self, event, target, player, data)
    return  data.to == player and player:getMark("@joyex__lie") > 0
  end,
  on_cost = function(self, event, target, player, data)
    return player.room:askForSkillInvoke(player,self.name,nil,"#joyex__wulie_buff2")
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    room:removePlayerMark(player,"@joyex__lie",1)
    return true
  end,
}
wulie:addRelatedSkill(wulie_active)
wulie:addRelatedSkill(wulie_buff)
sunjian:addSkill(yinghun)
sunjian:addSkill(wulie)
Fk:loadTranslationTable{
  ["joyex__sunjian"] = "界孙坚",

  ["joyex__yinghun"] = "英魂",
  [":joyex__yinghun"] = "准备阶段，你可以选择一名其他角色并选择一项：<br>①令其摸X张牌；<br>②令其摸一张牌，然后弃置X张牌。<br>（X为你已损失的体力值）",
  ["joyex__yinghundraw"] = "令其摸 %arg 张牌",
  ["joyex__yinghundis"] = "令其摸一张牌，然后弃置 %arg 张牌",
  ["#joyex__yinghun-choose"] = "选择你要发动“英魂”的对象。",

  ["joyex__wulie"] = "武烈",
  [":joyex__wulie"] = "<br>①限定技，你的回合开始前，你可以失去任意点体力获得等量枚“烈”标记，然后可以令等量名其他角色各获得一枚“烈”标记。<br>②当有“烈”的角色受到伤害时，可以弃置一枚“烈”标记来防止该伤害。",
  ["@joyex__lie"] = "烈",
  ["#joyex__wulie"] = "武烈：你可以选择至多 %arg 名其他角色令其各获得一枚“烈”",
  ["#joyex__wulie_buff"] = "武烈",
  ["#joyex__wulie_buff2"] = "武烈：是否弃置一枚“烈”来防止此次伤害？",
  ["#joyex__wulie_active"] = "武烈",
  [ "#joyex__wulie-hp"] = "武烈：选择要失去的体力值",

}
--[[
local lusu = General(extension, "joy__lusu", "wu", 3)
local haoshi = fk.CreateTriggerSkill{
  name = "joy__haoshi",
  anim_type = "drawcard",
  events = {fk.DrawNCards},
  on_use = function(self, event, target, player, data)
    data.n = data.n + 2
  end,
}
local haoshi_active = fk.CreateActiveSkill{
  name = "#joy__haoshi_active",
  visible = false,
  can_use = Util.FalseFunc,
  card_num = function ()
    return Self:getHandcardNum() // 2
  end,
  card_filter = function(self, to_select, selected)
    return #selected < Self:getHandcardNum() // 2 and Fk:currentRoom():getCardArea(to_select) == Player.Hand
  end,
}
local haoshi_give = fk.CreateTriggerSkill{
  name = "#joy__haoshi_give",
  events = {fk.AfterDrawNCards},
  mute = true,
  anim_type = "support",
  frequency = Skill.Compulsory,
  visible = false,
  can_trigger = function(self, event, target, player, data)
    return target == player and player:usedSkillTimes("joy__haoshi", Player.HistoryPhase) > 0 and player:getHandcardNum() > 5
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local cards = {}
    local _, ret = room:askForUseActiveSkill(player, "#joy__haoshi_active", "#joy__haoshi-givecard:::"..player:getHandcardNum() // 2, false)
    if ret then
      cards = ret.cards
    else
      cards = table.random(player:getCardIds(Player.Hand), player:getHandcardNum() // 2)
    end
    local num = 999
    local targets = {}
    for _, p in ipairs(room:getOtherPlayers(player)) do
        local n = p:getHandcardNum()
        if n <= num then
          if n < num then
            num = n
            targets = {}
          end
          table.insert(targets, p.id)
        end
    end
    local target = room:askForChoosePlayers(player,targets,1,1,"#joy__haoshi-choose",self.name,true)
    if #target > 0 then
      room:moveCardTo(cards, Card.PlayerHand, room:getPlayerById(target[1]), fk.ReasonGive, self.name, nil, false, player.id)
    else
      room:moveCardTo(cards,Card.DiscardPile)
    end
  end
}
local dimeng = fk.CreateActiveSkill{
  name = "joy__dimeng",
  anim_type = "control",
  card_num = 0,
  target_num = 2,
  prompt = "#joy__dimeng",
  can_use = function(self, player)
    return player:usedSkillTimes(self.name) == 0 and #Fk:currentRoom().alive_players > 2
  end,
  card_filter = function(self, to_select, selected, selected_targets)
    return false
  end,
  target_filter = function(self, to_select, selected, selected_cards)
    if to_select == Self.id or #selected > 1 then return false end
    if #selected == 0 then
      return true
    else
      local target1 = Fk:currentRoom():getPlayerById(to_select)
      local target2 = Fk:currentRoom():getPlayerById(selected[1])
      local num, num2 = target1:getHandcardNum(), target2:getHandcardNum()
      if num == 0 and num2 == 0 then
        return false
      end
      local x = #table.filter(Self:getCardIds({Player.Hand, Player.Equip}), function(cid) return not Self:prohibitDiscard(Fk:getCardById(cid)) end)
      return math.abs( num - num2 ) <= x
    end
  end,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    local target1 = room:getPlayerById(effect.tos[1])
    local target2 = room:getPlayerById(effect.tos[2])
    local num = math.abs(target1:getHandcardNum() - target2:getHandcardNum())
    if num > 0 then
      room:askForDiscard(player, num, num, true, self.name, false, nil, "#joy__dimeng-discard:" .. effect.tos[1] .. ":" .. effect.tos[2] .. ":" .. num)
    end
    U.swapHandCards(room, player, target1, target2, self.name)
  end,
}
haoshi:addRelatedSkill(haoshi_active)
haoshi:addRelatedSkill(haoshi_give)
lusu:addSkill(haoshi)
lusu:addSkill(dimeng)
Fk:loadTranslationTable{
  ["joy__lusu"] = "鲁肃",
  ["#joy__lusu"] = "独断的外交家",

  ["joy__haoshi"] = "好施",
  [":joy__haoshi"] = "摸牌阶段，你可以多摸两张牌，然后若你的手牌数大于5，你将半数（向下取整）手牌交给手牌牌最少的一名其他角色或弃置。",
  ["joy__dimeng"] = "缔盟",
  [":joy__dimeng"] = "出牌阶段限一次，你可以选择两名其他角色并弃置X张牌（X为这些角色手牌数差），令这两名角色交换手牌。",
  ["#joy__haoshi-givecard"] = "好施：选择%arg张手牌弃置或交给手牌最少的一名其他角色",
  ["#joy__haoshi_active"] = "好施",
  ["#joy__haoshi_give"] = "好施",
  ["#joy__haoshi-choose"] = "好施：将这些牌交给一名手牌最少的其他角色，或者点“取消”弃置。",
  ["#joy__dimeng"] = "缔盟：选择两名其他角色，点击“确定”后，选择与其手牌数之差等量的牌，这两名角色交换手牌",
  ["#joy__dimeng-discard"] = "缔盟：弃置 %arg 张牌，交换%src和%dest的手牌",

  ["$joy__haoshi1"] = " ",
  ["$joy__haoshi2"] = " ",
  ["$joy__dimeng1"] = " ",
  ["$joy__dimeng2"] = " ",
  ["~joy__lusu"] = " ",
}
]]

local lusu = General(extension, "joyex__lusu", "wu", 3)
local haoshi = fk.CreateTriggerSkill{
  name = "joyex__haoshi",
  anim_type = "drawcard",
  events = {fk.DrawNCards},
  can_trigger = function (self, event, target, player, data)
    return target == player and player:hasSkill(self)
  end,
  on_cost = function (self, event, target, player, data)
    return true
  end,
  on_use = function(self, event, target, player, data)
    data.n = data.n + 2
  end,
  refresh_events = {fk.TurnStart,fk.TargetConfirmed},
  can_refresh = function (self, event, target, player, data)
    if target ~= player then return false end
    local yes = false
    for _, p in ipairs(player.room:getOtherPlayers(player)) do
      if type(p:getMark("@@joyex__haoshi")) == "table" and table.contains(p:getMark("@@joyex__haoshi"),player.id)then
        yes = true
        break
      end
    end
    if event == fk.TurnStart then
      return yes
    elseif event == fk.TargetConfirmed then
      return yes and data.card and (data.card.trueName == "slash" or data.card:isCommonTrick())
    end
  end,
  on_refresh = function (self, event, target, player, data)
    local room = player.room
    if event == fk.TurnStart then
      for _, p in ipairs(player.room:getOtherPlayers(player)) do
        local mark = type(p:getMark("@@joyex__haoshi")) == "table" and p:getMark("@@joyex__haoshi") or {}
        table.removeOne(mark,player.id)
        if #mark == 0 then
          room:setPlayerMark(p,"@@joyex__haoshi",0)
        else
          room:setPlayerMark(p,"@@joyex__haoshi",mark)
        end
      end
    elseif event == fk.TargetConfirmed then
      for _, p in ipairs(player.room:getOtherPlayers(player)) do
        if type(p:getMark("@@joyex__haoshi")) == "table" and table.contains(p:getMark("@@joyex__haoshi"),player.id) and not p:isKongcheng() then
          local card = room:askForCard(p,1,1,false,self.name,true,".","你可以交给 "..Fk:translate(player.id).." 一张手牌")
          if #card > 0 then
            room:moveCardTo(card, Card.PlayerHand,player, fk.ReasonGive, self.name, nil, false, player.id)
          end
        end
      end
    end
  end,
}
local haoshi_active = fk.CreateActiveSkill{
  name = "#joyex__haoshi_active",
  visible = false,
  can_use = Util.FalseFunc,
  target_num = 1,
  target_filter = function (self, to_select, selected, selected_cards, card, extra_data)
    return to_select ~= Self.id
  end,
  card_num = function ()
    return Self:getHandcardNum() // 2
  end,
  card_filter = function(self, to_select, selected)
    return #selected < Self:getHandcardNum() // 2 and Fk:currentRoom():getCardArea(to_select) == Player.Hand
  end,
}
local haoshi_give = fk.CreateTriggerSkill{
  name = "#joyex__haoshi_give",
  events = {fk.AfterDrawNCards},
  mute = true,
  anim_type = "support",
  frequency = Skill.Compulsory,
  visible = false,
  can_trigger = function(self, event, target, player, data)
    return target == player and player:usedSkillTimes("joyex__haoshi", Player.HistoryPhase) > 0 and player:getHandcardNum() > 5
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local cards = {}
    local target
    local _, ret = room:askForUseActiveSkill(player, "#joyex__haoshi_active", "#joyex__haoshi-givecard:::"..player:getHandcardNum() // 2,true)
    if not (_ and ret) then return end
    if ret then
      cards = ret.cards
      target = room:getPlayerById(ret.targets[1])
    else
      cards = table.random(player:getCardIds(Player.Hand), player:getHandcardNum() // 2)
    end
    room:moveCardTo(cards, Card.PlayerHand,target, fk.ReasonGive, self.name, nil, false, player.id)
    if player.dead or target.dead then return false end
    local mark = type(target:getMark("@@joyex__haoshi")) == "table" and target:getMark("@@joyex__haoshi") or {}
    table.insertIfNeed(mark,player.id)
    room:setPlayerMark(target,"@@joyex__haoshi",mark)
  end
}
local dimeng = fk.CreateActiveSkill{
  name = "joyex__dimeng",
  anim_type = "control",
  card_num = 0,
  target_num = 2,
  prompt = "#joyex__dimeng",
  can_use = function(self, player)
    return player:usedSkillTimes(self.name) == 0 and player:getMark("@"..self.name) == 0
  end,
  card_filter = function(self, to_select, selected, selected_targets)
    return false
  end,
  target_filter = function(self, to_select, selected, selected_cards)
    if #selected > 1 then return false end
    if #selected == 0 then
      return true
    else
      local target1 = Fk:currentRoom():getPlayerById(to_select)
      local target2 = Fk:currentRoom():getPlayerById(selected[1])
      local num, num2 = target1:getHandcardNum(), target2:getHandcardNum()
      if num == 0 and num2 == 0 then
        return false
      end
      return true
    end
  end,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    local target1 = room:getPlayerById(effect.tos[1])
    local target2 = room:getPlayerById(effect.tos[2])
    local num = math.abs(target1:getHandcardNum() - target2:getHandcardNum())
    room:addPlayerMark(player,"@"..self.name,num)
    U.swapHandCards(room, player, target1, target2, self.name)
  end,
}
local dimeng_delay = fk.CreateTriggerSkill{
  name = "#joyex__dimeng_delay",
  events = {fk.EventPhaseEnd,fk.TurnEnd},
  can_trigger = function (self, event, target, player, data)
    return target == player and player:hasSkill(dimeng) and player:getMark("@joyex__dimeng") > 0 and not player:isNude() and (player.phase == Player.Draw or event == fk.TurnEnd) 
  end,
  on_cost = function (self, event, target, player, data)
    local room = player.room
    local cards = room:askForCard(player,1,player:getMark("@joyex__dimeng"),true,self.name,true,".","缔盟：你可以弃置任意张牌并移除等量“盟”标记。")
    if #cards > 0 then
      self.cost_data = cards
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    player.room:throwCard(self.cost_data,self.name,player,player)
    if not player.dead then
      player.room:removePlayerMark(player,"@joyex__dimeng",#self.cost_data)
    end
  end,
}
haoshi:addRelatedSkill(haoshi_active)
haoshi:addRelatedSkill(haoshi_give)
dimeng:addRelatedSkill(dimeng_delay)
lusu:addSkill(haoshi)
lusu:addSkill(dimeng)
Fk:loadTranslationTable{
  ["joyex__lusu"] = "界鲁肃",
  ["#joyex__lusu"] = "独断的外交家",
  ["@@joyex__haoshi"] = "好施",
  ["joyex__haoshi"] = "好施",
  [":joyex__haoshi"] = "你摸牌阶段多摸两张牌，然后若你的手牌数大于5，你可以将半数（向下取整）手牌交给一名其他角色，若如此做，直到你下个回合开始，当你成为【杀】或普通锦囊牌的目标后，该角色可交给你一张手牌。",
  ["joyex__dimeng"] = "缔盟",
  [":joyex__dimeng"] = "出牌阶段限一次，若你没有“盟”标记，你可以令两名角色交换手牌并获得这两名角色手牌差枚“盟”标记；<br>你的摸牌阶段结束时或回合结束时，可以弃置任意张牌移除等量“盟”标记。",
  ["#joyex__haoshi-givecard"] = "好施：你可以将%arg张手牌交给一名其他角色",
  ["#joyex__haoshi_active"] = "好施",
  ["#joyex__haoshi_give"] = "好施",
  ["#joyex__dimeng"] = "缔盟：选择两名角色交换手牌，然后你获得交换牌数之差的“盟”标记。",
  ["#joyex__dimeng_delay"] = "缔盟",
  ["@joyex__dimeng"] = "盟",

}

local benghuai = fk.CreateTriggerSkill{
  name = "joy__benghuai",
  anim_type = "negative",
  frequency = Skill.Compulsory,
  events = {fk.EventPhaseStart},
  can_trigger = function(self, event, target, player, data)
    if target == player and player:hasSkill(self) and player.phase == Player.Finish then
      return table.find(player.room.alive_players, function (p)
        return p.hp < player.hp
      end)
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local choice = room:askForChoice(player, {"loseMaxHp", "loseHp"}, self.name)
    if choice == "loseMaxHp" then
      room:changeMaxHp(player, -1)
    else
      room:loseHp(player, 1, self.name)
    end
    if not player.dead then
      player:drawCards(1, self.name)
    end
  end,
}
Fk:addSkill(benghuai)
Fk:loadTranslationTable{
  ["joy__benghuai"] = "崩坏",
  [":joy__benghuai"] = "锁定技，结束阶段，若你不是体力值最低的角色，则你失去1点体力或减少1点体力上限，并摸一张牌。",
}


local jiuchi = fk.CreateViewAsSkill{
  name = "joy__jiuchi",
  anim_type = "offensive",
  pattern = "analeptic",
  prompt = "你可以将一张黑色牌当做【酒】使用。",
  card_filter = function(self, to_select, selected)
    return #selected == 0 and Fk:getCardById(to_select).color == Card.Black 
  end,
  view_as = function(self, cards)
    if #cards ~= 1 then return nil end
    local c = Fk:cloneCard("analeptic")
    c.skillName = self.name
    c:addSubcard(cards[1])
    return c
  end,
}
local jiuchi_targetmod = fk.CreateTargetModSkill{
  name = "#joy__jiuchi_targetmod",
  bypass_times = function(self, player, skill, scope)
    return player:hasSkill(jiuchi) and skill.trueName == "analeptic_skill" and scope == Player.HistoryTurn
  end,
}
local jiuchi_trigger = fk.CreateTriggerSkill{
  name = "#joy__jiuchi_trigger",
  events = {fk.Damage},
  mute = true,
  main_skill = jiuchi,
  can_trigger = function(self, event, target, player, data)
    if target == player and player:hasSkill(jiuchi) and data.card and data.card.trueName == "slash"
        and player:getMark("@@joyex__benghuai_invalidity-turn") == 0 then
      local parentUseData = player.room.logic:getCurrentEvent():findParent(GameEvent.UseCard)
      if parentUseData then
        local drankBuff = parentUseData and (parentUseData.data[1].extra_data or {}).drankBuff or 0
        return drankBuff > 0
      end
    end
  end,
  on_cost = function (self, event, target, player, data)
    return player.room:askForSkillInvoke(player,self.name,nil,"酒池：是否摸一张牌并令本回合〖崩坏〗失效？")
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    room:notifySkillInvoked(player, "joy__jiuchi", "drawcard")
    player:broadcastSkillInvoke("joy__jiuchi")
    player:drawCards(1,self.name)
    if not player.dead then
      room:addPlayerMark(player, "@@joyex__benghuai_invalidity-turn")
    end
  end,
}
local jiuchi_invalidity = fk.CreateInvaliditySkill {
  name = "#joy__jiuchi_invalidity",
  invalidity_func = function(self, from, skill)
    return from:getMark("@@joyex__benghuai_invalidity-turn") > 0 and skill.name == "joyex__benghuai"
  end
}
local roulin = fk.CreateTriggerSkill{
  name = "joy__roulin",
  anim_type = "offensive",
  frequency = Skill.Compulsory,
  events = {fk.TargetSpecified, fk.TargetConfirmed,fk.CardUseFinished},
  can_trigger = function(self, event, target, player, data)
    if target == player and player:hasSkill(self) and data.card.trueName == "slash" then
      if event == fk.TargetSpecified then
        return player:getMark("@@joy__roulin-turn") > 0 or player.room:getPlayerById(data.to).gender == General.Female
      elseif event ==  fk.TargetConfirmed then
        return player:getMark("@@joy__roulin-turn") > 0 or player.room:getPlayerById(data.from).gender == General.Female
      end
    elseif player:hasSkill(self) and data.card.trueName == "slash" then
      return not data.damageDealt and data.extra_data and data.extra_data.joy__roulin
    end
  end,
  on_use = function(self, event, target, player, data)
    if event == fk.CardUseFinished then
      local room = player.room
      local ids = room:getCardsFromPileByRule(".|.|club,spade", 1, "DrawPile")
      if #ids > 0 then
        room:obtainCard(player, ids[1], false, fk.ReasonPrey)
      end
    else
      data.extra_data = data.extra_data or {}
      data.extra_data.joy__roulin = true
      data.fixedResponseTimes = data.fixedResponseTimes or {}
      data.fixedResponseTimes["jink"] = 2
    end
  end,
}
local ex__benghuai = fk.CreateTriggerSkill{
  name = "joyex__benghuai",
  anim_type = "negative",
  frequency = Skill.Compulsory,
  events = {fk.TurnStart,fk.EventPhaseStart},
  can_trigger = function(self, event, target, player, data)
    if event == fk.TurnStart then
      return target == player and player:hasSkill(self) and not table.find(player.room.alive_players, function (p)
        return p.hp < player.hp
      end)
    elseif event == fk.EventPhaseStart then
      return target.phase == Player.Finish and target:hasSkill(self) and  target == player  and  table.find(player.room.alive_players, function (p)
        return p.hp < player.hp
      end)
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if event == fk.TurnStart then
      room:setPlayerMark(player,"@@joy__roulin-turn",1)
    else
      room:doIndicate(target.id,{player.id})
      local choice = room:askForChoice(player, {"loseMaxHp", "loseHp"}, self.name)
      if choice == "loseMaxHp" then
        room:changeMaxHp(player, -1)
      else
        room:loseHp(player, 1, self.name)
      end
      if not player.dead then
        player:drawCards(1, self.name)
      end
    end
  end,
}
jiuchi:addRelatedSkill(jiuchi_targetmod)
jiuchi:addRelatedSkill(jiuchi_trigger)
jiuchi:addRelatedSkill(jiuchi_invalidity)
local dongzhuo = General(extension, "joyex__dongzhuo", "qun", 8)
dongzhuo:addSkill(jiuchi)
dongzhuo:addSkill(roulin)
dongzhuo:addSkill(ex__benghuai)
dongzhuo:addSkill("ol_ex__baonue")

Fk:loadTranslationTable{
  ["joyex__dongzhuo"] = "界董卓",
  ["#joyex__dongzhuo"] = "魔王",
  
  ["joy__jiuchi"] = "酒池",
  ["#joy__jiuchi_trigger"] = "酒池",
  [":joy__jiuchi"] = "①你可将一张黑色牌转化为【酒】使用。②你使用【酒】无次数限制。③当你造成伤害后，若渠道为受【酒】效果影响的【杀】，你可摸一张牌并令〖崩坏〗于当前回合内无效。",

  ["@@joyex__benghuai_invalidity-turn"] = "崩坏失效",
  ["joy__roulin"] = "肉林",
  [":joy__roulin"] = "锁定技，你对女性角色使用【杀】，或女性角色对你使用【杀】均需两张【闪】才能抵消；若受此技能影响的【杀】未造成过伤害，你从牌堆获得一张黑色牌。",
  ["joyex__benghuai"] = "崩坏",
  [":joyex__benghuai"] = "锁定技，<br>①你的结束阶段开始时，若你体力值不为全场最低，你选择失去一点体力或减少一点体力上限，然后摸一张牌。<br>②你的回合开始时，若你的体力值为全场最低，本回合〖肉林〗改为对所有角色生效。",
  ["@@joy__roulin-turn"] = "肉林 全场生效",
}

local wansha = fk.CreateProhibitSkill{
  name = "joy__wansha",
  frequency = Skill.Compulsory,
  prohibit_use = function(self, player, card)
    return card and card.name == "peach" and table.find(Fk:currentRoom().alive_players, function (p)
      return p.phase ~= Player.NotActive and p ~= player and p:hasSkill(self)
    end)
  end,
}
Fk:addSkill(wansha)

Fk:loadTranslationTable{
  ["joy__wansha"] = "完杀",
  [":joy__wansha"] = "锁定技，其他角色无法于你的回合内使用【桃】",
}

local ex__wansha = fk.CreateTriggerSkill{
  name = "joyex__wansha",
  anim_type = "control",
  frequency = Skill.Compulsory,
  events = {fk.EventPhaseStart},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and player.phase == Player.Play
  end,
  on_cost = function (self, event, target, player, data)
    local tars = {}
    for _, p in ipairs(player.room.alive_players) do
      if p.hp > 1 then
        table.insertIfNeed(tars,p.id)
      end
    end
    if #tars > 0 then
      local tar = player.room:askForChoosePlayers(player,tars,1,1,"完杀：你可令一名体力值大于1的角色失去1点体力，然后此阶段结束时其回复1点体力")
      if #tar > 0 then
        self.cost_data = tar[1]
        return true
      end
    end
  end,
  on_use = function(self, event, target, player, data)
    local p = player.room:getPlayerById(self.cost_data)
    player.room:setPlayerMark(p,"@@joyex__wansha-phase",1)
    player.room:loseHp(p,1,self.name)
  end,
  refresh_events = {fk.EventPhaseEnd},
  can_refresh = function(self, event, target, player, data)
    return  player:getMark("@@joyex__wansha-phase") > 0 and player:isWounded()
  end,
  on_refresh = function(self, event, target, player, data)
    player.room:recover({
      who = player,
      num = 1,
      skillName = self.name
    })
  end,
}
local ex__wansha_prohibit = fk.CreateProhibitSkill{
  name = "#joyex__wansha_prohibit",
  frequency = Skill.Compulsory,
  prohibit_use = function(self, player, card)
    return card and card.name == "peach" and table.find(Fk:currentRoom().alive_players, function (p)
      return p.phase ~= Player.NotActive and p ~= player and p:hasSkill(ex__wansha)
    end)
  end,
}
ex__wansha:addRelatedSkill(ex__wansha_prohibit)

local jiaxu = General(extension, "joyex__jiaxu", "qun", 3)
jiaxu:addSkill(ex__wansha)
jiaxu:addSkill("ol_ex__luanwu")
jiaxu:addSkill("ol_ex__weimu")

Fk:loadTranslationTable{
  ["joyex__jiaxu"] = "界贾诩",
  ["#joyex__jiaxu"] = "冷酷的毒士",
  ["joyex__wansha"] = "完杀",
  ["#joyex__wansha_prohibit"] = "完杀",
  ["@@joyex__wansha-phase"] = "完杀",
  [":joyex__wansha"] = "锁定技，其他角色无法于你的回合内使用【桃】；你的出牌阶段开始时，可以令一名体力值大于1的角色失去1点体力，若如此做，此阶段结束时其回复1点体力。",
}
------------------------山----------------------------

local jiangwei = General(extension, "joy__jiangwei", "shu", 4)
local tiaoxin = fk.CreateTriggerSkill{
  name = "joy__tiaoxin",
  events = {fk.EventPhaseStart},
  can_trigger = function (self, event, target, player, data)
    return target == player and player:hasSkill(self) and player.phase == Player.Play
  end,
  on_cost = function (self,event,target,player,data)
    local targets = {}
    for _, p in ipairs(player.room:getOtherPlayers(player)) do
      if not p:isKongcheng() then
        table.insert(targets,p.id)
      end
    end
    local to = player.room:askForChoosePlayers(player,targets,1,1,"#joy__tiaoxin-ask",self.name,true)
    if #to > 0 then
      self.cost_data = to[1]
      return true
    end
  end,
  on_use = function (self, event, target, player, data)
    local room = player.room
    local to = room:getPlayerById(self.cost_data)
    local cards = player.room:askForCardsChosen(player, to,1,2, "h", self.name,"joy__tiaoxin-throw")
    local n = false
    for _, ids in ipairs(cards) do
      local card = Fk:getCardById(ids)
      if card.trueName == "slash" then
        n = true
      end
    end
    room:throwCard(cards,self.name,to,player)
    if n and not player:isNude() and not player.dead then
      room:askForDiscard(player,1,1,true,self.name,false)
    end
  end
}
local zhiji = fk.CreateTriggerSkill{
  name = "joy__zhiji",
  frequency = Skill.Wake,
  events = {fk.TurnStart},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and
      player:usedSkillTimes(self.name, Player.HistoryGame) == 0 
  end,
  can_wake = function(self, event, target, player, data)
    return player:isKongcheng()
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if player.general == "joy__jiangwei" then 
    room:notifySkillInvoked(player, "joy__zhiji","drawcard")
      room:changeHero(player,"joy_skin__jiangwei",false,false,true,false)
    elseif player.deputyGeneral == "joy__jiangwei" then
    room:notifySkillInvoked(player, "joy__zhiji","drawcard")
      room:changeHero(player,"joy_skin__jiangwei",false,true,true,false)
    end
    local choices = {"draw2"}
    if player:isWounded() then
      table.insert(choices, "recover")
    end
    local choice = room:askForChoice(player, choices, self.name)
    if choice == "draw2" then
      player:drawCards(2, self.name)
    else
      room:recover({
        who = player,
        num = 1,
        recoverBy = player,
        skillName = self.name
      })
    end
    room:changeMaxHp(player, -1)
    room:handleAddLoseSkills(player, "joy__guanxing", nil, true, false)
  end,
}
--[[
local guanxing = fk.CreateTriggerSkill{
  name = "joy__guanxing",
  anim_type = "control",
  events = {fk.EventPhaseStart},
  can_trigger = function(self, event, target, player, data)
    if target == player and player:hasSkill(self) then
      return player.phase == Player.Start or player.phase == Player.Finish
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    room:askForGuanxing(player, room:getNCards(#room.alive_players > 2 and 5 or 3))
  end,
}
]]
local guanxing = fk.CreateTriggerSkill{
  name = "joy__guanxing",
  anim_type = "control",
  events = {fk.EventPhaseStart},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and
    (player.phase == Player.Start or (player.phase == Player.Finish and player:getMark("joy__guanxing-turn") > 0))
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local result = room:askForGuanxing(player, room:getNCards(#room.alive_players == 2 and 3 or 5))
    if #result.top == 0 and player.phase == Player.Start then
      room:setPlayerMark(player, "joy__guanxing-turn", 1)
    end
  end,
}
jiangwei:addSkill(tiaoxin)
jiangwei:addSkill(zhiji)
jiangwei:addRelatedSkill(guanxing)
Fk:loadTranslationTable{
  ["joy__jiangwei"] = "姜维",
  ["#joy__jiangwei"] = "龙的衣钵",

  ["joy__tiaoxin"] = "挑衅",
  [":joy__tiaoxin"] = "出牌阶段开始时，你可以弃置一名其他角色至多两张手牌，若其中有【杀】，则你需弃置一张牌。",
  ["joy__zhiji"] = "志继",
  [":joy__zhiji"] = "觉醒技，回合开始时，若你没有手牌，你回复1点体力或摸两张牌，然后减1点体力上限，获得〖观星〗。",
  ["joy__guanxing"] = "观星",
  [":joy__guanxing"] = "准备阶段，你可以观看牌堆顶5张牌（人数仅两人时改为3张），然后以任意顺序置于牌堆顶或牌堆底；若你将这些牌均放至牌堆底，则结束阶段你可以发动〖观星〗。",

  ["#joy__tiaoxin-ask"] = "挑衅：你可以弃置一名其他角色至多俩张手牌，若其中有【杀】，则你需弃置一张牌。",
  ["joy__tiaoxin-throw"] = "挑衅：选择弃置其中至多俩张手牌",


  ["$joy__guanxing1"] = " ",
  ["$joy__zhiji1"] = " ",
  ["$joy__tiaoxin1"] = " ",
  ["$joy__guanxing2"] = " ",
  ["$joy__zhiji2"] = " ",
  ["$joy__tiaoxin2"] = " ",
  ["~$joy__jiangwei"] = " ",
}


local liushan = General(extension, "joyex__liushan", "shu", 4)
local xiangle = fk.CreateTriggerSkill{
  name = "joyex__xiangle",
  events = {fk.TargetConfirmed},
  anim_type = "defensive",
  frequency = Skill.Compulsory,
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and data.card.trueName == "slash"
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if #room:askForDiscard(room:getPlayerById(data.from), 1, 1, false, self.name, true, ".|.|.|.|.|basic", "#xiangle-discard:"..player.id) == 0 then
      table.insertIfNeed(data.nullifiedTargets, player.id)
    end
  end,
}
local xiangle_delay = fk.CreateTriggerSkill{
  name = "#joyex__xiangle_delay",
  events = {fk.RoundEnd},
  anim_type = "support",
  frequency = Skill.Compulsory,
  can_trigger = function(self, event, target, player, data)
    if target == player and player:hasSkill(self) then
      local events = player.room.logic:getEventsOfScope(GameEvent.UseCard,1, function(e)
        local use = e.data[1]
        return use.from == player.id
      end, Player.HistoryRound)
      return #events == 0
    end
  end,
  on_cost = function (self, event, target, player, data)
    local n = player.seat//2
    local cards = player.room:askForCard(player,n,n,true,self.name,true,".","享乐：你可以弃置"..n.."张牌并翻面，然后发动一次〖放权〗的效果。")
    if #cards == n then
      self.cost_data = cards
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if #self.cost_data > 0 then
      room:throwCard(self.cost_data,self.name,player,player)
    end
    if player.dead then return end
    player:turnOver()
    if player.dead then return end
    local tar, cards = {},{}
    tar = room:askForChoosePlayers(player, table.map(room:getOtherPlayers(player), Util.IdMapper),1,1, "放权：选择一名其他角色，然后交给其任意张牌并令其获得一个额外的回合。","joyex__fangquan",true)
       if #tar > 0  then
      local to =  room:getPlayerById(tar[1])
      cards = room:askForCard(player,1,#player:getCardIds("he"),true,"joyex__fangquan",true,".","你可以交给其任意张牌。")
      if #cards > 0 then
        room:moveCardTo(cards, Card.PlayerHand,to,fk.ReasonGive,"joyex__fangquan", nil,false, player.id)
      end
      if not to.dead then
        to:gainAnExtraTurn()
      end
    end
  end,
}
local fangquan = fk.CreateTriggerSkill{
  name = "joyex__fangquan",
  anim_type = "support",
  events = {fk.EventPhaseChanging},
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(self) and player == target and data.to == Player.Play
  end,
  on_use = function(self, event, target, player, data)
    player:skip(Player.Play)
    return true
  end,
}
local fangquan_delay = fk.CreateTriggerSkill{
  name = "#joyex__fangquan_delay",
  events = {fk.EventPhaseStart},
  mute = true,
  can_trigger = function(self, event, target, player, data)
    return target == player and player.phase == Player.Discard and player:usedSkillTimes(fangquan.name, Player.HistoryTurn) > 0
  end,
  on_cost = Util.TrueFunc,
  on_use = function(self, event, target, player, data)
    local room = player.room
    room:notifySkillInvoked(player, fangquan.name, "support")
    local tar, cards = {},{}
    tar = room:askForChoosePlayers(player, table.map(room:getOtherPlayers(player), Util.IdMapper),1,1, "放权：选择一名其他角色，然后交给其任意张牌并令其获得一个额外的回合。","joyex__fangquan",true)
    if #tar > 0  then
      local to =  room:getPlayerById(tar[1])
      cards = room:askForCard(player,1,#player:getCardIds("he"),true,"joyex__fangquan",true,".","你可以交给其任意张牌。")
      if #cards > 0 then
        room:moveCardTo(cards, Card.PlayerHand,to,fk.ReasonGive,"joyex__fangquan", nil,false, player.id)
      end
      if not to.dead then
        to:gainAnExtraTurn()
      end
    end
  end,
}
local ruoyu = fk.CreateTriggerSkill{
  name = "joyex__ruoyu$",
  frequency = Skill.Wake,
  events = {fk.EventPhaseStart},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and
      player:usedSkillTimes(self.name, Player.HistoryGame) == 0 and
      player.phase == Player.Start
  end,
  can_wake = function(self, event, target, player, data)
    return table.every(player.room:getOtherPlayers(player), function(p) return p.hp >= player.hp end)
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    room:changeMaxHp(player, 1)
    if player:isWounded()  then
      room:recover({
        who = player,
        num = 1,
        recoverBy = player,
        skillName = self.name,
      })
    end
    room:handleAddLoseSkills(player, "joyex__jijiang|ol_ex__sishu", nil, true, false)
  end,
}
fangquan:addRelatedSkill(fangquan_delay)
xiangle:addRelatedSkill(xiangle_delay)
liushan:addSkill(xiangle)
liushan:addSkill(fangquan)
liushan:addSkill(ruoyu)
liushan:addRelatedSkill("ol_ex__sishu")
liushan:addRelatedSkill("joyex__jijiang")
Fk:loadTranslationTable{
  ["joyex__liushan"] = "界刘禅",
  ["#joyex__liushan"] = "无为的真命主",
  ["joyex__xiangle"] = "享乐",
  ["#joyex__xiangle_delay"] = "享乐",
  [":joyex__xiangle"] = "锁定技，<br>①当你成为一名角色使用【杀】的目标后，该角色需弃置一张基本牌，否则此【杀】对你无效;<br>②每轮结束时，若你本轮未使用过牌，你可弃置X张牌并翻面，然后发动一次〖放权〗的效果(X为你座位号的一半，向下取整)。",
  ["#joyex__fangquan_delay"] = "放权",
  ["joyex__fangquan"] = "放权",
  [":joyex__fangquan"] = "你的出牌阶段开始时，你可以跳过此阶段，然后弃牌阶段开始时，你选择一名其他角色，交给其任意张牌并令其获得一个额外的回合。",
  ["joyex__ruoyu"] = "若愚",
  [":joyex__ruoyu"] = "主公技，觉醒技，准备阶段，若你是体力值为场上最小的角色，你增加1点体力上限，回复1点体力，然后获得〖激将〗和〖思蜀〗。",

}
--[[
local liushan = General(extension, "joy__liushan", "shu", 4)
local fangquan = fk.CreateTriggerSkill{
  name = "joy__fangquan",
  anim_type = "support",
  events = {fk.EventPhaseChanging},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and data.to == Player.Play
  end,
  on_use = function(self, event, target, player, data)
    player:skip(Player.Play)
    player.room:setPlayerMark(player, "joy__fangquan_extra", 1)
    return true
  end,

  refresh_events = {fk.TurnEnd},
  can_refresh = function(self, event, target, player, data)
    return target == player and player:getMark("joy__fangquan_extra") > 0
  end,
  on_refresh = function(self, event, target, player, data)
    local room = player.room
    room:setPlayerMark(player, "joy__fangquan_extra", 0)
    local tos = room:askForChoosePlayers(player, table.map(room:getOtherPlayers(player), function(p)
      return p.id end), 1, 1, "#joy__fangquan-give", self.name,false)
    room:getPlayerById(tos[1]):gainAnExtraTurn()
    
  end,
}
local ruoyu = fk.CreateTriggerSkill{
  name = "joy__ruoyu$",
  frequency = Skill.Wake,
  events = {fk.EventPhaseStart},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and
      player:usedSkillTimes(self.name, Player.HistoryGame) == 0 and
      player.phase == Player.Start
  end,
  can_wake = function(self, event, target, player, data)
    return table.every(player.room:getOtherPlayers(player), function(p) return p.hp >= player.hp end)
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    room:changeMaxHp(player, 1)
    if player:isWounded() then 
      room:recover({
        who = player,
        num = 1,
        recoverBy = player,
        skillName = self.name,
      })
    end
    room:handleAddLoseSkills(player, "joy__jijiang", nil, true, false)
  end,
}
local jijiang = fk.CreateViewAsSkill{
  name = "joy__jijiang$",
  anim_type = "offensive",
  pattern = "slash",
  card_filter = Util.FalseFunc,
  view_as = function(self, cards)
    if #cards ~= 0 then
      return nil
    end
    local c = Fk:cloneCard("slash")
    c.skillName = self.name
    return c
  end,
  before_use = function(self, player, use)
    local room = player.room
    if use.tos then
      room:doIndicate(player.id, TargetGroup:getRealTargets(use.tos))
    end

    for _, p in ipairs(room:getOtherPlayers(player)) do
      if p.kingdom == "shu" then
        local cardResponded = room:askForResponse(p, "slash", "slash", "#joy__jijiang-ask:" .. player.id, true)
        if cardResponded then
          room:responseCard({
            from = p.id,
            card = cardResponded,
            skipDrop = true,
          })

          use.card = cardResponded
          if not p.dead and not player.dead then
            player:drawCards(1,self.name)
            p:drawCards(1,self.name)
          end
          return
        end
      end
    end

    room:setPlayerMark(player, "joy__jijiang-failed-phase", 1)
    return self.name
  end,
  enabled_at_play = function(self, player)
    return player:getMark("joy__jijiang-failed-phase") == 0 and not table.every(Fk:currentRoom().alive_players, function(p)
      return p == player or p.kingdom ~= "shu"
    end)
  end,
  enabled_at_response = function(self, player)
    return not table.every(Fk:currentRoom().alive_players, function(p)
      return p == player or p.kingdom ~= "shu"
    end)
  end,
}
liushan:addSkill("xiangle")
liushan:addSkill(fangquan)
liushan:addSkill(ruoyu)
liushan:addRelatedSkill(jijiang)
Fk:loadTranslationTable{
  ["joy__liushan"] = "刘禅",
  ["#joy__liushan"] = "无为的真命主",

  ["joy__fangquan"] = "放权",
  [":joy__fangquan"] = "你可以跳过你的出牌阶段，然后此回合结束时，令一名其他角色进行一个额外的回合。",
  ["#joy__fangquan-give"] = "选择一名其他角色进行一个额外的回合",
  ["joy__ruoyu"] = "若愚",
  [":joy__ruoyu"] = "主公技，觉醒技，准备阶段，若你是体力值为场上最小的角色，你增加1点体力上限，回复1点体力，然后获得“激将”。",
  ["joy__jijiang"] = "激将",
  [":joy__jijiang"] = "主公技，其他蜀势力角色可以在你需要时代替你使用或打出【杀】，若以此法出【杀】，则你与其各摸一张牌。",

  ["#joy__jijiang-ask"] =  "激将：你可代替 %src 打出一张杀，然后其与你各摸一张牌"

}
]]

local zhanghe = General(extension, "joy__zhanghe", "wei", 4)
local qiaobian = fk.CreateTriggerSkill{
  name = "joy__qiaobian",
  anim_type = "offensive",
  events = {fk.EventPhaseChanging,fk.EventPhaseStart},
  can_trigger = function(self, event, target, player, data)
    if event == fk.EventPhaseStart then
      return target == player and player.phase == Player.Finish and player:hasSkill(self) and player:getMark("joy__qiaobian-turn") > 2 and not player.dead
    else
      return target == player and player:hasSkill(self) and not player:isKongcheng() and
    data.to > Player.Start and data.to < Player.Finish
    end
  end,
  on_cost = function(self, event, target, player, data)
    if event == fk.EventPhaseStart then return true end
    local phase_name_table = {
      [3] = "phase_judge",
      [4] = "phase_draw",
      [5] = "phase_play",
      [6] = "phase_discard",
    }
    local card = player.room:askForDiscard(player, 1, 1, false, self.name, true, ".", "#joy__qiaobian-invoke:::" .. phase_name_table[data.to], true)
    if #card > 0 then
      self.cost_data = card
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if event == fk.EventPhaseStart then
      player:drawCards(2,self.name)
    else
    room:throwCard(self.cost_data, self.name, player, player)
    player:skip(data.to)
    room:addPlayerMark(player,"joy__qiaobian-turn",1)
    if data.to == Player.Draw then
      local targets = table.map(table.filter(room:getOtherPlayers(player), function(p)
        return not p:isKongcheng() end), Util.IdMapper)
      if #targets > 0 then
        local n = math.min(2, #targets)
        local tos = room:askForChoosePlayers(player, targets, 1, n, "#joy__qiaobian-choose:::"..n, self.name, true)
        if #tos > 0 then
          room:sortPlayersByAction(tos)
          for _, id in ipairs(tos) do
            local p = room:getPlayerById(id)
            if not p:isKongcheng() then
              local card_id = room:askForCardChosen(player, p, "h", self.name)
              room:obtainCard(player, card_id, false, fk.ReasonPrey)
            end
          end
        end
      end
    elseif data.to == Player.Play then
      local targets = room:askForChooseToMoveCardInBoard(player, "#joy__qiaobian-move", self.name, true, nil)
      if #targets ~= 0 then
        targets = table.map(targets, function(id) return room:getPlayerById(id) end)
        room:askForMoveCardInBoard(player, targets[1], targets[2], self.name)
      end
    end
    return true
    end
  end,
}
zhanghe:addSkill(qiaobian)
Fk:loadTranslationTable{
  ["joy__zhanghe"] = "张郃",
  ["#joy__zhanghe"] = "料敌机先",

  ["joy__qiaobian"] = "巧变",
  [":joy__qiaobian"] = "<rbr>①你的阶段开始前（准备阶段和结束阶段除外），你可以弃置一张手牌跳过该阶段。若以此法跳过摸牌阶段，"..
  "你可以获得至多两名其他角色的各一张手牌；若以此法跳过出牌阶段，你可以将场上的一张牌移动至另一名角色相应的区域内。"..
  "<br>②回合结束时，若你以此法跳过了至少三个阶段，则你摸两张牌。",
  ["#joy__qiaobian-invoke"] = "巧变：你可以弃一张手牌，跳过 %arg",
  ["#joy__qiaobian-choose"] = "巧变：你可以依次获得%arg名角色的各一张手牌",
  ["#joy__qiaobian-move"] = "巧变：请选择两名角色，移动场上的一张牌",

  ["$joy__qiaobian1"] = "兵无常势，水无常形。",
  ["$joy__qiaobian2"] = "用兵之道，变化万千。",
  ["~joy__zhanghe"] = "啊……膝盖……中箭了……",
}

local dengai = General(extension, "joy__dengai", "wei", 4)
local tuntian = fk.CreateTriggerSkill{
  name = "joy__tuntian",
  anim_type = "special",
  derived_piles = "dengai_field",
  events = {fk.AfterCardsMove,fk.TurnEnd},
  can_trigger = function(self, event, target, player, data)
    if event == fk.AfterCardsMove then
    if player:hasSkill(self) and player.phase == Player.NotActive then
      for _, move in ipairs(data) do
        if move.from == player.id then
          for _, info in ipairs(move.moveInfo) do
            if info.fromArea == Card.PlayerHand or info.fromArea == Card.PlayerEquip then
              return true
            end
          end
        end
      end
    end
    elseif event == fk.TurnEnd then
      return target == player and player:hasSkill(self) and not player:isKongcheng()
    end
  end,
  on_cost = function (self, event, target, player, data)
    if event == fk.TurnEnd then
      local card = player.room:askForDiscard(player,1,1,false,self.name,true,nil,"#joy__tuntian",true)
      if #card > 0 then
        self.cost_data = card
        return true
      end
    else
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if event == fk.TurnEnd then
      room:throwCard(self.cost_data, self.name, player, player)
    end
    if player.dead then return end
    local judge = {
      who = player,
      reason = self.name,
      pattern = ".|.|spade,club,diamond",
    }
    room:judge(judge)
  end,

  refresh_events = {fk.FinishJudge},
  can_refresh = function(self, event, target, player, data)
    return player:hasSkill(self) and data.reason == self.name
  end,
  on_refresh = function(self, event, target, player, data)
    if data.card.suit ~= Card.Heart and player.room:getCardArea(data.card) == Card.Processing then
      player:addToPile("dengai_field", data.card, true, self.name)
    end
  end,
}
local tuntian_distance = fk.CreateDistanceSkill{
  name = "#joy__tuntian_distance",
  correct_func = function(self, from, to)
    if from:hasSkill(self) then
      return -#from:getPile("dengai_field")
    end
  end,
}
tuntian:addRelatedSkill(tuntian_distance)
dengai:addSkill(tuntian)
dengai:addSkill("zaoxian")
dengai:addRelatedSkill("jixi")
Fk:loadTranslationTable{
  ["joy__dengai"] = "邓艾",
  ["#dengai"] = "矫然的壮士",
  
  ["joy__tuntian"] = "屯田",
  [":joy__tuntian"] = "当你于回合外失去牌后，你可以进行判定：若结果不为<font color='red'>♥</font>，你将生效后的判定牌置于你的武将牌上，称为“田”；<br>回合结束时，你可以弃置一张手牌进行一次“田”判定；"..
  "<br>你计算与其他角色的距离-X（X为“田”的数量）。",
  ["#joy__tuntian"] = "屯田：你可以弃置一张手牌进行一次“田”判定。",
}

local sunce = General(extension, "joyex__sunce", "wu", 4)
local jiang = fk.CreateTriggerSkill{
  name = "joyex__jiang",
  anim_type = "drawcard",
  events ={fk.TargetSpecified, fk.TargetConfirmed},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and data.firstTarget and
      (data.card.trueName == "slash"or data.card.name == "duel")
  end,
  on_use = function(self, event, target, player, data)
    player:drawCards(1, self.name)
  end,
}
local hunzi = fk.CreateTriggerSkill{
  name = "joyex__hunzi",
  frequency = Skill.Wake,
  events = {fk.GameStart,fk.HpChanged},
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(self) and
      player:usedSkillTimes(self.name, Player.HistoryGame) == 0
  end,
  can_wake = function(self, event, target, player, data)
    return player.hp == 1
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if player:hasSkill("joyex__zhiba") and player.role == "lord" and #room.players >= 5 then lord = true end
    if player.general == "joyex__sunce" then 
      room:notifySkillInvoked(player, "joyex__hunzi","drawcard")
      room:changeHero(player,"joy_skin__sunce",false,false,true,false)
      if lord then room:handleAddLoseSkills(player,"joyex__zhiba",nil) end
    elseif player.deputyGeneral == "joyex__sunce" then
      room:notifySkillInvoked(player, "joyex__hunzi","drawcard")
      room:changeHero(player,"joy_skin__sunce",false,true,true,false)
      if lord then room:handleAddLoseSkills(player,"joyex__zhiba",nil) end
    end
    room:changeMaxHp(player, -1)
    room:handleAddLoseSkills(player, "ex__yingzi|joy__yinghun", nil, true, false)
  end,
}
local yinghun = fk.CreateTriggerSkill{
  name = "joy__yinghun",
  anim_type = "control",
  mute = true,
  events = {fk.EventPhaseStart},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and player.phase == Player.Start
  end,
  on_cost = function(self, event, target, player, data)
    local to
    if player:getLostHp() > 0 then
      to = player.room:askForChoosePlayers(player, table.map(player.room:getOtherPlayers(player), Util.IdMapper), 1, 1, "#joy__yinghun-discard:::"..player:getLostHp(), self.name, true)
    else
      to = player.room:askForChoosePlayers(player, table.map(player.room:getOtherPlayers(player), Util.IdMapper), 1, 1, "#joy__yinghun-drawcard", self.name, true)
    end
    if #to > 0 then
      self.cost_data = to[1]
      return true
    end
  end,
  on_use = function (self,event,target,player,data)
    local p = player.room:getPlayerById(self.cost_data)
    if not p.dead then
      player.room:drawCards(p,1,self.name)
    end
    if not p.dead and player:getLostHp() > 0 then
      player.room:askForDiscard(p,player:getLostHp(),player:getLostHp(),true,self.name,false)
    end
  end
}
local zhiba = fk.CreateTriggerSkill{
  name = "joyex__zhiba$",
  mute = true,
  refresh_events = {fk.EventAcquireSkill, fk.EventLoseSkill, fk.BuryVictim, fk.AfterPropertyChange},
  can_refresh = function(self, event, target, player, data)
    if event == fk.EventAcquireSkill or event == fk.EventLoseSkill then
      return data == self
    elseif event == fk.BuryVictim then
      return target:hasSkill(self, true, true)
    elseif event == fk.AfterPropertyChange then
      return target == player
    end
  end,
  on_refresh = function(self, event, target, player, data)
    local room = player.room
    local attached_huangtian = player.kingdom == "wu" and table.find(room.alive_players, function (p)
      return p ~= player and p:hasSkill(self, true)
    end)
    if attached_huangtian and not player:hasSkill("joyex__zhiba_other&", true, true) then
      room:handleAddLoseSkills(player, "joyex__zhiba_other&", nil, false, true)
    elseif not attached_huangtian and player:hasSkill("joyex__zhiba_other&", true, true) then
      room:handleAddLoseSkills(player, "-joyex__zhiba_other&", nil, false, true)
    end
  end,
}
local zhiba_other = fk.CreateActiveSkill{
  name = "joyex__zhiba_other&",
  anim_type = "support",
  prompt = "#joyex__zhiba-active",
  mute = true,
  can_use = function(self, player)
    if player:usedSkillTimes(self.name, Player.HistoryPhase) < 1 and player.kingdom == "wu" then
      return table.find(Fk:currentRoom().alive_players, function(p) return p:hasSkill("joyex__zhiba") and p ~= player end)
    end
    return false
  end,
  card_num = 1,
  card_filter = function(self, to_select, selected)
    return #selected < 1 and (Fk:getCardById(to_select).trueName == "slash" or Fk:getCardById(to_select).name == "duel")
  end,
  target_num = 0,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    local targets = table.filter(room.alive_players, function(p) return p:hasSkill("joyex__zhiba") and p ~= player end)
    local target
    if #targets == 1 then
      target = targets[1]
    else
      target = room:getPlayerById(room:askForChoosePlayers(player, table.map(targets, Util.IdMapper), 1, 1, nil, self.name, false)[1])
    end
    if not target then return false end
    room:notifySkillInvoked(player, "joyex__zhiba")
    player:broadcastSkillInvoke("joyex__zhiba")
    room:doIndicate(effect.from, { target.id })
    room:moveCardTo(effect.cards, Player.Hand, target, fk.ReasonGive, self.name, nil, true)
  end,
}
sunce:addSkill(jiang)
sunce:addSkill(hunzi)
sunce:addRelatedSkill("ex__yingzi")
sunce:addRelatedSkill(yinghun)
sunce:addSkill(zhiba)
Fk:addSkill(zhiba_other)
Fk:loadTranslationTable{
  ["joyex__sunce"] = "界孙策",
  ["#joyex__sunce"] = "江东的小霸王",
  ["joyex__jiang"] = "激昂",
  [":joyex__jiang"] = "当你使用【决斗】或【杀】指定目标后，或成为【决斗】或【杀】的目标后，你可以摸一张牌。",
  ["joyex__hunzi"] = "魂姿",
  [":joyex__hunzi"] = "觉醒技，当你的体力值为1时，你减1点体力上限，然后获得〖英姿〗和〖英魂〗。",
  ["joyex__zhiba"] = "制霸",
  [":joyex__zhiba"] = "主公技，其他吴势力角色的出牌阶段限一次，其可以交给你一张【杀】或【决斗】。",
  --抄的标张角主公技

  ["joyex__zhiba_other&"] = "制霸",
  [":joyex__zhiba_other&"] = "出牌阶段限一次，你可以交给孙策一张【杀】或【决斗】。",
  ["#joyex__zhiba-active"] = "制霸：你可以交给一名拥有“制霸”的角色一张【杀】或【决斗】。",
  ["joy__yinghun"] = "英魂",
  [":joy__yinghun"] = "准备阶段，你可以令一名其他角色摸一张牌，然后弃X张牌（X为你已损失的体力值）",

  ["#joy__yinghun-drawcard"] = "英魂:你可以选择一名其他角色摸一张牌。",
  ["#joy__yinghun-discard"] = "英魂：你可以选择一名其他角色摸一张牌，然后弃%arg张牌",

  ["$joyex__jiang1"] = " ",
  ["$joyex__hunzi1"] = " ",
  ["$joyex__zhiba1"] = " ",
  ["$joy__yinghun1"] = " ",
  ["$joyex__jiang2"] = " ",
  ["$joyex__hunzi2"] = " ",
  ["$joyex__zhiba2"] = " ",
  ["$joy__yinghun2"] = " ",
  ["$ex__yingzi_joyex__sunce1"] = " ",
  ["$ex__yingzi_joyex__sunce2"] = " ",
  ["$~joyex__sunce"] = " ",
}

local joyex__beige = fk.CreateTriggerSkill{
  name = "joyex__beige",
  anim_type = "masochism",
  events = {fk.Damaged},
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(self) and data.card and data.card.trueName == "slash" and not data.to.dead and not player:isNude()
  end,
  on_cost = function(self, event, target, player, data)
    local card = player.room:askForDiscard(player, 1, 1, true, self.name, true, ".", "#joyex__beige-discard::"..target.id, true)
    if #card > 0 then
      self.cost_data = card
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    room:doIndicate(player.id, {target.id})
    if player.dead then return false end
    local dis_card = Fk:getCardById(self.cost_data[1])
    room:throwCard(self.cost_data, self.name, player, player)
    if dis_card.color == Card.Red then
      if not target.dead and target:isWounded() then
        room:recover{
          who = target,
          num = 1,
          recoverBy = player,
          skillName = self.name
        }
      end
      if not target.dead then
        target:drawCards(2, self.name)
      end
    elseif dis_card.suit == Card.Club then
      if data.from and not data.from.dead then
        room:askForDiscard(data.from, 2, 2, true, self.name, false)
      end
    elseif dis_card.suit == Card.Spade then
      if data.from and not data.from.dead then
        data.from:turnOver()
      end
    end
  end,
}
local caiwenji = General(extension, "joyex__caiwenji", "qun", 3, 3, General.Female)
caiwenji:addSkill(joyex__beige)
caiwenji:addSkill("duanchang")

Fk:loadTranslationTable{
  ["joyex__caiwenji"] = "界蔡文姬",
  ["#joyex__caiwenji"] = "异乡的孤女",
  
  ["joyex__beige"] = "悲歌",
  [":joyex__beige"] = "当一名角色受到【杀】造成的伤害后，你可以弃置一张牌，若弃置的牌为："..
  "<font color='red'>红色</font>，其回复1点体力并摸两张牌；"..
  "♣，伤害来源弃置两张牌；♠，伤害来源翻面。",

  ["#joyex__beige-discard"] = "悲歌：你可以弃置一张牌令%dest根据弃置牌的花色或颜色执行对应效果",

}
------------------------阴--------------------------------


local yanyan = General(extension, "joy__yanyan", "shu", 4)
local joy__juzhan = fk.CreateTriggerSkill{
  name = "joy__juzhan",
  mute = true,
  events = {fk.TargetSpecified, fk.TargetConfirmed},
  can_trigger = function(self, event, target, player, data)
    if not (target == player and player:hasSkill(self) and data.card.trueName == "slash") then return false end
    if event == fk.TargetConfirmed then
      return player.id ~= data.from and not player.room:getPlayerById(data.from).dead
    else
      return not player.room:getPlayerById(data.to):isNude()
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    player:broadcastSkillInvoke(self.name)
    if event == fk.TargetConfirmed then
        room:notifySkillInvoked(player, self.name, "defensive")
        local from = room:getPlayerById(data.from)
        player:drawCards(1, self.name)
        if from.dead then return end
        from:drawCards(1, self.name)
        if from.dead then return end
        local mark = from:getTableMark(  "@@joy__juzhan-turn")
        table.insertIfNeed(mark, player.id)
        room:setPlayerMark(from, "@@joy__juzhan-turn", mark)
    else
        room:notifySkillInvoked(player, self.name, "control")
        local to = room:getPlayerById(data.to)
        room:moveCardTo(room:askForCardChosen(player, to, "he", self.name), Card.PlayerHand, player, fk.ReasonPrey, self.name, "", false, player.id)
        local mark = player:getTableMark(  "joy__juzhan_red-turn")
        table.insertIfNeed(mark, to.id)
        room:setPlayerMark(player, "joy__juzhan_red-turn", mark)
    end
  end,
}
local joy__juzhan_prohibit = fk.CreateProhibitSkill{
  name = "#joy__juzhan_prohibit",
  is_prohibited = function(self, from, to, card)
    return table.contains(from:getTableMark(  "@@joy__juzhan-turn"), to.id)
    or (card and card.color == Card.Red and card.trueName == "slash" and table.contains(from:getTableMark(  "joy__juzhan_red-turn"), to.id))
  end,
}
joy__juzhan:addRelatedSkill(joy__juzhan_prohibit)
yanyan:addSkill(joy__juzhan)
Fk:loadTranslationTable{
  ["joy__yanyan"] = "严颜",
  ["#joy__yanyan"] = "断头将军",

  ["joy__juzhan"] = "拒战",
  [":joy__juzhan"] = "当你成为其他角色使用【杀】的目标后，你可以与其各摸一张牌，然后其本回合不能再对你使用牌；当你使用【杀】指定一名角色为目标后，你可以获得其一张牌，然后你本回合不能对其使用红色【杀】。",
  ["@@joy__juzhan-turn"] = "拒战",

  ["~yanyan"] = "宁可断头死，安能屈膝降！",
  ["$joy__juzhan1"] = "砍头便砍头，何为怒耶！",
  ["$joy__juzhan2"] = "我州但有断头将军，无降将军也！",
}

local wangji = General(extension, "joyex__wangji", "wei", 3)
local joyex__qizhi = fk.CreateTriggerSkill{
  name = "joyex__qizhi",
  anim_type = "control",
  events = {fk.TargetSpecified},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and player.phase ~= Player.NotActive and
      data.firstTarget and data.card.type ~= Card.TypeEquip
  end,
  on_cost = function(self, event, target, player, data)
    local room = player.room
    local targets = table.map(table.filter(room.alive_players, function(p)
      return not p:isNude() and not table.contains(AimGroup:getAllTargets(data.tos), p.id) end), function(p) return p.id end)
    if #targets == 0 then return end
    local tos = room:askForChoosePlayers(player, targets, 1, 1, "#joyex__qizhi-choose:::"..data.card:getTypeString(), self.name, true)
    if #tos > 0 then
      self.cost_data = tos[1]
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    room:addPlayerMark(player, "@joyex__qizhi-turn", 1)
    local to = room:getPlayerById(self.cost_data)
    local id = room:askForCardChosen(player, to, "he", self.name)
    local type = Fk:getCardById(id).type
    room:throwCard({id}, self.name, to, player)
    if type == data.card.type and not player.dead then
      player:drawCards(1, self.name)
    elseif type ~= data.card.type and not to.dead then
      to:drawCards(1, self.name)
    end
  end,
}
local joyex__jinqu = fk.CreateTriggerSkill{
  name = "joyex__jinqu",
  anim_type = "drawcard",
  events = {fk.EventPhaseChanging},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and data.to == Player.Discard
  end,
  on_use = function(self, event, target, player, data)
    player:drawCards(2, self.name)
    local n = player:getHandcardNum() - player:usedSkillTimes("joyex__qizhi", Player.HistoryTurn) - 1
    if n > 0 then
      player.room:askForDiscard(player, n, n, false, self.name, false)
    end
    return true
  end,
}
wangji:addSkill(joyex__qizhi)
wangji:addSkill(joyex__jinqu)
Fk:loadTranslationTable{
  ["joyex__wangji"] = "界王基",
  ["joyex__qizhi"] = "奇制",
  [":joyex__qizhi"] = "当你于回合内使用基本牌或锦囊牌指定目标后，你可以弃置不为此牌目标的一名角色一张牌。若弃置的牌与你使用的牌类型相同，"..
  "你摸一张牌；类型不同，其摸一张牌。",
  ["joyex__jinqu"] = "进趋",
  [":joyex__jinqu"] = "弃牌阶段开始前，你可以跳过此阶段并摸两张牌，然后将手牌弃至X张（X为你本回合发动〖奇制〗次数+1）。",
  ["@joyex__qizhi-turn"] = "奇制",
  ["#joyex__qizhi-choose"] = "奇制：弃置一名角色一张牌，若为%arg，你摸一张牌，否则其摸一张牌",
}

local luji = General(extension, "joy__luji", "wu", 3)
local zhenglun = fk.CreateTriggerSkill{
  name = "joy__zhenglun",
  events = { fk.DrawNCards },
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and player:getMark("@orange") == 0
  end,
  on_cost = function (self, event, target, player, data)
    return player.room:askForSkillInvoke(player,self.name,data,"#joy__zhenglun")
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    data.n = data.n - 2
    room:addPlayerMark(player, "@orange")
  end,
}
luji:addSkill("huaiju")
luji:addSkill("yili")
luji:addSkill(zhenglun)
Fk:loadTranslationTable{
  ["joy__luji"] = "陆绩",
  ["#joy__luji"] = "瑚琏之器",

  ["joy__zhenglun"] = "整论",
  [":joy__zhenglun"] = "摸牌阶段，若你没有“橘”，你可以少摸一张牌并获得1枚“橘”。",
  ["#joy__zhenglun"] = "整论：是否少摸一张牌获得一枚“橘”？",
}

local xuyou = General(extension, "joy__xuyou", "qun", 3)
local chenglue = fk.CreateActiveSkill{
  name = "joy__chenglue",
  anim_type = "offensive",
  prompt = "#joy__xuyou-active",
  can_use = function(self, player)
    return player:usedSkillTimes(self.name, Player.HistoryPhase) < 1
  end,
  card_filter = Util.FalseFunc,
  target_filter = function(self, to_select, selected, selected_cards)
    return false
  end,
  on_use = function(self, room, effect)
    local from = room:getPlayerById(effect.from)

    from:drawCards(1, self.name)
    local cardsDiscarded = table.filter(room:askForDiscard(from, 1, 1, false, self.name, false), function(id)
      return Fk:getCardById(id).suit < Card.NoSuit
    end)

    if #cardsDiscarded > 0 then
      local suitsToRecord = table.map(cardsDiscarded, function(id)
        return Fk:getCardById(id):getSuitString(true)
      end)

      local suitsRecorded = type(from:getMark("@joy__chenglue-turn")) == "table" and from:getMark("@joy__chenglue-turn") or {}
      for _, suit in ipairs(suitsToRecord) do
        table.insertIfNeed(suitsRecorded, suit)
      end
      room:setPlayerMark(from, "@joy__chenglue-turn", suitsRecorded)
    end
  end,
}
local chenglue_targetmod = fk.CreateTargetModSkill{
  name = "#joy__chenglue_targetmod",
  bypass_times = function(self, player, skill, scope, card, to)
    return card and player:getMark("@joy__chenglue-turn") ~= 0 and table.contains(player:getMark("@joy__chenglue-turn"), "log_"..card:getSuitString())
  end,
  bypass_distances = function(self, player, skill, card, to)
    return card and player:getMark("@joy__chenglue-turn") ~= 0 and table.contains(player:getMark("@joy__chenglue-turn"), "log_"..card:getSuitString())
  end,
}
local shicai = fk.CreateTriggerSkill{
  name = "joy__shicai",
  events = {fk.CardUseFinished, fk.TargetConfirmed},
  anim_type = "drawCard",
  can_trigger = function(self, event, target, player, data)
    if
      not (
        target == player and
        player:hasSkill(self) and
        (data.extra_data or {}).firstCardTypeUsed and
        player.room:getCardArea(data.card) == Card.Processing
      )
    then
      return false
    end

    if event == fk.CardUseFinished then
      return data.card:isCommonTrick() or data.card.type == Card.TypeBasic
    else
      return data.from == player.id and data.card.type == Card.TypeEquip
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local toPut = room:getSubcardsByRule(data.card, { Card.Processing })

    if #toPut > 1 then
      toPut = room:askForGuanxing(player, toPut, { #toPut, #toPut }, { 0, 0 }, "joy__ShiCaiPut", true).top
    end

    room:moveCardTo(table.reverse(toPut), Card.DrawPile, nil, fk.ReasonPut, self.name, nil, true)
    player:drawCards(1, self.name)
  end,

  refresh_events = {fk.EventPhaseStart, fk.AfterCardUseDeclared},
  can_refresh = function(self, event, target, player, data)
    if target ~= player then
      return false
    end

    if event == fk.EventPhaseStart then
      return
        player.phase == Player.NotActive and
        table.find(player.room.alive_players, function(p)
          return type(p:getMark("@joy__shicai")) == "table"
        end)
    else
      return
        player:hasSkill(self.name, true) and
        (type(player:getMark("@joy__shicai")) ~= "table" or
        not table.contains(player:getMark("@joy__shicai"), data.card:getTypeString() .. "_char"))
    end
  end,
  on_refresh = function(self, event, target, player, data)
    local room = player.room
    if event == fk.EventPhaseStart then
      for _, p in ipairs(room.alive_players) do
        if type(p:getMark("@joy__shicai")) == "table" then
          room:setPlayerMark(p, "@joy__shicai", 0)
        end
      end
    else
      local typesRecorded = type(player:getMark("@joy__shicai")) == "table" and player:getMark("@joy__shicai") or {}
      table.insert(typesRecorded, data.card:getTypeString() .. "_char")
      room:setPlayerMark(player, "@joy__shicai", typesRecorded)

      data.extra_data = data.extra_data or {}
      data.extra_data.firstCardTypeUsed = true
    end
  end,
}
local cunmu = fk.CreateTriggerSkill{
  name = "joy__cunmu",
  events = {fk.BeforeDrawCard},
  anim_type = "negative",
  frequency = Skill.Compulsory,
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self)
  end,
  on_use = function(self, event, target, player, data)
    data.fromPlace = "bottom"
  end,
}
chenglue:addRelatedSkill(chenglue_targetmod)
xuyou:addSkill(chenglue)
xuyou:addSkill(shicai)
xuyou:addSkill(cunmu)
Fk:loadTranslationTable{
  ["joy__xuyou"] = "许攸",
  ["#joy__xuyou"] = "朝秦暮楚",

  ["joy__chenglue"] = "成略",
  [":joy__chenglue"] = "出牌阶段限一次，你可以摸一张牌，然后弃置一张手牌。"..
  "<br>若如此做，你于此阶段内使用与你以此法弃置的牌花色相同的牌无距离和次数限制。",
  ["joy__shicai"] = "恃才",
  [":joy__shicai"] = "当你每回合首次使用一种类别的牌结算结束后，你可以将之置于牌堆顶，然后摸一张牌。",
  ["joy__cunmu"] = "寸目",
  [":joy__cunmu"] = "锁定技，当你摸牌时，改为从牌堆底摸牌。",
  ["@joy__chenglue-turn"] = "成略",
  ["@joy__shicai"] = "恃才",
  ["#joy__xuyou-active"] = "成略：你可以摸一张牌，然后弃置一张牌，本回合使用弃置牌花色的牌无距离次数限制。",

  ["$joy__chenglue1"] = " ",
  ["$joy__chenglue2"] = " ",
  ["$joy__shicai1"] = " ",
  ["$joy__shicai2"] = " ",
  ["$joy__cunmu_xuyou1"] = " ",
  ["$joy__cunmu_xuyou2"] = " ",
  ["~joy__xuyou"] = " ",
}

-------------------------雷---------------------------

local zhugezhan = General(extension, "joy__zhugezhan", "shu", 3)
local zuilun = fk.CreateTriggerSkill{
  name = "joy__zuilun",
  anim_type = "control",
  events = {fk.EventPhaseStart},
  can_trigger = function(self, event, target, player, data)
    return target == player and target:hasSkill(self) and player.phase == Player.Finish
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local n = 0
    local events = player.room.logic:getEventsOfScope(GameEvent.ChangeHp, 1, function(e)
      local damage = e.data[5]
      return damage and player == damage.from
    end, Player.HistoryTurn)
    if #events > 0 then
      n = n + 1
    end
    events = room.logic:getEventsOfScope(GameEvent.MoveCards, 1, function(e)
      for _, move in ipairs(e.data) do
        if move.from == player.id and move.moveReason == fk.ReasonDiscard and table.find(move.moveInfo, function (info)
          return info.fromArea == Card.PlayerHand or info.fromArea == Card.PlayerEquip
        end) then
          return true
        end
      end
    end, Player.HistoryTurn)
    if #events == 0 then
      n = n + 1
    end
    if table.every(room.alive_players, function(p) return p:getHandcardNum() >= player:getHandcardNum() end) then
      n = n + 1
    end
    local cards = room:getNCards(4)
    local result = room:askForGuanxing(player, cards, {4 - n, 4 - n}, {n, n}, self.name, true, {"zuilun_top", "zuilun_get"})
    if #result.top > 0 then
      for i = #result.top, 1, -1 do
        table.insert(room.draw_pile, 1, result.top[i])
      end
      room:sendLog{
        type = "#GuanxingResult",
        from = player.id,
        arg = #result.top,
        arg2 = #result.bottom,
      }
    end
    if #result.bottom > 0 then
      room:moveCardTo(result.bottom, Player.Hand, player, fk.ReasonJustMove, self.name, "", false, player.id)
    end
    if n == 0 and not player.dead then
      local targets = table.map(room:getOtherPlayers(player), Util.IdMapper)
      local to = room:askForChoosePlayers(player, targets, 1, 1, "#zuilun-choose", self.name, false)
      if #to > 0 then
        to = room:getPlayerById(to[1])
      else
        to = room:getPlayerById(table.random(targets))
      end
      room:loseHp(player, 1, self.name)
      room:loseHp(to, 1, self.name)
    end
  end,
}
zhugezhan:addSkill(zuilun)
zhugezhan:addSkill("fuyin")
Fk:loadTranslationTable{
  ["joy__zhugezhan"] = "诸葛瞻",
  ["#joy__zhugezhan"] = "临难死义",
  ["joy__zuilun"] = "罪论",
  [":joy__zuilun"] = "结束阶段，你可以观看牌堆顶四张牌，你每满足以下一项便获得其中的一张，然后以任意顺序放回其余的牌：1.你于此回合内造成过伤害；"..
  "2.你于此回合内未弃置过牌；3.手牌数为全场最少。若均不满足，你与一名其他角色失去1点体力。",
}



local haozhao = General(extension, "joy__haozhao", "wei", 4)
haozhao:addSkill("zhengu")
Fk:loadTranslationTable{
  ["joy__haozhao"] = "郝昭",
  ["#joy__haozhao"] = "扣弦的豪将",
}

local zhoufei = General(extension, "joy__zhoufei", "wu", 3, 3, General.Female)
local liangyin = fk.CreateTriggerSkill{
  name = "joy__liangyin",
  mute = true,
  events = {fk.AfterCardsMove},
  can_trigger = function(self, event, target, player, data)
    if player:hasSkill(self) then
      for _, move in ipairs(data) do
        if move.toArea == Card.PlayerSpecial then
          for _, info in ipairs(move.moveInfo) do
            if info.fromArea ~= Card.PlayerSpecial then
              return true
            end
          end
        end
        if move.toArea == Card.PlayerHand then
          for _, info in ipairs(move.moveInfo) do
            if info.fromArea == Card.PlayerSpecial then
              return table.find(player.room.alive_players, function(p)
                return  not p:isNude() end)
            end
          end
        end
      end
    end
  end,
  on_trigger = function(self, event, target, player, data)
    local dat = ""
    for _, move in ipairs(data) do
      if move.toArea == Card.PlayerSpecial then
        for _, info in ipairs(move.moveInfo) do
          if info.fromArea ~= Card.PlayerSpecial then
            dat = "drawcard"
          end
        end
      end
      if move.toArea ~= Card.PlayerSpecial then
        for _, info in ipairs(move.moveInfo) do
          if info.fromArea == Card.PlayerSpecial then
            dat = "discard"
          end
        end
      end
    end
    if dat ~= "" then
      self:doCost(event, target, player, dat)
    end
  end,
  on_cost = function(self, event, target, player, data)
    local room = player.room
    local targets
    if data == "drawcard" then
      targets = table.map(room.alive_players, Util.IdMapper)
    elseif data == "discard" then
      targets = table.map(table.filter(room.alive_players, function(p)
        return not p:isNude() end), Util.IdMapper)
    end
    local to = room:askForChoosePlayers(player, targets, 1, 1, "#joy__liangyin-"..data, self.name, true)
    if #to > 0 then
      self.cost_data = to[1]
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local to = room:getPlayerById(self.cost_data)
    if data == "drawcard" then
      room:notifySkillInvoked(player, self.name, "support")
      player:broadcastSkillInvoke(self.name)
      to:drawCards(1, self.name)
    elseif data == "discard" then
      room:notifySkillInvoked(player, self.name, "control")
      player:broadcastSkillInvoke(self.name)
      room:askForDiscard(to, 1, 1, true, self.name, false)
    end
  end,
}
zhoufei:addSkill(liangyin)
zhoufei:addSkill("kongsheng")
Fk:loadTranslationTable{
  ["joy__zhoufei"] = "周妃",
  ["#joy__zhoufei"] = "软玉温香",

  ["joy__liangyin"] = "良姻",
  [":joy__liangyin"] = "当有牌移出游戏时，你可以令一名角色摸一张牌；当有牌从游戏外加入任意角色手牌时，你可以令一名角色弃置一张牌。",
  ["#joy__liangyin-drawcard"] = "良姻：你可以令一名角色摸一张牌",
  ["#joy__liangyin-discard"] = "良姻：你可以令一名角色弃置一张牌",
}


local lukang = General(extension, "joy__lukang", "wu", 4)
lukang:addSkill("qianjie")
local jueyan = fk.CreateActiveSkill{
  name = "joy__jueyan",
  can_use = function (self, player)
    if player:usedSkillTimes(self.name, Player.HistoryPhase) == 0 then
      local slots = player:getAvailableEquipSlots()
      return table.contains(slots, Player.WeaponSlot) or table.contains(slots, Player.ArmorSlot) or
      (table.contains(slots, Player.OffensiveRideSlot) and table.contains(slots, Player.DefensiveRideSlot))
    end
  end,
  card_filter = Util.FalseFunc,
  card_num = 0,
  target_num = 0,
  interaction = function()
    local slots = Self:getAvailableEquipSlots()
    local choices = {}
    if table.contains(slots, Player.WeaponSlot) then table.insert(choices, "WeaponSlot") end
    if table.contains(slots, Player.ArmorSlot) then table.insert(choices, "ArmorSlot") end
    if table.contains(slots, Player.OffensiveRideSlot) and table.contains(slots, Player.DefensiveRideSlot) then
      table.insert(choices, "RideSlot")
    end
    if #slots == 0 then return end
    return UI.ComboBox {choices = choices}
  end,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    local choice = self.interaction.data
    if choice == "RideSlot" then
      choice = {Player.OffensiveRideSlot, Player.DefensiveRideSlot}
    end
    room:abortPlayerArea(player, choice)
    if player.dead then return end
    if choice == 'WeaponSlot' then
      room:addPlayerMark(player, MarkEnum.SlashResidue.."-turn", 3)
    elseif choice == 'ArmorSlot' then
      room:addPlayerMark(player, MarkEnum.AddMaxCardsInTurn, 3)
      player:drawCards(3, self.name)
    else
      room:addPlayerMark(player, "jueyan_distance-turn")
      if player:isWounded() then
        room:recover { num = 1, skillName = self.name, who = player, recoverBy = player}
      end
      if not player:hasSkill("ex__jizhi",true) then
        room:handleAddLoseSkills(player, "ex__jizhi", nil, false)
        room.logic:getCurrentEvent():findParent(GameEvent.Turn):addCleaner(function()
          room:handleAddLoseSkills(player, "-ex__jizhi", nil, false)
        end)
      end
    end
  end,
}
local jueyan_targetmod = fk.CreateTargetModSkill{
  name = "#joy__jueyan_targetmod",
  bypass_distances = function(self, player, skill, card, to)
    return player:getMark("jueyan_distance-turn") > 0
  end,
}
jueyan:addRelatedSkill(jueyan_targetmod)
lukang:addSkill(jueyan)
lukang:addRelatedSkill("ex__jizhi")
local huairou = fk.CreateActiveSkill{
  name = "joy__huairou",
  anim_type = "drawcard",
  can_use = function(self, player)
    return not player:isNude()
  end,
  card_num = 1,
  card_filter = function(self, to_select, selected)
    return #selected < 1 and Fk:getCardById(to_select).type == Card.TypeEquip
    and table.contains(Self.sealedSlots, Util.convertSubtypeAndEquipSlot(Fk:getCardById(to_select).sub_type))
  end,
  target_num = 0,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    room:moveCards({
      ids = effect.cards,
      from = player.id,
      toArea = Card.DiscardPile,
      skillName = self.name,
      moveReason = fk.ReasonPutIntoDiscardPile,
      proposer = player.id
    })
    if player.dead then return end
    local allCardMapper = {}
    local allCardNames = {}
    local mark = player:getTableMark( "joy__huairou-turn")
    for _, id in ipairs(Fk:getAllCardIds()) do
      local card = Fk:getCardById(id)
      if not table.contains(mark, card.name) and card.type ~= Card.TypeEquip and (room:getCardArea(id) == Card.DrawPile or room:getCardArea(id) == Card.DiscardPile) then
        if allCardMapper[card.name] == nil then
          allCardMapper[card.name] = {}
          table.insert(allCardNames, card.name)
        end
        table.insert(allCardMapper[card.name], id)
      end
    end
    if #allCardNames == 0 then return end
    local cardName = room:askForChoice(player, allCardNames, self.name)
    table.insert(mark, cardName)
    room:setPlayerMark(player, "joy__huairou-turn", mark)
    room:moveCardTo(table.random(allCardMapper[cardName]), Card.PlayerHand, player, fk.ReasonPrey, self.name)
  end,
}
lukang:addSkill(huairou)

Fk:loadTranslationTable{
  ["joy__lukang"] = "陆抗",
  ["#joy__lukang"] = "社稷之瑰宝",
  ["joy__jueyan"] = "决堰",
  [":joy__jueyan"] = "出牌阶段限一次，你可以废除你装备区里的一种装备栏，然后执行对应的一项：武器栏，你于此回合内可以多使用三张【杀】；防具栏，摸三张牌，本回合手牌上限+3；2个坐骑栏，回复1点体力，本回合获得技能〖集智〗，且你使用牌无距离限制。",
  ["RideSlot"] = "坐骑栏",
  ["joy__huairou"] = "怀柔",
  [":joy__huairou"] = "出牌阶段，你可以将一张已废除装备栏对应的装备牌置入弃牌堆，然后获得一张指定牌名的基本牌或锦囊牌（每牌名每回合限一次)",
}


local zhangxiu = General(extension, "joy__zhangxiu", "qun", 4)
zhangxiu:addSkill("xiongluan")
zhangxiu:addSkill("congjian")
Fk:loadTranslationTable{
  ["joy__zhangxiu"] = "张绣",
  ["#joy__zhangxiu"] = "北地枪王",
}

return extension
