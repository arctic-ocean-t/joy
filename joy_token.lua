local extension = Package:new("joy_token", Package.CardPack)
extension.extensionName = "joy"
Fk:loadTranslationTable{
  ["joy_token"] = "欢乐杀衍生牌",
}


local U = require "packages/utility/utility"

local siege_engine_slash = fk.CreateViewAsSkill{
    name = "joy__siege_engine_slash",
    card_filter = Util.FalseFunc,
    view_as = function(self, cards)
      local card = Fk:cloneCard("slash")
      card.skillName = "#joy__siege_engine_skill"
      return card
    end,
  }
  Fk:addSkill(siege_engine_slash)
  local siegeEngineSkill = fk.CreateTriggerSkill{
    name = "#joy__siege_engine_skill",
    attached_equip = "joy__siege_engine",
    events = {fk.EventPhaseStart, fk.Damage, fk.BeforeCardsMove, fk.TargetSpecified},
    can_trigger = function(self, event, target, player, data)
      if event == fk.EventPhaseStart then
        return target == player and player:hasSkill(self) and player.phase == Player.Play
      elseif event == fk.Damage then
        return target == player and player:hasSkill(self) and data.card and table.contains(data.card.skillNames, self.name) and
        player.room.logic:damageByCardEffect() and not data.to.dead and not data.to:isNude()
      elseif event == fk.BeforeCardsMove then
        if player:getEquipment(Card.SubtypeWeapon) and Fk:getCardById(player:getEquipment(Card.SubtypeWeapon)).name == "joy__siege_engine" and
          (player:getMark("joy__xianzhu1") == 0 and player:getMark("joy__xianzhu2") == 0 and player:getMark("joy__xianzhu3") == 0) then
          for _, move in ipairs(data) do
            return move.from == player.id
          end
        end
      elseif event == fk.TargetSpecified then
        return target == player and player:hasSkill(self) and data.card.trueName == "slash" and player:getMark("joy__xianzhu1") > 0
      end
    end,
    on_cost = function(self, event, target, player, data)
      if event == fk.EventPhaseStart then
        local success, dat = player.room:askForUseActiveSkill(player, "joy__siege_engine_slash", "#joy__siege_engine-invoke", true)
        if success then
          self.cost_data = dat
          return true
        end
      else
        return true
      end
    end,
    on_use = function(self, event, target, player, data)
      local room = player.room
      if event == fk.EventPhaseStart then
        local card = Fk:cloneCard("slash")
        card.skillName = self.name
        room:useCard{
          from = player.id,
          tos = table.map(self.cost_data.targets, function(id) return {id} end),
          card = card,
          extraUse = true,
        }
      elseif event == fk.Damage then
        local cards = room:askForCardsChosen(player, data.to, 1, 1 + player:getMark("joy__xianzhu3"), "he", self.name)
        room:throwCard(cards, self.name, data.to, player)
      elseif event == fk.BeforeCardsMove then
        for _, move in ipairs(data) do
          if move.from == player.id and move.toArea ~= Card.Void then
            for _, info in ipairs(move.moveInfo) do
              if info.fromArea == Card.PlayerEquip and move.moveReason == fk.ReasonDiscard then
                if Fk:getCardById(info.cardId, true).name == "joy__siege_engine" then
                  room:notifySkillInvoked(player, self.name, "defensive")
                  return true
                end
              end
            end
          end
        end
      elseif event == fk.TargetSpecified then
        room:addPlayerMark(room:getPlayerById(data.to), fk.MarkArmorNullified)
  
        data.extra_data = data.extra_data or {}
        data.extra_data.siege_engineNullified = data.extra_data.siege_engineNullified or {}
        data.extra_data.siege_engineNullified[tostring(data.to)] = (data.extra_data.siege_engineNullified[tostring(data.to)] or 0) + 1
      end
    end,
  
    refresh_events = {fk.CardUseFinished},
    can_refresh = function(self, event, target, player, data)
      return data.extra_data and data.extra_data.siege_engineNullified
    end,
    on_refresh = function(self, event, target, player, data)
      local room = player.room
      for key, num in pairs(data.extra_data.siege_engineNullified) do
        local p = room:getPlayerById(tonumber(key))
        if p:getMark(fk.MarkArmorNullified) > 0 then
          room:removePlayerMark(p, fk.MarkArmorNullified, num)
        end
      end
      data.siege_engineNullified = nil
    end,
  }
  local siege_engine_targetmod = fk.CreateTargetModSkill{
    name = "#joy__siege_engine_targetmod",
    distance_limit_func =  function(self, player, skill, card, to)
      if skill.trueName == "slash_skill" and card and card.trueName == "slash" and player:getMark("joy__xianzhu1") > 0 then
        return 999
      end
    end,
    extra_target_func = function(self, player, skill, card)
      if skill.trueName == "slash_skill" and card and table.contains(card.skillNames, "#joy__siege_engine_skill") then
        return player:getMark("joy__xianzhu2")
      end
    end,
  }
  siegeEngineSkill:addRelatedSkill(siege_engine_targetmod)
  Fk:addSkill(siegeEngineSkill)
  local siegeEngine = fk.CreateWeapon{
    name = "&joy__siege_engine",
    suit = Card.Spade,
    number = 9,
    attack_range = 2,
    equip_skill = siegeEngineSkill,
  }
  extension:addCard(siegeEngine)
  Fk:loadTranslationTable{
    ["joy__siege_engine"] = "大攻车",
    [":joy__siege_engine"] = "装备牌·武器<br /><b>攻击范围</b>：2<br /><b>武器技能</b>：出牌阶段开始时，你可以视为使用一张【杀】（不计入次数），当此【杀】对目标角色造成伤害后，你弃置其一张牌。"..
    "若此牌未升级，则不能被弃置。离开装备区后销毁。<br>升级选项：<br>1.此【杀】无视距离和防具（装备大攻车后的所有【杀】）；<br>2.此【杀】可指定目标+1；<br>3.此【杀】造成伤害后弃牌数+1。",
    ["#joy__siege_engine_skill"] = "大攻车",
    ["joy__siege_engine_slash"] = "大攻车",
    ["#joy__siege_engine-invoke"] = "大攻车：你可以视为使用【杀】",
  }

  local shuiyan_skill = fk.CreateActiveSkill {
    name = "joy_shuiyan_skill",
    prompt = "#joy_shuiyan_skill",
    min_target_num = 1,
    max_target_num = 2,
    mod_target_filter = Util.TrueFunc,
    target_filter = function(self, to_select, selected, selected_cards, card)
      if #selected < self:getMaxTargetNum(Self, card) then
        return self:modTargetFilter(to_select, selected, Self.id, card)
      end
    end,
    on_use = function(self, room, cardUseEvent)
      if cardUseEvent.tos == nil or #cardUseEvent.tos == 0 then return end
      cardUseEvent.extra_data = cardUseEvent.extra_data or {}
      cardUseEvent.extra_data.joy_shuiyan = cardUseEvent.tos[1][1]
    end,
    on_effect = function(self, room, effect)
      local from = room:getPlayerById(effect.from)
      local to = room:getPlayerById(effect.to)
      room:damage({
        from = from,
        to = to,
        card = effect.card,
        damage = 1,
        damageType = fk.ThunderDamage,
        skillName = self.name
      })
      if not to.dead then
        if effect.extra_data and effect.extra_data.joy_shuiyan == effect.to then
          room:askForDiscard(to, 1, 1, true, self.name, false)
        else
          to:drawCards(1, self.name)
        end
      end
    end,
  }
  local shuiyan = fk.CreateTrickCard {
    name = "&joy_shuiyan",
    skill = shuiyan_skill,
    is_damage_card = true,
    suit = Card.Heart,
    number = 1,
  }
  extension:addCards {
    shuiyan,
    shuiyan:clone(Card.Heart, 13),
    shuiyan:clone(Card.Spade, 3),
    shuiyan:clone(Card.Spade, 9),
    shuiyan:clone(Card.Diamond, 5),
    shuiyan:clone(Card.Diamond, 7),
    shuiyan:clone(Card.Club, 11),
  }
  Fk:loadTranslationTable {
    ["joy_shuiyan"] = "水淹七军",
    [":joy_shuiyan"] = "锦囊牌<br/><b>时机</b>：出牌阶段<br/><b>目标</b>：一至两名角色<br/><b>效果</b>：第一名角色受到一点雷电伤害并弃置一张牌，第二名角色受到一点雷电伤害并摸一张牌",
    ["joy_shuiyan_skill"] = "水淹七军",
    ["#joy_shuiyan_skill"] = "选择至多两名角色，第一名角色受到一点雷电伤害并弃置一张牌，第二名角色受到一点雷电伤害并摸一张牌",
  }

    local catapultSkill = fk.CreateTriggerSkill{
    name = "#joy__catapult_skill",
    attached_equip = "joy__catapult",
    frequency = Skill.Compulsory,
    events = {fk.CardUsing, fk.CardResponding},
    can_trigger = function(self, event, target, player, data)
      return target == player and player:hasSkill(self) and data.card.type == Card.TypeBasic
    end,
    on_use = function(self, event, target, player, data)
      if event == fk.CardUsing and player.phase ~= Player.NotActive then
        if data.card.is_damage_card then
          data.additionalDamage = (data.additionalDamage or 0) + 1
        elseif data.card.name == "peach" then
          data.additionalRecover = (data.additionalRecover or 0) + 1
        elseif data.card.name == "analeptic" then
          if data.extra_data and data.extra_data.analepticRecover then
            data.additionalRecover = (data.additionalRecover or 0) + 1
          else
            data.extra_data = data.extra_data or {}
            data.extra_data.additionalDrank = (data.extra_data.additionalDrank or 0) + 1
          end
        end
      elseif player.phase == Player.NotActive then
        player:drawCards(1, self.name)
      end
    end,
  }
  local catapult_targetmod = fk.CreateTargetModSkill{
    name = "#joy__catapult_targetmod",
    bypass_distances =  function(self, player, skill, card)
      return player:hasSkill(catapultSkill) and player.phase ~= Player.NotActive and card and card.type == Card.TypeBasic
    end,
  }
  local catapult_diss = fk.CreateDistanceSkill{
    name = "#joy__catapult_diss",
    correct_func = function(self, from, to)
      if from:hasSkill(catapultSkill) then
        return -1
      end
    end,
  }
  catapultSkill:addRelatedSkill(catapult_targetmod)
  catapultSkill:addRelatedSkill(catapult_diss)
  Fk:addSkill(catapultSkill)
  local catapult = fk.CreateOffensiveRide{
    name = "&joy__catapult",
    suit = Card.Diamond,
    number = 9,
    equip_skill = catapultSkill,
  }
  extension:addCard(catapult)
  Fk:loadTranslationTable{
    ["joy__catapult"] = "霹雳车",
    ["#joy__catapult_skill"] = "霹雳车",
    [":joy__catapult"] = "装备牌·坐骑<br /><b>坐骑技能</b>：锁定技，你计算与其他角色的距离-1。<br>你回合内使用基本牌的伤害和回复数值+1且无距离限制。"..
    "你回合外使用或打出基本牌时摸一张牌。离开装备区时销毁。",
  }
  
  return extension