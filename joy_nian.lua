local extension = Package("joy_nian")
extension.extensionName = "joy"

Fk:loadTranslationTable{
  ["joy_nian"] = "欢乐-念",
  ["nian"] = "念",
}

local U = require "packages/utility/utility"

--念效果

local tssj = fk.CreateActiveSkill{
    name = "joy_nian_tssj",
    anim_type = "drawcard",
    target_num = 0,
    card_num = 0,
    prompt = "#joy_nian_tssj",
    card_filter = Util.FalseFunc,
    target_filter = Util.FalseFunc,
    on_use = function(self, room, effect)
      local player = room:getPlayerById(effect.from)
      local kin = player.kingdom
      for _, p in ipairs(room:getOtherPlayers(player)) do
        if p.kingdom == kin then
            local card = p:drawCards(1,self.name)
            if player.dead then break end
            if not p.dead then
                local tri = false
                for _, cid in ipairs(p:getCardIds("h")) do
                    if Fk:getCardById(cid).type == Card.TypeTrick then
                        tri = true
                        break
                    end
                end
                if tri then
                   local cs = room:askForCard(p,1,1,false,self.name,false,".|.|.|.|.|trick","#joy_nian_tssj-ask::"..player.id)
                    if #cs > 0 then
                        room:moveCardTo(cs,Card.PlayerHand,player,fk.ReasonGive)
                    end
                elseif room:getCardArea(card[1]) == Player.Hand and room:getCardOwner(card[1]) == p then
                    room:moveCardTo(card,Card.PlayerHand,player,fk.ReasonGive)
                end
            end
        end
      end
    end,
}
Fk:addSkill(tssj)
Fk:loadTranslationTable{
    ["joy_nian_tssj"] = "奖率三军",
    [":joy_nian_tssj"] = "念影效果；令所有其他与你势力相同的角色各摸一张牌，然后交给你一张锦囊牌，若没有锦囊牌则改为交给你因此技能获得的牌。",
    ["#joy_nian_tssj"] = '令所有其他与你势力相同的角色各摸一张牌，然后交给你一张锦囊牌，若没有锦囊牌则改为交给你因此技能获得的牌。',
    ["#joy_nian_tssj-ask"] = "奖率三军：选择一张锦囊牌交给 %dest 。",
  
}

local dpqj = fk.CreateActiveSkill{
  name = "joy_nian_dpqj",
  anim_type = "offensive",
  card_num = 0,
  target_num = 0,
  prompt = "你视为使用一张决斗，于此决斗结算结束前你的手牌均视为【杀】。",
  card_filter = Util.FalseFunc,
  target_filter = Util.FalseFunc,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    room:setPlayerMark(player,"joy_nian_dpqj-phase",1)
    U.askForUseVirtualCard(room, player, "duel", nil, self.name)
    room:setPlayerMark(player,"joy_nian_dpqj-phase",0)
  end,
}
local dpqj_mark = fk.CreateTriggerSkill{
  name = "#joy_nian_dpqj_mark",
  anim_type = "special",
  priority = 2,
  refresh_events = {fk.CardUseFinished},
  can_refresh = function (self, event, target, player, data)
     return target == player and player:getMark("joy_nian_dpqj-phase") > 0 and data.card and data.card.name == "duel" and data.skillName == "joy_nian_dpqj"
  end,
  on_refresh = function (self, event, target, player, data)
    player.room:setPlayerMark(player,"joy_nian_dpqj-phase",0)
  end,
}
local dpqj_filter = fk.CreateFilterSkill{
  name = "#joy_nian_dpqj_filter",
  card_filter = function(self, card, player)
    return player:getMark("joy_nian_dpqj-phase") > 0 and table.contains(player.player_cards[Player.Hand], card.id)
  end,
  view_as = function(self, card)
    return Fk:cloneCard("slash", card.suit, card.number)
  end,
}
dpqj:addRelatedSkill(dpqj_filter)
dpqj:addRelatedSkill(dpqj_mark)
Fk:addSkill(dpqj)
Fk:loadTranslationTable{
  ["joy_nian_dpqj"] = "斗破千军",
  ["#joy_nian_dpqj_mark"] = "斗破千军",
  ["#joy_nian_dpqj_filter"] = "斗破千军",
  [":joy_nian_dpqj"] = "念影效果；你视为使用一张【决斗】，于此【决斗】结算完成前你的手牌均视为【杀】。",

}

local function nianying(player)
  local room = player.room
  local nian = {tssj.name,dpqj.name}
  local choice = room:askForChoice(player,nian,"念影","念影：选择一项效果触发之。",true)
  room:handleAddLoseSkills(player,choice,nil,false)
  room:handleAddLoseSkills(player,"-"..choice,nil,false)
  room:askForUseActiveSkill(player,choice,nil,false)
end

----------
local xinghan = fk.CreateTriggerSkill{
    name = "joy__xinghan",
    anim_type = "special",
    priority = 2,
    events = {fk.TurnStart,fk.DamageInflicted},
    can_trigger = function(self, event, target, player, data)
    if target ~= player or not player:hasSkill(self) or player:usedSkillTimes(self.name, Player.HistoryTurn) ~= 0 then return false end
    local room = player.room
    local ta = player.room:getTag(self.name)
    local kin
    if type(ta) == "table" then
       kin = ta
      for _, p in ipairs(player.room.alive_players) do
        table.insertIfNeed(kin,p.kingdom)
      end
      room:setTag(self.name,kin)
    else
       kin = {"wei","wu","qun","god"}
      for _, p in ipairs(player.room.alive_players) do
        table.insertIfNeed(kin,p.kingdom)
      end
      room:setTag(self.name,kin)
    end
    table.removeOne(kin,"shu")
    local mark = type(player:getMark(self.name)) == "table" and player:getMark(self.name) or {}
    for _, k in ipairs(kin) do
      if not table.contains(mark,k) then
        return  target == player and player:hasSkill(self)  and player:usedSkillTimes(self.name, Player.HistoryTurn) == 0
      end
    end
  end,
    on_cost = function(self, event, target, player, data)
      
      return  player.room:askForSkillInvoke(player,self.name,nil,"兴汉：你可以发动一次“定乱”，成功后你增加一点体力上限和体力，并将场上对应势力角色全部修改为蜀势力。")
    end,
    on_use = function(self, event, target, player, data)
      local room = player.room
      local ta = player.room:getTag(self.name)
      if type(ta) == "table" then
        local kin = ta
        for _, p in ipairs(player.room.alive_players) do
          table.insertIfNeed(kin,p.kingdom)
        end
        room:setTag(self.name,kin)
      else
        local kin = {"wei","wu","qun","god"}
        for _, p in ipairs(player.room.alive_players) do
          table.insertIfNeed(kin,p.kingdom)
        end
        room:setTag(self.name,kin)
      end
      local mark = type(player:getMark(self.name)) == "table" and player:getMark(self.name) or {}
      local cs = player.room:getTag(self.name)
      table.removeOne(cs,"shu")
      local asc = table.simpleClone(cs)
      if #mark > 0 then
        for _, c in ipairs(mark) do
          table.removeOne(cs,c)
        end
      end
      if #cs <= 0 then return end
      local choice = room:askForChoice(player,cs,"定乱","选择要进行“定乱”的势力。",false,asc)
      room:changeMaxHp(player,1)
      if player:isWounded() and not player.dead then
        room:recover{
        who = player,
        num = 1,
        recoverBy = player,
        skillName = self.name
        }
      end
      if choice then
        local mark = type(player:getMark(self.name)) == "table" and player:getMark(self.name) or {}
        table.insert(mark,choice)
        room:setPlayerMark(player,self.name,mark)
        for _, p in ipairs(room.alive_players) do
            if p.kingdom == choice then
                room:changeKingdom(p, "shu", true)
            end
        end
      end
    end,
}
Fk:loadTranslationTable{
    ["joy__xinghan"] = "定乱",
    [":joy__xinghan"] = "每回合限一次，你的回合开始时或你受到伤害时，你可以选择一个势力（势力包括 魏吴群神，每个势力限选择一次）。若如此做，你增加一点体力上限和体力，然后将场上对应势力角色全部修改为“蜀”势力。",
}
local liaoyuan = fk.CreateActiveSkill{
    name = "joy__liaoyuan",
    anim_type = "offensive",
    card_filter = Util.FalseFunc,
    target_filter = Util.FalseFunc,
    target_num = 0,
    card_num = 0,
    can_use = function(self, player)
        return   player:usedSkillTimes(self.name, Player.HistoryPhase) == 0 and  not player:prohibitUse(Fk:cloneCard("fire_attack"))
      end,
    on_use = function(self, room, effect)
      local player = room:getPlayerById(effect.from)
      U.askForUseVirtualCard(room, player, "fire_attack", nil, self.name)
      
    end,
}
local liaoyuann_targetmod = fk.CreateTargetModSkill{
    name = "#joy__liaoyuan_targetmod",
    extra_target_func = function(self, player, skill, card)
      if card and card.name == "fire_attack"   then
        return 999
      end
    end,
}
local liaoyuan_trigger = fk.CreateTriggerSkill{
    name = "#joy__liaoyuan_trigger",
    anim_type = "offensive",
    events = {fk.CardUsing},
    mute = true,
    can_trigger = function(self, event, target, player, data)
      return target == player and player:hasSkill(self) and data.card.name == "fire_attack"
    end,
    on_cost = Util.TrueFunc,
    on_use = function(self, event, target, player, data)
      local room = player.room
      local targets = player.room:getUseExtraTargets( data)
      local n = #targets
      if n == 0 then return false end
      local tos = room:askForChoosePlayers(player, targets, 1, n, "#joy__liaoyuan-choose:::"..data.card:toLogString(), self.name, true)
      if #tos > 0 then
        table.forEach(tos, function (id)
          table.insert(data.tos, {id})
        end)
      end
    end,
}
liaoyuan:addRelatedSkill(liaoyuan_trigger)
--liaoyuan:addRelatedSkill(liaoyuann_targetmod)
Fk:loadTranslationTable{
    ["joy__liaoyuan"] = "燎原",
    [":joy__liaoyuan"] = "出牌阶段限一次，你可以视为使用一张【火攻】；你使用【火攻】可以指定任意名角色为目标。",
    ["#joy__liaoyuan"] = "你可以视为使用一张【火攻】。",
    ["#joy__liaoyuan_trigger"] = "燎原",
    ["#joy__liaoyuan-choose"] = "燎原：你可以多选择任意名角色成为 %arg 的目标。",
}
local nianying1 = fk.CreateTriggerSkill{
    name = "joy__nianying1",
    anim_type = "special",
    events = {fk.TurnStart,fk.TurnEnd},
    can_trigger = function(self, event, target, player, data)
        local room = player.room
        if event == fk.TurnStart then
            for _, p in ipairs(room.alive_players) do
                if p.kingdom ~= "shu" then
                    return false
                end
            end
            return target == player and  player:hasSkill(self) 
        elseif event == fk.TurnEnd then
            if player:hasSkill(self) then
                local m = player:getMark("joy__nianying1-turn") or 0
                local n = 0
                for _, p in ipairs(room.alive_players) do
                    if p.kingdom == "shu" then
                        n = n + 1
                    end
                end
                return n ~= m
            end
        end
    end,
    on_cost = function(self, event, target, player, data)
      return player.room:askForSkillInvoke(player,self.name,nil,"念影：你可以摸两张牌并执行一项“念影”效果。")
    end,
    on_use = function(self, event, target, player, data)
      local room = player.room
      if not player.dead then
        player:drawCards(2,self.name)
      end
      if player.dead then return end
      nianying(player)
    end,
    refresh_events = {fk.TurnStart},
    can_refresh = function(self, event, target, player, data)
        return  player:hasSkill(self)
    end,
    on_refresh = function(self, event, target, player, data)
        local room = player.room
        local n = 0
        for _, p in ipairs(room.alive_players) do
            if p.kingdom == "shu" then
                n = n + 1
            end
        end
        room:setPlayerMark(player,"joy__nianying1-turn",n)
    end,
}
Fk:loadTranslationTable{
    ["joy__nianying1"] = "念影",
    [":joy__nianying1"] = "你的回合开始时，若场上角色势力均为“蜀”；或每个回合结束时，本回合场上“蜀”势力角色数发生变化时可发动：你摸两张牌，然后选择一项“念影”效果发动。",
    ["@joy__nianying1-turn"] = "蜀",
}
local zhugeliang = General(extension, "joy_nian__zhugeliang", "shu", 3)
zhugeliang:addSkill(xinghan)
zhugeliang:addSkill(liaoyuan)
zhugeliang:addSkill(nianying1)
zhugeliang:addRelatedSkill("joy_nian_tssj")
Fk:loadTranslationTable{
  ["joy_nian__zhugeliang"] = "念·诸葛亮",
  ["$joy__liaoyuan1"] = "星星之火，亦可燎原。",
  ["$joy__liaoyuan2"] = "管乐用兵，未必过此。",
  ["$joy__xinghan1"] = "汉贼不两立，王业不偏安。",
  ["$joy__xinghan2"] = "助兴艺于四海，揽英杰于九州，臣如事，则汉室可兴。",
  ["$joy__nianying11"] = "今兵甲已足，当奖率三军。",
  ["$joy__nianying12"] = "策，先帝于内，公，后臣于外。",

}

local duelSkill = fk.CreateActiveSkill{
  name = "joy_doupo_duel_skill",
  prompt = "#joy_doupo_duel_skill",
  mod_target_filter = function(self, to_select, selected, user, card)
    return user ~= to_select
  end,
  target_filter = function(self, to_select, selected, _, card)
    if #selected < self:getMaxTargetNum(Self, card) then
      return self:modTargetFilter(to_select, selected, Self.id, card)
    end
  end,
  on_action = function(self, room, use, finished)
    local from = room:getPlayerById(use.from)
    if not finished then

      use.extra_data = use.extra_data or {}
      use.extra_data.diy_doupo = use.extra_data.diy_doupo or {}
    else
      for _, pid in ipairs(use.extra_data.diy_doupo) do
        if from.dead then break end
        local p = room:getPlayerById(pid)
        if not p.dead and not p:isKongcheng() then
            room:moveCardTo(table.random(p:getCardIds("h")),Card.PlayerHand,from,fk.ReasonPrey,self.name)
        end
      end
      use.extra_data.diy_doupo = nil
    end
  end,
  on_effect = function(self, room, effect)
    local to = room:getPlayerById(effect.to)
    local from = room:getPlayerById(effect.from)
    local responsers = { to, from }
    local currentTurn = 1
    local currentResponser = to

    while currentResponser:isAlive() do
      local loopTimes = 1
      if effect.fixedResponseTimes then
        local canFix = currentResponser == to
        if effect.fixedAddTimesResponsors then
          canFix = table.contains(effect.fixedAddTimesResponsors, currentResponser.id)
        end

        if canFix then
          if type(effect.fixedResponseTimes) == 'table' then
            loopTimes = effect.fixedResponseTimes["slash"] or 1
          elseif type(effect.fixedResponseTimes) == 'number' then
            loopTimes = effect.fixedResponseTimes
          end
        end
      end

      local cardResponded
      for i = 1, loopTimes do
        cardResponded = room:askForResponse(currentResponser, 'slash', nil, nil, false, nil, effect)
        if cardResponded then
          room:responseCard({
            from = currentResponser.id,
            card = cardResponded,
            responseToEvent = effect,
          })
        else
          break
        end
      end

      if not cardResponded then
        break
      end

      currentTurn = currentTurn % 2 + 1
      currentResponser = responsers[currentTurn]
    end

    if currentResponser ~= from then
      local use = effect
      use.extra_data = use.extra_data or {}
      use.extra_data.diy_doupo = use.extra_data.diy_doupo or {}
      table.insertIfNeed(use.extra_data.diy_doupo,currentResponser.id)
    end
    if currentResponser:isAlive() then
      room:damage({
        from = responsers[currentTurn % 2 + 1],
        to = currentResponser,
        card = effect.card,
        damage = 1,
        damageType = fk.NormalDamage,
        skillName = self.name,
      })
    end
  end
}
Fk:addSkill(duelSkill)
duelSkill.cardSkill = true
local doupo = fk.CreateTriggerSkill{
  name = "joy_doupo",
  events = {fk.CardUsing},
  anim_type = "offensive",
  frequency = Skill.Compulsory,
  can_trigger = function(self, event, target, player, data)
    if event == fk.CardUsing then
      return player:hasSkill(self) and data.from == player.id and data.card.trueName == "duel"
    end
  end,
  on_cost = Util.TrueFunc,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if event == fk.CardUsing then
      local card = data.card:clone()
      local c = table.simpleClone(data.card)
      for k, v in pairs(c) do
        card[k] = v
      end
      card.skill = duelSkill
      data.card = card
    end
  end,
}
local doupo_targetmod = fk.CreateTargetModSkill{
  name = "#joy_doupo_targetmod",
  extra_target_func = function(self, player, skill, card)
    if card and card.name == "duel"   then
      return 2
    end
  end,
}
doupo:addRelatedSkill(doupo_targetmod)
local tazhen = fk.CreateTriggerSkill{
  name = "joy__tazhen",
  anim_type = "offensive",
  frequency = Skill.Compulsory,
  events = {fk.TurnStart},
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(self) and target == player
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local victims, record = {}, {}
    -- prepare tazhen content
    local targets = table.map(room:getOtherPlayers(player, false), Util.IdMapper)
    if #targets > 0 then
      local list = table.random(targets, 3)
      local blank_num = 3 - #list
      if blank_num > 0 then
        blank_num = math.random(blank_num + 1) - 1
        for i = 1, blank_num do
          table.insert(list, "blank")
        end
      end
      for i = 1, math.random(2, 5) do
        table.insert(list, "slash")
      end
      for i = #list, 8 do
        table.insert(list, table.random({"analeptic", "horse"}))
      end
      table.shuffle(list)
      local result = room:askForCustomDialog(
        player, self.name,
        "packages/joy/qml/TazhenBox.qml",
        { list, math.max(0, player.hp) + 1 }
      )
      -- check tazhen rewards
      if result ~= "" then
        result = json.decode(result)
        local choices = table.map(result, function(i) return i + 1 end)
        if table.contains(choices, 5) then
          table.insert(record, "main")
        end
        for i, v in ipairs(choices) do
          local next, nnext = choices[i+1], choices[i+2]
          if nnext == nil then break end
          if math.abs(v - next) == 1 and (v - next) == (next - nnext) then
            table.insert(record, "line")
            break
          end
        end
        for i, v in ipairs(choices) do
          local next, nnext = choices[i+1], choices[i+2]
          if nnext == nil then break end
          if math.abs(v - next) == 3 and (v - next) == (next - nnext) then
            table.insert(record, "row")
            break
          end
        end
        local atk, buff = 0, 0
        for _, index in ipairs(choices) do
          local dat = list[index]
          if dat == "slash" then
            atk = atk + 1
          elseif dat == "analeptic" then
            buff = buff + 2
          elseif type(dat) == "number" then
            local hp = room:getPlayerById(dat).hp
            if buff + atk >= hp then
              table.insert(victims, dat)
            end
            buff = 0
          end
        end
      end
    end
    room:sendLog{ type = "#JoyTazhenResult", from = player.id, arg = #victims > 0 and "success" or "fail", toast = true }
    if #victims == 0 then return false end
    local slash = room:getCardsFromPileByRule("slash")
    if #slash > 0 then
      room:moveCards({
        ids = slash,
        to = player.id,
        toArea = Player.Hand,
        moveReason = fk.ReasonJustMove,
        proposer = player.id,
        skillName = self.name,
      })
    end
    if player.dead then return end
    if not player:hasSkill("wushuang", true) then
      room:setPlayerMark(player,self.name,1)
      room:handleAddLoseSkills(player, "wushuang")
      if player.dead then return end
    end
    room:sortPlayersByAction(victims)
    room:doIndicate(player.id, victims)
    victims = table.map(victims, Util.Id2PlayerMapper)
    if not table.contains(record, "line")  then
      for _, p in ipairs(victims) do
        if player.dead then break end
        if not p.dead and not p:isNude() then
          local cards = room:askForCard(p, 1, 1, true, self.name, false, ".", "#joy__tazhen-give:"..player.id)
          room:moveCardTo(cards, Player.Hand, player, fk.ReasonGive, self.name, nil, false, p.id)
        end
      end
    end
    if not table.contains(record, "row") then
      for _, p in ipairs(victims) do
        if player.dead then break end
        if not p.dead then
          room:useVirtualCard("slash", nil, player, p, self.name, true)
        end
      end
    end
    if not table.contains(record, "main") then
      local mark = player:getTableMark( "joy__tazhen_target")
      for _, p in ipairs(victims) do
        if not p.dead and table.insertIfNeed(mark, p.id) then
          room:addPlayerMark(p, "@@joy__tazhen")
        end
      end
      room:setPlayerMark(player, "joy__tazhen_target", mark)
    end
  end,

  refresh_events = {fk.TurnStart, fk.BuryVictim},
  can_refresh = function (self, event, target, player, data)
    return target == player and (player:getMark(self.name) ~= 0 or player:getMark("joy__tazhen_target") ~= 0)
  end,
  on_refresh = function (self, event, target, player, data)
    local room = player.room
    if player:getMark(self.name) > 0  then
      room:setPlayerMark(player, self.name, 0)
      room:handleAddLoseSkills(player, "-wushuang")
    end
    local mark = player:getMark("joy__tazhen_target")
    if mark ~= 0 then
      room:setPlayerMark(player, "joy__tazhen_target", 0)
      for _, pid in ipairs(mark) do
        local p = room:getPlayerById(pid)
        room:removePlayerMark(p, "@@joy__tazhen")
      end
    end
  end,
}

local tazhen_prohibit = fk.CreateProhibitSkill{
  name = "#joy__tazhen_prohibit",
  is_prohibited = function(self, from, to, card)
    return from and to and table.contains(to:getTableMark( "joy__tazhen_target") ,from.id) and card and card.trueName == "slash"
  end,
}
tazhen:addRelatedSkill(tazhen_prohibit)
--[[
local tazheng = fk.CreateTriggerSkill{
  name = "joy_tazheng",
  anim_type = "offensive",
  frequency = Skill.Compulsory,
  events = {fk.TurnStart},
  can_trigger = function(self, event, target, player, data)
      return target == player and player:hasSkill(self) 
  end,
  on_cost = function(self, event, target, player, data)
    return player.room:askForSkillInvoke(player,self.name,nil,"你可以进行一次“踏阵”,然后你摸一张【杀】并获得技能〖无双〗直到你的下回合开始。")
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local choices = room:askForChoices(player,{"joy_tazheng1","joy_tazheng2","joy_tazheng3"},1,2,self.name,"踏阵：选择至多两个效果，若只选一个选项则可多一个目标",false,true)
    local n = #room:getOtherPlayers(player)
    if n%2 ~= 0 then
      n = n//2 + 1
    else
      n = n//2
    end
    if #choices == 1 then
      n = n + 1
    end
    local tars2 = room:askForChoosePlayers(player,table.map(room:getOtherPlayers(player), Util.IdMapper),1,n,"选择至多 "..n.." 个角色成为“踏阵”目标",self.name,false)
    local cards = room:getCardsFromPileByRule("slash",1,"DrawPile")
    if #cards > 0 then
      room:moveCards({
        ids = cards,
        to = player.id,
        toArea = Card.PlayerHand,
        moveReason = fk.ReasonJustMove,
        proposer = player.id,
        skillName = self.name,
      })
    end
    if player.dead then return end
    if not player:hasSkill("wushuang") then
      room:setPlayerMark(player,self.name,1)
      room:handleAddLoseSkills(player,"wushuang")
    end
    if player.dead then return end
    room:sortPlayersByAction(tars2)
    --room:doIndicate(player.id,tars2)
    local tars = {}
    for _, pid in ipairs(tars2) do
      table.insert(tars,room:getPlayerById(pid))
    end
    if table.contains(choices,"joy_tazheng1")  then
    for _, p in ipairs(tars) do
      if player.dead then break end
      if not p.dead then
          if not p:isNude() then
            local card = room:askForCard(p,1,1,true,self.name,false,".","踏阵：请交给 "..Fk:translate(player.general).." 一张牌。")
            if #card > 0 then
              room:moveCardTo(card,Card.PlayerHand,player,fk.ReasonGive,self.name)
            end
          end
      end
    end
    end
    if table.contains(choices,"joy_tazheng2") then
    for _, p in ipairs(tars) do
      if player.dead then break end
      if not p.dead then
          if not player:isProhibited(p,Fk:cloneCard "slash") then
            local slash = Fk:cloneCard "slash"
            slash.skillName = self.name
            room:useCard {
            from = player.id,
            tos = {{p.id}},
            card = slash,
            extraUse = true,
            }
            
          end
        if table.contains(choices,"joy_tazheng3") then
          local mark = type(player:getMark(self.name.."2")) == "table" and player:getMark(self.name.."2") or {}
          table.insertIfNeed(mark,p.id)
          room:setPlayerMark(player,self.name.."2",mark)
          local mark2 = type(p:getMark("@@"..self.name)) == "table" and p:getMark("@@"..self.name) or {}
          table.insertIfNeed(mark2,player.id)
          room:setPlayerMark(p,"@@"..self.name,mark2)
        end
      end
    end
    end
    if table.contains(choices,"joy_tazheng3") then
      for _, p in ipairs(tars) do
        if player.dead then break end
        if not p.dead then
            local mark = type(player:getMark(self.name.."2")) == "table" and player:getMark(self.name.."2") or {}
            table.insertIfNeed(mark,p.id)
            room:setPlayerMark(player,self.name.."2",mark)
            local mark2 = type(p:getMark("@@"..self.name)) == "table" and p:getMark("@@"..self.name) or {}
            table.insertIfNeed(mark2,player.id)
            room:setPlayerMark(p,"@@"..self.name,mark2)
        end
      end
      end
  end,
  refresh_events = {fk.TurnStart,fk.BuryVictim},
  can_refresh = function (self, event, target, player, data)
    return target == player and (player:getMark(self.name) > 0 or (type(player:getMark(self.name.."2")) == "table" and #player:getMark(self.name.."2") > 0))
  end,
  on_refresh = function (self, event, target, player, data)
    if player:getMark(self.name) > 0  then
      player.room:handleAddLoseSkills(player,"-wushuang")
    end
    local mark = type(player:getMark(self.name.."2")) == "table" and player:getMark(self.name.."2") or {}
    if #mark > 0 then
      local room = player.room
      room:setPlayerMark(player,self.name.."2",0)
      for _, pid in ipairs(mark) do
        local p = room:getPlayerById(pid)
        local mark2 = type(p:getMark("@@"..self.name)) == "table" and p:getMark("@@"..self.name) or {}
        table.removeOne(mark2,player.id)
        if #mark2 > 0 then
          room:setPlayerMark(p,"@@"..self.name,mark2)
        else
          room:setPlayerMark(p,"@@"..self.name,0)
        end
      end
    end
  end,
}
local tazheng_buff = fk.CreateProhibitSkill{
  name = "#joy_tazheng_buff",
  frequency = Skill.Compulsory,
  is_prohibited = function(self, from, to, card)
    local m = type(to:getMark(tazheng.name.."2")) == "table" and to:getMark(tazheng.name.."2") or {}
    if table.contains(m,from.id) then
      return card.trueName == "slash"
    end
  end,
}
]]
local nianying2 = fk.CreateTriggerSkill{
  name = "joy__nianying2",
  anim_type = "special",
  events = {fk.Damage,fk.Damaged},
  can_trigger = function(self, event, target, player, data)
      if target ~= player or not player:hasSkill(self) or player:usedSkillTimes(self.name, Player.HistoryTurn) ~= 0 then return end
      if event == fk.Damage then
        local n = 0
        player.room.logic:getActualDamageEvents( 3, function(e)
      local damage = e.data[1]
      if damage.from  and damage.from == player then
        n = n + damage.damage
      end
    end)
        return n >= 2
      elseif event == fk.Damaged then
        local n = 0
        player.room.logic:getActualDamageEvents( 3, function(e)
      local damage = e.data[1]
      if damage.to and damage.to == player then
        n = n + damage.damage
      end
    end)
        return n >= 2
      end
  end,
  on_cost = function(self, event, target, player, data)
    return player.room:askForSkillInvoke(player,self.name,nil,"念影：你可以执行一项“念影”效果。")
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    nianying(player)
  end,
}
local lvbu = General(extension, "joy_nian__lvbu", "qun",5)
lvbu:addSkill(tazhen)
lvbu:addSkill(doupo)
lvbu:addSkill(nianying2)
lvbu:addRelatedSkill("wushuang")
lvbu:addRelatedSkill("joy_nian_dpqj")
Fk:loadTranslationTable{
  ["joy_nian__lvbu"] = "念·吕布",
  ["joy__tazhen"] = "踏阵",
  ["#joy__tazhen_prohibit"] = "踏阵",
  [":joy__tazhen"] = "锁定技，你的回合开始时进行一次“踏阵”，然后摸一张【杀】并获得技能〖无双〗直到你的下回合开始，"..
  "若你本次“踏阵”过程未路经："..
  "<br>①一整列，令“踏阵”中击败的角色依次交给你一张牌；"..
  "<br>②一整行，依次视为对“踏阵”中击败的角色使用一张不计次数的【杀】。"..
  "<br>③中心格，直到你的下回合开始前，“踏阵”中击败的角色无法对你使用【杀】。"..
  "<br><font color='grey'><b>踏阵</b>：规划路径并行进于九宫格中，提升伤害并尽可能击败拦路的敌人，踏阵可行进步数等同于当前体力值+1。击败阵中任意一名武将则视为踏阵成功，否则为踏阵失败。</font>",
  ["joy_doupo"] = "斗破",
  [":joy_doupo"] = "锁定技，你使用的【决斗】可以多指定2个目标，且于你的【决斗】结算完成后，随机获得此轮【决斗】失败角色的各一张手牌。",
  ["joy__nianying2"] = "念影",
  [":joy__nianying2"] = "每回合限一次，当你本回合造成或受到的伤害累计2点及以上时，你可选择一项“念影”效果执行之。",

  ["joy__tazhen_prompt1"] = "未路经一整列",
  [":joy__tazhen_prompt1"] = "交给你一张牌",
  ["joy__tazhen_prompt2"] = "未路经一整行",
  [":joy__tazhen_prompt2"] = "视为使用【杀】",
  ["joy__tazhen_prompt3"] = "未路经中心路",
  [":joy__tazhen_prompt3"] = "不能对你用【杀】",
  ["#JoyTazhenResult"] = "%from 踏阵 %arg",
  ["#joy__tazhen-give"] = "踏阵：请交给 %src 一张牌",
  ["Rest Step"] = "剩余步数",
  ["ATK Num"] = "攻击力",
  ["@@joy__tazhen"] = "踏阵",
}
--[[
  踏阵出现逻辑（推测）：
  尽量且至多3名存活其他角色，若没有合法角色，结束踏阵
  令X=3-目标人数，当X>0时，会出现0-X个空格
  2-5张【杀】
  剩下随机用【酒】、【马】填充
]]

return extension