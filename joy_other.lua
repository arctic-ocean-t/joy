local extension = Package("joy_other")
extension.extensionName = "joy"

Fk:loadTranslationTable{
  ["joy_other"] = "欢乐-限时地主",
  ["joyo"] = "欢乐地主",
}

local U = require "packages/utility/utility"

-- 限时地主武将，以及难以分类的武将






local joy__libai = General(extension, "joyo__libai", "qun", 3)

local joy__shixian = fk.CreateTriggerSkill{
  name = "joy__shixian",
  events = {fk.TurnStart},
  anim_type = "drawcard",
  frequency = Skill.Compulsory,
  can_trigger = function(self, _, target, player, data)
    return player:hasSkill(self) and target == player
  end,
  on_use = function(self, _, target, player, data)
    local room = player.room
    local names = {"joy__xiakexing", "joy__qiangjinjiu", "joy__jingyesi", "joy__xinglunan"}
    room:handleAddLoseSkills(player, "-"..table.concat(names, "|-"))
    local cards = room:getNCards(4)
    room:moveCardTo(cards, Card.Processing, nil, fk.ReasonPut, self.name)
    room:delay(1000)
    local map = {}
    for _, id in ipairs(cards) do
      local suit = Fk:getCardById(id).suit
      map[suit] = map[suit] or {}
      table.insert(map[suit], id)
    end
    local get = {}
    for suit, ids in pairs(map) do
      room:handleAddLoseSkills(player, names[suit])
      if #ids > 1 then
        table.insertTable(get, ids)
      end
    end
    if #get > 0 and not player.dead then
      room:moveCardTo(get, Card.PlayerHand, player, fk.ReasonPrey, self.name)
    end
    cards = table.filter(cards, function(id) return Fk:getCardById(id) == Card.Processing end)
    if #cards > 0 then
      room:moveCards({
        ids = cards,
        toArea = Card.DiscardPile,
        moveReason = fk.ReasonPutIntoDiscardPile,
      })
    end
  end,
}
joy__libai:addSkill(joy__shixian)

local joy__jingyesi = fk.CreateTriggerSkill{
  name = "joy__jingyesi",
  events = {fk.EventPhaseEnd},
  anim_type = "drawcard",
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(self) and target == player and (player.phase == Player.Play or player.phase == Player.Discard)
  end,
  on_cost = function (self, event, target, player, data)
    return player.phase == Player.Discard or player.room:askForSkillInvoke(player, self.name)
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if player.phase == Player.Play then
      local id = room:getNCards(1)[1]
      table.insert(room.draw_pile, 1, id)
      U.askForUseRealCard(room, player, {id}, ".", self.name, "#joy__jingyesi-use:::"..Fk:getCardById(id):toLogString(),
      {expand_pile = {id}})
    else
      player:drawCards(1, self.name, "bottom")
    end
  end,
}
joy__libai:addRelatedSkill(joy__jingyesi)

local joy__xinglunan = fk.CreateTriggerSkill{
  name = "joy__xinglunan",
  events = {fk.CardUseFinished},
  anim_type = "defensive",
  frequency = Skill.Compulsory,
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(self) and target ~= player and data.card.trueName == "slash" and player ~= player.room.current
    and table.contains(TargetGroup:getRealTargets(data.tos), player.id)
  end,
  on_use = function(self, event, target, player, data)
    player.room:addPlayerMark(player, "@@joy__xinglunan")
  end,

  refresh_events = {fk.TurnStart},
  can_refresh = function (self, event, target, player, data)
    return target == player and player:getMark("@@joy__xinglunan") > 0
  end,
  on_refresh = function (self, event, target, player, data)
    player.room:setPlayerMark(player, "@@joy__xinglunan", 0)
  end,
}
local joy__xinglunan_distance = fk.CreateDistanceSkill{
  name = "#joy__xinglunan_distance",
  correct_func = function(self, from, to)
    return to:getMark("@@joy__xinglunan")
  end,
}
joy__xinglunan:addRelatedSkill(joy__xinglunan_distance)
joy__libai:addRelatedSkill(joy__xinglunan)

local joy__xiakexing = fk.CreateTriggerSkill{
  name = "joy__xiakexing",
  events = {fk.CardUsing, fk.Damage},
  anim_type = "offensive",
  can_trigger = function(self, event, target, player, data)
    if player:hasSkill(self) and target == player then
      if event == fk.CardUsing then
        return data.card.sub_type == Card.SubtypeWeapon and string.find(Fk:translate(data.card.name, "zh_CN"), "剑") ~= nil
      else
        return player:getEquipment(Card.SubtypeWeapon) ~= nil and not data.to.dead and player:canPindian(data.to)
      end
    end
  end,
  on_cost = function (self, event, target, player, data)
    return event == fk.CardUsing or player.room:askForSkillInvoke(player, self.name, nil, "#joy__xiakexing-invoke:"..data.to.id)
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if event == fk.CardUsing then
      local card = Fk:cloneCard("archery_attack")
      card.skillName = self.name
      if not player:prohibitUse(card) and player:canUse(card) then
        room:useCard{from = player.id, card = card, tos = {}}
      end
    else
      local pindian = player:pindian({data.to}, self.name)
      if pindian.results[data.to.id].winner == player then
        room:changeMaxHp(data.to, -1)
      else
        local throw = player:getEquipments(Card.SubtypeWeapon)
        if #throw > 0 then
          room:throwCard(throw, self.name, player, player)
        end
      end
    end
  end,
}
joy__libai:addRelatedSkill(joy__xiakexing)

local joy__qiangjinjiu = fk.CreateTriggerSkill{
  name = "joy__qiangjinjiu",
  events = {fk.EventPhaseStart},
  anim_type = "control",
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(self) and target ~= player and target.phase == Player.Start and not player:isKongcheng()
  end,
  on_cost = function (self, event, target, player, data)
    local cards = player.room:askForDiscard(player, 1, 1, false, self.name, true, ".", "#joy__qiangjinjiu-invoke:"..target.id, true)
    if #cards > 0 then
      self.cost_data = cards
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    room:throwCard(self.cost_data, self.name, player, player)
    if player.dead or target.dead then return end
    local choice = room:askForChoice(player, {"joy__qiangjinjiu_choice1", "joy__qiangjinjiu_choice2"}, self.name)
    if choice:endsWith("1") then
      local throw = target:getCardIds("e")
      if #throw > 0 then
        room:throwCard(throw, self.name, target, player)
      end
      if not target.dead then
        local cards = room:getCardsFromPileByRule("analeptic")
        if #cards > 0 then
          room:moveCardTo(cards, Card.PlayerHand, target, fk.ReasonPrey, self.name, nil, true)
        end
      end
    else
      local cards = table.filter(target:getCardIds("h"), function(id) return Fk:getCardById(id).name == "analeptic" end)
      if #cards == 0 and not target:isNude() then
        cards = {room:askForCardChosen(player, target, "he", self.name)}
      end
      if #cards > 0 then
        room:moveCardTo(cards, Card.PlayerHand, player, fk.ReasonPrey, self.name, nil, false, player.id)
      end
    end
  end,
}
joy__libai:addRelatedSkill(joy__qiangjinjiu)

Fk:loadTranslationTable{
  ["joyo__libai"] = "李白",
  ["joy__shixian"] = "诗仙",
  [":joy__shixian"] = "锁定技，回合开始时，你清除已有的诗篇并亮出牌堆顶四张牌，根据花色创作对应的诗篇：<font color='red'>♥</font>《静夜思》；"..
  "<font color='red'>♦</font>《行路难》；♠《侠客行》；♣《将进酒》。然后你获得其中重复花色的牌。",
  -- 三个字的技能会超出UI……
  ["joy__jingyesi"] = "夜思",
  [":joy__jingyesi"] = "出牌阶段结束时，你可以观看牌堆顶一张牌，然后可以使用此牌；弃牌阶段结束时，你获得牌堆底的一张牌。",
  ["#joy__jingyesi-use"] = "静夜思：你可以使用 %arg",

  ["joy__xinglunan"] = "路难",
  [":joy__xinglunan"] = "锁定技，你的回合外，当其他角色对你使用的【杀】结算后，直到你的回合开始，其他角色计算与你的距离+1。",
  ["@@joy__xinglunan"] = "路难",

  ["joy__xiakexing"] = "侠行",
  [":joy__xiakexing"] = "当你使用牌名中有“剑”的武器时，你视为使用一张【万箭齐发】；当你使用【杀】造成伤害后，若你装备了武器，你可以与其拼点："..
  "若你赢，其减1点体力上限；若你没赢，则弃置你装备区内的武器。",
  ["#joy__xiakexing-invoke"] = "侠客行：可以与 %src 拼点，若你赢，其减1点体力上限，否则弃置你的武器",

  ["joy__qiangjinjiu"] = "进酒",
  [":joy__qiangjinjiu"] = "其他角色准备阶段，你可以弃置一张手牌并选择一项：1.弃置其装备区内所有的装备，令其从牌堆中获得一张【酒】；"..
  "2.获得其手牌中所有【酒】，若其手牌中没有【酒】，则改为获得其一张牌。",
  ["#joy__qiangjinjiu-invoke"] = "将进酒：你可以弃置一张手牌并对 %src 执行一项",
  ["joy__qiangjinjiu_choice1"] = "弃置其装备区内所有的装备，令其从牌堆中获得一张【酒】",
  ["joy__qiangjinjiu_choice2"] = "获得其手牌中所有【酒】，若没有【酒】，获得其一张牌",

  ["$joy__shixian1"] = "吾乃诗仙李太白！",
  ["$joy__shixian2"] = "笔落惊风雨，诗成泣鬼神！",
  ["$joy__jingyesi1"] = "举头望明月，低头思故乡。",
  ["$joy__xinglunan1"] = "行路难，行路难！多歧路，今安在？",
  ["$joy__xiakexing1"] = "十步杀一人，千里不留行。",
  ["$joy__qiangjinjiu1"] = "人生得意须尽欢，莫使金樽空对月。",
  ["~joyo__libai"] = "生者为过客，死者为归人。",
}

local joy__change = General(extension, "joyo__change", "god", 1, 4, General.Female)
local joy__daoyao = fk.CreateActiveSkill{
  name = "joy__daoyao",
  anim_type = "drawcard",
  card_num = 1,
  target_num = 0,
  can_use = function(self, player)
    return player:usedSkillTimes(self.name, Player.HistoryPhase) == 0 and not player:isKongcheng()
  end,
  card_filter = function(self, to_select, selected)
    return #selected == 0 and Fk:currentRoom():getCardArea(to_select) ~= Player.Equip and
      not Self:prohibitDiscard(Fk:getCardById(to_select))
  end,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    room:throwCard(effect.cards, self.name, player, player)
    if player.dead then return end
    local ids = room:getCardsFromPileByRule("peach")
    if #ids > 0 then
      room:obtainCard(player, ids[1], false, fk.ReasonPrey)
    end
    if not player.dead then
      player:drawCards(3 - #ids, self.name)
    end
  end,
}
joy__change:addSkill(joy__daoyao)
local joy__benyue = fk.CreateTriggerSkill{
  name = "joy__benyue",
  frequency = Skill.Wake,
  events = {fk.HpRecover, fk.AfterCardsMove},
  can_trigger = function(self, event, target, player, data)
    if player:hasSkill(self) and player:usedSkillTimes(self.name, Player.HistoryGame) == 0 then
      if event == fk.HpRecover then
        return target == player
      else
        for _, move in ipairs(data) do
          if move.to == player.id and move.toArea == Card.PlayerHand then
            for _, info in ipairs(move.moveInfo) do
              if info.fromArea == Card.DrawPile and Fk:getCardById(info.cardId).name == "peach" then
                return true
              end
            end
          end
        end
      end
    end
  end,
  can_wake = function(self, event, target, player, data)
    if event == fk.HpRecover then
      local n = 0
      player.room.logic:getEventsOfScope(GameEvent.ChangeHp, 1, function(e)
        local damage = e.data[1]
        if e.data[1] == player and e.data[3] == "recover" then
          n = n + e.data[2]
        end
      end, Player.HistoryGame)
      return n > 2
    else
      return #table.filter(player:getCardIds("h"), function (id)
        return Fk:getCardById(id).name == "peach"
      end) > 2
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if player.maxHp < 15 then
      room:changeMaxHp(player, 15-player.maxHp)
    end
    room:handleAddLoseSkills(player, "joy__guanghan")
  end,
}
joy__change:addSkill(joy__benyue)
local joy__guanghan = fk.CreateTriggerSkill{
  name = "joy__guanghan",
  anim_type = "offensive",
  events = {fk.Damaged},
  frequency = Skill.Compulsory,
  can_trigger = function(self, event, target, player, data)
    if player:hasSkill(self) then
      return data.extra_data and table.find(data.extra_data.joy__guanghan or {}, function (pid)
        return pid ~= player.id and not player.room:getPlayerById(pid).dead
      end)
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    for _, pid in ipairs(data.extra_data.joy__guanghan) do
      local p = room:getPlayerById(pid)
      if pid ~= player.id and not p.dead then
        if p:isKongcheng() or #room:askForDiscard(p, 1, 1, false, self.name, true, ".", "#joy__guanghan-discard:::"..data.damage) == 0 then
          room:loseHp(p, data.damage, self.name)
        end
      end
    end
  end,

  refresh_events = {fk.BeforeHpChanged},
  can_refresh = function(self, event, target, player, data)
    return data.damageEvent and target == player
  end,
  on_refresh = function(self, event, target, player, data)
    local damage = data.damageEvent
    damage.extra_data = damage.extra_data or {}
    local list = {}
    for _, p in ipairs(player.room:getAlivePlayers()) do
      if target:getNextAlive() == p or p:getNextAlive() == target then
        table.insertIfNeed(list, p.id)
      end
    end
    damage.extra_data.joy__guanghan = list
  end,
}
joy__change:addRelatedSkill(joy__guanghan)
Fk:loadTranslationTable{
  ["joyo__change"] = "嫦娥",
  ["#joyo__change"] = "广寒仙子",
  
  ["joy__daoyao"] = "捣药",
  [":joy__daoyao"] = "出牌阶段限一次，你可以弃置一张手牌，从牌堆获得一张【桃】并摸两张牌，若牌堆没有【桃】，改为摸三张牌。",

  ["joy__benyue"] = "奔月",
  [":joy__benyue"] = "觉醒技，当你摸到【桃】后若你有至少三张【桃】，或你累计回复3点体力后，你将体力上限增加至15，并获得技能〖广寒〗。",

  ["joy__guanghan"] = "广寒",
  [":joy__guanghan"] = "锁定技，当一名角色受到伤害后，与其相邻的其他角色需弃置一张手牌，否则失去等量体力。",
  ["#joy__guanghan-discard"] = "广寒：你需弃置一张手牌，否则失去 %arg 点体力",

  ["$joy__daoyao1"] = "入河蟾不没，捣药兔长生！",
  ["$joy__daoyao2"] = "转空轧軏冰轮响，捣药叮当玉杵鸣！",
  ["$joy__benyue1"] = "一入月宫去，千秋闭峨眉。",
  ["$joy__benyue2"] = "纵令奔月成仙去，且作行云入梦来。",
  ["$joy__guanghan1"] = "银河无声月宫冷，思念如影伴孤灯。",
  ["$joy__guanghan2"] = "月宫清冷人独立，寒梦纷飞思绪深。",
}

local joy__nvwa = General(extension, "joyo__nvwa", "god", 69, 159, General.Female)
local joy__butian = fk.CreateTriggerSkill{
  name = "joy__butian",
  frequency = Skill.Compulsory,
  events = { fk.DamageCaused , fk.DamageInflicted, fk.RoundEnd, fk.HpChanged, fk.MaxHpChanged, fk.GameStart, fk.EventAcquireSkill},
  can_trigger = function(self, event, target, player, data)
    if not player:hasSkill(self) then return false end
    if event == fk.DamageCaused then
      return player:getLostHp() > 4 and target == player and data.to ~= player
    elseif event == fk.DamageInflicted then
      return player:getLostHp() > 4 and target == player
    elseif event == fk.RoundEnd then
      return player:getLostHp() > 4
    elseif player.maxHp == player.hp then
      if event == fk.GameStart then return true end
      if event == fk.EventAcquireSkill then return data == self and target == player and player.room:getTag("RoundCount") end
      return target == player
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local x = player:getLostHp() // 5
    if event == fk.DamageCaused then
      room:recover { num = x, skillName = self.name, who = player, recoverBy = player}
    elseif event == fk.DamageInflicted or event == fk.RoundEnd then
      room:loseHp(player, x, self.name)
    else
      for _, p in ipairs(room:getOtherPlayers(player)) do
        if not p.dead then
          room:killPlayer({ who = p.id })
        end
      end
    end
  end
}
joy__nvwa:addSkill(joy__butian)
local joy__lianshi = fk.CreateTriggerSkill{
  name = "joy__lianshi",
  frequency = Skill.Compulsory,
  events = { fk.AfterCardsMove },
  can_trigger = function(self, event, target, player, data)
    if player:hasSkill(self) then
      local mark = player:getTableMark( "@joy__lianshi")
      local suits, num = {}, 0
      for _, move in ipairs(data) do
        if move.from == player.id and (move.moveReason == fk.ReasonUse or move.moveReason == fk.ReasonResonpse or move.moveReason == fk.ReasonDiscard) then
          for _, info in ipairs(move.moveInfo) do
            local card = Fk:getCardById(info.cardId)
            if card.suit ~= Card.NoSuit and not table.contains(mark, card:getSuitString(true)) and (info.fromArea == Card.PlayerHand or info.fromArea == Card.PlayerHand) then
              num = math.max(num, card.number)
              table.insertIfNeed(suits, card:getSuitString(true))
            end
          end
        end
      end
      if #suits > 0 then
        self.cost_data = {suits, num}
        return true
      end
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local suits, num = table.unpack(self.cost_data)
    local mark = player:getTableMark( "@joy__lianshi")
    table.insertTable(mark, suits)
    room:setPlayerMark(player, "@joy__lianshi", mark)
    if #mark == 4 then
      player:drawCards(1, self.name)
      room:recover { num = num, skillName = self.name, who = player, recoverBy = player}
      room:setPlayerMark(player, "@joy__lianshi", 0)
    end
  end,
}
local joy__lianshi_maxcards = fk.CreateMaxCardsSkill{
  name = "#joy__lianshi_maxcards",
  fixed_func = function(self, player)
    if player:hasSkill(joy__lianshi) then
      return 5
    end
  end
}
joy__lianshi:addRelatedSkill(joy__lianshi_maxcards)
joy__nvwa:addSkill(joy__lianshi)
local joy__tuantu = fk.CreateActiveSkill{
  name = "joy__tuantu",
  anim_type = "drawcard",
  card_num = 0,
  target_num = 0,
  card_filter = Util.FalseFunc,
  can_use = function(self, player)
    return player:usedSkillTimes(self.name, Player.HistoryPhase) == 0 and not player:isKongcheng()
  end,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    local get, map = {}, {}
    for _, id in ipairs(player.player_cards[Player.Hand]) do
      map[Fk:getCardById(id).trueName] = {}
    end
    for _, id in ipairs(room.discard_pile) do
      local name = Fk:getCardById(id).trueName
      if map[name] then
        table.insert(map[name], id)
      end
    end
    for _, cards in pairs(map) do
      if #cards > 0 then
        table.insert(get, table.random(cards))
      end
    end
    if #get > 0 then
      room:moveCardTo(get, Card.PlayerHand, player, fk.ReasonPrey, self.name, nil, true, player.id)
    end
  end,
}
joy__nvwa:addSkill(joy__tuantu)
Fk:loadTranslationTable{
  ["joyo__nvwa"] = "女娲",
  ["#joyo__nvwa"] = "创世女神",
  ["joy__butian"] = "补天",
  [":joy__butian"] = "锁定技，你对其他角色造成伤害时，回复X点体力。每轮结束时，或你受到伤害时，你失去X点体力。当你体力值等于体力上限时，令所有其他角色依次死亡（X为你已损失体力的20%，向下取整）。",
  ["joy__lianshi"] = "炼石",
  [":joy__lianshi"] = "锁定技，你的手牌上限基数为5；每当你使用、打出或弃置牌时，记录此牌花色，然后若已记录四种花色，你摸一张牌并回复X点体力，然后清空花色记录（X为最后记录的花色对应的牌的点数，对应多张牌时取最高值）。",
  ["@joy__lianshi"] = "炼石",
  ["joy__tuantu"] = "抟土",
  [":joy__tuantu"] = "出牌阶段限一次，你可以从弃牌堆获得与手牌中牌名相同的牌各一张。",

  ["$joy__butian1"] = "断鳌足，以立四极！",
  ["$joy__butian2"] = "洪水涸，九州平！",
  ["$joy__lianshi1"] = "采五方，凝五色，炼石补天，定胜万难！",
  ["$joy__lianshi2"] = "五色蕴华，万物化生！",
  ["$joy__tuantu1"] = "抟黄土作人，力不暇供！",
  ["$joy__tuantu2"] = "引绳于泥中，举以为人！",
}


local joy__xiaoshan = General(extension, "joyo__xiaoshan", "qun", 4, 4, General.Female)

local joy__shanshan = fk.CreateViewAsSkill{
  name = "joy__shanshan",
  anim_type = "defensive",
  pattern = "jink",
  card_filter = function(self, to_select, selected)
    return #selected == 0 and Fk:getCardById(to_select).type == Card.TypeEquip
  end,
  view_as = function(self, cards)
    if #cards ~= 1 then return nil end
    local c = Fk:cloneCard("jink")
    c.skillName = self.name
    c:addSubcard(cards[1])
    return c
  end,
}
local joy__shanshan_trigger = fk.CreateTriggerSkill{
  name = "#joy__shanshan_trigger",
  mute = true,
  main_skill = joy__shanshan,
  events = {fk.TargetConfirmed},
  frequency = Skill.Compulsory,
  can_trigger = function(self, event, target, player, data)
    return data.from ~= player.id and target == player and player:hasSkill(joy__shanshan) and (data.card.trueName == "slash" or data.card:isCommonTrick())
  end,
  on_cost = function (self, event, target, player, data)
    local card = player.room:askForResponse(player, "jink", "jink", "#joy__shanshan-card:::"..data.card:toLogString(), true)
    if card then
      self.cost_data = card
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    room:responseCard{
      from = player.id,
      card = self.cost_data,
    }
    table.insertIfNeed(data.nullifiedTargets, player.id)
    if not player.dead then
      player:drawCards(1, joy__shanshan.name)
    end
  end,
}
joy__shanshan:addRelatedSkill(joy__shanshan_trigger)
joy__xiaoshan:addSkill(joy__shanshan)

local joy__anshi = fk.CreateTriggerSkill{
  name = "joy__anshi",
  anim_type = "control",
  events = {fk.RoundStart, fk.CardUsing, fk.CardResponding, fk.AfterCardsMove, fk.TargetSpecifying, fk.RoundEnd},
  frequency = Skill.Compulsory,
  can_trigger = function(self, event, target, player, data)
    if not player:hasSkill(self) then return end
    if event == fk.RoundStart then return true end
    local mark = player:getMark("@[joy__anshi]joy__anshi-round")
    if not mark then return end
    if event == fk.AfterCardsMove then
      if mark ~= 3 then return end
      local list = {}
      for _, move in ipairs(data) do
        if move.to and move.toArea == Card.PlayerEquip then
          table.insertIfNeed(list, move.to)
        end
        if move.from and table.find(move.moveInfo, function(info) return info.fromArea == Card.PlayerEquip end) then
          table.insertIfNeed(list, move.from)
        end
      end
      list = table.filter(list, function(pid) return #player.room:getPlayerById(pid).player_cards[Player.Equip] > 0 end)
      if #list > 0 then
        player.room:sortPlayersByAction(list)
        self.cost_data = list
        return true
      end
    elseif event == fk.TargetSpecifying then
      return mark == 5 and data.card.type == Card.TypeTrick and data.firstTarget and data.card:isCommonTrick() and not data.card.multiple_targets
    elseif event == fk.RoundEnd then
      return mark == 2
    else
      if mark == 1 then
        return not target.dead and data.card.trueName == "slash"
      elseif mark == 4 then
        return not target.dead and (data.card.name == "peach" or data.card.name == "analeptic")
        and event == fk.CardUsing
      end
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if event == fk.RoundStart then
      local n = math.random(5)
      room:setPlayerMark(player, "@[joy__anshi]joy__anshi-round", n)
    else
      local mark = player:getMark("@[joy__anshi]joy__anshi-round")
      if mark == 1 then
        target:throwAllCards("h")
      elseif mark == 2 then
        local players = table.filter(room:getAlivePlayers(), function (p)
          return #room.logic:getEventsOfScope(GameEvent.UseCard, 1, function(e)
            return e.data[1].from == p.id and e.data[1].card.name == "jink"
          end, Player.HistoryRound) == 0
          and
          #room.logic:getEventsOfScope(GameEvent.RespondCard, 1, function(e)
            return e.data[1].from == p.id and e.data[1].card.name == "jink"
          end, Player.HistoryRound) == 0
          and
          #room.logic:getEventsOfScope(GameEvent.MoveCards, 1, function(e)
            for _, move in ipairs(e.data) do
              if move.from == p.id and move.moveReason == fk.ReasonDiscard then
                for _, info in ipairs(move.moveInfo) do
                  if (info.fromArea == Card.PlayerHand or info.fromArea == Card.PlayerEquip)
                  and Fk:getCardById(info.cardId).name == "jink" then
                    return true
                  end
                end
              end
            end
          end, Player.HistoryRound) == 0
        end)
        for _, p in ipairs(players) do
          if not p.dead then
            room:damage { from = nil, to = p, damage = 1, skillName = self.name, damageType = fk.ThunderDamage }
          end
        end
      elseif mark == 3 then
        for _, pid in ipairs(self.cost_data) do
          room:getPlayerById(pid):throwAllCards("e")
        end
      elseif mark == 4 then
        room:setPlayerMark(target, "@@joy__anshi_prohibit-turn", 1)
      elseif mark == 5 then
        player:drawCards(1, self.name)
      end
    end
  end,
}
local joy__anshi_prohibit = fk.CreateProhibitSkill{
  name = "#joy__anshi_prohibit",
  prohibit_response = function(self, player, card)
    if player:getMark("@@joy__anshi_prohibit-turn") > 0 then
      local subcards = card:isVirtual() and card.subcards or {card.id}
      return #subcards > 0 and table.every(subcards, function(id)
        return table.contains(player.player_cards[Player.Hand], id)
      end)
    end
  end,
  prohibit_use = function(self, player, card)
    if player:getMark("@@joy__anshi_prohibit-turn") > 0 then
      local subcards = card:isVirtual() and card.subcards or {card.id}
      return #subcards > 0 and table.every(subcards, function(id)
        return table.contains(player.player_cards[Player.Hand], id)
      end)
    end
  end,
}
joy__anshi:addRelatedSkill(joy__anshi_prohibit)
joy__xiaoshan:addSkill(joy__anshi)

-- 临时使用 待 private mark优化后再做处理
Fk:addQmlMark{
  name = "joy__anshi",
  qml_path = "",
  how_to_show = function(name, value, p)
    if Self == p then
      return tostring(value)
    end
    return " "
  end,
}

Fk:loadTranslationTable{
  ["joyo__xiaoshan"] = "小闪",

  ["joy__shanshan"] = "闪闪",
  [":joy__shanshan"] = "当你成为其他角色【杀】或普通锦囊的目标后，你可以打出一张【闪】，令此牌对你无效，并摸一张牌。你的装备牌可以当作【闪】使用或打出。",
  ["#joy__shanshan_trigger"] = "闪闪",
  ["#joy__shanshan-card"] = "闪闪：你可以打出一张【闪】，令%arg对你无效",

  ["joy__anshi"] = "暗示",
  [":joy__anshi"] = "锁定枝，每轮开始时，随机选取下列一项效果，于本轮中对每名角色生效（对你可见）："..
  "<br>①使用或打出【杀】时，弃置所有手牌；"..
  "<br>②本轮结束时，未使用、打出、弃置过【闪】的角色依次受到1点雷电伤害；"..
  "<br>③装备区牌数变化后，弃置装备区所有牌；"..
  "<br>④使用【桃】或【酒】时，本回合不能使用或打出手牌；"..
  "<br>⑤使用普通单目标锦囊牌指定目标时，你模一张牌。",
  ["@[joy__anshi]joy__anshi-round"] = "暗示",
  ["@@joy__anshi_prohibit-turn"] = "暗示封牌",

  ["$joy__shanshan1"] = "你等着，略略略~",
  ["$joy__shanshan2"] = "我闪！",
  ["$joy__anshi1"] = "女孩的心思，别猜~",
  ["$joy__anshi2"] = "一个小小的惊喜！",
  ["~joyo__xiaoshan"] = "时间已到，闪人咯！",
}

local khan = General(extension, "joyo__khan", "god", 3)
local tongliao = fk.CreateTriggerSkill{
  name = "joy__tongliao",
  anim_type = "special",
  events = {fk.EventPhaseEnd},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and player.phase == player.Draw and not player:isKongcheng()
  end,
  on_cost = function(self, event, target, player, data)
    local room = player.room
    local ids = table.filter(player.player_cards[Player.Hand], function(id)
      return table.every(player.player_cards[Player.Hand], function(id2)
        return Fk:getCardById(id).number <= Fk:getCardById(id2).number end) end)
    local cards = room:askForCard(player, 1, 1, false, self.name, true, ".|.|.|.|.|.|"..table.concat(ids, ","), "#joy__tongliao-invoke")
    if #cards > 0 then
      self.cost_data = cards[1]
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local id = self.cost_data
    local mark = type(player:getMark("joy__tongliao")) == "table" and player:getMark("joy__tongliao") or {}
    table.insertIfNeed(mark, id)
    room:setPlayerMark(player, "joy__tongliao", mark)
    room:setCardMark(Fk:getCardById(id), "@@joy__tongliao-inhand", 1)
  end,
}
local tongliao_delay = fk.CreateTriggerSkill{
  name = "#joy__tongliao_delay",
  mute = true,
  events = {fk.AfterCardsMove},
  can_trigger = function(self, event, target, player, data)
    if player.dead or type(player:getMark("joy__tongliao")) ~= "table" then return false end
    local mark = player:getMark("joy__tongliao")
    for _, move in ipairs(data) do
      if move.from == player.id then
        for _, info in ipairs(move.moveInfo) do
          if info.fromArea == Card.PlayerHand and table.contains(mark, info.cardId) then
            return true
          end
        end
      end
    end
  end,
  on_cost = Util.TrueFunc,
  on_use = function(self, event, target, player, data)
    local room = player.room
    room:notifySkillInvoked(player, "joy__tongliao", "drawcard")
    player:broadcastSkillInvoke("joy__tongliao")
    local mark = player:getMark("joy__tongliao")
    local x = 0
    for _, move in ipairs(data) do
      if move.from == player.id then
        for _, info in ipairs(move.moveInfo) do
          if info.fromArea == Card.PlayerHand and table.removeOne(mark, info.cardId) then
            x = x + Fk:getCardById(info.cardId).number
          end
        end
      end
    end
    room:setPlayerMark(player, "joy__tongliao", #mark > 0 and mark or 0)
    if x > 0 then
      room:drawCards(player, x, "joy__tongliao")
    end
  end,
}
local wudao = fk.CreateTriggerSkill{
  name = "joy__wudao",
  anim_type = "offensive",
  events = {fk.CardUsing},
  can_trigger = function(self, event, target, player, data)
    if target == player and player:hasSkill(self) and data.card.type ~= Card.TypeEquip then
      local mark = player:getTableMark( "@joy__wudao-turn")
      if table.contains(mark, data.card:getTypeString().."_char") then
        return data.card.sub_type ~= Card.SubtypeDelayedTrick
      else
        local use_e = player.room.logic:getCurrentEvent():findParent(GameEvent.UseCard, true)
        if not use_e then return false end
        local events = player.room.logic.event_recorder[GameEvent.UseCard] or Util.DummyTable
        for i = #events, 1, -1 do
          local e = events[i]
          local use = e.data[1]
          if use.from == player.id then
            if e.id < use_e.id then
              return use.card.type == data.card.type
            end
          end
        end
      end
    end
  end,
  on_cost = function(self, event, target, player, data)
    return table.contains(player:getTableMark( "@joy__wudao-turn"), data.card:getTypeString().."_char") or
    player.room:askForSkillInvoke(player, self.name, nil, "#joy__wudao-invoke:::"..data.card:getTypeString())
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local mark = player:getTableMark( "@joy__wudao-turn")
    local type_name = data.card:getTypeString().."_char"
    if table.contains(mark, type_name) then
      data.disresponsiveList = table.map(room.alive_players, Util.IdMapper)
      if data.card.is_damage_card then
        data.additionalDamage = (data.additionalDamage or 0) + 1
      end
    else
      table.insert(mark, type_name)
      room:setPlayerMark(player, "@joy__wudao-turn", mark)
    end
  end,
}
tongliao:addRelatedSkill(tongliao_delay)
khan:addSkill(tongliao)
khan:addSkill(wudao)
Fk:loadTranslationTable{
  ["joyo__khan"] = "小约翰可汗",
  ["joy__tongliao"] = "通辽",
  [":joy__tongliao"] = "摸牌阶段结束时，你可以将手牌中点数最小的一张牌标记为“通辽”。当你失去“通辽”牌后，你摸X张牌（X为“通辽”牌的点数）。",
  ["joy__wudao"] = "悟道",
  [":joy__wudao"] = "每回合每种类别限一次，当你使用基本牌或锦囊牌时，若此牌与你使用的上一张牌类别相同，你可以令此牌结算结束后，你本回合使用此类型的牌不能被响应且造成的伤害+1。",
  ["#joy__tongliao-invoke"] = "通辽：你可以将一张点数最小的手牌标记为“通辽”牌",
  ["@@joy__tongliao-inhand"] = "通辽",
  ["#joy__tongliao_delay"] = "通辽",
  ["#joy__wudao-invoke"] = "悟道：你可以令当前结算结束后，本回合你使用 %arg 伤害+1且不可被响应",
  ["@joy__wudao-turn"] = "悟道",

  ["$joy__tongliao1"] = "发动偷袭。",
  ["$joy__tongliao2"] = "不够心狠手辣，怎配江山如画。",
  ["$joy__tongliao3"] = "必须出重拳，而且是物理意义上的出重拳。",
  ["$joy__wudao1"] = "众所周知，能力越大，能力也就越大。",
  ["$joy__wudao2"] = "龙争虎斗彼岸花，约翰给你一个家。",
  ["$joy__wudao3"] = "唯一能够打破命运牢笼的，只有我们自己。",
  ["~joy__khan"] = "留得青山在，老天爷饿不死瞎家雀。",
}

local baili = General(extension, "joyo__baili", "god",4,4,General.Female)
local heili = General(extension, "joyo__heili", "god",4,4,General.Female)
baili.total_hidden = true
local tunxing = fk.CreateTriggerSkill{
  name = "joy__tunxing",
  anim_type = "special",
  mute = true,
  frequency = Skill.Compulsory,
  events = {fk.GameStart,"fk.joy__tunxing"},
  can_trigger = function(self, event, target, player, data)
    if event == fk.GameStart then
      return player:hasSkill(self) 
    else
      return target == player and player:hasSkill(self) 
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local mark = type(player:getMark(self.name)) == "table" and player:getMark(self.name) or {}
    if #mark > 0 then
      for _, s in ipairs(mark) do
        room:handleAddLoseSkills(player,"-"..s)
      end
    end
    local lords = {}
    local gp = table.simpleClone(Fk:getAllGenerals())
    table.shuffle(gp)
    for _, general in ipairs(gp) do
      if #lords >= 3 then break end
      for _, s in ipairs(general.skills) do
        if s.lordSkill then
          table.insertIfNeed(lords,general.name)
        end
      end
    end
    local choice
    local result = player.room:askForCustomDialog(player, self.name,
      "packages/utility/qml/ChooseGeneralsAndChoiceBox.qml", {
        lords,
        {"OK"},
        "选择一张武将牌获得其除限定技、主公技外的所有技能",
      })
      if result ~= "" then
        local reply = json.decode(result)
        choice = reply.cards[1]
      end
    mark = {}
    if choice and Fk.generals[choice] ~= nil then
    local skills = table.simpleClone(Fk.generals[choice].skills)
    local skills2 = table.simpleClone(Fk.generals[choice].other_skills)
    if #skills2 > 0 then
      for _, s in ipairs(skills2) do
        table.insertIfNeed(skills,s)
      end
    end
    for _, s in ipairs(skills) do
        if not (table.contains({Skill.Limited, Skill.Wake, Skill.Quest},s.frequency) or s.lordSkill) and not player:hasSkill(s) then
          table.insertIfNeed(mark,s.name)
        end
    end
    room:notifySkillInvoked(player,self.name)
    player:broadcastSkillInvoke(self.name)
    room:setPlayerMark(player,self.name,mark)
    room:handleAddLoseSkills(player,table.concat(mark,"|"))
    end
  end,
}

local mengli = fk.CreateTriggerSkill{
  name = "joy__mengli",
  anim_type = "drawcard",
  frequency = Skill.Compulsory,
  events = {fk.HpChanged},
  can_trigger = function(self, event, target, player, data)
      return target == player and player:hasSkill(self) and (((player.general == "joyo__baili" or player.deputyGeneral == "joyo__baili") and player.hp > 3) or ((player.general == "joyo__heili" or player.deputyGeneral == "joyo__heili") and player.hp <= 3))
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if player.general == "joyo__baili" and player.hp > 3 then
      player.general = "joyo__heili"
      player.room:broadcastProperty(player, "general")
    elseif player.general == "joyo__heili" and player.hp <= 3 then
      player.general = "joyo__baili"
      player.room:broadcastProperty(player, "general")
    end
    if player.deputyGeneral == "joyo__baili" and player.hp > 3 then
      player.deputyGeneral = "joyo__heili"
      player.room:broadcastProperty(player, "deputyGeneral")
    elseif player.deputyGeneral == "joyo__heili" and player.hp <= 3 then
      player.deputyGeneral = "joyo__baili"
      player.room:broadcastProperty(player, "deputyGeneral")
    end
    if not player.dead then
      player:drawCards(1,self.name)
    end
    room.logic:trigger("fk.joy__tunxing", player)
  end,
}
heili:addSkill(tunxing)
heili:addSkill(mengli)
baili:addSkill("joy__tunxing")
baili:addSkill("joy__mengli")
Fk:loadTranslationTable{
  ["joyo__heili"] = "吞星梦狸",
  ["joyo__baili"] = "吞星梦狸",
  ["joy__tunxing"] = "吞星",
  ["joy__mengli"] = "梦狸",
  [":joy__tunxing"] = "锁定技，游戏开始时或你因〖梦狸〗改变形态时，你失去上次以此法获得的技能，然后从3张拥有主公技的武将牌中选择一张，获得该武将牌上除限定技、主公技外的所有技能。",
  [":joy__mengli"] = "锁定技，若你的武将牌为“吞星梦狸”，当你的体力值发生变化后，若你当前体力大于3，你变更为黑狸形态；若你当前体力小于等于3，你变更为白狸形态；你变更形态后摸1张牌。",

  ["$joy__tunxing1"] = "哼哼，每颗星星，看起来都这么美味。",
  ["$joy__tunxing2"] = "北斗七星，快让我尝尝。",
  ["$joy__mengli1"] = "生杀有时，轮回不止。",
  ["$joy__mengli2"] = "破去执念，再塑新生！",
  ["~joyo__heili"] = "这颗北斗星，就是我为你选的..孩子了..",
  ["~joyo__baili"] = "这颗北斗星，就是我为你选的..孩子了..",
}

local joy__dalanmao = General(extension, "joyo__dalanmao", "god", 4)

local joy__zuzhou = fk.CreateTriggerSkill{
  name = "joy__zuzhou",
  events = {fk.TurnStart},
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(self) and not target.dead and target.hp > 0
  end,
  on_cost = function(self, event, target, player, data)
    return player.room:askForSkillInvoke(player, self.name, nil, "#joy__zuzhou-invoke:"..target.id)
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    room:loseHp(player, 1, self.name)
    local choice = room:askForChoice(player, {"joy__zuzhou_slash2jink","joy__zuzhou_jink2slash"}, self.name)
    room:setPlayerMark(target, "@joy__zuzhou-turn", choice)
    target:filterHandcards()
  end,
}
local joy__zuzhou_filter = fk.CreateFilterSkill{
  name = "#joy__zuzhou_filter",
  card_filter = function(self, card, player)
    local mark = player:getMark("@joy__zuzhou-turn")
    if mark ~= 0 then
      if mark == "joy__zuzhou_slash2jink" then
        return table.contains(player.player_cards[Player.Hand], card.id) and card.trueName == "slash"
      else
        return table.contains(player.player_cards[Player.Hand], card.id) and card.trueName == "jink"
      end
    end
  end,
  view_as = function(self, card)
    if card.trueName == "slash" then
      return Fk:cloneCard("jink", card.suit, card.number)
    else
      return Fk:cloneCard("slash", card.suit, card.number)
    end
  end,
}
joy__zuzhou:addRelatedSkill(joy__zuzhou_filter)
joy__dalanmao:addSkill(joy__zuzhou)


local joy__moyu = fk.CreateTriggerSkill{
  name = "joy__moyu",
  events = {fk.EventPhaseStart},
  anim_type = "defensive",
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(self) and target == player and player.phase == Player.Play
  end,
  on_cost = function(self, event, target, player, data)
    return player.room:askForSkillInvoke(player, self.name, nil, "#joy__moyu-invoke")
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    room:addPlayerMark(player, MarkEnum.AddMaxCardsInTurn, 2)
    room:addPlayerMark(player, "joy__moyu_draw")
    room:addPlayerMark(player, "@@joy__moyu-turn")
    room:broadcastProperty(player, "MaxCards")
  end,
}

local joy__moyu_delay = fk.CreateTriggerSkill{
  name = "#joy__moyu_delay",
  mute = true,
  events = {fk.TurnEnd, fk.DrawNCards},
  can_trigger = function(self, event, target, player, data)
    if event == fk.TurnEnd then
      return target == player and player:isWounded() and player:getMark("@@joy__moyu-turn") > 0
    else
      return target == player and player:getMark("joy__moyu_draw-turn") > 0
    end
  end,
  on_cost = Util.TrueFunc,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if event == fk.TurnEnd then
      room:recover { num = math.min(player.maxHp-player.hp,player:getMark("@@joy__moyu-turn")), skillName = "joy__moyu", who = player, recoverBy = player }
    else
      data.n = data.n + 2 * player:getMark("joy__moyu_draw-turn")
    end
  end,

  refresh_events = {fk.TurnStart},
  can_refresh = function (self, event, target, player, data)
    return target == player and player:getMark("joy__moyu_draw") > 0
  end,
  on_refresh = function (self, event, target, player, data)
    player.room:setPlayerMark(player, "joy__moyu_draw-turn", player:getMark("joy__moyu_draw"))
    player.room:setPlayerMark(player, "joy__moyu_draw", 0)
  end,
}
joy__moyu:addRelatedSkill(joy__moyu_delay)

local joy__moyu_prohibit = fk.CreateProhibitSkill{
  name = "#joy__moyu_prohibit",
  is_prohibited = function(self, from, to, card)
    return from:getMark("@@joy__moyu-turn") > 0 and from ~= to
  end,
}
joy__moyu:addRelatedSkill(joy__moyu_prohibit)

joy__dalanmao:addSkill(joy__moyu)

local joy__sanlian = fk.CreateActiveSkill{
  name = "joy__sanlian",
  anim_type = "drawcard",
  card_num = 3,
  target_num = 0,
  card_filter = function(self, to_select, selected)
    return #selected < 3
    and table.contains(Self.player_cards[Player.Hand], to_select)
    and not Self:prohibitDiscard(Fk:getCardById(to_select))
    and table.every(selected, function (id)
      return Fk:getCardById(id).type == Fk:getCardById(to_select).type
    end)
  end,
  can_use = function(self, player)
    return player:getHandcardNum() > 2
  end,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    local same = table.every(effect.cards, function (id)
      return Fk:getCardById(id).trueName == Fk:getCardById(effect.cards[1]).trueName
    end)
    room:throwCard(effect.cards, self.name, player, player)
    if not player.dead then
      player:drawCards(player:getLostHp(), self.name)
    end
    for _, p in ipairs(room:getAlivePlayers()) do
      if not p.dead then
        room:damage { from = player, to = p, damage = 1, skillName = self.name }
      end
    end
    if not player.dead and same then
      for _, p in ipairs(room:getOtherPlayers(player)) do
        if player.dead then break end
        if not p:isNude() then
          local id = room:askForCardChosen(player, p, "he", self.name)
          room:throwCard({id}, self.name, p, player)
        end
      end
    end
  end,
}
joy__dalanmao:addSkill(joy__sanlian)

Fk:loadTranslationTable{
  ["joyo__dalanmao"] = "大懒猫",

  ["joy__zuzhou"] = "诅咒",
  [":joy__zuzhou"] = "一名角色的回合开始时，你可以失去1点体力并选择一项：1.令其手牌中的所有【杀】视为【闪】直到其回合结束；2.令其手牌中的所有【闪】视为【杀】直到其回合结束。",
  ["#joy__zuzhou-invoke"] = "诅咒:你可以令 %src 本回合【杀】视为【闪】，或【闪】视为【杀】",
  ["joy__zuzhou_slash2jink"] = "杀视为闪",
  ["joy__zuzhou_jink2slash"] = "闪视为杀",
  ["@joy__zuzhou-turn"] = "诅咒",
  ["#joy__zuzhou_filter"] = "诅咒",

  ["joy__moyu"] = "摸鱼",
  [":joy__moyu"] = "出牌阶段开始时，你可以令你本回合手牌上限+2，下个回合摸牌阶段摸牌数+2；若如此做，则你本回合使用牌不能指定其他角色为目标，且回合结束时回复1点体力。",
  ["#joy__moyu-invoke"] = "摸鱼:可以令本回合手牌上限+2，下个回合摸牌数+2，本回合不能指定其他角色为目标，回合结束时回复1点体力",
  ["#joy__moyu_delay"] = "摸鱼",
  ["@@joy__moyu-turn"] = "摸鱼",

  ["joy__sanlian"] = "三连",
  [":joy__sanlian"] = "出牌阶段，你可以弃置三张类型相同的手牌并摸X张牌（X为你已损失的体力值），然后对所有角色造成1点伤害，若你弃置的牌的牌名相同，则你弃置所有其他角色各一张牌。",
  ["#joy__sanlian-prompt"] = "三连：弃置三张类型相同的手牌并摸已损失的体力值张牌，对所有角色造成1点伤害",

  ["$joy__zuzhou1"] = "嗯喵！喵！！",
  ["$joy__moyu1"] = "呜呜呜~喵~",
  ["$joy__sanlian1"] = "喵！喵！！喵！！！",
  ["~joyo__dalanmao"] = "喵……",
}

local sunwukong = General(extension, "joyo__sunwukong", "god",4)
local seven = fk.CreateActiveSkill{
  name = "joy__seven",
  anim_type = "drawcard",
  card_num = 1,
  prompt = "每个类型限一次，你可以将基本牌重铸为锦囊牌，锦囊牌重铸为装备牌，装备牌重铸为基本牌。",
  can_use = function (self, player, card, extra_data)
    local mark = type(Self:getMark(self.name.."-turn")) == "table" and Self:getMark(self.name.."-turn") or {}
    return #mark < 3
  end,
  card_filter = function (self, to_select, selected, selected_targets)
    local mark = type(Self:getMark(self.name.."-turn")) == "table" and Self:getMark(self.name.."-turn") or {}
    return #selected == 0 and not table.contains(mark,Fk:getCardById(to_select).type)
  end,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    local card = Fk:getCardById(effect.cards[1])
    local typec
    local mark = type(player:getMark(self.name.."-turn")) == "table" and player:getMark(self.name.."-turn") or {}
    table.insertIfNeed(mark,card.type)
    room:setPlayerMark(player,self.name.."-turn",mark)
    if card.type == Card.TypeBasic then
      typec = "trick"
    elseif card.type == Card.TypeTrick then
      typec = "equip"
    elseif card.type == Card.TypeEquip then
      typec = "basic"
    end
    local card_ids = effect.cards
    local who = room:getPlayerById(effect.from)
    if type(card_ids) == "number" then
      card_ids = {card_ids}
    end
    local skillName = self.name
    room:moveCards({
      ids = card_ids,
      from = who.id,
      toArea = Card.DiscardPile,
      skillName = skillName,
      moveReason = fk.ReasonRecast,
      proposer = who.id
    })
    room:sendFootnote(card_ids, {
      type = "##RecastCard",
      from = who.id,
    })
    room:broadcastPlaySound("./audio/system/recast")
    room:sendLog{
      type = skillName == "recast" and "#Recast" or "#RecastBySkill",
      from = who.id,
      card = card_ids,
      arg = skillName,
    }
    if player.dead then return end
    local cards = room:getCardsFromPileByRule(".|.|.|.|.|"..typec,1,"allPiles")
    if #cards > 0 then
      room:moveCards({
        ids = cards,
        to = player.id,
        toArea = Card.PlayerHand,
        moveReason = fk.ReasonJustMove,
        proposer = player.id,
        skillName = self.name,
      })
    end
  end,
}
local qitian = fk.CreateTriggerSkill{
  name = "joy__qitian",
  frequency = Skill.Wake,
  events = {fk.GameStart,fk.HpChanged},
  can_trigger = function(self, event, target, player, data)
    if event == fk.GameStart then
      return  player:hasSkill(self) and
      player:usedSkillTimes(self.name, Player.HistoryGame) == 0 and player.hp == 1
    else
      return  target == player and player:hasSkill(self) and
      player:usedSkillTimes(self.name, Player.HistoryGame) == 0 and player.hp == 1
    end
  end,
  can_wake = function(self, event, target, player, data)
    return player.hp == 1
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    room:changeMaxHp(player,-1)
    if not player.dead then
      room:handleAddLoseSkills(player,"joy__huoyan|joy__jindouyun")
    end
  end,
}
local huoyan = fk.CreateVisibilitySkill{
  name = "joy__huoyan",
  frequency = Skill.Compulsory,
  card_visible = function(self, player, card)
    if player:hasSkill(self) and Fk:currentRoom():getCardArea(card) == Card.PlayerHand then
      return true
    end
  end
}
local jindouyun = fk.CreateDistanceSkill{
  name = "joy__jindouyun",
  correct_func = function(self, from, to)
    if to:hasSkill(self) then
      return 1
    end
    if from:hasSkill(self) then
      return - 1
    end
    return 0
  end,
}
local ruyi_change = fk.CreateActiveSkill{
  name = "joy__ruyi_change",
  prompt = "选择【如意金箍棒】的范围。",
  frequency = Skill.Compulsory,
  card_num = 0,
  target_num = 0,
  interaction = function()
    return UI.Spin { from = 1, to = 4 }
  end,
  card_filter = Util.FalseFunc,
}
Fk:addSkill(ruyi_change)
local ruyi = fk.CreateTriggerSkill{
  name = "joy__ruyi",
  frequency = Skill.Compulsory,
  anim_type = "offensive",
  events = {fk.TurnStart},
  can_trigger = function(self, event, target, player, data)
    if event == fk.TurnStart then
      return target == player and player:hasSkill(self) and not player:getEquipment(Card.SubtypeWeapon)
    end
  end,
  on_cost = function (self, event, target, player, data)
    local room = player.room
    local _, ret = room:askForUseActiveSkill(player, "joy__ruyi_change", "选择【如意金箍棒】的攻击范围。", true)
    local n
    if _ and ret then
      n = ret.interaction

    if n and n > 0 then
      self.cost_data = n
      return true
    end
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    room:setPlayerMark(player,"@joy_ruyi",self.cost_data)
  end,
  refresh_events = {fk.CardUsing},
  can_refresh = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and
      data.card.trueName == "slash" and  not player:getEquipment(Card.SubtypeWeapon)
  end,
  on_refresh = function(self, event, target, player, data)
    player:broadcastSkillInvoke(self.name)
    if player:getMark("@joy_ruyi") <= 1 then
      player.room:broadcastPlaySound("./packages/mobile/audio/card/ex_crossbow")
    else
      player.room:broadcastPlaySound("./packages/mobile/audio/card/ex_vineburn")
    end
    player.room:doAnimate("InvokeSkill", {
      name = "金箍棒",
      player = player.id,
      skill_type = "offensive",
    })
  end,
}
local ruyi_attackrange = fk.CreateAttackRangeSkill{
  name = "#joy__ruyi_attackrange",
  fixed_func = function (self, player)
    if player:hasSkill(ruyi) and player:getMark("@joy_ruyi") ~= 0 then
      return player:getMark("@joy_ruyi")
    end
  end,
}
local ruyi_targetmod = fk.CreateTargetModSkill{
  name = "#joy__ruyi_targetmod",
  bypass_times = function(self, player, skill, scope)
    if player:hasSkill(self) and not player:getEquipment(Card.SubtypeWeapon)  then 
      return player:hasSkill(ruyi) and player:getMark("@joy_ruyi") <= 1 and skill.trueName == "slash_skill" and scope == Player.HistoryPhase
    end
  end,
}
local ruyi_trigger = fk.CreateTriggerSkill{
  name = "#joy__ruyi_trigger",
  mute = true,
  events = {fk.AfterCardUseDeclared, fk.AfterCardTargetDeclared,fk.EventAcquireSkill},
  frequency = Skill.Compulsory,
  can_trigger = function(self, event, target, player, data)
    if not player:hasSkill(self) or player:getEquipment(Card.SubtypeWeapon) ~= nil then return false end
    if event == fk.EventAcquireSkill then
      return data == ruyi and target == player 
    end
    if player == target and player:hasSkill(ruyi) and data.card.trueName == "slash" then
      if event == fk.AfterCardUseDeclared then
        if player:getMark("@joy_ruyi") == 2 then
        local n=0
        player.room.logic:getEventsOfScope(GameEvent.UseCard,2, function (e)
          local use = e.data[1]
          if use.from and use.from==player.id and use.card.trueName == "slash"  then
            n=n+1
          end
          return false
        end, Player.HistoryTurn)
        return n == 1  
        else
          return player:getMark("@joy_ruyi") == 3 
        end
      else
        return player:getMark("@joy_ruyi") == 4 and #player.room:getUseExtraTargets( data) > 0
      end
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if not player:hasSkill(self) or player:getEquipment(Card.SubtypeWeapon) ~= nil then return false end
    if event == fk.EventAcquireSkill  then
      room:setPlayerMark(player, "@joy_ruyi", 1)
    else
      if player:getMark("@joy_ruyi") == 2 then
        data.additionalDamage = (data.additionalDamage or 0) + 1
      elseif player:getMark("@joy_ruyi") == 3 then
        data.disresponsiveList = table.map(room.alive_players, Util.IdMapper)
      else
        local to = room:askForChoosePlayers(player, player.room:getUseExtraTargets( data),
        1, 1, "#ruyi-choose:::"..data.card:toLogString(), ruyi.name, true)
        if #to > 0 then
          table.insert(data.tos, to)
        end
      end
    end
  end,
}
ruyi:addRelatedSkill(ruyi_attackrange)
ruyi:addRelatedSkill(ruyi_targetmod)
ruyi:addRelatedSkill(ruyi_trigger)
sunwukong:addSkill(seven)
sunwukong:addSkill(ruyi)
sunwukong:addSkill(qitian)
sunwukong:addRelatedSkill(huoyan)
sunwukong:addRelatedSkill(jindouyun)
Fk:loadTranslationTable{
  ["joyo__sunwukong"] = "孙悟空",
  ["joy__seven"] = "72变",
  [":joy__seven"] = "出牌阶段每个类型牌限一次，你可以将基本牌重铸为锦囊牌，锦囊牌重铸为装备牌，装备牌重铸为基本牌。",
  ["joy__qitian"] = "齐天",
  [":joy__qitian"] = "觉醒技，当你的体力值为1时，你减1点体力上限，然后获得〖火眼〗和〖筋斗云〗。",
  ["joy__huoyan"] = "火眼",
  [":joy__huoyan"] = "锁定技，全场其他角色手牌对你可见。",
  ["joy__jindouyun"] = "筋斗云",
  [":joy__jindouyun"] = "锁定技，你计算与其他角色距离-1，其他角色计算与你的距离+1。",
  ["joy__ruyi"] = "如意",
  ["@joy_ruyi"] = "如意",
  ["joy__ruyi_change"] = "如意",
  ["#joy__ruyi_attackrange"] = "金箍棒",
  ["#joy__ruyi_targetmod"] = "金箍棒",
  ["#joy__ruyi_trigger"] = "金箍棒",
  [":joy__ruyi"] = "锁定技，若你的装备区里没有武器牌，你视为装备着【如意金箍棒】。"..
  "<br><font color='grey'>【如意金箍棒】<br>装备牌·武器<br /><b>攻击范围</b>：1~4<br /><b>武器技能</b>：初始攻击范围为1，你的回合开始时，可以调整【如意金箍棒】的攻击范围并获得对应效果："..
  "<br>1，你使用的【杀】不计次数。<br>2,你每回合使用的第一张【杀】伤害+1。<br>3,你使用的【杀】无法被响应。<br>4,你使用【杀】可以额外增加一个目标。",

  ["$joy__seven1"] = "看我72变！",
  ["$joy__qitian1"] = "俺来500年前大闹天宫的齐天大圣——孙悟空！",
  ["$joy__ruyi1"] = "呔！吃俺老孙一棒！",
  ["~joyo__sunwukong"] = "俺老孙，不服..",
}
return extension
