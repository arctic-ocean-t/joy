local desc_joy = [[
  # 欢杀身份模式

  ___

  起始手牌增加为5张，主公开局30秒内可以投降，全场移除宝物栏

]]

local joy_getlogic = function()
  local joy_logic = GameLogic:subclass("joy_logic")


  function joy_logic:chooseGenerals()
    local room = self.room ---@class Room
    local generalNum = room.settings.generalNum
    local n = room.settings.enableDeputy and 2 or 1
    local lord = room:getLord()
    local lord_generals = {}
    local lord_num = 3

    if lord ~= nil then
      room.current = lord
      local generals = table.connect(room:findGenerals(function(g)
        return table.find(Fk.generals[g].skills, function(s) return s.lordSkill end)
      end, lord_num), room:getNGenerals(generalNum))
      if #room.general_pile < (#room.players - 1) * generalNum then
        room:gameOver("")
      end
      --if room.settings.generalNum == 3 then
       -- lord_generals = table.random(generals,n)
     -- else
        lord_generals = room:askForGeneral(lord, generals, n)
     -- end
      local lord_general, deputy
      if type(lord_generals) == "table" then
        deputy = lord_generals[2]
        lord_general = lord_generals[1]
      else
        lord_general = lord_generals
        lord_generals = {lord_general}
      end
      generals = table.filter(generals, function(g)
        return not table.find(lord_generals, function(lg)
          return Fk.generals[lg].trueName == Fk.generals[g].trueName
        end)
      end)
      room:returnToGeneralPile(generals)

      room:setPlayerGeneral(lord, lord_general, true)
      room:askForChooseKingdom({lord})
      room:broadcastProperty(lord, "general")
      room:broadcastProperty(lord, "kingdom")
      room:setDeputyGeneral(lord, deputy)
      room:broadcastProperty(lord, "deputyGeneral")

    -- 显示技能
      local canAttachSkill = function(player, skillName)
        local skill = Fk.skills[skillName]
        if not skill then
          fk.qCritical("Skill: "..skillName.." doesn't exist!")
          return false
        end
        if skill.lordSkill and (player.role ~= "lord" or #room.players < 5) then
          return false
        end

        if #skill.attachedKingdom > 0 and not table.contains(skill.attachedKingdom, player.kingdom) then
          return false
        end

        return true
      end

      local lord_skills = {}
      for _, s in ipairs(Fk.generals[lord.general].skills) do
        if canAttachSkill(lord, s.name) then
          table.insertIfNeed(lord_skills, s.name)
        end
      end
      for _, sname in ipairs(Fk.generals[lord.general].other_skills) do
        if canAttachSkill(lord, sname) then
          table.insertIfNeed(lord_skills, sname)
        end
      end

      local deputyGeneral = Fk.generals[lord.deputyGeneral]
      if deputyGeneral then
        for _, s in ipairs(deputyGeneral.skills) do
          if canAttachSkill(lord, s.name) then
            table.insertIfNeed(lord_skills, s.name)
          end
        end
        for _, sname in ipairs(deputyGeneral.other_skills) do
          if canAttachSkill(lord, sname) then
            table.insertIfNeed(lord_skills, sname)
          end
        end
      end
      for _, skill in ipairs(lord_skills) do
        room:doBroadcastNotify("AddSkill", json.encode{
          lord.id,
          skill
        })
      end
    end

    local nonlord = room:getOtherPlayers(lord, true)
    local generals = room:getNGenerals(#nonlord * generalNum)
    table.shuffle(generals)
    for i, p in ipairs(nonlord) do
      local arg = table.slice(generals, (i - 1) * generalNum + 1, i * generalNum + 1)
      p.request_data = json.encode{ arg, n }
      p.default_reply = table.random(arg, n)
    end
   -- if room.settings.generalNum == 3 then
     -- for _, p in ipairs(nonlord) do
      --  p.general = table.random(generals,n)
     -- end
   -- else
      room:notifyMoveFocus(nonlord, "AskForGeneral")
      room:doBroadcastRequest("AskForGeneral", nonlord)
   -- end

    local selected = {}
    for _, p in ipairs(nonlord) do
      if p.general == "" and p.reply_ready then
        local general_ret = json.decode(p.client_reply)
        local general = general_ret[1]
        local deputy = general_ret[2]
        table.insertTableIfNeed(selected, general_ret)
        room:setPlayerGeneral(p, general, true, true)
        room:setDeputyGeneral(p, deputy)
      else
        table.insertTableIfNeed(selected, p.default_reply)
        room:setPlayerGeneral(p, p.default_reply[1], true, true)
        room:setDeputyGeneral(p, p.default_reply[2])
      end
      p.default_reply = ""
    end

    generals = table.filter(generals, function(g)
      return not table.find(selected, function(lg)
        return Fk.generals[lg].trueName == Fk.generals[g].trueName
      end)
    end)
    room:returnToGeneralPile(generals)

    room:askForChooseKingdom(nonlord)
  end

return joy_logic
end


local joy_rule = fk.CreateTriggerSkill{
  name = "#joy_rule",
  priority = 0.001,
  refresh_events = {fk.DrawInitialCards,fk.GamePrepared},
  can_refresh = function(self, event, target, player, data)
    return  target == player or event == fk.GamePrepared
  end,
  on_refresh = function(self, event, target, player, data)
    local room = player.room
    if event == fk.DrawInitialCards then
        data.num = data.num + 1
    elseif event == fk.GamePrepared then
      for _, p in ipairs(room.alive_players) do
        --table.contains(p.sealedSlots,Player.TreasureSlot)
        --room:abortPlayerArea(p,Player.TreasureSlot)
        table.removeOne(p.equipSlots, Player.TreasureSlot)
      end
    end
  end,
}
Fk:addSkill(joy_rule)

local joy_mode = fk.CreateGameMode{
  name = "joy_mode",
  minPlayer = 2,
  maxPlayer = 8,
  rule = joy_rule,
  logic = joy_getlogic,
  surrender_func = function(self, playedTime)
    local roleCheck = false
    local roleText = ""
    local roleTable = {
      { "lord" },
      { "lord", "rebel" },
      { "lord", "rebel", "renegade" },
      { "lord", "loyalist", "rebel", "renegade" },
      { "lord", "loyalist", "rebel", "rebel", "renegade" },
      { "lord", "loyalist", "rebel", "rebel", "rebel", "renegade" },
      { "lord", "loyalist", "loyalist", "rebel", "rebel", "rebel", "renegade" },
      { "lord", "loyalist", "loyalist", "rebel", "rebel", "rebel", "rebel", "renegade" },
    }

    roleTable = roleTable[#Fk:currentRoom().players]

    if Self.role == "renegade" then
      local rebelNum = #table.filter(roleTable, function(role)
        return role == "rebel"
      end)

      for _, p in ipairs(Fk:currentRoom().players) do
        if p.role == "rebel" then
          if not p.dead then
            break
          else
            rebelNum = rebelNum - 1
          end
        end
      end

      roleCheck = rebelNum == 0
      roleText = "left lord and loyalist alive"
    elseif Self.role == "rebel" then
      local rebelNum = #table.filter(roleTable, function(role)
        return role == "rebel"
      end)

      local renegadeDead = not table.find(roleTable, function(role)
        return role == "renegade"
      end)
      for _, p in ipairs(Fk:currentRoom().players) do
        if p.role == "renegade" and p.dead then
          renegadeDead = true
        end

        if p ~= Self and p.role == "rebel" then
          if not p.dead then
            break
          else
            rebelNum = rebelNum - 1
          end
        end
      end

      roleCheck = renegadeDead and rebelNum == 1
      roleText = "left one rebel alive"
    else
      if Self.role == "loyalist" then
        return { { text = "loyalist never surrender", passed = false } }
      else
        if #Fk:currentRoom().alive_players == 2 then
          roleCheck = true
        else
          local lordNum = #table.filter(roleTable, function(role)
            return role == "lord" or role == "loyalist"
          end)

          local renegadeDead = not table.find(roleTable, function(role)
            return role == "renegade"
          end)
          for _, p in ipairs(Fk:currentRoom().players) do
            if p.role == "renegade" and p.dead then
              renegadeDead = true
            end

            if p ~= Self and (p.role == "lord" or p.role == "loyalist") then
              if not p.dead then
                break
              else
                lordNum = lordNum - 1
              end
            end
          end

          roleCheck = (renegadeDead and lordNum == 1) or playedTime <= 30
        end
      end

      roleText = "left you alive or time < 30s "
    end

    return {
      { text = roleText, passed = roleCheck },
    } 
  end,

}
Fk:loadTranslationTable{
  ["joy_mode"] = "欢杀身份场",
  [":joy_mode"] = desc_joy,
  ["time limitation: 5 min or < 30s"] = "游戏时长大于5分钟或小于30秒",
  ["left you alive or time < 30s "] = "同阵营仅你存活或游戏时长小于30秒"
}

return joy_mode