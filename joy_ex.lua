local extension = Package("joy_ex")
extension.extensionName = "joy"

Fk:loadTranslationTable{
  ["joy_ex"] = "欢乐-界标",
  ["joyex"] = "欢乐界",
}

local U = require "packages/utility/utility"

local zhugeliang = General(extension, "joyex__zhugeliang", "shu", 3)
local guanxing = fk.CreateActiveSkill{
  name = "joyex__guanxing",
  anim_type = "control",
  can_use = function(self, player)
    return #player:getPile("joyex__xing") > 0
  end,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    room:addPlayerMark(player,"joyex__getxing-turn",#player:getPile("joyex__xing"))
    room:moveCardTo(player:getPile("joyex__xing"), Card.PlayerHand, player, fk.ReasonPrey, self.name)
    if player:getMark("joyex__getxing-turn") >= (#room.alive_players > 2 and 5 or 3) then
      room:setPlayerMark(player,"joyex__zhugelianlu-turn",1)
    end
  end,
}
local guanxing_trigger = fk.CreateTriggerSkill{
  name = "#joyex__guanxing_trigger",
  anim_type = "control",
  events = {fk.EventPhaseStart},
  can_trigger = function(self, event, target, player, data)
    if target == player and player:hasSkill(self) then
      return player.phase == Player.Start or player.phase == Player.Finish
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local x = #room.alive_players > 2 and 5 or 3
    local top = room:getNCards(#room.alive_players > 2 and 5 or 3)
    if #player:getPile("joyex__xing") < (#room.alive_players > 2 and 5 or 3) then
      local ids , _ = U.askforChooseCardsAndChoice(player, top, {"OK"}, self.name, "#joyex__guanxing-card", {"Cancel"}, 1, 1, top)
      if #ids > 0 then
        player:addToPile("joyex__xing",ids, true, self.name)
        top = table.filter(top, function(id) return room:getCardArea(id) ~= Card.PlayerSpecial end)
      else
      end
    end
    room:askForGuanxing(player,top)
  end,
}
local zhugelianlu = fk.CreateTargetModSkill{
  name = "#joyex__zhugelianlu",
  frequency = Skill.Compulsory,
  bypass_times = function(self, player, skill, scope)
    if player:getMark("joyex__zhugelianlu-turn") > 0 and skill.trueName == "slash_skill"
      and scope == Player.HistoryPhase then
      return true
    end
  end,
}
local zhugelianluAudio = fk.CreateTriggerSkill{
  name = "#joyex__zhugelianluAudio",
  visible = false,
  refresh_events = {fk.CardUsing},
  can_refresh = function(self, event, target, player, data)
    return target == player and player:getMark("joyex__zhugelianlu") > 0 and
      data.card.trueName == "slash" and
      player:usedCardTimes("slash") > 1
  end,
  on_refresh = function(self, event, target, player, data)
    local room = player.room
    room:broadcastPlaySound("./packages/standard_cards/audio/card/crossbow")
    room:setEmotion(player, "./packages/standard_cards/image/anim/crossbow")
    room:sendLog{
      type = "#InvokeSkill",
      from = player.id,
      arg = "crossbow",
    }
  end,
}
local kongchengAudio = fk.CreateTriggerSkill{
  name = "#joyex__kongchengAudio",
  refresh_events = {fk.AfterCardsMove},
  can_refresh = function(self, event, target, player, data)
    if not player:hasSkill(self) then return end
    if not player:isKongcheng() then return end
    for _, move in ipairs(data) do
      if move.from == player.id then
        for _, info in ipairs(move.moveInfo) do
          if info.fromArea == Card.PlayerHand then
            return true
          end
        end
      end
    end
  end,
  on_refresh = function(self, event, target, player, data)
    player:broadcastSkillInvoke("joyex__kongcheng")
    player.room:notifySkillInvoked(player, "joyex__kongcheng", "defensive")
  end,
}
local kongcheng = fk.CreateProhibitSkill{
  name = "joyex__kongcheng",
  frequency = Skill.Compulsory,
  is_prohibited = function(self, from, to, card)
    if to:hasSkill(self) and to:isKongcheng() then
      return card.trueName == "slash" or card.trueName == "duel" or card.trueName == "snatch"
    end
  end,
}
guanxing:addRelatedSkill(zhugelianluAudio)
guanxing:addRelatedSkill(zhugelianlu)
guanxing:addRelatedSkill(guanxing_trigger)
zhugeliang:addSkill(guanxing)
zhugeliang:addSkill(kongcheng)
Fk:loadTranslationTable{
  ["joyex__zhugeliang"] = "界诸葛亮",
  ["joyex__guanxing"] = "观星",
  [":joyex__guanxing"] = "准备阶段和结束阶段，你可以观看牌堆顶的X张牌，然后你可以将其中一张牌置于武将牌上称为“星”，剩余牌以任意顺序置于牌堆顶或牌堆底。"..
  "<br>出牌阶段，你可以将你所有的“星”置入手牌中，若你本回合以此法获得的“星”不少于X，则视为装备了【诸葛连弩】。<br>（“星”至多为X，X为5，仅2人时改为3）",
  ["joyex__kongcheng"] = "空城",
  [":joyex__kongcheng"] = "锁定技，若你没有手牌，你不能被选择为【杀】,【决斗】或【顺手牵羊】的目标。",
  ["#joyex__guanxing_trigger"] = "观星",
  ["#joyex__zhugelianlu"] = "诸葛连弩",
  ["#joyex__zhugelianluAudio"] = "诸葛连弩",
  ["#joyex__kongchengAudio"] = "空城",

  ["#joyex__guanxing-card"] = "观星：你可以将其中一张牌置入“星”",
  ["joyex__xing"] = "星",

  ["$joyex__guanxing1"] = "半生韶华付社稷，一枕清梦压星河。",
  ["$joyex__guanxing2"] = "繁星四百八十万，颗颗鉴照老臣心。",
  ["$joyex__kongcheng1"] = "一琴一曲一城，一人一诺一生。",
  ["$joyex__kongcheng2"] = "老夫独守此城，何惧万马千军！",
  ["~joyex__zhugeliang"] = "独木难支益州地，上方雨落万事空。",

}

local zhangfei = General(extension, "joyex__zhangfei", "shu", 4)
local paoxiaoAudio = fk.CreateTriggerSkill{
  name = "#joyex__paoxiaoAudio",
  refresh_events = {fk.CardUsing},
  can_refresh = function(self, event, target, player, data)
    return target == player and player:hasSkill("joyex__paoxiao") and
      data.card.trueName == "slash" and
      player:usedCardTimes("slash") > 1
  end,
  on_refresh = function(self, event, target, player, data)
    player:broadcastSkillInvoke("joyex__paoxiao")
    player.room:doAnimate("InvokeSkill", {
      name = "joyex__paoxiao",
      player = player.id,
      skill_type = "offensive",
    })
  end,
}
local paoxiao = fk.CreateTargetModSkill{
  name = "joyex__paoxiao",
  frequency = Skill.Compulsory,
  residue_func = function(self, player, skill, scope)
    if player:hasSkill(self) and skill.trueName == "slash_skill"
      and scope == Player.HistoryPhase then
      return 999
    end
  end,
  bypass_distances = function(self, player, skill, scope)
    return player:hasSkill(self) and skill.trueName == "slash_skill" and player:usedCardTimes("slash", Player.HistoryPhase) > 0 and player.phase == Player.Play
  end,
}
paoxiao:addRelatedSkill(paoxiaoAudio)
local tishen = fk.CreateTriggerSkill{
  name = "joyex__tishen",
  anim_type = "offensive",
  frequency = Skill.Compulsory,
  events = {fk.CardEffectCancelledOut, fk.TargetSpecified,fk.CardUseFinished},
  can_trigger = function(self, event, target, player, data)
    if event == fk.CardEffectCancelledOut then
      return target == player and player:hasSkill(self) and data.card.trueName == "slash" and player:getMark("@@joyex__tishen-phase") == 0 and player.phase == Player.Play
    elseif event == fk.TargetSpecified  then
      return target == player and player:hasSkill(self) and player:getMark("@@joyex__tishen-phase") > 0 and data.card.trueName == "slash"
    elseif event == fk.CardUseFinished then
      return table.contains(TargetGroup:getRealTargets(data.tos), player.id) and player:hasSkill(self) and not (data.damageDealt and data.damageDealt[player.id])  and data.card and data.card.trueName == "slash" and player.room:getCardArea(data.card) == Card.Processing
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if event == fk.CardEffectCancelledOut then
        room:setPlayerMark(player, "@@joyex__tishen-phase", 1)
    elseif event == fk.TargetSpecified then
      room:setPlayerMark(player, "@@joyex__tishen-phase", 0)
      data.disresponsive = true
      data.additionalDamage = (data.additionalDamage or 0) + 1
    elseif  event == fk.CardUseFinished then
      if not player.dead then
        player.room:obtainCard(player.id, data.card, true, fk.ReasonPrey)
      end
    end
  end,

}

zhangfei:addSkill(paoxiao)
zhangfei:addSkill(tishen)
Fk:loadTranslationTable{
  ["joyex__zhangfei"] = "界张飞",
  ["joyex__paoxiao"] = "咆哮",
  [":joyex__paoxiao"] = "锁定技，①你使用【杀】无次数限制。②你的出牌阶段，若你于当前阶段使用过【杀】，则你于此阶段使用【杀】无距离限制。",
  ["joyex__tishen"] = "替身",
  [":joyex__tishen"] = "锁定技，①回合外，获得所有对你使用且未对你造成伤害的【杀】。<br>②出牌阶段，当你使用的【杀】被【闪】抵消后，本阶段你使用的下一张【杀】不可响应且伤害+1。",

  ["@@joyex__tishen-phase"] = "替身",

  ["$joyex__paoxiao1"] = " ",
  ["$joyex__paoxiao2"] = " ",
  ["$joyex__tishen1"] = " ",
  ["$joyex__tishen2"] = " ",
  ["~joyex__zhangfei"] = " ",
}

local zhaoyun = General:new(extension, "joyex__zhaoyun", "shu", 4)
local longdan = fk.CreateViewAsSkill{
  name = "joyex__longdan",
  pattern = "slash,jink,peach,analeptic",
  card_filter = function(self, to_select, selected)
    if #selected ~= 0 then return false end
    local _c = Fk:getCardById(to_select)
    local c
    if _c.trueName == "slash" then
      c = Fk:cloneCard("jink")
    elseif _c.name == "jink" then
      c = Fk:cloneCard("slash")
    elseif _c.name == "peach" then
      c = Fk:cloneCard("analeptic")
    elseif _c.name == "analeptic" then
      c = Fk:cloneCard("peach")
    else
      return false
    end
    return (Fk.currentResponsePattern == nil and c.skill:canUse(Self, c)) or (Fk.currentResponsePattern and Exppattern:Parse(Fk.currentResponsePattern):match(c))
  end,
  view_as = function(self, cards)
    if #cards ~= 1 then
      return nil
    end
    local _c = Fk:getCardById(cards[1])
    local c
    if _c.trueName == "slash" then
      c = Fk:cloneCard("jink")
    elseif _c.name == "jink" then
      c = Fk:cloneCard("slash")
    elseif _c.name == "peach" then
      c = Fk:cloneCard("analeptic")
    elseif _c.name == "analeptic" then
      c = Fk:cloneCard("peach")
    end
    c.skillName = self.name
    c:addSubcard(cards[1])
    return c
  end,
}
local yajiao = fk.CreateTriggerSkill{
  name = "joyex__yajiao",
  anim_type = "control",
  events = {fk.CardUsing, fk.CardResponding, fk.EventPhaseStart},
  can_trigger = function(self, event, target, player, data)
    if player:hasSkill(self) and target == player then
      if event == fk.EventPhaseStart then
        return player.phase == Player.Finish and player:usedSkillTimes("joyex__longdan", Player.HistoryTurn) > 0
      else
        return U.IsUsingHandcard(player, data) and player ~= player.room.current
      end
    end
  end,
  on_cost = function(self,event,target,player,data)
    if event == fk.EventPhaseStart then
      return true
    else
      return player.room:askForSkillInvoke(player,self.name,data)
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if event == fk.EventPhaseStart then
      player:drawCards(1 ,self.name)
    else
      local cards = room:getNCards(1)
      player:showCards(cards)
      local tos = room:askForChoosePlayers(player, table.map(room.alive_players, Util.IdMapper), 1, 1, "#joyex__yajiao-choose:::"..Fk:getCardById(cards[1]):toLogString(), self.name, false)
      if #tos > 0 then
        local to = room:getPlayerById(tos[1])
        room:moveCardTo(cards, Card.PlayerHand, to, fk.ReasonGive, self.name, nil, true, player.id)
      end
    end
  end,
}
zhaoyun:addSkill(longdan)
zhaoyun:addSkill(yajiao)
Fk:loadTranslationTable{
  ["joyex__zhaoyun"] = "界赵云",
  ["joyex__longdan"] = "龙胆",
  [":joyex__longdan"] = "你可以将一张【杀】当做【闪】、【闪】当做【杀】、【酒】当做【桃】、【桃】当做【酒】使用或打出。",
  ["joyex__yajiao"] = "涯角",
  [":joyex__yajiao"] = "每当你于回合外使用或打出手牌时，你可以展示牌堆顶一张牌并交给一名角色；结束阶段，若你本回合发动过【龙胆】，你摸一张牌。",
  ["#joyex__yajiao-choose"] = "涯角: 将 %arg 交给一名角色",
}

local joyex__tieji = fk.CreateTriggerSkill{
  name = "joyex__tieji",
  anim_type = "offensive",
  events = {fk.TargetSpecified},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and
      data.card.trueName == "slash"
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local to = room:getPlayerById(data.to)
    room:addPlayerMark(to, "@@joytieji-turn")
    room:addPlayerMark(to, MarkEnum.UncompulsoryInvalidity .. "-turn")
    local judge = {
      who = player,
      reason = self.name,
      pattern = ".",
    }
    room:judge(judge)
    if judge.card.color == Card.Red then
      data.disresponsive = true
    elseif judge.card.color == Card.Black then
      if not player.dead then
      player:drawCards(2, self.name)
      end
    end
  end,
}

local joyex__machao = General:new(extension, "joyex__machao", "shu", 4)
joyex__machao:addSkill("joy__yuma")
joyex__machao:addSkill(joyex__tieji)
Fk:loadTranslationTable{
  ["joyex__machao"] = "界马超",
  ["joyex__tieji"] = "铁骑",
  [":joyex__tieji"] = "当你使用【杀】指定目标后，你可令其本回合非锁定技失效，然后你进行判定，若为红色，该角色不能使用【闪】；黑色，你摸两张牌。",
  ["joyex__tieji_invalidity"] = "铁骑",
  ["@@joytieji-turn"] = "铁骑",
}

local huangyueying = General(extension, "joyex__huangyueying", "shu", 3, 3, General.Female)
local joyex__jizhi = fk.CreateTriggerSkill{
  name = "joyex__jizhi",
  anim_type = "drawcard",
  events = {fk.CardUsing},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and data.card.type == Card.TypeTrick
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local card = Fk:getCardById(player:drawCards(1)[1])
    if card.type == Card.TypeBasic and player.phase == Player.Play then
      room:addPlayerMark(player, MarkEnum.AddMaxCardsInTurn, 1)
    elseif card.type == Card.TypeEquip and room:getCardOwner(card) == player and room:getCardArea(card) == Player.Hand then
      local targets = table.map(table.filter(room:getOtherPlayers(player), function(p)
        return p:hasEmptyEquipSlot(card.sub_type) end), function(p) return p.id end)
      if #targets == 0 then return end
      local to = room:askForChoosePlayers(player, targets, 1, 1, "#joyex__jizhi-choose:::"..card:toLogString(), self.name, true)
      if #to > 0 then
        room:moveCards({
          ids = {card:getEffectiveId()},
          from = player.id,
          to = to[1],
          toArea = Card.PlayerEquip,
          moveReason = fk.ReasonPut,
          proposer = player.id,
          skillName = self.name,
        })
      end
    elseif card.type == Card.TypeTrick and player.phase == Player.Play then
      room:addPlayerMark(player, "joyex__jizhi-phase", 1)
    end
  end,
}
local joyex__jizhi_targetmod = fk.CreateTargetModSkill{
  name = "#joyex__jizhi_targetmod",
  main_skill = joyex__jizhi,
  residue_func = function(self, player, skill, scope)
    if skill.trueName == "slash_skill" and player:getMark("joyex__jizhi-phase") > 0 and scope == Player.HistoryPhase then
      return player:getMark("joyex__jizhi-phase")
    end
    return 0
  end,
}
local joyex__qicai = fk.CreateTriggerSkill{
  name = "joyex__qicai",
  anim_type = "defensive",
  frequency = Skill.Compulsory,
  events = {fk.BeforeCardsMove},
  can_trigger = function(self, event, target, player, data)
    if player:hasSkill(self) and (player:getEquipment(Card.SubtypeWeapon) or player:getEquipment(Card.SubtypeArmor)) then
      for _, move in ipairs(data) do
        if move.from == player.id and move.moveReason == fk.ReasonDiscard and (not move.proposer or move.proposer ~= player.id) then
          for _, info in ipairs(move.moveInfo) do
            if info.fromArea == Card.PlayerEquip and
              table.contains({Card.SubtypeWeapon, Card.SubtypeArmor}, Fk:getCardById(info.cardId).sub_type) then
              return true
            end
          end
        end
      end
    end
  end,
  on_use = function(self, event, target, player, data)
    local ids = {}
    for _, move in ipairs(data) do
      if move.from == player.id and move.moveReason == fk.ReasonDiscard and (not move.proposer or move.proposer ~= player.id) then
        local move_info = {}
        for _, info in ipairs(move.moveInfo) do
          local id = info.cardId
          if info.fromArea == Card.PlayerEquip and table.contains({Card.SubtypeWeapon, Card.SubtypeArmor}, Fk:getCardById(id).sub_type) then
            table.insert(ids, id)
          else
            table.insert(move_info, info)
          end
        end
        if #ids > 0 then
          move.moveInfo = move_info
        end
      end
    end
    if #ids > 0 then
      player.room:sendLog{
        type = "#cancelDismantle",
        card = ids,
        arg = self.name,
      }
    end
  end,
}
local joyex__qicai_targetmod = fk.CreateTargetModSkill{
  name = "#joyex__qicai_targetmod",
  main_skill = joyex__qicai,
  bypass_distances = function(self, player, skill, card)
    return player:hasSkill("joyex__qicai") and card and card.type == Card.TypeTrick
  end,
}
joyex__jizhi:addRelatedSkill(joyex__jizhi_targetmod)
joyex__qicai:addRelatedSkill(joyex__qicai_targetmod)
huangyueying:addSkill(joyex__jizhi)
huangyueying:addSkill(joyex__qicai)
Fk:loadTranslationTable{
  ["joyex__huangyueying"] = "界黄月英",
  ["joyex__jizhi"] = "集智",
  [":joyex__jizhi"] = "当你使用一张锦囊牌时，你可以摸一张牌，若此牌为：基本牌，你本回合手牌上限+1；装备牌，你可以将之置入一名其他角色装备区；"..
  "锦囊牌，你本阶段使用【杀】次数上限+1。",
  ["joyex__qicai"] = "奇才",
  [":joyex__qicai"] = "锁定技，你使用锦囊牌无距离限制；其他角色不能弃置你装备区里的防具和武器牌。",
  ["#joyex__jizhi-choose"] = "集智：你可以将%arg置入一名其他角色装备区",
}


local liubei = General(extension, "joyex__liubei", "shu", 4)
local rende = fk.CreateActiveSkill{
  name = "joyex__rende",
  anim_type = "support",
  card_filter = function(self, to_select, selected)
    return Fk:currentRoom():getCardArea(to_select) ~= Card.PlayerEquip
  end,
  target_filter = function(self, to_select, selected)
    return #selected == 0 and to_select ~= Self.id and Fk:currentRoom():getPlayerById(to_select):getMark("_joyex__rende-phase") == 0
  end,
  target_num = 1,
  min_card_num = 1,
  on_use = function(self, room, effect)
    local target = room:getPlayerById(effect.tos[1])
    local player = room:getPlayerById(effect.from)
    local cards = effect.cards
    local marks = player:getMark("_joyrende_cards-phase")
    room:moveCardTo(cards, Player.Hand, target, fk.ReasonGive, self.name, nil, false)
    room:addPlayerMark(player, "_joyrende_cards-phase", #cards)
    room:addPlayerMark(target, "_joyex__rende-phase", 1)
    if room:askForSkillInvoke(player,self.name,nil,"#joyex__rengde-buff::"..target.id) then
      local mark = target:getTableMark("joyex__rende_buff")
      table.insert(mark,player.id)
      room:setPlayerMark(target,"joyex__rende_buff",mark)
    end
    if marks < 2 and marks + #cards >= 2 then
      U.askForUseVirtualCard(room, player, U.getAllCardNames("bt"), nil, self.name, "#joyex__rende-ask", true, false, false, false)
    end
    
  end,
}
local rende_buff = fk.CreateProhibitSkill{
  name = "#joyex__rende_buff",
  frequency = Skill.Compulsory,
  is_prohibited = function(self, from, to, card)
    local m = from:getTableMark("joyex__rende_buff")
    if table.contains(m,to.id) then
      return card.trueName == "slash" and card.color == Card.Red
    end
  end,
}
local rende_trigger = fk.CreateTriggerSkill{
  name = "#joyex__rende_trigger",
  anim_type = "control",
  mute = true,
  events = {fk.TurnStart},
  can_trigger = function(self, event, target, player, data)
    return target == player
  end,
  on_cost = Util.TrueFunc,
  on_use = function(self, event, target, player, data)
    local room = player.room
    for _, p in ipairs(room.alive_players) do
      local mark = p:getTableMark("joyex__rende_buff")
      local mark2 = {}
      if table.contains(mark,player.id) then
        for _, j in ipairs(mark) do
          if j ~= player.id then
            table.insert(mark2,j)
          end
        end
        room:setPlayerMark(p,"joyex__rende_buff",mark2)
      end
    end
  end,
}
local jijiang = fk.CreateViewAsSkill{
  name = "joyex__jijiang$",
  anim_type = "offensive",
  pattern = "slash",
  card_filter = Util.FalseFunc,
  view_as = function(self, cards)
    if #cards ~= 0 then
      return nil
    end
    local c = Fk:cloneCard("slash")
    c.skillName = self.name
    return c
  end,
  before_use = function(self, player, use)
    local room = player.room
    if use.tos then
      room:doIndicate(player.id, TargetGroup:getRealTargets(use.tos))
    end

    for _, p in ipairs(room:getOtherPlayers(player)) do
      if p.kingdom == "shu" then
        local cardResponded = room:askForResponse(p, "slash", "slash", "#joyex__jijiang-ask::" .. player.id, true)
        if cardResponded then
          room:responseCard({
            from = p.id,
            card = cardResponded,
            skipDrop = true,
          })
          use.card = cardResponded
          --room:setPlayerMark(p,"@@joyex__jijiang-phase",1)
          if not player.dead then player:drawCards(1,self.name) end
          if not p.dead then p:drawCards(1,self.name) end
          return
        end
      end
    end

    room:setPlayerMark(player, "jijiang-failed-phase", 1)
    return self.name
  end,
  --[[
  after_use = function(self,player,use)
    local room = player.room
    if not player.dead then
      player:drawCards(1,self.name)
    end
    for _, p in ipairs(room.alive_players) do
      if p:getMark("@@joyex__jijiang-phase") > 0 then
        p:drawCards(1,self.name)
        room:setPlayerMark(p,"@@joyex__jijiang-phase",0)
      end
    end
  end,]]
  enabled_at_play = function(self, player)
    return player:getMark("jijiang-failed-phase") == 0 and not table.every(Fk:currentRoom().alive_players, function(p)
      return p == player or p.kingdom ~= "shu"
    end)
  end,
  enabled_at_response = function(self, player)
    return not table.every(Fk:currentRoom().alive_players, function(p)
      return p == player or p.kingdom ~= "shu"
    end)
  end,
}
local jijiang_trigger = fk.CreateTriggerSkill{
  name = "#joyex__jijiang_trigger",
  anim_type = "drawcard",
  events = {fk.CardUsing, fk.CardResponding},
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(self) and target ~= player and target.kingdom == "shu" and data.card.trueName == "slash" and player.phase == player.NotActive and player:getMark("joyex__jijiangdraw-turn") == 0
  end,
  on_cost = function(self, event, target, player, data)
    if player.room:askForSkillInvoke(target,self.name,data,"#joyex__jijiangdraw::"..player.id) then
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if not player.dead then
      room:setPlayerMark(player,"joyex__jijiangdraw-turn",1)
      player:drawCards(1,self.name)
    end
  end,
}
rende:addRelatedSkill(rende_trigger)
jijiang:addRelatedSkill(jijiang_trigger)
rende:addRelatedSkill(rende_buff)
liubei:addSkill(rende)
liubei:addSkill(jijiang)

Fk:loadTranslationTable{
  ["joyex__liubei"] = "界刘备",
  ["joyex__rende"] = "仁德",
  [":joyex__rende"] = "出牌阶段每名角色限一次，你可以将任意张手牌交给一名其他角色，并可以让其直到你的下回合开始前无法对你使用红【杀】。每阶段你以此法给出第二张牌时，你可以视为使用一张基本牌或普通锦囊牌。",
  ["joyex__jijiang"] = "激将",
  [":joyex__jijiang"] = "主公技，<br>①当你需要使用或打出【杀】时，你可以令其他蜀势力角色选择是否打出一张【杀】（视为由你使用或打出），若有角色以此法打出【杀】，则你与其各摸一张牌。"..
  "<br>②每回合限一次，你的回合外，当其他蜀势力角色使用或打出一张【杀】时，其可令你摸一张牌。",
  ["#joyex__rende_buff"] = "仁德",
  ["#joyex__rende_trigger"] = "仁德",
  ["#joyex__rengde-buff"] = "仁德：你可以令%dest无法对你使用红【杀】直到你的下回合开始。",
  ["#joyex__jijiang_trigger"] = "激将",

  ["#joyex__rende-ask"] = "仁德：你可视为使用一张基本牌或普通锦囊牌",
  ["@@joyex__rende_buff"] = "无法红【杀】",
  ["#joyex__jijiang-ask"] = "激将：是否代替%dest出杀，若出杀，则其与你各摸一张牌。",
  ["@@joyex__jijiang-phase"] = "激将摸牌",
  ["@@joyex__jijiangdraw-turn"] = "激将-已摸牌",
  ["#joyex__jijiangdraw"] = "激将：是否让%dest摸一张牌",

  ["$joyex__rende1"] = " ",
  ["$joyex__rende2"] = " ",
  ["$joyex__jijiang1"] = " ",
  ["$joyex__jijiang2"] = " ",
  ["~joyex__liubei"] = " ",
}

local guanyu = General:new(extension, "joyex__guanyu", "shu", 4)
local wusheng = fk.CreateViewAsSkill{
  name = "joyex__wusheng",
  anim_type = "offensive",
  pattern = "slash",
  card_filter = function(self, to_select, selected)
    return #selected == 0 and Fk:getCardById(to_select).color == Card.Red
  end,
  view_as = function(self, cards)
    if #cards ~= 1 then return nil end
    local c = Fk:cloneCard("slash")
    c.skillName = self.name
    c:addSubcard(cards[1])
    return c
  end,
}
local wusheng_trigger = fk.CreateTriggerSkill{
  name = "#joyex__wusheng_trigger",
  main_skill = wusheng,
  mute = true,
  events = {fk.TurnStart, fk.AfterCardUseDeclared},
  can_trigger = function(self, event, target, player, data)
    if target == player and player:hasSkill(self) then
      return (event == fk.TurnStart) or (data.card.trueName == "slash" and data.card.color == Card.Red)
    end
  end,
  on_cost = Util.TrueFunc,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if event == fk.TurnStart then
      room:notifySkillInvoked(player, "joyex__wusheng", "drawcard")
      local ids = room:getCardsFromPileByRule(".|.|heart,diamond", 1, "allPiles")
      if #ids > 0 then
        room:obtainCard(player, ids[1], false, fk.ReasonPrey)
      end
    else
      room:notifySkillInvoked(player, "joyex__wusheng", "offensive")
      data.additionalDamage = (data.additionalDamage or 0) + 1
    end
  end,
}
local joy__tuodao = fk.CreateTriggerSkill{
  name = "joy__tuodao",
  anim_type = "offensive",
  frequency = Skill.Compulsory,
  events = {fk.CardUseFinished, fk.CardRespondFinished},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and data.card.name == "jink"
  end,
  on_use = function(self, event, target, player, data)
    player.room:addPlayerMark(player, "@joy__tuodao")
  end,

  refresh_events = {fk.CardUsing},
  can_refresh = function (self, event, target, player, data)
    return player == target and data.card.trueName == "slash" and player:getMark("@joy__tuodao") > 0
  end,
  on_refresh = function (self, event, target, player, data)
    data.additionalDamage = (data.additionalDamage or 0) + player:getMark("@joy__tuodao")
    player.room:setPlayerMark(player, "@joy__tuodao", 0)
  end,
}
local guanjue = fk.CreateTriggerSkill{
  name = "joyex__guanjue",
  anim_type = "offensive",
  frequency = Skill.Compulsory,
  events = {fk.CardUsing, fk.CardResponding},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and data.card.suit ~= Card.NoSuit and
      (player:getNextAlive():getMark("@joyex__guanjue-turn") == 0 or
      not table.contains(player:getNextAlive():getMark("@joyex__guanjue-turn"), data.card:getSuitString(true)))
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local mark = player:getNextAlive():getMark("@joyex__guanjue-turn")
    if mark == 0 then mark = {} end
    table.insert(mark, data.card:getSuitString(true))
    for _, p in ipairs(room:getOtherPlayers(player)) do
      room:doIndicate(player.id, {p.id})
      room:setPlayerMark(p, "@joyex__guanjue-turn", mark)
    end
  end,
}
local guanjue_prohibit = fk.CreateProhibitSkill{
  name = "#joyex__guanjue_prohibit",
  prohibit_use = function(self, player, card)
    return player:getMark("@joyex__guanjue-turn") ~= 0 and table.contains(player:getMark("@joyex__guanjue-turn"), card:getSuitString(true))
  end,
  prohibit_response = function(self, player, card)
    return player:getMark("@joyex__guanjue-turn") ~= 0 and table.contains(player:getMark("@joyex__guanjue-turn"), card:getSuitString(true))
  end,
}
guanjue:addRelatedSkill(guanjue_prohibit)
wusheng:addRelatedSkill(wusheng_trigger)
guanyu:addSkill(wusheng)
guanyu:addSkill(joy__tuodao)
guanyu:addSkill(guanjue)
Fk:loadTranslationTable{
  ["joyex__guanyu"] = "界关羽",
  ["#joyex__guanyu"] = "美髯公",
  ["joyex__wusheng"] = "武圣",
  [":joyex__wusheng"] = "回合开始时，你获得一张红色牌；你可以将一张红色牌当做【杀】使用或打出；你使用的红色【杀】伤害+1。",
  ["#joyex__wusheng_trigger"] = "武圣",
  ["joyex__guanjue"] = "冠绝",
  [":joyex__guanjue"] = "锁定技，当你使用或打出一张牌时，所有其他角色不能使用或打出此花色的牌直到回合结束。",
  ["joy__tuodao"] = "拖刀",
  [":joy__tuodao"] = "锁定技，每当你使用或打出一张【闪】后，令你下一张使用的【杀】伤害+1。",
  ["@joy__tuodao"] = "拖刀",
  ["@joyex__guanjue-turn"] = "冠绝",

  ["$guanjue_joyex__guanyu1"] = " ",
  ["$joyex__wusheng1"] = " ",
  ["$joy__tuodao1"] = " ",
  ["$guanjue_joyex__guanyu2"] = " ",
  ["$joyex__wusheng2"] = " ",
  ["$joy__tuodao2"] = " ",
}



local simayi = General(extension, "joyex__simayi", "wei", 3)
local joyex__guicai = fk.CreateTriggerSkill{
  name = "joyex__guicai",
  anim_type = "control",
  events = {fk.AskForRetrial},
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(self) and not player:isNude()
  end,
  on_cost = function(self, event, target, player, data)
    local card = player.room:askForResponse(player, self.name, ".|.|.|hand,equip|.|", "#joyex__guicai-ask::" .. target.id .. ":" .. data.reason, true)
    if card ~= nil then
      self.cost_data = card
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    player.room:retrial(self.cost_data, player, data, self.name)
    if player.dead then return end
    if self.cost_data.suit == Card.Club then
      player:drawCards(2, self.name)
    elseif self.cost_data.suit == Card.Heart and player:isWounded() then
      player.room:recover({
      who = player,
      num = 1,
      recoverBy = player,
      skillName = self.name
      })
    end
  end,
}
local joyex__fankui = fk.CreateTriggerSkill{
  name = "joyex__fankui",
  anim_type = "masochism",
  events = {fk.Damaged},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self)
  end,
  on_trigger = function(self, event, target, player, data)
    self.cancel_cost = false
    for i = 1, data.damage do
      if self.cancel_cost or player.dead then break end
      self:doCost(event, target, player, data)
    end
  end,
  on_cost = function(self, event, target, player, data)
    if player.room:askForSkillInvoke(player, self.name, data) then
      return true
    end
    self.cancel_cost = true
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local judge = {
      who = player,
      reason = self.name,
      pattern = ".",
    }
    room:judge(judge)
    if judge.card.suit == Card.Heart then
      local targets = table.map(table.filter(room.alive_players, function(p) return not p:isNude() end), Util.IdMapper)
      if #targets == 0 then return end
      local tos = room:askForChoosePlayers(player, targets, 1, 1, "#joyex__fankui-choose", self.name, false)
      local to = room:getPlayerById(tos[1])
      local card = room:askForCardChosen(player, to, "he", self.name)
      room:obtainCard(player, card, false, fk.ReasonPrey)
    else
      local from = data.from
      if from and not from.dead and not from:isNude() then
        room:doIndicate(player.id, {from.id})
        local card = room:askForCardChosen(player, from, "he", self.name)
        room:obtainCard(player, card, false, fk.ReasonPrey)
      end
    end
  end,
}
simayi:addSkill(joyex__guicai)
simayi:addSkill(joyex__fankui)
Fk:loadTranslationTable{
  ["joyex__simayi"] = "界司马懿",
  ["joyex__guicai"] = "鬼才",
  [":joyex__guicai"] = "任何判定牌生效前，你可以打出一张牌代替之，若此牌为红桃，你回复1点体力，若此牌为梅花，你摸两张牌。",
  ["joyex__fankui"] = "反馈",
  [":joyex__fankui"] = "当你受到1点伤害后，你可以进行判定：红桃，你获得场上任意角色的一张牌；其他花色，你获得伤害来源一张牌。",

  ["#joyex__guicai-ask"] = "鬼才：你可打出一张牌代替 %dest 的 %arg 判定;若为红桃，你回复1点体力，若为梅花，你摸两张牌。",
  ["#joyex__fankui-choose"] = "反馈：获得一名角色的一张牌。",

  ["$joyex__fankui1"] = "天狼加身之人，从不会甘居人下！",
  ["$joyex__fankui2"] = "贪狼有吞天之力，吞你，还不是易如反掌？",
  ["$joyex__guicai2"] = "天狼星光大盛，天下易主可期。",
  ["$joyex__guicai1"] = "煞星聚顶，你死到临头了！",
  ["~joyex__simayi"] = "天煞孤星，这都是命中注定的吧……",
}



local joyex__guojia = General(extension, "joyex__guojia", "wei", 3)

local checkShenglunMark = function (player)
  local mark = player.dead and 0 or ("胜"..player:getMark("joy__shenglun_win").." 败"..player:getMark("joy__shenglun_lose"))
  player.room:setPlayerMark(player, "@joy__shenglun", mark)
end

local joy__shenglun = fk.CreateActiveSkill{
  name = "joy__shenglun",
  anim_type = "control",
  card_num = 0,
  min_target_num = 1,
  max_target_num = 2,
  card_filter = Util.FalseFunc,
  target_filter = function(self, to_select, selected, selected_cards)
    return #selected < 2 and Self.id ~= to_select
  end,
  can_use = function(self, player)
    return player:usedSkillTimes(self.name, Player.HistoryPhase) == 0
  end,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    room:sortPlayersByAction(effect.tos)
    for _, pid in ipairs(effect.tos) do
      local to = room:getPlayerById(pid)
      local list = {
        player.hp - to.hp,
        player:getHandcardNum() - to:getHandcardNum(),
        #player:getEquipments(Card.SubtypeWeapon)-#to:getEquipments(Card.SubtypeWeapon),
        #player:getEquipments(Card.SubtypeArmor)-#to:getEquipments(Card.SubtypeArmor),
        (#player:getEquipments(Card.SubtypeDefensiveRide)+#player:getEquipments(Card.SubtypeOffensiveRide)) -
        (#to:getEquipments(Card.SubtypeDefensiveRide)+#to:getEquipments(Card.SubtypeOffensiveRide)),
      }
      for _, n in ipairs(list) do
        if n > 0 then
          room:addPlayerMark(player, "joy__shenglun_win")
        else
          room:addPlayerMark(player, "joy__shenglun_lose")
        end
      end
    end
    checkShenglunMark(player)
    local yiji = Fk.skills["joyex__yiji"]
    if player:getMark("joy__shenglun_win") > 9 then
      if player:isWounded() then
        room:recover { num = 1, skillName = self.name, who = player, recoverBy = player}
        if player.dead then return end
      end
      room:useSkill(player, yiji, function()
        return yiji:use(fk.Damaged, player, player, {to = player, num = 1})
      end)
      room:setPlayerMark(player, "joy__shenglun_win", 0)
      checkShenglunMark(player)
    end
    if player.dead then return end
    if player:getMark("joy__shenglun_lose") > 9 then
      local tos = room:askForChoosePlayers(player, table.map(room.alive_players, Util.IdMapper), 1, 1, "#joy__shenglun-damage", self.name, false)
      room:damage { from = player, to = room:getPlayerById(tos[1]), damage = 1, skillName = self.name }
      if player.dead then return end
      room:useSkill(player, yiji, function()
        return yiji:use(fk.Damaged, player, player, {to = player, num = 1})
      end)
      room:setPlayerMark(player, "joy__shenglun_lose", 0)
      checkShenglunMark(player)
    end
  end,
}
local yiji = fk.CreateTriggerSkill{
  name = "joyex__yiji",
  anim_type = "masochism",
  events = {fk.Damaged},
  on_trigger = function(self, event, target, player, data)
    self.cancel_cost = false
    for _ = 1, data.damage do
      if player.dead or self.cancel_cost then return end
      self:doCost(event, target, player, data)
    end
  end,
  on_cost = function(self, event, target, player, data)
    if player.room:askForSkillInvoke(target, self.name, data) then return true end
    self.cancel_cost = true
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    player:drawCards(2, self.name)
    if player.dead or player:isKongcheng() then return end
    player.room:askForYiji (player, player:getCardIds("h"), room:getOtherPlayers(player), self.name, 0, 2)
  end
}
local tiandu = fk.CreateTriggerSkill{
  name = "joy__tiandu",
  anim_type = "drawcard",
  events = {fk.FinishJudge},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and player.room:getCardArea(data.card) == Card.Processing
  end,
  on_use = function(self, event, target, player, data)
    player.room:obtainCard(player.id, data.card, true, fk.ReasonJustMove)
  end,
}
joyex__guojia:addSkill(joy__shenglun)
joyex__guojia:addSkill(tiandu)
joyex__guojia:addSkill(yiji)

Fk:loadTranslationTable{
  ["joyex__guojia"] = "界郭嘉",
  ["joy__shenglun"] = "胜论",
  [":joy__shenglun"] = "出牌阶段限一次，你可以选择至多两名其他角色，你依次与这些角色比较体力、手牌、武器、防具、坐骑的数量（数量大于其为胜，否则为负），若胜或败累计达到10次，胜：你回复1点体力，败：你对一名角色造成1点伤害。然后发动一次“遗计”并重置对应的胜败次数。",
  ["@joy__shenglun"] = "胜论",
  ["#joy__shenglun-damage"] = "胜论：对一名角色造成1点伤害",
  ["joyex__yiji"] = "遗计",
  [":joyex__yiji"] = "当你受到1点伤害后，你可以摸两张牌，然后你可以将至多两张手牌交给一至两名其他角色。",
  ["joy__tiandu"] = "天妒",
  [":joy__tiandu"] = "当你的判定牌生效后，你可以获得之。",
  ["#joyex__yiji-give"] = "遗计：将至多%arg张手牌分配给其他角色",
}

local xiahoudun = General(extension, "joyex__xiahoudun", "wei", 4)
local qingjian = fk.CreateTriggerSkill{
  name = "joyex__qingjian",
  anim_type = "support",
  events = {fk.AfterCardsMove},
  can_trigger = function(self, event, target, player, data)
    if player:hasSkill(self) and player:usedSkillTimes(self.name, Player.HistoryTurn) == 0 then
      if event == fk.AfterCardsMove then
        for _, move in ipairs(data) do
          if move.to == player.id and move.toArea == Player.Hand and move.skillName ~= self.name and player.phase ~= Player.Draw then
            return not player:isNude()
          end
        end
      end
    end
  end,
  on_cost = function(self, event, target, player, data)
    local n = #player:getCardIds("he")
    local cards = player.room:askForCard(player,1,n,true,self.name,true,".","#joyex__qingjian-invoke1")
    if #cards > 0 then
      self.cost_data = {cards}
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    local n = #player:getCardIds("he")
    local cards = self.cost_data
    local tos = player.room:askForChoosePlayers(player,table.map(player.room:getOtherPlayers(player), Util.IdMapper),1,1,"#joyex__qingjian-invoke2",self.name,false)
    if #tos > 0 then
      local room = player.room
      local to = room:getPlayerById(tos[1])
      player:showCards(cards)
      room:moveCardTo(cards, Player.Hand, to, fk.ReasonGive, self.name, nil, true)
      if not player.dead then
        player:drawCards(1,self.name)
      end
    end
  end,
}

xiahoudun:addSkill("ex__ganglie")
xiahoudun:addSkill(qingjian)
Fk:loadTranslationTable{
  ["joyex__xiahoudun"] = "界夏侯惇",

  ["joyex__qingjian"] = "清俭",
  [":joyex__qingjian"] = "每回合限一次，当你于摸牌阶段外获得牌后，你可以展示任意张牌并交给一名其他角色,然后你摸一张牌。",
  ["#joyex__qingjian-invoke1"] = "清俭：你可以将任意张牌展示并交给一名其他角色，然后你摸一张牌",
  ["#joyex__qingjian-invoke2"] = "清俭：选择要将牌交给的角色",
}

local xuchu = General:new(extension, "joyex__xuchu", "wei", 4)
local luoyi = fk.CreateTriggerSkill{
  name = "joy__luoyi",
  anim_type = "offensive",
  events = {fk.DrawNCards},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and data.n > 0
  end,
  on_cost = function (self, event, target, player, data)
    return player.room:askForSkillInvoke(player,self.name,data,"#joy__luoyi")
  end,
  on_use = function(self, event, target, player, data)
    data.n = data.n - 1
    player.room:setPlayerMark(player,"@@joy__luoyi-turn",1)
  end,
}
local luoyi_trigger = fk.CreateTriggerSkill{
  name = "#joy__luoyi_trigger",
  mute = true,
  events = {fk.DamageCaused},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:getMark("@@joy__luoyi-turn") > 0 and
      not data.chain and data.card and (data.card.trueName == "slash" or data.card.name == "duel")
  end,
  on_cost = Util.TrueFunc,
  on_use = function(self, event, target, player, data)
    local room = player.room
    player:broadcastSkillInvoke("joy__luoyi")
    room:notifySkillInvoked(player, "joy__luoyi")
    data.damage = data.damage + 1
  end,
}
local huchi = fk.CreateActiveSkill{
  name = "joyex__huchi",
  anim_type = "drawcard",
  prompt = "#joyex__huchi-draw",
  can_use = function(self, player)
    return player:getMark("@joyex__huchi") > 0 and player:usedSkillTimes(self.name, Player.HistoryPhase) == 0
  end,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    local n = player:getMark("@joyex__huchi")
    player.room:setPlayerMark(player,"@joyex__huchi",0)
    if not player.dead then
      player:drawCards(n,self.name)
    end
  end,
}
local huchi_trigger = fk.CreateTriggerSkill{
  name = "#joyex__huchi",
  anim_type = "drawcard",
  events = {fk.CardEffectCancelledOut, fk.TurnEnd},
  can_trigger = function(self, event, target, player, data)
    if event == fk.CardEffectCancelledOut then
      return target == player and player:hasSkill(self) and data.card.trueName == "slash"
    else
      return target == player  and player:hasSkill(self) and player:getHandcardNum() < 2
    end
  end,
  on_cost = Util.TrueFunc,
  on_use = function(self, event, target, player, data)
    if event == fk.CardEffectCancelledOut then
      player.room:addPlayerMark(player,"@joyex__huchi",1)
    else
      player:drawCards( 2 - player:getHandcardNum(),self.name)
    end
  end,
}
huchi:addRelatedSkill(huchi_trigger)
luoyi:addRelatedSkill(luoyi_trigger)
xuchu:addSkill(huchi)
xuchu:addSkill(luoyi)
Fk:loadTranslationTable{
  ["joyex__xuchu"] = "界许褚",
  ["#joyex__xuchu"] = "虎痴",
  
  
  ["joy__luoyi"] = "裸衣",
  [":joy__luoyi"] = "摸牌阶段，你可以少摸一张牌，然后本回合你使用【杀】或【决斗】对目标角色造成伤害时，此伤害+1。",
  ["joyex__huchi"] = "虎痴",
  [":joyex__huchi"] = "<br>①回合结束时，若你的手牌数小于2，则将手牌摸至2。<br>②你使用的【杀】被【闪】抵消时，你获得一枚“痴”。<br>③出牌阶段限一次，你可以弃置所有的“痴”并摸等量的牌。",
  ["#joyex__huchi"] = "虎痴",
  ["@joyex__huchi"] = "痴",
  ["#joyex__huchi-draw"] = "虎痴：你可以弃置所有“痴”并摸等量的牌。",
  ["#joy__luoyi_trigger"] = "裸衣",
  ["#joy__luoyi"] = "是否少摸一张牌来发动裸衣？",
  ["@@joy__luoyi-turn"] = "裸衣",
  
  ["$joy__luoyi1"] = "脱！",
  ["$joy__luoyi2"] = "谁来与我大战三百回合？",
  ["~joyex__xuchu"] = "冷，好冷啊……",
}

local caocao = General:new(extension, "joyex__caocao", "wei", 4)
local jianxiong = fk.CreateTriggerSkill{
  name = "joyex__jianxiong",
  anim_type = "masochism",
  events = {fk.Damaged},
  on_trigger = function(self, event, target, player, data)
    self.cancel_cost = false
    for i = 1, data.damage do
      if self.cancel_cost or player.dead then break end
      self:doCost(event, target, player, data)
    end
  end,
  on_cost = function(self, event, target, player, data)
    if player.room:askForSkillInvoke(player, self.name) then
      return true
    end
    self.cancel_cost = true
  end,
  on_use = function(self, event, target, player, data)
    local choice = player.room:askForChoice(player, {"joyex__jianxiong_prey", "draw2"}, self.name)
    if choice == "joyex__jianxiong_prey" then
      player:drawCards(1, self.name)
      if not player.dead and data.card and player.room:getCardArea(data.card) == Card.Processing then
        player.room:obtainCard(player.id, data.card, true, fk.ReasonPrey)
      end
    else
      player:drawCards(2, self.name)
    end
  end,
}

local hujia = fk.CreateTriggerSkill{
  name = "joyex__hujia$",
  anim_type = "defensive",
  events = {fk.AskForCardUse, fk.AskForCardResponse},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and
      (data.cardName == "jink" or (data.pattern and Exppattern:Parse(data.pattern):matchExp("jink|0|nosuit|none"))) and
      (data.extraData == nil or data.extraData.hujia_ask == nil) and
      not table.every(player.room.alive_players, function(p)
        return p == player or p.kingdom ~= "wei"
      end)
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    for _, p in ipairs(room:getOtherPlayers(player)) do
      if p:isAlive() and p.kingdom == "wei" then
        local cardResponded = room:askForResponse(p, "jink", "jink", "#joyex__hujia-ask:" .. player.id, true, {hujia_ask = true})
        if cardResponded then
          room:responseCard({
            from = p.id,
            card = cardResponded,
            skipDrop = true,
          })

          if event == fk.AskForCardUse then
            data.result = {
              from = player.id,
              card = Fk:cloneCard('jink'),
            }
            data.result.card:addSubcards(room:getSubcardsByRule(cardResponded, { Card.Processing }))
            data.result.card.skillName = self.name

            if data.eventData then
              data.result.toCard = data.eventData.toCard
              data.result.responseToEvent = data.eventData.responseToEvent
            end
          else
            data.result = Fk:cloneCard('jink')
            data.result:addSubcards(room:getSubcardsByRule(cardResponded, { Card.Processing }))
            data.result.skillName = self.name
          end
          if player:getMark("joyex__hujia-turn") == 0 then
            if p.room:askForSkillInvoke(p,self.name,nil,"#joyex__hujia-drawcard:" .. player.id) and not player.dead then
              player.room:drawCards(player,1,self.name)
              player.room:setPlayerMark(player,"joyex__hujia-turn",1)
            end
          end
          return true
        end
      end
    end
  end,
}

caocao:addSkill(jianxiong)
caocao:addSkill(hujia)
Fk:loadTranslationTable{
  ["joyex__caocao"] = "界曹操",

  ["joyex__jianxiong"] = "奸雄",
  [":joyex__jianxiong"] = "当你每受到1点伤害后，你可以摸一张牌并获得对你造成伤害的牌；或摸两张牌",
  ["joyex__hujia"] = "护驾",
  [":joyex__hujia"] = "主公技，其他魏势力角色可以替你使用或打出【闪】。"..
  "其他魏势力角色若以此法使用或打出【闪】时，可以令你摸一张牌，每回合限一张。",

  ["joyex__jianxiong_prey"] = "摸一张牌，获得对你造成伤害的牌",

  ["#joyex__hujia-ask"] = "护驾:是否为 %src 打出一张闪,若打出可令其摸一张牌(每回合限一张)。",
  ["#joyex__hujia-drawcard"] = "护驾:是否令 %src 摸一张牌？"
}


local zhangliao = General(extension, "joyex__zhangliao", "wei", 4)
local tuxi = fk.CreateTriggerSkill{
  name = "joyex__tuxi",
  anim_type = "control",
  events = {fk.DrawNCards},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and data.n > 0 and
      not table.every(player.room:getOtherPlayers(player), function(p) return p:isKongcheng() end)
  end,
  on_cost = function(self, event, target, player, data)
    local room = player.room
    local targets = table.map(table.filter(room:getOtherPlayers(player), function(p)
      return not p:isKongcheng() end), function (p) return p.id end)
    local tos = room:askForChoosePlayers(player, targets, 1, data.n, "#joyex__tuxi-choose:::"..data.n, self.name, true)
    if #tos > 0 then
      self.cost_data = tos
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    for _, id in ipairs(self.cost_data) do
      local c = room:askForCardChosen(player, room:getPlayerById(id), "h", self.name)
      room:obtainCard(player, c, false, fk.ReasonPrey)
      local card = Fk:getCardById(c)
      room:setCardMark(card,"@@joyex__zhengbing-inhand",1)
    end
    data.n = data.n - #self.cost_data
  end,
}
local zhengbing = fk.CreateActiveSkill{
  name = "joyex__zhengbing",
  anim_type = "drawcard",
  card_num = 1,
  prompt = "#joyex__zhengbing",
  card_filter = function (self, to_select, selected, selected_targets)
    return Fk:getCardById(to_select):getMark("@@joyex__zhengbing-inhand") > 0
  end,
  on_use = function(self, room, effect)
    local room = Self.room
    local player = room:getPlayerById(effect.from)
    local card = effect.cards
    room:recastCard(effect.cards, room:getPlayerById(effect.from))
  end,
}
local zhengbing_maxcards = fk.CreateMaxCardsSkill{
  name = "#zhengbing_maxcards",
  exclude_from = function(self, player, card)
    return card:getMark("@@joyex__zhengbing-inhand") > 0
  end,
}
zhengbing:addRelatedSkill(zhengbing_maxcards)
zhangliao:addSkill(tuxi)
zhangliao:addSkill(zhengbing)
Fk:loadTranslationTable{
  ["joyex__zhangliao"] = "界张辽",
  ["#joyex__zhangliao"] = "前将军",
  ["joyex__tuxi"] = "突袭",
  [":joyex__tuxi"] = "摸牌阶段，你可以少摸任意张牌并获得等量其他角色各一张手牌。",
  ["#joyex__tuxi-choose"] = "突袭：你可以少摸至多%arg张牌，获得等量其他角色各一张手牌",
  ["joyex__zhengbing"] = "整兵",
  [":joyex__zhengbing"] = "你通过〖突袭〗获得的牌不计入手牌上限，且可以被重铸；当最后一张“突袭”牌被重铸时你额外摸一张牌。",
  ["#joyex__zhengbing"] = "整兵：你可以重铸一张“突袭”牌。",
  ["@@joyex__zhengbing-inhand"] = "突袭",

  ["$joyex__tuxi1"] = "快马突袭，占尽先机！",
  ["$joyex__tuxi2"] = "马似飞影，枪如霹雳！",
  ["~joyex__zhangliao"] = "被敌人占了先机，呃。",
}

local ex__zhenji = General(extension, "joyex__zhenji", "wei", 3, 3, General.Female)

local luoshen = fk.CreateTriggerSkill{
  name = "joyex__luoshen",
  anim_type = "drawcard",
  events = {fk.EventPhaseStart},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and
      player.phase == Player.Start
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    while true do
      local judge = {
        who = player,
        reason = self.name,
        pattern = ".|.|spade,club",
      }
      room:judge(judge)
      if judge.card.color ~= Card.Black then
        break
      end
      if not room:askForSkillInvoke(player, self.name) then
        break
      end
    end
  end,
}
local luoshen_obtain = fk.CreateTriggerSkill{
  name = "#joyex__luoshen_obtain",
  mute = true,
  frequency = Skill.Compulsory,
  events = {fk.FinishJudge},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill("joyex__luoshen") and
      data.reason == "joyex__luoshen" 
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if data.card.color == Card.Red then
      room:obtainCard(player.id, data.card,false,fk.ReasonJustMove,player.id,self.name,"@@joyex__luoshen-inhand")
    else
      room:obtainCard(player.id, data.card)
    end
  end,
}
local luoshen_delay = fk.CreateTriggerSkill{
  name = "#joyex__luoshen_delay",
  mute = true,
  events = {fk.AfterCardsMove},
  can_trigger = function(self, event, target, player, data)
    if player.dead or not player:hasSkill(luoshen) or player:usedSkillTimes(self.name, Player.HistoryRound) ~= 0 then return false end
    local mark = player:getMark("xingchong-round")
    for _, move in ipairs(data) do
      if move.from == player.id and move.extra_data and move.extra_data.joyex__luoshen then
        return true
      end
    end
  end,
  on_cost = function(self, event, target, player, data)
    return player.room:askForSkillInvoke(player,self.name,nil,"是否立即发动一次〖洛神〗?")
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local luoshens = Fk.skills["joyex__luoshen"]
    room:useSkill(player, luoshens, function()
        return luoshens:use(fk.EventPhaseStart, player, player)
      end)
  end,
  refresh_events = {fk.BeforeCardsMove},
  can_refresh = function(self, event, target, player, data)
    if player.dead or not player:hasSkill(luoshen) or player:usedSkillTimes(self.name, Player.HistoryRound) ~= 0 then return false end
    for _, move in ipairs(data) do
      if move.from == player.id then
        for _, info in ipairs(move.moveInfo) do
          if info.fromArea == Card.PlayerHand and Fk:getCardById(info.cardId):getMark("@@joyex__luoshen-inhand") > 0 then
            return true
          end
        end
      end
    end
  end,
  on_refresh = function(self, event, target, player, data)
    local room = player.room
    for _, move in ipairs(data) do
      if move.from == player.id then
        move.extra_data = move.extra_data or {}
        move.extra_data.joyex__luoshen = true
      end
    end
  end,
}
luoshen:addRelatedSkill(luoshen_delay)
luoshen:addRelatedSkill(luoshen_obtain)
local qingguo = fk.CreateViewAsSkill{
  name = "joyex__qingguo",
  anim_type = "defensive",
  pattern = "jink",
  card_filter = function(self, to_select, selected)
    return #selected == 0 and Fk:getCardById(to_select).color == Card.Black
  end,
  view_as = function(self, cards)
    if #cards ~= 1 then return end
    local card = Fk:cloneCard("jink")
    card.skillName = self.name
    card:addSubcard(cards[1])
    return card
  end,
}
ex__zhenji:addSkill(luoshen)
ex__zhenji:addSkill(qingguo)

Fk:loadTranslationTable{
  ["joyex__zhenji"] = "界甄姬",
  ["joyex__luoshen"] = "洛神",
  [":joyex__luoshen"] = "准备阶段开始时，你可以进行判定，当判定牌生效后，你获得之，然后若判定牌为黑色则可以重复此流程;<br>每轮限一次，当你失去因〖洛神〗获得的红色手牌后，你可以立即发动一次〖洛神〗。",
  ["#joyex__luoshen_obtain"] = "洛神",
  ["joyex__qingguo"] = "倾国",
  [":joyex__qingguo"] = "你可以将一张黑色牌当【闪】使用或打出。",
  ["@@joyex__luoshen-inhand"] = '洛神 红',
  ["#joyex__luoshen_delay"] = "洛神",

  ["$joyex__luoshen1"] = "",
  ["$joyex__luoshen2"] = "",
  ["~joy__zhenji"] = "",
}

local huanggai = General(extension, "joyex__huanggai", "wu", 4)
local zhaxiang_targetmod = fk.CreateTargetModSkill{
  name = "#joyex__zhaxiang_targetmod",
  residue_func = function(self, player, skill, scope)
    if skill.trueName == "slash_skill" and player:hasSkill(self) then
      return 1
    end
    return 0
  end,
  bypass_distances = function(self, player, skill, card)
    return skill.trueName == "slash_skill" and card.color == Card.Red and player:hasSkill(self)
  end,
}
local zhaxiang = fk.CreateTriggerSkill{
  name = "joyex__zhaxiang",
  anim_type = "offensive",
  frequency = Skill.Compulsory,
  events = {fk.AfterCardUseDeclared},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and
      data.card.trueName == "slash" and data.card.color == Card.Red
  end,
  on_use = function(self, event, target, player, data)
    data.disresponsiveList = table.map(player.room.alive_players, Util.IdMapper)
  end,
}
zhaxiang:addRelatedSkill(zhaxiang_targetmod)
huanggai:addSkill("kurou")
huanggai:addSkill(zhaxiang)
Fk:loadTranslationTable{
  ["joyex__huanggai"] = "界黄盖",
  ["joyex__zhaxiang"] = "诈降",
  [":joyex__zhaxiang"] = "锁定技，你使用【杀】次数上限+1、使用红色【杀】无距离限制且不可被响应。",
}

local lvmeng = General(extension, "joyex__lvmeng", "wu", 4)
local keji = fk.CreateTriggerSkill{
  name = "joy__keji",
  anim_type = "defensive",
  events = {fk.EventPhaseChanging},
  can_trigger = function(self, event, target, player, data)
    if target == player and player:hasSkill(self) and data.to == Player.Discard then
        if player:getMark("joy__keji-turn") > 0  then return true end
      local play_ids = {}
      player.room.logic:getEventsOfScope(GameEvent.Phase, 1, function (e)
        if e.data[2] == Player.Play and e.end_id then
          table.insert(play_ids, {e.id, e.end_id})
        end
        return false
      end, Player.HistoryTurn)
      if #play_ids == 0 then return true end
      local function PlayCheck (e)
        local in_play = false
        for _, ids in ipairs(play_ids) do
          if e.id > ids[1] and e.id < ids[2] then
            in_play = true
            break
          end
        end
        return in_play and e.data[1].from == player.id and e.data[1].card.trueName == "slash"
      end
      return #player.room.logic:getEventsOfScope(GameEvent.UseCard, 1, PlayCheck, Player.HistoryTurn) == 0
      and #player.room.logic:getEventsOfScope(GameEvent.RespondCard, 1, PlayCheck, Player.HistoryTurn) == 0
    end
  end,
  on_use = function(self, event, target, player, data)
    player:drawCards(1,self.name)
    return true
  end
}
local qinxue = fk.CreateTriggerSkill{
  name = "joyex__qinxue",
  frequency = Skill.Wake,
  events = {fk.EventPhaseStart},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and
      player.phase == Player.Finish and
      player:usedSkillTimes(self.name, Player.HistoryGame) == 0
  end,
  can_wake = function(self, event, target, player, data)
    return #player.player_cards[Player.Hand] >= player.hp*3
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if player:isWounded() then
      local n = player.maxHp - player.hp
      room:changeMaxHp(player, -n)
    end
    room:handleAddLoseSkills(player, "-joy__keji", nil)
    room:handleAddLoseSkills(player, "joy__gongxin", nil)
  end,
}
lvmeng:addSkill(keji)
lvmeng:addSkill(qinxue)
lvmeng:addSkill("ol_ex__botu")
lvmeng:addRelatedSkill("joy__gongxin")
Fk:loadTranslationTable{
  ["joyex__lvmeng"] = "界吕蒙",

  ["joy__keji"] = "克己",
  [":joy__keji"] = "若你未于出牌阶段内打出或使用过【杀】，你可以跳过弃牌阶段并摸一张牌。",
  ["joyex__qinxue"] = "勤学",
  [":joyex__qinxue"] = "觉醒技，结束阶段，若你的手牌数大于等于体力值的3倍，你将体力上限减少至当前体力值，然后获得技能〖攻心〗失去〖克己〗。",

}

local sunquan = General(extension, "joyex__sunquan", "wu", 4)
local zhiheng = fk.CreateActiveSkill{
  name = "joyex__zhiheng",
  anim_type = "drawcard",
  min_card_num = 1,
  card_filter = function(self, to_select, selected)
    return not Self:prohibitDiscard(Fk:getCardById(to_select))
  end,
  target_num = 0,
  can_use = function(self, player)
    return player:usedSkillTimes(self.name, Player.HistoryPhase) < 1 + player:getMark("joyex__zhiheng-phase")
  end,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    local hand = player:getCardIds(Player.Hand)
    local more = #hand > 0 and table.every(hand, function(id) return table.contains(effect.cards, id) end)
    room:throwCard(effect.cards, self.name, player, player)
    if player.dead then return end
    local cards = player:drawCards(#effect.cards + (more and 1 or 0), self.name)
    if player:usedSkillTimes(self.name, Player.HistoryPhase) == 1
    and table.every(cards, function(id) return Fk:getCardById(id).sub_type ~= Card.SubtypeDelayedTrick end) then
      room:addPlayerMark(player, "joyex__zhiheng-phase")
    end
    if player:usedSkillTimes(self.name, Player.HistoryPhase) == 2
    and table.every(cards, function(id) return Fk:getCardById(id).type == Card.TypeBasic end) then
      room:addPlayerMark(player, "joyex__zhiheng-phase")
    end
  end
}
local jiuyuan = fk.CreateTriggerSkill{
  name = "joyex__jiuyuan$",
  anim_type = "support",
  events = {fk.PreHpRecover, fk.TargetSpecified},
  can_trigger = function(self, event, target, player, data)
    if event == fk.PreHpRecover then
      return target ~= player and player:hasSkill(self) and player:isWounded()
      and target.kingdom == "wu" and target == player.room.current
    else
      return target ~= player and player:hasSkill(self) and player.dying
      and target.kingdom == "wu" and data.card.name == "peach"
    end
  end,
  on_cost = function(self, event, target, player, data)
    if event == fk.TargetSpecified then return true end
    return player.room:askForSkillInvoke(target, self.name, nil, "#ex__jiuyuan-ask::"..player.id)
  end,
  on_use = function(self, event, target, player, data)
    if event == fk.PreHpRecover then
      player.room:recover{ who = player, num = 1, skillName = self.name, recoverBy = target }
      if not target.dead then
        target:drawCards(1, self.name)
      end
      return true
    else
      data.additionalRecover = (data.additionalRecover or 0) + 1
    end
  end,
}
sunquan:addSkill(zhiheng)
sunquan:addSkill(jiuyuan)
Fk:loadTranslationTable{
  ["joyex__sunquan"] = "界孙权",
  ["joyex__zhiheng"] = "制衡",
  [":joyex__zhiheng"] = "出牌阶段限一次，你可以弃置至少一张牌，然后摸等量的牌。若你以此法弃置了所有的手牌，则额外摸一张牌，若你本阶段第一次以此法获得的牌不含延时锦囊牌，则本阶段此技能使用次数+1；若你本阶段第二次以此法获得的牌均为基本牌，则本阶段此技能使用次数+1。",
  ["joyex__jiuyuan"] = "救援",
  [":joyex__jiuyuan"] = "主公技，其他吴势力角色于其回合内回复体力前，该角色可以改为令你回复1点体力，然后其摸一张牌。其他吴势力武将使用【桃】指定你为目标后，若你处于濒死状态，此【桃】回复体力值+1。",
}

local luxun = General(extension, "joyex__luxun", "wu", 3)
local qianxun = fk.CreateTriggerSkill{
  name = "joyex__qianxun",
  events = {fk.CardEffecting},
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(self) and target == player and data.card.type == Card.TypeTrick and #TargetGroup:getRealTargets(data.tos) == 1 and not player:isKongcheng()
  end,
  on_cost = function(self, event, target, player, data)
    local cards = player.room:askForCard(player,1,player.hp,false,self.name,true,".","#joyex__qianxun-choose:::"..player.hp)
    if #cards > 0 then
      self.cost_data = cards
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    player:addToPile("joyex__qianxun", self.cost_data, false, self.name)
  end,
}
local qianxun_delay = fk.CreateTriggerSkill{
  name = "#joyex__qianxun_delay",
  mute = true,
  events = {fk.EventPhaseChanging},
  frequency = Skill.Compulsory,
  can_trigger = function(self, event, target, player, data)
    return data.to == Player.NotActive and #player:getPile("joyex__qianxun") > 0
  end,
  on_use = function(self, event, target, player, data)
    local dummy = Fk:cloneCard("zixing")
    dummy:addSubcards(player:getPile("joyex__qianxun"))
    local room = player.room
    room:obtainCard(player.id, dummy, false)
  end,
}
local lianying = fk.CreateTriggerSkill{
  name = "joyex__lianying",
  anim_type = "drawcard",
  events = {fk.AfterCardsMove},
  can_trigger = function(self, event, target, player, data)
    if not player:hasSkill(self) then return end
    if not player:isKongcheng() then return end
    for _, move in ipairs(data) do
      if move.from == player.id then
        for _, info in ipairs(move.moveInfo) do
          if info.fromArea == Card.PlayerHand then
            return true
          end
        end
      end
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if not player.dead then
      player:drawCards(2,self.name)
    end
    local to,card = room:askForChooseCardAndPlayers(player,table.map(player.room:getOtherPlayers(player), function(p)
      return p.id end),1,1,".|.|.|hand","#joyex__lianying-choose",self.name,true)
    if #to > 0 then
      local get = Fk:getCardById(card)
      local p = room:getPlayerById(to[1])
      room:moveCardTo(get, Player.Hand, p, fk.ReasonGive, self.name, nil, false)
    end
  end,
}
qianxun:addRelatedSkill(qianxun_delay)
luxun:addSkill(qianxun)
luxun:addSkill(lianying)

Fk:loadTranslationTable{
  ["joyex__luxun"] = "界陆逊",

  ["joyex__qianxun"] = "谦逊",
  [":joyex__qianxun"] = "当一张锦囊牌对你生效时，若你是此牌唯一目标，则你可以将至多X张手牌扣置于武将牌上（X为你的体力值），然后此回合结束时，你获得这些牌。",
  ["joyex__lianying"] = "连营",
  [":joyex__lianying"] = "当你失去手牌后，若你没有手牌，你可以摸两张牌，然后你可以将一张手牌交给一名其他角色。",
  ["#joyex__lianying-choose"] = "连营：你可以将一张手牌交给一名其他角色",
  ["#joyex__qianxun-choose"] = "谦逊：你可以将至多%arg张手牌扣置于武将牌上",
  ["#joyex__qianxun_delay"] = "谦逊",

  ["$joyex__qianxun1"] = " ",
  ["$joyex__qianxun2"] = " ",
  ["$joyex__lianying1"] = " ",
  ["$joyex__lianying2"] = " ",
  ["~joyex__luxun"] = " ",
}

local sunshangxiang = General(extension, "joyex__sunshangxiang", "wu", 3, 3, General.Female)
local jieyi = fk.CreateActiveSkill{
  name = "joyex__jieyi",
  anim_type = "support",
  card_num = 1,
  target_num = 1,
  prompt = "#joyex__jieyi",
  can_use = function(self, player)
    return player:usedSkillTimes(self.name) == 0 and not player:isNude()
  end,
  card_filter = function(self, to_select, selected)
    return #selected == 0
  end,
  target_filter = function(self, to_select, selected, selected_cards)
    if #selected == 0 and to_select ~= Self.id and #selected_cards == 1 then
      if Fk:currentRoom():getCardArea(selected_cards[1]) == Player.Hand then
        return true
      else
        local target = Fk:currentRoom():getPlayerById(to_select)
        local card = Fk:getCardById(selected_cards[1])
        return #target:getAvailableEquipSlots(card.sub_type) > 0
      end
    end
  end,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    local target = room:getPlayerById(effect.tos[1])
    if room:getCardArea(effect.cards[1]) == Player.Hand then
      room:obtainCard(target, effect.cards[1], false, fk.ReasonGive)
    else
      room:moveCards({
        ids = effect.cards,
        from = effect.from,
        to = effect.tos[1],
        toArea = Card.PlayerEquip,
        skillName = self.name,
        moveReason = fk.ReasonPut,
      })
    end
    if player.dead then return end
    if player:isWounded() then
      room:recover({
        who = player,
        num = 1,
        recoverBy = player,
        skillName = self.name
      })
    end
    player:drawCards(1, self.name)
    if player.dead or target.dead then return end
    if target.hp < player.hp and room:askForSkillInvoke(player, self.name, nil, "#joyex__jieyi-invoke1::"..target.id) then
      room:doIndicate(player.id, {target.id})
      if target:isWounded() then
        room:recover({
          who = target,
          num = 1,
          recoverBy = player,
          skillName = self.name
        })
      end
    elseif target.hp >= player.hp and not target.dead and room:askForSkillInvoke(player, self.name, nil, "#joyex__jieyi-invoke2::"..target.id) then
      target:drawCards(1,self.name)
    end
  end,
}
sunshangxiang:addSkill(jieyi)
sunshangxiang:addSkill("xiaoji")
Fk:loadTranslationTable{
  ["joyex__sunshangxiang"] = "界孙尚香",
  ["joyex__jieyi"] = "结谊",
  [":joyex__jieyi"] = "出牌阶段限一次，你可以选择一名其他角色，交给其一张手牌或将装备区内一张装备牌置入其装备区，然后你回复1点体力并摸一张牌；"..
  "若其体力小于你，你可令其回复1点体力；反之，你可令其摸一张牌。",

  ["#joyex__jieyi"] = "结谊：你可以交给一名其他角色一张手牌，或将装备区内一张装备置入其装备区",
  ["#joyex__jieyi-invoke1"] = "结谊：你可以令 %dest 回复1点体力",
  ["#joyex__jieyi-invoke2"] = "结谊：你可以令 %dest 摸一张牌",
}

local daqiao = General(extension, "joyex__daqiao", "wu", 3, 3, General.Female)
local wanrong = fk.CreateActiveSkill{
  name = "joyex__wanrong",
  anim_type = "control",
  card_num = 1,
  target_num = 1,
  can_use = function(self, player)
    return player:usedSkillTimes(self.name, Player.HistoryPhase) == 0 and not player:isNude()
  end,
  interaction = function()
    return UI.ComboBox {choices = {"joyex__wanrong_use" , "joyex__wanrong_throw"}}
  end,
  card_filter = function(self, to_select, selected)
    if #selected > 0 or not self.interaction.data or Fk:getCardById(to_select).suit ~= Card.Diamond then return false end
    if self.interaction.data == "joyex__wanrong_use" then
      local card = Fk:cloneCard("indulgence")
      card:addSubcard(to_select)
      return Self:canUse(card) and not Self:prohibitUse(card)
    else
      return not Self:prohibitDiscard(Fk:getCardById(to_select))
    end
  end,
  target_filter = function(self, to_select, selected, cards)
    if #cards ~= 1 or #selected > 0 or not self.interaction.data then return false end
    local target = Fk:currentRoom():getPlayerById(to_select)
    if self.interaction.data == "joyex__wanrong_use" then
      local card = Fk:cloneCard("indulgence")
      card:addSubcard(cards[1])
      return to_select ~= Self.id and not Self:isProhibited(target, card)
    else
      return target:hasDelayedTrick("indulgence")
    end
  end,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    local target = room:getPlayerById(effect.tos[1])
    if self.interaction.data == "joyex__wanrong_use" then
      room:useVirtualCard("indulgence", effect.cards, player, target, self.name)
    else
      room:throwCard(effect.cards, self.name, player, player)
      for _, id in ipairs(target.player_cards[Player.Judge]) do
        local card = target:getVirualEquip(id)
        if not card then card = Fk:getCardById(id) end
        if card.name == "indulgence" then
          room:throwCard({id}, self.name, target, player)
        end
      end
    end
  end,
}
daqiao:addSkill(wanrong)
daqiao:addSkill("joy__guose")
daqiao:addSkill("liuli")
Fk:loadTranslationTable{
  ["joyex__daqiao"] = "界大乔",

  ["joyex__wanrong"] = "婉容",
  [":joyex__wanrong"] = "出牌阶段限一次，你可以选择一项：1.将一张<font color='red'>♦</font>牌当【乐不思蜀】使用；"..
  "2.弃置一张<font color='red'>♦</font>牌并弃置场上的一张【乐不思蜀】。",
  ["joyex__wanrong_use"] = "使用乐不思蜀",
  ["joyex__wanrong_throw"] = "弃置乐不思蜀",

  ["$ex__guose1"] = " ",
  ["$ex__guose2"] = " ",
  ["$liuli_ex__daqiao1"] = " ",
  ["$liuli_ex__daqiao2"] = " ",
  ["~ex__daqiao"] = " ",
}

local ganning = General(extension, "joyex__ganning", "wu", 4)
local fenwei = fk.CreateTriggerSkill{
  name = "joyex__fenwei",
  anim_type = "defensive",
  frequency = Skill.Limited,
  events = {fk.TargetSpecified,fk.AfterCardsMove},
  can_trigger = function(self, event, target, player, data)
    if event == fk.TargetSpecified then
      return player:hasSkill(self) and data.card.type == Card.TypeTrick and #AimGroup:getAllTargets(data.tos) > 1 and
        player:usedSkillTimes(self.name, Player.HistoryGame) == 0
    elseif event == fk.AfterCardsMove and player:hasSkill(self) and player:isKongcheng() and player:usedSkillTimes("joyex__fenwei", Player.HistoryGame) > 0 then
      for _, move in ipairs(data) do
        if move.from == player.id then
          for _, info in ipairs(move.moveInfo) do
            if info.fromArea == Card.PlayerHand then
              return true
            end
          end
        end
      end
    end
  end,
  on_cost = function(self, event, target, player, data)
    if event == fk.AfterCardsMove then return true end
    local tos = player.room:askForChoosePlayers(player, AimGroup:getAllTargets(data.tos),
      1, 10, "#fenwei-choose:::"..data.card:toLogString(), self.name, true)
    if #tos > 0 then
      self.cost_data = tos
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    if event == fk.AfterCardsMove then
      player:setSkillUseHistory("joyex__fenwei", 0, Player.HistoryGame)
    else
      table.insertTable(data.nullifiedTargets, self.cost_data)
      if not player.dead then
        player:drawCards(1,self.name)
      end
    end
  end,
}
local qixi_trigger = fk.CreateTriggerSkill{
  name = "#joyex__qixi_trigger",
  anim_type = "control",
  events = {fk.EventPhaseStart},
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(self.name) and target == player and player.phase == Player.Play
  end,
  on_cost = function(self, event, target, player, data)
    local targets = {}
    for _, p in ipairs(player.room.alive_players) do
      if #p:getCardIds("hej") > 0 then
        table.insert(targets,p.id)
      end
    end
    local to = player.room:askForChoosePlayers(player, targets, 1, 1, "#joyex__qixi_trigger-choose", self.name, true)
    if #to > 0 then
      self.cost_data = to
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    local to = player.room:getPlayerById(self.cost_data[1])
  if not player.dead and not to.dead then
    local card = player.room:askForCardChosen(player, to,"hej", self.name)
    player.room:throwCard(card,self.name,to,player)
  end
  end,
}
local qixi = fk.CreateViewAsSkill{
  name = "joyex__qixi",
  anim_type = "control",
  pattern = "dismantlement",
  card_filter = function(self, to_select, selected)
    if #selected == 1 then return false end
    return Fk:getCardById(to_select).color == Card.Black
  end,
  view_as = function(self, cards)
    if #cards ~= 1 then
      return nil
    end
    local c = Fk:cloneCard("dismantlement")
    c.skillName = self.name
    c:addSubcard(cards[1])
    return c
  end,
}
qixi:addRelatedSkill(qixi_trigger)
ganning:addSkill(qixi)
ganning:addSkill(fenwei)
Fk:loadTranslationTable{
  ["joyex__ganning"] = "界甘宁",

  ["joyex__fenwei"] = "奋威",
  [":joyex__fenwei"] = "限定技，当一张锦囊牌指定多个目标后，你可令此牌对其中任意个目标无效，然后你摸一张牌。当你失去最后的手牌时，复原本技能。",
  ["#joyex__fenwei-choose"] = "奋威：你可以令此%arg对任意个目标无效，然后你摸一张牌",
  ["joyex__qixi"] = "奇袭",
  [":joyex__qixi"] = "奇袭：出牌阶段开始时，你可以选择一名角色弃置其区域内的一张牌。出牌阶段，你可以将一张黑色牌当【过河拆桥】使用。",
  ["#joyex__qixi_trigger-choose"] = "奇袭：选择一名角色弃置其区域内的一张牌。",
  ["#joyex__qixi_trigger"] = "奇袭",

  ["$joyex__qixi1"] = " ",
  ["$joyex__qixi2"] = " ",
  ["$joyex__fenwei1"] = " ",
  ["$joyex__fenwei2"] = " ",
  ["~joyex__ganning"] = " ",
}

local zhouyu = General(extension, "joyex__zhouyu", "wu", 3)
local yingzi = fk.CreateTriggerSkill{
  name = "joyex__yingzi",
  anim_type = "drawcard",
  frequency = Skill.Compulsory,
  events = {fk.DrawNCards},
  can_trigger = function(self, event, target, player, data)
    return  target == player and player:hasSkill(self.name)
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    local n = 0
    if player:getHandcardNum() >= 2 then
      n = n + 1
    end
    if player.hp >= 2 then
      n = n + 1
    end
    if #player:getCardIds("e") >= 1 then
      n = n + 1
    end
    n = math.max(1,n)
    data.n = data.n + n
  end,
}
local yingzi_maxcards = fk.CreateMaxCardsSkill{
  name = "#joy__yingzi_maxcards",
  fixed_func = function(self, player)
    if player:hasSkill(yingzi) then
      return player.maxHp
    end
  end,
  correct_func = function(self, player)
    if player:hasSkill(yingzi) then
      local n = 0
    if player:getHandcardNum() >= 2 then
      n = n + 1
    end
    if player.hp >= 2 then
      n = n + 1
    end
    if #player:getCardIds("e") >= 1 then
      n = n + 1
    end
    n = math.max(1,n)
    return n
    end
  end,
}
yingzi:addRelatedSkill(yingzi_maxcards)
local fanjian = fk.CreateActiveSkill{
  name = "joyex__fanjian",
  prompt = "#joyex__fanjian",
  can_use = function(self, player)
    return player:usedSkillTimes(self.name, Player.HistoryPhase) == 0 and not player:isKongcheng()
  end,
  card_filter = function(self, to_select, selected)
    return #selected == 0 and Fk:getCardById(to_select):getSuitString() ~= "nosuit"
  end,
  target_filter = function(self, to_select, selected)
    return #selected == 0 and to_select ~= Self.id
  end,
  target_num = 1,
  card_num = 1,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    local target = room:getPlayerById(effect.tos[1])
    local suit = Fk:getCardById(effect.cards[1]):getSuitString()
    local choice = room:askForChoice(target, {"spade", "heart", "club", "diamond"}, self.name,"反间：请猜测"..Fk:translate(player.general).."声明的花色，若猜对额外摸一张牌,最后受到对方的一点伤害。:"..player.id)
    if suit == choice and target:isAlive() then
      target:drawCards(1,self.name)
    end
    if target.dead then return end
    room:damage{
        from = player,
        to = target,
        damage = 1,
        skillName = self.name,
    }
  end,
}
zhouyu:addSkill(yingzi)
zhouyu:addSkill(fanjian)
Fk:loadTranslationTable{
  ["joyex__zhouyu"] = "界周瑜",
  ["joyex__yingzi"] = "英姿",
  [":joyex__yingzi"] = "锁定技，你摸牌阶段多摸X张牌，你的手牌上限等于体力上限+X。<br>(X为你满足的以下条件数且至少为1：<br>手牌数不小于2、体力值不小于2、装备区牌数不小于1)",
  ["joyex__fanjian"] = "反间",
  [":joyex__fanjian"] = "出牌阶段限一次，你可以选择一张有花色的手牌声明其花色，然后令一名其他角色猜测你声明的花色，若其猜对，其摸一张牌；最后对其造成一点伤害。",
  ["#joyex__fanjian"] = "反间：选择一张有花色的手牌令一名角色猜测其花色。",
  ["#joy__yingzi_maxcards"] = "英姿",

  ["$joy__fanjian1"] = " ",
  ["$joy__fanjian2"] = " ",
  ["~joy__zhouyu"] = " ",
}

local lvbu = General(extension, "joyex__lvbu", "qun", 5)
local wushuang = fk.CreateTriggerSkill{
  name = "joyex__wushuang",
  anim_type = "offensive",
  frequency = Skill.Compulsory,
  events = {fk.TargetSpecified, fk.TargetConfirmed,fk.AfterCardTargetDeclared},
  can_trigger = function(self, event, target, player, data)
    if not player:hasSkill(self) then
      return false
    end
    if event == fk.TargetSpecified then
      return target == player and table.contains({ "slash", "duel" }, data.card.trueName)
    elseif event ==  fk.TargetConfirmed then
      return data.to == player.id and data.card.trueName == "duel"
    elseif event == fk.AfterCardTargetDeclared and data.card.trueName == "slash" and target == player then
      local current_targets = TargetGroup:getRealTargets(data.tos)
      for _, p in ipairs(player.room.alive_players) do
        if not table.contains(current_targets, p.id) and not player:isProhibited(p, data.card)  and
            data.card.skill:modTargetFilter(p.id, current_targets, data.from, data.card, true) then
          return true
        end
      end
    end
  end,
  on_cost = function(self, event, target, player, data)
    local room = player.room
    if event == fk.AfterCardTargetDeclared then
      local current_targets = TargetGroup:getRealTargets(data.tos)
      local targets = {}
      for _, p in ipairs(room.alive_players) do
        if not table.contains(current_targets, p.id) and not player:isProhibited(p, data.card) and
            data.card.skill:modTargetFilter(p.id, current_targets, data.from, data.card, true) then
          table.insert(targets, p.id)
        end
      end
      local tos = room:askForChoosePlayers(player, targets, 1, 1,
      "#joyex__wushuang-choose:::"..data.card:toLogString(), self.name, true)
      if #tos > 0 then
        self.cost_data = tos
        return true
      end
    else
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    if event == fk.AfterCardTargetDeclared then
      table.insertTable(data.tos, table.map(self.cost_data, function (p)
        return {p}
      end))
    else
      data.fixedResponseTimes = data.fixedResponseTimes or {}
      if data.card.trueName == "slash" then
        data.fixedResponseTimes["jink"] = 2
      else
        data.fixedResponseTimes["slash"] = 2
        data.fixedAddTimesResponsors = data.fixedAddTimesResponsors or {}
        table.insert(data.fixedAddTimesResponsors, (event == fk.TargetSpecified and data.to or data.from))
      end
    end
  end,
}
local wuchang = fk.CreateActiveSkill{
  name = "joyex__wuchang",
  target_num = 1,
  prompt = "#joyex__wuchang",
  can_use = function(self, player)
    return  player:usedSkillTimes(self.name, Player.HistoryPhase) == 0
  end,
  card_filter = Util.FalseFunc,
  target_filter = function(self, to_select, selected)
      return to_select ~= self.id
  end,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    local to = room:getPlayerById(effect.tos[1])
    if not to.dead then
      to:drawCards(1,self.name)
    end
    if to.kingdom ~= player.kingdom then
      room:changeKingdom(player, to.kingdom, true)
    end
    local ids = player.room:getCardsFromPileByRule("slash|.|.", 1, "DrawPile")
    if #ids > 0 then
      player.room:obtainCard(player, ids[1], true, fk.ReasonPrey)
    end
  end,
}
local wuchang_trigger = fk.CreateTriggerSkill{
  name = "#joyex__wuchang_trigger",
  anim_type = "defensive",
  events = {fk.DamageCaused},
  can_trigger = function(self, event, target, player, data)
    return player:hasSkill(self) and target == player and data.to.kingdom == player.kingdom and data.to:getMark("@@joyex__wuchang-phase") == 0 and data.card and (data.card.trueName == "slash"  or data.card.name == "duel" )
  end,
  on_cost = function(self, event, target, player, data)
    if player.room:askForSkillInvoke(player,self.name,data,"#joyex__wuchang2::"..data.to.id) then
      return true
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    data.damage = data.damage + 1
    local kingdom = {"shu","wei","wu","qun"}
    local king = {}
    for _, ki in ipairs(kingdom) do
      if player.kingdom ~= ki then
        table.insert(king,ki)
      end
    end
    local k = room:askForChoice(player,king,self.name,"#joyex__wuchang-kingdom")
    room:changeKingdom(player, k, true)

  end,
  refresh_events = {fk.CardUsing, fk.CardResponding,fk.CardUseFinished},
  can_refresh = function(self, event, target, player, data)
    if event == fk.CardUseFinished then
      return target == player and player:hasSkill(self) and (data.card.trueName == "slash"  or data.card.name == "duel" )
    else
      return target.kingdom == player.kingdom and target ~= player and player:hasSkill(self) and data.responseToEvent and data.responseToEvent.from == player.id and (data.responseToEvent.card.trueName == "slash" or data.responseToEvent.card.name == "duel")
    end
  end,
  on_refresh = function(self, event, target, player, data)
    local room = player.room
    if event == fk.CardUseFinished then
      for _, p in ipairs(room:getOtherPlayers(player)) do
        if p:getMark("@@joyex__wuchang-phase") > 0 then
          room:setPlayerMark(p,"@@joyex__wuchang-phase",0)
        end
      end
    else
      room:setPlayerMark(target,"@@joyex__wuchang-phase",1)
    end
  end,
}
wuchang:addRelatedSkill(wuchang_trigger)
lvbu:addSkill(wushuang)
lvbu:addSkill(wuchang)
Fk:loadTranslationTable{
  ["joyex__lvbu"] = "界吕布",
  ["#joyex__lvbu"] = "三姓家奴",

  ["joyex__wushuang"] = "无双",
  [":joyex__wushuang"] = "锁定技，<br>①你使用的【杀】需要连续使用俩张【闪】才能抵消，与你进行【决斗】的角色每次需要连续打出俩张【杀】。<br>②当你使用【杀】后，你可以额外选择一名其他角色也成为目标",
  ["joyex__wuchang"] = "无常",
  [":joyex__wuchang"] = "<br>①当你使用【杀】或【决斗】对势力与你相同的目标造成伤害时，若其未响应过此牌，则你可令此伤害+1，然后你选择变更为其他势力。"..
  "<br>②出牌阶段限一次，你可以选择一名其他角色，令其摸一张牌，然后你将势力变更为与其相同并从牌堆获得一张【杀】。",
  ["#joyex__wushuang-choose"] = "无双：你可以为 %arg 增加一个目标",
  ["#joyex__wuchang"] = "无常：你可以令一名其他角色摸一张牌，然后你变更为势力与其相同并摸一张【杀】",
  ["#joyex__wuchang_trigger"] = "无常",
  ["#joyex__wuchang2"] = "无常：是否令对%arg的伤害增加1点？",
  ["#joyex__wuchang-kingdom"] = "无常：选择一个势力变更之。",
  ["@@joyex__wuchang-phase"] = "无常-已响应",
}

local diaochan = General(extension, "joyex__diaochan", "qun", 3, 3, General.Female)
local joyex__lijian = fk.CreateActiveSkill{
  name = "joyex__lijian",
  anim_type = "offensive",
  card_num = 1,
  target_num = 2,
  prompt = "#joyex__lijian",
  can_use = function(self, player)
    return player:usedSkillTimes(self.name, Player.HistoryPhase) < 2
  end,
  card_filter = function(self, to_select, selected)
    return #selected == 0 and not Self:prohibitDiscard(Fk:getCardById(to_select))
  end,
  target_filter = function(self, to_select, selected, cards)
    if #cards ~= 1 then return false end
    local target = Fk:currentRoom():getPlayerById(to_select)
    if target:getMark("joyex__lijian-turn") == 0 then
      if #selected == 0 then
        return true
      elseif #selected == 1 then
        return not target:isProhibited(Fk:currentRoom():getPlayerById(selected[1]), Fk:cloneCard("duel"))
      else
        return false
      end
    end
  end,
  on_use = function(self, room, effect)
    local player = room:getPlayerById(effect.from)
    room:throwCard(effect.cards, self.name, player, player)
    room:useVirtualCard("duel", nil, room:getPlayerById(effect.tos[2]), room:getPlayerById(effect.tos[1]), self.name)
  end,
}
local joyex__lijian_trigger = fk.CreateTriggerSkill{
  name = "#joyex__lijian_trigger",
  refresh_events = {fk.PreDamage},
  can_refresh = function(self, event, target, player, data)
    return target == player and data.card and table.contains(data.card.skillNames, joyex__lijian.name)
  end,
  on_refresh = function(self, event, target, player, data)
    player.room:setPlayerMark(data.to, "joyex__lijian-turn", 1)
  end,
}
local joyex__biyue = fk.CreateTriggerSkill{
  name = "joyex__biyue",
  anim_type = "drawcard",
  events = {fk.EventPhaseStart},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and player.phase == Player.Finish
  end,
  on_cost = function(self, event, target, player, data)
    return true
  end,
  on_use = function(self, event, target, player, data)
    player:drawCards(1 + player:usedSkillTimes("joyex__lijian", Player.HistoryTurn), self.name)
  end,
}
joyex__lijian:addRelatedSkill(joyex__lijian_trigger)
diaochan:addSkill(joyex__lijian)
diaochan:addSkill(joyex__biyue)
Fk:loadTranslationTable{
  ["joyex__diaochan"] = "界貂蝉",
  ["joyex__lijian"] = "离间",
  [":joyex__lijian"] = "出牌阶段限两次，你可以弃置一张牌并选择两名角色，视为其中一名角色对另一名角色使用一张【决斗】，此【决斗】失败的角色本回合无法成为〖离间〗的目标。",
  ["joyex__biyue"] = "闭月",
  [":joyex__biyue"] = "结束阶段，你摸X张牌（X为本回合你发动〖离间〗次数+1）。",
  ["#joyex__lijian"] = "离间：弃置一张牌，选择两名角色，视为第二名角色对第一名角色使用【决斗】",
}

local huatuo = General(extension, "joyex__huatuo", "qun", 3)
local qingnang = fk.CreateActiveSkill{
  name = "joyex__qingnang",
  anim_type = "control",
  min_card_num = 0,
  min_target_num = 1,
  prompt = "#joyex__qingnang",
  can_use = function(self, player)
    return player:usedSkillTimes(self.name, Player.HistoryPhase) == 0 and not player:isNude()
  end,
  card_filter = function (self, to_select, selected, selected_targets)
    return #selected < #Fk:currentRoom().alive_players - 1
  end,
  target_filter = function (self, to_select, selected, selected_cards)
      return #selected < #selected_cards + 1
  end,
  feasible = function (self, selected, selected_cards)
    return #selected > 0 and #selected == #selected_cards + 1
  end,
  on_use = function(self, room, effect)
    local room = Self.room
    local player = room:getPlayerById(effect.from)
    local cards = effect.cards
    if #cards > 0 then
        room:throwCard(cards,self.name,player,player)
    end
    for _, id in ipairs(effect.tos) do
      local p = room:getPlayerById(id)
      if not p.dead then
        room:changeMaxHp(p, 1)
        room:addPlayerMark(p,"joyex__qingnang_"..player.id,1)
      end
      if p:isWounded() then
        room:recover({
          who = p,
          num = 1,
          recoverBy = player,
          skillName = self.name
          })
      end
    end
  end,
}
local qingnang_trigger = fk.CreateTriggerSkill{
  name = "#joyex__qingnag_trigger",

  refresh_events = {fk.TurnStart},
  can_refresh = function(self, event, target, player, data)
      return target == player and table.find(player.room.alive_players, function(p) return p:getMark("joyex__qingnang_"..player.id) > 0  end)
  end,
  on_refresh = function(self, event, target, player, data)
    local room = player.room
      for _, p in ipairs(room.alive_players) do
        if p:getMark("joyex__qingnang_"..player.id) > 0 then
          room:changeMaxHp(p, -1)
          room:removePlayerMark(p,"joyex__qingnang_"..player.id,1)
        end
      end
  end,
}
local jijiu = fk.CreateViewAsSkill{
  name = "joyex__jijiu",
  anim_type = "support",
  pattern = "peach",
  card_filter = function(self, to_select, selected)
    if #selected == 1 then return false end
    return Fk:getCardById(to_select).color == Card.Red
  end,
  view_as = function(self, cards)
    if #cards ~= 1 then
      return nil
    end
    local c = Fk:cloneCard("peach")
    c.skillName = self.name
    c:addSubcard(cards[1])
    return c
  end,
  after_use = function(self, player, use)
    local room = player.room
    local targets = {}
    for _, p in ipairs(room:getOtherPlayers(player)) do
      if p:getHandcardNum() > 0 then
        table.insert(targets,p.id)
      end
    end
      local target = room:askForChoosePlayers(player,targets,1,1,"#joy__jijiu-invoke",self.name,true)
      if #target > 0 then
        local to = room:getPlayerById(target[1])
        local card = room:askForCardChosen(player,to,"h",self.name)
        if card  then
          room:throwCard(card, self.name, to, player)
        end
      end
  end,
  enabled_at_play = function(self, player)
    return false
  end,
  enabled_at_response = function(self, player)
    return player.phase == Player.NotActive
  end,
}
qingnang:addRelatedSkill(qingnang_trigger)
huatuo:addSkill(jijiu)
huatuo:addSkill(qingnang)
Fk:loadTranslationTable{
  ["joyex__huatuo"] = "界华佗",
  ["#joyex__huatuo"] = "神医",

  ["joyex__qingnang"] = "青囊",
  [":joyex__qingnang"] = "出牌阶段限一次，你可以选择场上任意名角色并弃置X-1张牌（X为你选择的角色数），然后令这些角色各增加一点体力上限并回复一点体力。<br>（你的下个回合开始时，减少所有以此法增加的体力上限）",
  ["joyex__jijiu"] = "急救",
  [":joyex__jijiu"] = "你的回合外，你可以将一张红色牌当【桃】使用，然后你可以弃置一名其他角色的一张手牌。",
  ["#joy__jijiu-invoke"] = "急救：你可以选择一名其他角色弃置其一张手牌。",
  ["#joyex__qingnang"] = "青囊：选择任意名角色并弃置X-1张牌（X为选择的角色数），令这些角色各增加一点体力上限并回复一点体力",
  ["#joyex__qingnag_trigger"] = "青囊",

  ["$joyex__qingnnag1"] = " ",
  ["$joyex__qingnang2"] = " ",
  ["$joyex__jijiu1"] = " ",
  ["$joyex__jijiu2"] = " ",
  ["~joyex__huatuo"] = " ",
}

local huaxiong = General(extension, "joyex__huaxiong", "qun", 6)
local yaowu = fk.CreateTriggerSkill{
  name = "joyex__yaowu",
  frequency = Skill.Compulsory,
  events = {fk.DamageInflicted},
  can_trigger = function(self, event, target, player, data)
    return target == player and player:hasSkill(self) and not data.chain and data.card and data.card.trueName == "slash"
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if data.card.color == Card.Black and not player.dead then
      player.room:notifySkillInvoked(player, self.name, "masochism")
      player:broadcastSkillInvoke(self.name, 1)
      player:drawCards(1, self.name)
    elseif data.card.color == Card.Red and not data.from.dead then
      player.room:notifySkillInvoked(player, self.name, "negative")
      player:broadcastSkillInvoke(self.name, 2)
      local from = data.from
        from:drawCards(1, self.name)
    end
    if not player.dead and data.card and player.room:getCardArea(data.card) == Card.Processing then
      player.room:obtainCard(player.id, data.card, true, fk.ReasonPrey)
    end
  end
}
local yangwei = fk.CreateTriggerSkill{
  name = "joyex__yangwei",
  frequency = Skill.Compulsory,
  events = {fk.CardUsing,fk.EventPhaseEnd},
  can_trigger = function(self, event, target, player, data)
    if event == fk.CardUsing then
      return target == player and player:hasSkill(self)  and data.card.trueName == "slash" and player.phase == Player.Play
    else
      return target == player  and player:hasSkill(self) and player.phase == Player.Play and player:usedCardTimes("slash", Player.HistoryTurn) > 2 and player:isWounded()
    end
  end,
  on_use = function(self, event, target, player, data)
    local room = player.room
    if event == fk.CardUsing then
      local card
      if data.card.color == Card.Black then
        room:setPlayerMark(player,"@joyex__yangwei-phase",{"red"})
      elseif data.card.color == Card.Red then
        room:setPlayerMark(player,"@joyex__yangwei-phase",{"black"})
      else
        room:setPlayerMark(player,"@joyex__yangwei-phase",0)
      end
    else
      room:recover({
        who = player,
        num = 1,
        recoverBy = player,
        skillName = self.name
        })
    end
  end
}
local yangwei_target = fk.CreateTargetModSkill{
  name = "#joyex__yangwei_target",
  frequency = Skill.Compulsory,
  bypass_times = function(self, player, skill, scope, card, to)
    return card and card.trueName == "slash" and player:getMark("@joyex__yangwei-phase") ~= 0 and table.contains(player:getTableMark("@joyex__yangwei-phase"), card:getColorString())
  end,
}
yangwei:addRelatedSkill(yangwei_target)
huaxiong:addSkill(yaowu)
huaxiong:addSkill(yangwei)
Fk:loadTranslationTable{
  ["joyex__huaxiong"] = "界华雄",
  ["joyex__yaowu"] = "耀武",
  [":joyex__yaowu"] = "锁定技，当你受到【杀】造成的伤害时，若此【杀】为红色，伤害来源摸一张牌；若此【杀】为黑色，你摸一张牌;然后你获得所有对你造成伤害的【杀】。",
  ["joyex__yangwei"] = "扬威",
  [":joyex__yangwei"] = "锁定技，<br>①出牌阶段，你使用一张有色【杀】后，本阶段下张颜色不同且有色【杀】无次数限制。<br>②出牌阶段结束时，若本回合出【杀】次数大于2，则你回复一点体力。",
  ["#joyex__yangwei_target"] = "扬威",
  ["@joyex__yangwei-phase"] = "扬威",

  ["$joyex__yaowu1"] = " ",
  ["$joyex__yaowu2"] = " ",
  ["~joyex__huaxiong"] = " ",
}

return extension
